-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 29-12-2019 a las 11:54:03
-- Versión del servidor: 10.0.38-MariaDB-cll-lve
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ninpodes_ssact`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_blogmeta`
--

DROP TABLE IF EXISTS `wp_blogmeta`;
CREATE TABLE `wp_blogmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `blog_id` bigint(20) NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_blogs`
--

DROP TABLE IF EXISTS `wp_blogs`;
CREATE TABLE `wp_blogs` (
  `blog_id` bigint(20) NOT NULL,
  `site_id` bigint(20) NOT NULL DEFAULT '0',
  `domain` varchar(200) NOT NULL DEFAULT '',
  `path` varchar(100) NOT NULL DEFAULT '',
  `registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `public` tinyint(2) NOT NULL DEFAULT '1',
  `archived` tinyint(2) NOT NULL DEFAULT '0',
  `mature` tinyint(2) NOT NULL DEFAULT '0',
  `spam` tinyint(2) NOT NULL DEFAULT '0',
  `deleted` tinyint(2) NOT NULL DEFAULT '0',
  `lang_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `wp_blogs`
--

INSERT INTO `wp_blogs` (`blog_id`, `site_id`, `domain`, `path`, `registered`, `last_updated`, `public`, `archived`, `mature`, `spam`, `deleted`, `lang_id`) VALUES
(1, 1, 'www.ninpodesign.com', '/ssact/', '2019-06-15 10:32:45', '0000-00-00 00:00:00', 1, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_blog_versions`
--

DROP TABLE IF EXISTS `wp_blog_versions`;
CREATE TABLE `wp_blog_versions` (
  `blog_id` bigint(20) NOT NULL DEFAULT '0',
  `db_version` varchar(20) NOT NULL DEFAULT '',
  `last_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_commentmeta`
--

DROP TABLE IF EXISTS `wp_commentmeta`;
CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_comments`
--

DROP TABLE IF EXISTS `wp_comments`;
CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_links`
--

DROP TABLE IF EXISTS `wp_links`;
CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_name` varchar(255) NOT NULL DEFAULT '',
  `link_image` varchar(255) NOT NULL DEFAULT '',
  `link_target` varchar(25) NOT NULL DEFAULT '',
  `link_description` varchar(255) NOT NULL DEFAULT '',
  `link_visible` varchar(20) NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) NOT NULL DEFAULT '',
  `link_notes` mediumtext NOT NULL,
  `link_rss` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_options`
--

DROP TABLE IF EXISTS `wp_options`;
CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) NOT NULL DEFAULT '',
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost:8888/ssact', 'yes'),
(2, 'home', 'http://localhost:8888/ssact', 'yes'),
(3, 'blogname', 'School Sport ACT', 'yes'),
(4, 'blogdescription', 'SSACT actively promotes school sport for all ACT students through the support of regional, state and national representative opportunities and pathways.', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'ninpodesigncrew@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/blog/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:236:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:16:\"blog/students/?$\";s:28:\"index.php?post_type=students\";s:46:\"blog/students/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=students&feed=$matches[1]\";s:41:\"blog/students/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=students&feed=$matches[1]\";s:33:\"blog/students/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=students&paged=$matches[1]\";s:15:\"blog/schools/?$\";s:27:\"index.php?post_type=schools\";s:45:\"blog/schools/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=schools&feed=$matches[1]\";s:40:\"blog/schools/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=schools&feed=$matches[1]\";s:32:\"blog/schools/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=schools&paged=$matches[1]\";s:14:\"blog/sports/?$\";s:26:\"index.php?post_type=sports\";s:44:\"blog/sports/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=sports&feed=$matches[1]\";s:39:\"blog/sports/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=sports&feed=$matches[1]\";s:31:\"blog/sports/page/([0-9]{1,})/?$\";s:44:\"index.php?post_type=sports&paged=$matches[1]\";s:13:\"blog/teams/?$\";s:25:\"index.php?post_type=teams\";s:43:\"blog/teams/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?post_type=teams&feed=$matches[1]\";s:38:\"blog/teams/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?post_type=teams&feed=$matches[1]\";s:30:\"blog/teams/page/([0-9]{1,})/?$\";s:43:\"index.php?post_type=teams&paged=$matches[1]\";s:17:\"blog/bulletins/?$\";s:29:\"index.php?post_type=bulletins\";s:47:\"blog/bulletins/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?post_type=bulletins&feed=$matches[1]\";s:42:\"blog/bulletins/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?post_type=bulletins&feed=$matches[1]\";s:34:\"blog/bulletins/page/([0-9]{1,})/?$\";s:47:\"index.php?post_type=bulletins&paged=$matches[1]\";s:52:\"blog/category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:47:\"blog/category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:28:\"blog/category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:40:\"blog/category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:22:\"blog/category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:49:\"blog/tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:44:\"blog/tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:25:\"blog/tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:37:\"blog/tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:19:\"blog/tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:50:\"blog/type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:45:\"blog/type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:26:\"blog/type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:38:\"blog/type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:20:\"blog/type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:41:\"blog/students/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:51:\"blog/students/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:71:\"blog/students/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"blog/students/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"blog/students/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:47:\"blog/students/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:30:\"blog/students/([^/]+)/embed/?$\";s:41:\"index.php?students=$matches[1]&embed=true\";s:34:\"blog/students/([^/]+)/trackback/?$\";s:35:\"index.php?students=$matches[1]&tb=1\";s:54:\"blog/students/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?students=$matches[1]&feed=$matches[2]\";s:49:\"blog/students/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?students=$matches[1]&feed=$matches[2]\";s:42:\"blog/students/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?students=$matches[1]&paged=$matches[2]\";s:49:\"blog/students/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?students=$matches[1]&cpage=$matches[2]\";s:38:\"blog/students/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?students=$matches[1]&page=$matches[2]\";s:30:\"blog/students/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:40:\"blog/students/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:60:\"blog/students/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"blog/students/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"blog/students/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:36:\"blog/students/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:40:\"blog/schools/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:50:\"blog/schools/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:70:\"blog/schools/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:65:\"blog/schools/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:65:\"blog/schools/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:46:\"blog/schools/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:29:\"blog/schools/([^/]+)/embed/?$\";s:40:\"index.php?schools=$matches[1]&embed=true\";s:33:\"blog/schools/([^/]+)/trackback/?$\";s:34:\"index.php?schools=$matches[1]&tb=1\";s:53:\"blog/schools/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?schools=$matches[1]&feed=$matches[2]\";s:48:\"blog/schools/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?schools=$matches[1]&feed=$matches[2]\";s:41:\"blog/schools/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?schools=$matches[1]&paged=$matches[2]\";s:48:\"blog/schools/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?schools=$matches[1]&cpage=$matches[2]\";s:37:\"blog/schools/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?schools=$matches[1]&page=$matches[2]\";s:29:\"blog/schools/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:39:\"blog/schools/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:59:\"blog/schools/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:54:\"blog/schools/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:54:\"blog/schools/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:35:\"blog/schools/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:39:\"blog/sports/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:49:\"blog/sports/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:69:\"blog/sports/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"blog/sports/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"blog/sports/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:45:\"blog/sports/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:28:\"blog/sports/([^/]+)/embed/?$\";s:39:\"index.php?sports=$matches[1]&embed=true\";s:32:\"blog/sports/([^/]+)/trackback/?$\";s:33:\"index.php?sports=$matches[1]&tb=1\";s:52:\"blog/sports/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?sports=$matches[1]&feed=$matches[2]\";s:47:\"blog/sports/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?sports=$matches[1]&feed=$matches[2]\";s:40:\"blog/sports/([^/]+)/page/?([0-9]{1,})/?$\";s:46:\"index.php?sports=$matches[1]&paged=$matches[2]\";s:47:\"blog/sports/([^/]+)/comment-page-([0-9]{1,})/?$\";s:46:\"index.php?sports=$matches[1]&cpage=$matches[2]\";s:36:\"blog/sports/([^/]+)(?:/([0-9]+))?/?$\";s:45:\"index.php?sports=$matches[1]&page=$matches[2]\";s:28:\"blog/sports/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:38:\"blog/sports/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:58:\"blog/sports/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:53:\"blog/sports/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:53:\"blog/sports/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:34:\"blog/sports/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:38:\"blog/teams/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:48:\"blog/teams/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:68:\"blog/teams/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:63:\"blog/teams/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:63:\"blog/teams/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:44:\"blog/teams/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:27:\"blog/teams/([^/]+)/embed/?$\";s:38:\"index.php?teams=$matches[1]&embed=true\";s:31:\"blog/teams/([^/]+)/trackback/?$\";s:32:\"index.php?teams=$matches[1]&tb=1\";s:51:\"blog/teams/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?teams=$matches[1]&feed=$matches[2]\";s:46:\"blog/teams/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?teams=$matches[1]&feed=$matches[2]\";s:39:\"blog/teams/([^/]+)/page/?([0-9]{1,})/?$\";s:45:\"index.php?teams=$matches[1]&paged=$matches[2]\";s:46:\"blog/teams/([^/]+)/comment-page-([0-9]{1,})/?$\";s:45:\"index.php?teams=$matches[1]&cpage=$matches[2]\";s:35:\"blog/teams/([^/]+)(?:/([0-9]+))?/?$\";s:44:\"index.php?teams=$matches[1]&page=$matches[2]\";s:27:\"blog/teams/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"blog/teams/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"blog/teams/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"blog/teams/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"blog/teams/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"blog/teams/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:42:\"blog/bulletins/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:52:\"blog/bulletins/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:72:\"blog/bulletins/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:67:\"blog/bulletins/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:67:\"blog/bulletins/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:48:\"blog/bulletins/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:31:\"blog/bulletins/([^/]+)/embed/?$\";s:42:\"index.php?bulletins=$matches[1]&embed=true\";s:35:\"blog/bulletins/([^/]+)/trackback/?$\";s:36:\"index.php?bulletins=$matches[1]&tb=1\";s:55:\"blog/bulletins/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?bulletins=$matches[1]&feed=$matches[2]\";s:50:\"blog/bulletins/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?bulletins=$matches[1]&feed=$matches[2]\";s:43:\"blog/bulletins/([^/]+)/page/?([0-9]{1,})/?$\";s:49:\"index.php?bulletins=$matches[1]&paged=$matches[2]\";s:50:\"blog/bulletins/([^/]+)/comment-page-([0-9]{1,})/?$\";s:49:\"index.php?bulletins=$matches[1]&cpage=$matches[2]\";s:39:\"blog/bulletins/([^/]+)(?:/([0-9]+))?/?$\";s:48:\"index.php?bulletins=$matches[1]&page=$matches[2]\";s:31:\"blog/bulletins/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:41:\"blog/bulletins/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:61:\"blog/bulletins/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\"blog/bulletins/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\"blog/bulletins/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:37:\"blog/bulletins/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:41:\"blog/products/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:51:\"blog/products/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:71:\"blog/products/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"blog/products/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"blog/products/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:47:\"blog/products/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:30:\"blog/products/([^/]+)/embed/?$\";s:41:\"index.php?products=$matches[1]&embed=true\";s:34:\"blog/products/([^/]+)/trackback/?$\";s:35:\"index.php?products=$matches[1]&tb=1\";s:42:\"blog/products/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?products=$matches[1]&paged=$matches[2]\";s:49:\"blog/products/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?products=$matches[1]&cpage=$matches[2]\";s:38:\"blog/products/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?products=$matches[1]&page=$matches[2]\";s:30:\"blog/products/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:40:\"blog/products/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:60:\"blog/products/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"blog/products/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"blog/products/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:36:\"blog/products/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:52:\"blog/states/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?states=$matches[1]&feed=$matches[2]\";s:47:\"blog/states/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?states=$matches[1]&feed=$matches[2]\";s:28:\"blog/states/([^/]+)/embed/?$\";s:39:\"index.php?states=$matches[1]&embed=true\";s:40:\"blog/states/([^/]+)/page/?([0-9]{1,})/?$\";s:46:\"index.php?states=$matches[1]&paged=$matches[2]\";s:22:\"blog/states/([^/]+)/?$\";s:28:\"index.php?states=$matches[1]\";s:53:\"blog/regions/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?regions=$matches[1]&feed=$matches[2]\";s:48:\"blog/regions/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?regions=$matches[1]&feed=$matches[2]\";s:29:\"blog/regions/([^/]+)/embed/?$\";s:40:\"index.php?regions=$matches[1]&embed=true\";s:41:\"blog/regions/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?regions=$matches[1]&paged=$matches[2]\";s:23:\"blog/regions/([^/]+)/?$\";s:29:\"index.php?regions=$matches[1]\";s:50:\"blog/fees/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?fees=$matches[1]&feed=$matches[2]\";s:45:\"blog/fees/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?fees=$matches[1]&feed=$matches[2]\";s:26:\"blog/fees/([^/]+)/embed/?$\";s:37:\"index.php?fees=$matches[1]&embed=true\";s:38:\"blog/fees/([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?fees=$matches[1]&paged=$matches[2]\";s:20:\"blog/fees/([^/]+)/?$\";s:26:\"index.php?fees=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:52:\"blog/author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:47:\"blog/author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:28:\"blog/author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:40:\"blog/author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:22:\"blog/author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:74:\"blog/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:69:\"blog/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:50:\"blog/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:62:\"blog/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:44:\"blog/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:61:\"blog/([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:56:\"blog/([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:37:\"blog/([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:49:\"blog/([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:31:\"blog/([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:48:\"blog/([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:43:\"blog/([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:24:\"blog/([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:36:\"blog/([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:18:\"blog/([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:63:\"blog/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:73:\"blog/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:93:\"blog/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:88:\"blog/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:88:\"blog/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:69:\"blog/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:58:\"blog/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:62:\"blog/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:82:\"blog/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:77:\"blog/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:70:\"blog/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:77:\"blog/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:66:\"blog/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:52:\"blog/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:62:\"blog/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:82:\"blog/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:77:\"blog/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:77:\"blog/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:58:\"blog/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:69:\"blog/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:56:\"blog/([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:43:\"blog/([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:8:{i:0;s:29:\"acf-repeater/acf-repeater.php\";i:1;s:41:\"advanced-custom-fields-pro-master/acf.php\";i:2;s:30:\"advanced-custom-fields/acf.php\";i:3;s:19:\"members/members.php\";i:4;s:57:\"reveal-ids-for-wp-admin-25/reveal-ids-for-wp-admin-25.php\";i:5;s:21:\"schoolsport/index.php\";i:6;s:39:\"search-filter-pro/search-filter-pro.php\";i:7;s:37:\"wp-sendgrid-smtp/wp-sendgrid-smtp.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:5:{i:0;s:76:\"/home/ninpodes/public_html/ssact/wp-content/themes/schoolsportact/header.php\";i:2;s:75:\"/home/ninpodes/public_html/ssact/wp-content/themes/schoolsportact/index.php\";i:3;s:84:\"/home/ninpodes/public_html/ssact/wp-content/themes/schoolsportact/page-dashboard.php\";i:4;s:75:\"/home/ninpodes/public_html/ssact/wp-content/themes/schoolsportact/style.css\";i:5;s:74:\"/home/ninpodes/public_html/ssact/wp-content/themes/schoolsportact/page.php\";}', 'no'),
(40, 'template', 'schoolsportact', 'yes'),
(41, 'stylesheet', 'schoolsportact', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '45805', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '/home/ninpodes/public_html/ssact/wp-content/uploads', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'initial_db_version', '44719', 'yes'),
(94, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:73:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:14:\"ure_edit_roles\";b:1;s:16:\"ure_create_roles\";b:1;s:16:\"ure_delete_roles\";b:1;s:23:\"ure_create_capabilities\";b:1;s:23:\"ure_delete_capabilities\";b:1;s:18:\"ure_manage_options\";b:1;s:15:\"ure_reset_roles\";b:1;s:16:\"restrict_content\";b:1;s:10:\"list_roles\";b:1;s:12:\"create_roles\";b:1;s:12:\"delete_roles\";b:1;s:10:\"edit_roles\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"official\";a:2:{s:4:\"name\";s:8:\"Official\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:6:\"parent\";a:2:{s:4:\"name\";s:6:\"Parent\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:6:\"school\";a:2:{s:4:\"name\";s:6:\"School\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'cron', 'a:7:{i:1577581148;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1577591947;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1577591948;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1577615524;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1577615525;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1577615659;a:1:{s:21:\"update_network_counts\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}s:7:\"version\";i:2;}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'nonce_key', 'FcCsG$(^Bi_CqeZv&kVC_uyGO:~/__kD-TvZo<ff@Is<;lk^G!<c9o7U4H<4jlbT', 'no'),
(110, 'nonce_salt', 'nxJRr&x@n3KnRQP810`)rt=T(GD*L=dr=aPSi%s{zk-11b3ZQ5^g<|2{m=c~*}o=', 'no'),
(111, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'recovery_keys', 'a:0:{}', 'yes'),
(122, 'theme_mods_twentynineteen', 'a:1:{s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(124, 'auth_key', 'K]4{!&8ZLOP%{V]H]3=&~dn>2SX6La)15.!6qLBY|Ic :$<KGpkmi>N9u$HB!xGf', 'no'),
(125, 'auth_salt', 'Icx@+SpLZz/<Ho:dA6>w,f8Z&NTh1?Hlt)R?5Yzn~B{czDnZ)vPF)Ai4O,o5/P9H', 'no'),
(126, 'logged_in_key', '8W@%Afo^kG=zs8V0U.j9xHX#>>^(.rS}8&u~o&y/[Q)Q 4|U_$UEgXT+1Tdh#A1x', 'no'),
(127, 'logged_in_salt', '6wgE0)dXG<oBH%pDu]7p,8oi[SE*.u2Be#r0BKuouIN0wv0jyx~^VX=8TqSY[cD3', 'no'),
(148, 'recently_activated', 'a:0:{}', 'yes'),
(151, 'admin_email_lifespan', '1590451996', 'yes'),
(152, 'db_upgraded', '', 'yes'),
(154, '_transient_update_plugins', 'O:8:\"stdClass\":1:{s:12:\"last_checked\";i:0;}', 'yes'),
(157, '_transient_update_themes', 'O:8:\"stdClass\":1:{s:12:\"last_checked\";i:0;}', 'yes'),
(158, 'theme_mods_twentytwenty', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1575488594;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(168, 'limit_login_retries', 'a:1:{s:15:\"201.214.239.235\";i:4;}', 'no'),
(169, 'limit_login_retries_valid', 'a:1:{s:15:\"201.214.239.235\";i:1575884928;}', 'no'),
(170, 'limit_login_logged', 'a:2:{s:13:\"181.42.26.136\";a:1:{s:5:\"admin\";a:4:{s:7:\"counter\";i:1;s:4:\"date\";i:1575642041;s:7:\"gateway\";s:8:\"WP Login\";s:8:\"unlocked\";b:1;}}s:15:\"201.214.239.235\";a:3:{s:1:\"a\";a:4:{s:7:\"counter\";i:1;s:4:\"date\";i:1575682590;s:7:\"gateway\";s:8:\"WP Login\";s:8:\"unlocked\";b:1;}s:5:\"sadsa\";a:4:{s:7:\"counter\";i:1;s:4:\"date\";i:1575689426;s:7:\"gateway\";s:8:\"WP Login\";s:8:\"unlocked\";b:1;}s:25:\"fuentesguillermo@live.com\";a:3:{s:7:\"counter\";i:1;s:4:\"date\";i:1575841728;s:7:\"gateway\";s:8:\"WP Login\";}}}', 'yes'),
(175, 'can_compress_scripts', '0', 'no'),
(189, 'recovery_mode_email_last_sent', '1577304920', 'yes'),
(285, 'current_theme', 'SchoolSport ACT', 'yes'),
(286, 'theme_mods_schoolsportact', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:3:\"top\";i:11;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(287, 'theme_switched', '', 'yes'),
(295, 'acf_version', '5.8.3', 'yes'),
(314, 'category_children', 'a:0:{}', 'yes'),
(324, 'cptui_new_install', 'false', 'yes'),
(347, 'limit_login_lockouts', 'a:1:{s:15:\"201.214.239.235\";i:1575842928;}', 'yes'),
(348, 'limit_login_lockouts_total', '4', 'no'),
(440, 'ure_tasks_queue', 'a:0:{}', 'yes'),
(507, 'school_sport_register_parent', '109', 'yes'),
(508, 'school_sport_register_official', '110', 'yes'),
(509, 'school_sport_recovery', '111', 'yes'),
(510, 'school_sport_login', '112', 'yes'),
(511, 'school_sport_reset_password', '113', 'yes'),
(512, 'school_sport_register_confirmation', '114', 'yes'),
(519, 'school_sport_email_sent', '116', 'yes'),
(520, 'school_sport_dashboard', '115', 'yes'),
(521, 'school_sport_pages', 'a:9:{s:21:\"school_sport_register\";i:272;s:28:\"school_sport_register_parent\";i:109;s:30:\"school_sport_register_official\";i:110;s:21:\"school_sport_recovery\";i:111;s:18:\"school_sport_login\";i:112;s:27:\"school_sport_reset_password\";i:113;s:23:\"school_sport_email_sent\";i:116;s:34:\"school_sport_register_confirmation\";i:114;s:22:\"school_sport_dashboard\";i:115;}', 'yes'),
(544, 'options_introduction_title', 'Participation, Potential, Pathways', 'no'),
(545, '_options_introduction_title', 'field_5df13f81d99bc', 'no'),
(546, 'options_introduction_content', '<p style=\"text-align: center;\"><span class=\"font-weight-bold\">School Sport ACT (SSACT)</span> is the peak body for School Sport delivery in the ACT.\r\nSSACT actively promotes school sport for all ACT students through the support of regional, state and national representative opportunities and pathways.</p>\r\n<p style=\"text-align: center;\">Our motto of <span class=\"font-weight-bold\">‘Participation, Potential, Pathways’</span> perfectly encapsulates the role SSACT plays in the development and progression of ACT students to achieve their sporting goals.</p>', 'no'),
(547, '_options_introduction_content', 'field_5df13f89d99bd', 'no'),
(548, 'options_introduction', '', 'no'),
(549, '_options_introduction', 'field_5df13f188a27b', 'no'),
(550, 'options_banners_0_image', '231', 'no'),
(551, '_options_banners_0_image', 'field_5df13e941bca6', 'no'),
(552, 'options_banners_0_content', '2', 'no'),
(553, '_options_banners_0_content', 'field_5df13e9d1bca7', 'no'),
(554, 'options_banners', '2', 'no'),
(555, '_options_banners', 'field_5df13e5f1bca5', 'no'),
(556, 'options_sponsors', 'a:3:{i:0;s:3:\"234\";i:1;s:3:\"233\";i:2;s:3:\"235\";}', 'no'),
(557, '_options_sponsors', 'field_5df13c94bc10c', 'no'),
(558, 'options_partners', 'a:3:{i:0;s:3:\"235\";i:1;s:3:\"236\";i:2;s:3:\"232\";}', 'no'),
(559, '_options_partners', 'field_5df13d6354f26', 'no'),
(564, 'options_address', 'ACT Sports House 100 Maitland Street, Hackett, ACT 2602', 'no'),
(565, '_options_address', 'field_5df19ea9be375', 'no'),
(566, 'options_phone', '+61 2 6205 9174', 'no'),
(567, '_options_phone', 'field_5df19eb7be376', 'no'),
(568, 'options_fax', '+61 2 6205 7799', 'no'),
(569, '_options_fax', 'field_5df19ec1be377', 'no'),
(570, 'options_email', 'info@schoolsportact.org.au', 'no'),
(571, '_options_email', 'field_5df19edebe378', 'no'),
(572, 'options_abn', ' 95 825 767 889', 'no'),
(573, '_options_abn', 'field_5df19ee4be379', 'no'),
(574, 'options_google_secret_key', '6Lc2rcYUAAAAAHSfhD9WOF8Kteig3u2ySirZmxht', 'no'),
(575, '_options_google_secret_key', 'field_5df19e2ebe370', 'no'),
(576, 'options_facebook_app_id', '158660700815141', 'no'),
(577, '_options_facebook_app_id', 'field_5df19e3dbe371', 'no'),
(578, 'options_facebook_url', 'https://www.facebook.com/SchoolSportACT/', 'no'),
(579, '_options_facebook_url', 'field_5df19e4bbe372', 'no'),
(580, 'options_google_site_key', '6Lc2rcYUAAAAAFwXElkO3BxK4MKaMIhOrj9uuxwP', 'no'),
(581, '_options_google_site_key', 'field_5df1a27cb7dd1', 'no'),
(634, 'new_admin_email', 'ninpodesigncrew@gmail.com', 'yes'),
(639, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1577567646;s:7:\"checked\";a:5:{s:14:\"schoolsportact\";s:3:\"1.0\";s:14:\"twentynineteen\";s:3:\"1.4\";s:15:\"twentyseventeen\";s:3:\"2.2\";s:13:\"twentysixteen\";s:3:\"2.0\";s:12:\"twentytwenty\";s:3:\"1.1\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(662, 'WPLANG', 'en', 'yes'),
(666, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:2:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.3.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.3.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.3.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.3.2-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.3.2-partial-1.zip\";s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.3.2\";s:7:\"version\";s:5:\"5.3.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:5:\"5.3.1\";}i:1;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.3.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.3.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.3.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.3.2-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.3.2-partial-1.zip\";s:8:\"rollback\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.3.2-rollback-1.zip\";}s:7:\"current\";s:5:\"5.3.2\";s:7:\"version\";s:5:\"5.3.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:5:\"5.3.1\";s:9:\"new_files\";s:0:\"\";}}s:12:\"last_checked\";i:1577567641;s:15:\"version_checked\";s:5:\"5.3.1\";s:12:\"translations\";a:0:{}}', 'no'),
(698, 'postman_options', 'a:1:{s:21:\"fallback_smtp_enabled\";s:2:\"no\";}', 'yes'),
(699, 'postman_state', 'a:3:{s:15:\"locking_enabled\";b:1;s:12:\"install_date\";i:1576361558;s:7:\"version\";s:5:\"2.0.6\";}', 'yes'),
(704, 'widget_mc4wp_form_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(705, 'mc4wp_version', '4.7.4', 'yes'),
(706, 'mc4wp_flash_messages', 'a:0:{}', 'no'),
(710, 'mc4wp', 'a:6:{s:19:\"grecaptcha_site_key\";s:0:\"\";s:21:\"grecaptcha_secret_key\";s:0:\"\";s:7:\"api_key\";s:36:\"c5ffaa34aea6d22231a04347317697e6-us4\";s:20:\"allow_usage_tracking\";i:0;s:15:\"debug_log_level\";s:7:\"warning\";s:18:\"first_activated_on\";i:1576362889;}', 'yes'),
(725, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(730, 'wp_sendgrid_smtp_option', 'a:7:{s:10:\"from_email\";s:23:\"contact@ninpodesign.com\";s:9:\"is_active\";s:3:\"yes\";s:9:\"from_name\";s:11:\"Ninpodesign\";s:15:\"smtp_encryption\";s:4:\"none\";s:19:\"smtp_authentication\";s:4:\"true\";s:13:\"smtp_username\";s:15:\"ninpodesigncrew\";s:13:\"smtp_password\";s:10:\"1H2o3l4a5s\";}', 'yes'),
(735, 'widget_maxmegamenu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(736, 'megamenu_version', '2.7.3', 'yes'),
(737, 'megamenu_initial_version', '2.7.3', 'yes'),
(738, 'megamenu_multisite_share_themes', 'false', 'yes'),
(739, 'megamenu_settings', 'a:4:{s:6:\"prefix\";s:8:\"disabled\";s:12:\"descriptions\";s:7:\"enabled\";s:12:\"second_click\";s:2:\"go\";s:3:\"top\";a:7:{s:7:\"enabled\";s:1:\"1\";s:5:\"event\";s:5:\"hover\";s:6:\"effect\";s:7:\"fade_up\";s:12:\"effect_speed\";s:3:\"200\";s:13:\"effect_mobile\";s:8:\"disabled\";s:19:\"effect_speed_mobile\";s:3:\"200\";s:5:\"theme\";s:7:\"default\";}}', 'yes'),
(742, '_transient_timeout_megamenu_css_version', '4729971042', 'no'),
(743, '_transient_megamenu_css_version', '2.7.3', 'no'),
(744, '_transient_timeout_megamenu_css_last_updated', '4729971042', 'no'),
(745, '_transient_megamenu_css_last_updated', '1576371042', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(746, 'megamenu_themes', 'a:1:{s:7:\"default\";a:221:{s:5:\"title\";s:7:\"Default\";s:8:\"arrow_up\";s:9:\"dash-f142\";s:10:\"arrow_down\";s:9:\"dash-f140\";s:10:\"arrow_left\";s:9:\"dash-f141\";s:11:\"arrow_right\";s:9:\"dash-f139\";s:11:\"line_height\";s:3:\"1.7\";s:7:\"z_index\";s:3:\"999\";s:17:\"shadow_horizontal\";s:3:\"0px\";s:15:\"shadow_vertical\";s:3:\"0px\";s:11:\"shadow_blur\";s:3:\"5px\";s:13:\"shadow_spread\";s:3:\"0px\";s:12:\"shadow_color\";s:18:\"rgba(0, 0, 0, 0.1)\";s:21:\"menu_item_link_height\";s:4:\"40px\";s:25:\"container_background_from\";s:24:\"rgba(255, 255, 255, 0.1)\";s:23:\"container_background_to\";s:24:\"rgba(255, 255, 255, 0.1)\";s:21:\"container_padding_top\";s:3:\"0px\";s:23:\"container_padding_right\";s:3:\"0px\";s:24:\"container_padding_bottom\";s:3:\"0px\";s:22:\"container_padding_left\";s:3:\"0px\";s:32:\"container_border_radius_top_left\";s:3:\"0px\";s:33:\"container_border_radius_top_right\";s:3:\"0px\";s:36:\"container_border_radius_bottom_right\";s:3:\"0px\";s:35:\"container_border_radius_bottom_left\";s:3:\"0px\";s:15:\"menu_item_align\";s:4:\"left\";s:20:\"menu_item_link_color\";s:7:\"#ffffff\";s:24:\"menu_item_link_font_size\";s:4:\"14px\";s:19:\"menu_item_link_font\";s:7:\"inherit\";s:29:\"menu_item_link_text_transform\";s:4:\"none\";s:21:\"menu_item_link_weight\";s:6:\"normal\";s:30:\"menu_item_link_text_decoration\";s:4:\"none\";s:25:\"menu_item_link_text_align\";s:4:\"left\";s:26:\"menu_item_link_color_hover\";s:7:\"#ffffff\";s:27:\"menu_item_link_weight_hover\";s:6:\"normal\";s:36:\"menu_item_link_text_decoration_hover\";s:4:\"none\";s:25:\"menu_item_background_from\";s:13:\"rgba(0,0,0,0)\";s:23:\"menu_item_background_to\";s:13:\"rgba(0,0,0,0)\";s:31:\"menu_item_background_hover_from\";s:24:\"rgba(255, 255, 255, 0.1)\";s:29:\"menu_item_background_hover_to\";s:24:\"rgba(255, 255, 255, 0.1)\";s:17:\"menu_item_spacing\";s:3:\"0px\";s:26:\"menu_item_link_padding_top\";s:3:\"0px\";s:28:\"menu_item_link_padding_right\";s:4:\"10px\";s:29:\"menu_item_link_padding_bottom\";s:3:\"0px\";s:27:\"menu_item_link_padding_left\";s:4:\"10px\";s:22:\"menu_item_border_color\";s:4:\"#fff\";s:28:\"menu_item_border_color_hover\";s:4:\"#fff\";s:20:\"menu_item_border_top\";s:3:\"0px\";s:22:\"menu_item_border_right\";s:3:\"0px\";s:23:\"menu_item_border_bottom\";s:3:\"0px\";s:21:\"menu_item_border_left\";s:3:\"0px\";s:37:\"menu_item_link_border_radius_top_left\";s:3:\"0px\";s:38:\"menu_item_link_border_radius_top_right\";s:3:\"0px\";s:41:\"menu_item_link_border_radius_bottom_right\";s:3:\"0px\";s:40:\"menu_item_link_border_radius_bottom_left\";s:3:\"0px\";s:23:\"menu_item_divider_color\";s:24:\"rgba(255, 255, 255, 0.1)\";s:30:\"menu_item_divider_glow_opacity\";s:3:\"0.1\";s:27:\"menu_item_highlight_current\";s:2:\"on\";s:21:\"panel_background_from\";s:7:\"#f1f1f1\";s:19:\"panel_background_to\";s:7:\"#f1f1f1\";s:11:\"panel_width\";s:4:\"100%\";s:17:\"panel_inner_width\";s:4:\"100%\";s:17:\"panel_padding_top\";s:3:\"0px\";s:19:\"panel_padding_right\";s:3:\"0px\";s:20:\"panel_padding_bottom\";s:3:\"0px\";s:18:\"panel_padding_left\";s:3:\"0px\";s:18:\"panel_border_color\";s:4:\"#fff\";s:16:\"panel_border_top\";s:3:\"0px\";s:18:\"panel_border_right\";s:3:\"0px\";s:19:\"panel_border_bottom\";s:3:\"0px\";s:17:\"panel_border_left\";s:3:\"0px\";s:28:\"panel_border_radius_top_left\";s:3:\"0px\";s:29:\"panel_border_radius_top_right\";s:3:\"0px\";s:32:\"panel_border_radius_bottom_right\";s:3:\"0px\";s:31:\"panel_border_radius_bottom_left\";s:3:\"0px\";s:24:\"panel_widget_padding_top\";s:4:\"15px\";s:26:\"panel_widget_padding_right\";s:4:\"15px\";s:27:\"panel_widget_padding_bottom\";s:4:\"15px\";s:25:\"panel_widget_padding_left\";s:4:\"15px\";s:18:\"panel_header_color\";s:4:\"#555\";s:22:\"panel_header_font_size\";s:4:\"16px\";s:17:\"panel_header_font\";s:7:\"inherit\";s:27:\"panel_header_text_transform\";s:9:\"uppercase\";s:24:\"panel_header_font_weight\";s:4:\"bold\";s:28:\"panel_header_text_decoration\";s:4:\"none\";s:23:\"panel_header_text_align\";s:4:\"left\";s:24:\"panel_header_padding_top\";s:3:\"0px\";s:26:\"panel_header_padding_right\";s:3:\"0px\";s:27:\"panel_header_padding_bottom\";s:3:\"5px\";s:25:\"panel_header_padding_left\";s:3:\"0px\";s:23:\"panel_header_margin_top\";s:3:\"0px\";s:25:\"panel_header_margin_right\";s:3:\"0px\";s:26:\"panel_header_margin_bottom\";s:3:\"0px\";s:24:\"panel_header_margin_left\";s:3:\"0px\";s:25:\"panel_header_border_color\";s:13:\"rgba(0,0,0,0)\";s:31:\"panel_header_border_color_hover\";s:13:\"rgba(0,0,0,0)\";s:23:\"panel_header_border_top\";s:3:\"0px\";s:25:\"panel_header_border_right\";s:3:\"0px\";s:26:\"panel_header_border_bottom\";s:3:\"0px\";s:24:\"panel_header_border_left\";s:3:\"0px\";s:16:\"panel_font_color\";s:4:\"#666\";s:15:\"panel_font_size\";s:4:\"14px\";s:17:\"panel_font_family\";s:7:\"inherit\";s:29:\"panel_second_level_font_color\";s:4:\"#555\";s:28:\"panel_second_level_font_size\";s:4:\"16px\";s:23:\"panel_second_level_font\";s:7:\"inherit\";s:33:\"panel_second_level_text_transform\";s:9:\"uppercase\";s:30:\"panel_second_level_font_weight\";s:4:\"bold\";s:34:\"panel_second_level_text_decoration\";s:4:\"none\";s:29:\"panel_second_level_text_align\";s:4:\"left\";s:35:\"panel_second_level_font_color_hover\";s:4:\"#555\";s:36:\"panel_second_level_font_weight_hover\";s:4:\"bold\";s:40:\"panel_second_level_text_decoration_hover\";s:4:\"none\";s:40:\"panel_second_level_background_hover_from\";s:13:\"rgba(0,0,0,0)\";s:38:\"panel_second_level_background_hover_to\";s:13:\"rgba(0,0,0,0)\";s:30:\"panel_second_level_padding_top\";s:3:\"0px\";s:32:\"panel_second_level_padding_right\";s:3:\"0px\";s:33:\"panel_second_level_padding_bottom\";s:3:\"0px\";s:31:\"panel_second_level_padding_left\";s:3:\"0px\";s:29:\"panel_second_level_margin_top\";s:3:\"0px\";s:31:\"panel_second_level_margin_right\";s:3:\"0px\";s:32:\"panel_second_level_margin_bottom\";s:3:\"0px\";s:30:\"panel_second_level_margin_left\";s:3:\"0px\";s:31:\"panel_second_level_border_color\";s:13:\"rgba(0,0,0,0)\";s:37:\"panel_second_level_border_color_hover\";s:13:\"rgba(0,0,0,0)\";s:29:\"panel_second_level_border_top\";s:3:\"0px\";s:31:\"panel_second_level_border_right\";s:3:\"0px\";s:32:\"panel_second_level_border_bottom\";s:3:\"0px\";s:30:\"panel_second_level_border_left\";s:3:\"0px\";s:28:\"panel_third_level_font_color\";s:4:\"#666\";s:27:\"panel_third_level_font_size\";s:4:\"14px\";s:22:\"panel_third_level_font\";s:7:\"inherit\";s:32:\"panel_third_level_text_transform\";s:4:\"none\";s:29:\"panel_third_level_font_weight\";s:6:\"normal\";s:33:\"panel_third_level_text_decoration\";s:4:\"none\";s:28:\"panel_third_level_text_align\";s:4:\"left\";s:34:\"panel_third_level_font_color_hover\";s:4:\"#666\";s:35:\"panel_third_level_font_weight_hover\";s:6:\"normal\";s:39:\"panel_third_level_text_decoration_hover\";s:4:\"none\";s:39:\"panel_third_level_background_hover_from\";s:13:\"rgba(0,0,0,0)\";s:37:\"panel_third_level_background_hover_to\";s:13:\"rgba(0,0,0,0)\";s:29:\"panel_third_level_padding_top\";s:3:\"0px\";s:31:\"panel_third_level_padding_right\";s:3:\"0px\";s:32:\"panel_third_level_padding_bottom\";s:3:\"0px\";s:30:\"panel_third_level_padding_left\";s:3:\"0px\";s:28:\"panel_third_level_margin_top\";s:3:\"0px\";s:30:\"panel_third_level_margin_right\";s:3:\"0px\";s:31:\"panel_third_level_margin_bottom\";s:3:\"0px\";s:29:\"panel_third_level_margin_left\";s:3:\"0px\";s:30:\"panel_third_level_border_color\";s:13:\"rgba(0,0,0,0)\";s:36:\"panel_third_level_border_color_hover\";s:13:\"rgba(0,0,0,0)\";s:28:\"panel_third_level_border_top\";s:3:\"0px\";s:30:\"panel_third_level_border_right\";s:3:\"0px\";s:31:\"panel_third_level_border_bottom\";s:3:\"0px\";s:29:\"panel_third_level_border_left\";s:3:\"0px\";s:27:\"flyout_menu_background_from\";s:7:\"#f1f1f1\";s:25:\"flyout_menu_background_to\";s:7:\"#f1f1f1\";s:12:\"flyout_width\";s:5:\"250px\";s:18:\"flyout_padding_top\";s:3:\"0px\";s:20:\"flyout_padding_right\";s:3:\"0px\";s:21:\"flyout_padding_bottom\";s:3:\"0px\";s:19:\"flyout_padding_left\";s:3:\"0px\";s:19:\"flyout_border_color\";s:7:\"#ffffff\";s:17:\"flyout_border_top\";s:3:\"0px\";s:19:\"flyout_border_right\";s:3:\"0px\";s:20:\"flyout_border_bottom\";s:3:\"0px\";s:18:\"flyout_border_left\";s:3:\"0px\";s:29:\"flyout_border_radius_top_left\";s:3:\"0px\";s:30:\"flyout_border_radius_top_right\";s:3:\"0px\";s:33:\"flyout_border_radius_bottom_right\";s:3:\"0px\";s:32:\"flyout_border_radius_bottom_left\";s:3:\"0px\";s:22:\"flyout_background_from\";s:7:\"#f1f1f1\";s:20:\"flyout_background_to\";s:7:\"#f1f1f1\";s:28:\"flyout_background_hover_from\";s:7:\"#dddddd\";s:26:\"flyout_background_hover_to\";s:7:\"#dddddd\";s:18:\"flyout_link_height\";s:4:\"35px\";s:23:\"flyout_link_padding_top\";s:3:\"0px\";s:25:\"flyout_link_padding_right\";s:4:\"10px\";s:26:\"flyout_link_padding_bottom\";s:3:\"0px\";s:24:\"flyout_link_padding_left\";s:4:\"10px\";s:17:\"flyout_link_color\";s:4:\"#666\";s:16:\"flyout_link_size\";s:4:\"14px\";s:18:\"flyout_link_family\";s:7:\"inherit\";s:26:\"flyout_link_text_transform\";s:4:\"none\";s:18:\"flyout_link_weight\";s:6:\"normal\";s:27:\"flyout_link_text_decoration\";s:4:\"none\";s:23:\"flyout_link_color_hover\";s:4:\"#666\";s:24:\"flyout_link_weight_hover\";s:6:\"normal\";s:33:\"flyout_link_text_decoration_hover\";s:4:\"none\";s:30:\"flyout_menu_item_divider_color\";s:24:\"rgba(255, 255, 255, 0.1)\";s:21:\"responsive_breakpoint\";s:5:\"600px\";s:22:\"toggle_background_from\";s:4:\"#222\";s:20:\"toggle_background_to\";s:4:\"#222\";s:17:\"toggle_bar_height\";s:4:\"40px\";s:33:\"toggle_bar_border_radius_top_left\";s:3:\"2px\";s:34:\"toggle_bar_border_radius_top_right\";s:3:\"2px\";s:37:\"toggle_bar_border_radius_bottom_right\";s:3:\"2px\";s:36:\"toggle_bar_border_radius_bottom_left\";s:3:\"2px\";s:32:\"mobile_menu_force_width_selector\";s:4:\"body\";s:28:\"mobile_menu_off_canvas_width\";s:5:\"300px\";s:23:\"mobile_menu_item_height\";s:4:\"40px\";s:23:\"mobile_menu_padding_top\";s:3:\"0px\";s:25:\"mobile_menu_padding_right\";s:3:\"0px\";s:26:\"mobile_menu_padding_bottom\";s:3:\"0px\";s:24:\"mobile_menu_padding_left\";s:3:\"0px\";s:22:\"mobile_background_from\";s:4:\"#222\";s:20:\"mobile_background_to\";s:4:\"#222\";s:38:\"mobile_menu_item_background_hover_from\";s:4:\"#333\";s:36:\"mobile_menu_item_background_hover_to\";s:4:\"#333\";s:27:\"mobile_menu_item_link_color\";s:7:\"#ffffff\";s:31:\"mobile_menu_item_link_font_size\";s:4:\"14px\";s:32:\"mobile_menu_item_link_text_align\";s:4:\"left\";s:33:\"mobile_menu_item_link_color_hover\";s:7:\"#ffffff\";s:14:\"mobile_columns\";s:1:\"1\";s:10:\"custom_css\";s:67:\"/** Push menu onto new line **/ \r\n#{$wrap} { \r\n    clear: both; \r\n}\";s:6:\"shadow\";s:3:\"off\";s:11:\"transitions\";s:3:\"off\";s:6:\"resets\";s:3:\"off\";s:17:\"menu_item_divider\";s:3:\"off\";s:24:\"flyout_menu_item_divider\";s:3:\"off\";s:21:\"disable_mobile_toggle\";s:3:\"off\";s:19:\"mobile_menu_overlay\";s:3:\"off\";s:23:\"mobile_menu_force_width\";s:3:\"off\";}}', 'yes'),
(747, 'megamenu_themes_last_updated', 'default', 'yes'),
(748, 'megamenu_toggle_blocks', 'a:1:{s:7:\"default\";a:1:{i:1;a:11:{s:4:\"type\";s:11:\"menu_toggle\";s:5:\"align\";s:5:\"right\";s:11:\"closed_text\";s:4:\"MENU\";s:9:\"open_text\";s:4:\"MENU\";s:11:\"closed_icon\";s:9:\"dash-f333\";s:9:\"open_icon\";s:9:\"dash-f153\";s:10:\"text_color\";s:18:\"rgb(221, 221, 221)\";s:9:\"text_size\";s:4:\"14px\";s:10:\"icon_color\";s:18:\"rgb(221, 221, 221)\";s:9:\"icon_size\";s:4:\"24px\";s:13:\"icon_position\";s:5:\"after\";}}}', 'yes'),
(751, '_transient_timeout_megamenu_css', '4729971042', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(752, '_transient_megamenu_css', '/** Sunday 15th December 2019 00:50:42 UTC (core) **/\n/** THIS FILE IS AUTOMATICALLY GENERATED - DO NOT MAKE MANUAL EDITS! **/\n/** Custom CSS should be added to Mega Menu > Menu Themes > Custom Styling **/\n\n.mega-menu-last-modified-1576371042 { content: \'Sunday 15th December 2019 00:50:42 UTC\'; }\n\n#mega-menu-wrap-top, #mega-menu-wrap-top #mega-menu-top, #mega-menu-wrap-top #mega-menu-top ul.mega-sub-menu, #mega-menu-wrap-top #mega-menu-top li.mega-menu-item, #mega-menu-wrap-top #mega-menu-top li.mega-menu-row, #mega-menu-wrap-top #mega-menu-top li.mega-menu-column, #mega-menu-wrap-top #mega-menu-top a.mega-menu-link {\n  transition: none;\n  border-radius: 0;\n  box-shadow: none;\n  background: none;\n  border: 0;\n  bottom: auto;\n  box-sizing: border-box;\n  clip: auto;\n  color: #666;\n  display: block;\n  float: none;\n  font-family: inherit;\n  font-size: 14px;\n  height: auto;\n  left: auto;\n  line-height: 1.7;\n  list-style-type: none;\n  margin: 0;\n  min-height: auto;\n  max-height: none;\n  opacity: 1;\n  outline: none;\n  overflow: visible;\n  padding: 0;\n  position: relative;\n  pointer-events: auto;\n  right: auto;\n  text-align: left;\n  text-decoration: none;\n  text-indent: 0;\n  text-transform: none;\n  transform: none;\n  top: auto;\n  vertical-align: baseline;\n  visibility: inherit;\n  width: auto;\n  word-wrap: break-word;\n  white-space: normal;\n}\n#mega-menu-wrap-top:before, #mega-menu-wrap-top #mega-menu-top:before, #mega-menu-wrap-top #mega-menu-top ul.mega-sub-menu:before, #mega-menu-wrap-top #mega-menu-top li.mega-menu-item:before, #mega-menu-wrap-top #mega-menu-top li.mega-menu-row:before, #mega-menu-wrap-top #mega-menu-top li.mega-menu-column:before, #mega-menu-wrap-top #mega-menu-top a.mega-menu-link:before, #mega-menu-wrap-top:after, #mega-menu-wrap-top #mega-menu-top:after, #mega-menu-wrap-top #mega-menu-top ul.mega-sub-menu:after, #mega-menu-wrap-top #mega-menu-top li.mega-menu-item:after, #mega-menu-wrap-top #mega-menu-top li.mega-menu-row:after, #mega-menu-wrap-top #mega-menu-top li.mega-menu-column:after, #mega-menu-wrap-top #mega-menu-top a.mega-menu-link:after {\n  display: none;\n}\n#mega-menu-wrap-top {\n  border-radius: 0;\n}\n@media only screen and (min-width: 601px) {\n  #mega-menu-wrap-top {\n    background: rgba(255, 255, 255, 0.1);\n  }\n}\n#mega-menu-wrap-top.mega-keyboard-navigation .mega-menu-toggle:focus, #mega-menu-wrap-top.mega-keyboard-navigation .mega-toggle-block:focus, #mega-menu-wrap-top.mega-keyboard-navigation .mega-toggle-block a:focus, #mega-menu-wrap-top.mega-keyboard-navigation .mega-toggle-block .mega-search input[type=text]:focus, #mega-menu-wrap-top.mega-keyboard-navigation .mega-toggle-block button.mega-toggle-animated:focus, #mega-menu-wrap-top.mega-keyboard-navigation #mega-menu-top a:focus, #mega-menu-wrap-top.mega-keyboard-navigation #mega-menu-top input:focus, #mega-menu-wrap-top.mega-keyboard-navigation #mega-menu-top li.mega-menu-item a.mega-menu-link:focus {\n  outline: 3px solid #109cde;\n  outline-offset: -3px;\n}\n#mega-menu-wrap-top.mega-keyboard-navigation .mega-toggle-block button.mega-toggle-animated:focus {\n  outline-offset: 2px;\n}\n#mega-menu-wrap-top.mega-keyboard-navigation > li.mega-menu-item > a.mega-menu-link:focus {\n  background: rgba(255, 255, 255, 0.1);\n  color: #fff;\n  font-weight: normal;\n  text-decoration: none;\n  border-color: #fff;\n}\n@media only screen and (max-width: 600px) {\n  #mega-menu-wrap-top.mega-keyboard-navigation > li.mega-menu-item > a.mega-menu-link:focus {\n    color: #fff;\n    background: #333;\n  }\n}\n#mega-menu-wrap-top #mega-menu-top {\n  visibility: visible;\n  text-align: left;\n  padding: 0px 0px 0px 0px;\n}\n#mega-menu-wrap-top #mega-menu-top a.mega-menu-link {\n  cursor: pointer;\n  display: inline;\n}\n#mega-menu-wrap-top #mega-menu-top a.mega-menu-link .mega-description-group {\n  vertical-align: middle;\n  display: inline-block;\n  transition: none;\n}\n#mega-menu-wrap-top #mega-menu-top a.mega-menu-link .mega-description-group .mega-menu-title, #mega-menu-wrap-top #mega-menu-top a.mega-menu-link .mega-description-group .mega-menu-description {\n  transition: none;\n  line-height: 1.5;\n  display: block;\n}\n#mega-menu-wrap-top #mega-menu-top a.mega-menu-link .mega-description-group .mega-menu-description {\n  font-style: italic;\n  font-size: 0.8em;\n  text-transform: none;\n  font-weight: normal;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu li.mega-menu-item.mega-icon-left.mega-has-description.mega-has-icon > a.mega-menu-link {\n  display: flex;\n  align-items: center;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu li.mega-menu-item.mega-icon-left.mega-has-description.mega-has-icon > a.mega-menu-link:before {\n  flex: 0 0 auto;\n  align-self: flex-start;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-tabbed.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item.mega-icon-left.mega-has-description.mega-has-icon > a.mega-menu-link {\n  display: block;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-item.mega-icon-top > a.mega-menu-link {\n  display: table-cell;\n  vertical-align: middle;\n  line-height: initial;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-item.mega-icon-top > a.mega-menu-link:before {\n  display: block;\n  margin: 0 0 6px 0;\n  text-align: center;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-item.mega-icon-top > a.mega-menu-link > span.mega-title-below {\n  display: inline-block;\n  transition: none;\n}\n@media only screen and (max-width: 600px) {\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-item.mega-icon-top > a.mega-menu-link {\n    display: block;\n    line-height: 40px;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-item.mega-icon-top > a.mega-menu-link:before {\n    display: inline-block;\n    margin: 0 6px 0 0;\n    text-align: left;\n  }\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-item.mega-icon-right > a.mega-menu-link:before {\n  float: right;\n  margin: 0 0 0 6px;\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-animating > ul.mega-sub-menu {\n  pointer-events: none;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-disable-link > a.mega-menu-link, #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu li.mega-disable-link > a.mega-menu-link {\n  cursor: inherit;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-item-has-children.mega-disable-link > a.mega-menu-link, #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > li.mega-menu-item-has-children.mega-disable-link > a.mega-menu-link {\n  cursor: pointer;\n}\n#mega-menu-wrap-top #mega-menu-top p {\n  margin-bottom: 10px;\n}\n#mega-menu-wrap-top #mega-menu-top input, #mega-menu-wrap-top #mega-menu-top img {\n  max-width: 100%;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-item > ul.mega-sub-menu {\n  display: block;\n  visibility: hidden;\n  opacity: 1;\n  pointer-events: auto;\n}\n@media only screen and (max-width: 600px) {\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-item > ul.mega-sub-menu {\n    display: none;\n    visibility: visible;\n    opacity: 1;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-item.mega-toggle-on > ul.mega-sub-menu, #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu.mega-menu-item.mega-toggle-on ul.mega-sub-menu {\n    display: block;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu.mega-menu-item.mega-toggle-on li.mega-hide-sub-menu-on-mobile > ul.mega-sub-menu, #mega-menu-wrap-top #mega-menu-top li.mega-hide-sub-menu-on-mobile > ul.mega-sub-menu {\n    display: none;\n  }\n}\n@media only screen and (min-width: 601px) {\n  #mega-menu-wrap-top #mega-menu-top[data-effect=\"fade\"] li.mega-menu-item > ul.mega-sub-menu {\n    opacity: 0;\n    transition: opacity 200ms ease-in, visibility 200ms ease-in;\n  }\n  #mega-menu-wrap-top #mega-menu-top[data-effect=\"fade\"].mega-no-js li.mega-menu-item:hover > ul.mega-sub-menu, #mega-menu-wrap-top #mega-menu-top[data-effect=\"fade\"].mega-no-js li.mega-menu-item:focus > ul.mega-sub-menu, #mega-menu-wrap-top #mega-menu-top[data-effect=\"fade\"] li.mega-menu-item.mega-toggle-on > ul.mega-sub-menu, #mega-menu-wrap-top #mega-menu-top[data-effect=\"fade\"] li.mega-menu-item.mega-menu-megamenu.mega-toggle-on ul.mega-sub-menu {\n    opacity: 1;\n  }\n  #mega-menu-wrap-top #mega-menu-top[data-effect=\"fade_up\"] li.mega-menu-item.mega-menu-megamenu > ul.mega-sub-menu, #mega-menu-wrap-top #mega-menu-top[data-effect=\"fade_up\"] li.mega-menu-item.mega-menu-flyout ul.mega-sub-menu {\n    opacity: 0;\n    transform: translate(0, 10px);\n    transition: opacity 200ms ease-in, transform 200ms ease-in, visibility 200ms ease-in;\n  }\n  #mega-menu-wrap-top #mega-menu-top[data-effect=\"fade_up\"].mega-no-js li.mega-menu-item:hover > ul.mega-sub-menu, #mega-menu-wrap-top #mega-menu-top[data-effect=\"fade_up\"].mega-no-js li.mega-menu-item:focus > ul.mega-sub-menu, #mega-menu-wrap-top #mega-menu-top[data-effect=\"fade_up\"] li.mega-menu-item.mega-toggle-on > ul.mega-sub-menu, #mega-menu-wrap-top #mega-menu-top[data-effect=\"fade_up\"] li.mega-menu-item.mega-menu-megamenu.mega-toggle-on ul.mega-sub-menu {\n    opacity: 1;\n    transform: translate(0, 0);\n  }\n  #mega-menu-wrap-top #mega-menu-top[data-effect=\"slide_up\"] li.mega-menu-item.mega-menu-megamenu > ul.mega-sub-menu, #mega-menu-wrap-top #mega-menu-top[data-effect=\"slide_up\"] li.mega-menu-item.mega-menu-flyout ul.mega-sub-menu {\n    transform: translate(0, 10px);\n    transition: transform 200ms ease-in, visibility 200ms ease-in;\n  }\n  #mega-menu-wrap-top #mega-menu-top[data-effect=\"slide_up\"].mega-no-js li.mega-menu-item:hover > ul.mega-sub-menu, #mega-menu-wrap-top #mega-menu-top[data-effect=\"slide_up\"].mega-no-js li.mega-menu-item:focus > ul.mega-sub-menu, #mega-menu-wrap-top #mega-menu-top[data-effect=\"slide_up\"] li.mega-menu-item.mega-toggle-on > ul.mega-sub-menu, #mega-menu-wrap-top #mega-menu-top[data-effect=\"slide_up\"] li.mega-menu-item.mega-menu-megamenu.mega-toggle-on ul.mega-sub-menu {\n    transform: translate(0, 0);\n  }\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-item.mega-menu-megamenu ul.mega-sub-menu li.mega-collapse-children > ul.mega-sub-menu {\n  display: none;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-item.mega-menu-megamenu ul.mega-sub-menu li.mega-collapse-children.mega-toggle-on > ul.mega-sub-menu {\n  display: block;\n}\n#mega-menu-wrap-top #mega-menu-top.mega-no-js li.mega-menu-item:hover > ul.mega-sub-menu, #mega-menu-wrap-top #mega-menu-top.mega-no-js li.mega-menu-item:focus > ul.mega-sub-menu, #mega-menu-wrap-top #mega-menu-top li.mega-menu-item.mega-toggle-on > ul.mega-sub-menu {\n  visibility: visible;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-item.mega-menu-megamenu ul.mega-sub-menu ul.mega-sub-menu {\n  visibility: inherit;\n  opacity: 1;\n  display: block;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-item.mega-menu-megamenu ul.mega-sub-menu li.mega-1-columns > ul.mega-sub-menu > li.mega-menu-item {\n  float: left;\n  width: 100%;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-item.mega-menu-megamenu ul.mega-sub-menu li.mega-2-columns > ul.mega-sub-menu > li.mega-menu-item {\n  float: left;\n  width: 50%;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-item.mega-menu-megamenu ul.mega-sub-menu li.mega-3-columns > ul.mega-sub-menu > li.mega-menu-item {\n  float: left;\n  width: 33.33333%;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-item.mega-menu-megamenu ul.mega-sub-menu li.mega-4-columns > ul.mega-sub-menu > li.mega-menu-item {\n  float: left;\n  width: 25%;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-item a[class^=\'dashicons\']:before {\n  font-family: dashicons;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-item a.mega-menu-link:before {\n  display: inline-block;\n  font: inherit;\n  font-family: dashicons;\n  position: static;\n  margin: 0 6px 0 0px;\n  vertical-align: top;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n  color: inherit;\n  background: transparent;\n  height: auto;\n  width: auto;\n  top: auto;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-item.mega-hide-text a.mega-menu-link:before {\n  margin: 0;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-item.mega-hide-text li.mega-menu-item a.mega-menu-link:before {\n  margin: 0 6px 0 0;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-align-bottom-left.mega-toggle-on > a.mega-menu-link {\n  border-radius: 0;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-align-bottom-right > ul.mega-sub-menu {\n  right: 0;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-align-bottom-right.mega-toggle-on > a.mega-menu-link {\n  border-radius: 0;\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu.mega-menu-item {\n  position: static;\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-item {\n  margin: 0 0px 0 0;\n  display: inline-block;\n  height: auto;\n  vertical-align: middle;\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-item.mega-item-align-right {\n  float: right;\n}\n@media only screen and (min-width: 601px) {\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-item.mega-item-align-right {\n    margin: 0 0 0 0px;\n  }\n}\n@media only screen and (min-width: 601px) {\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-item.mega-item-align-float-left {\n    float: left;\n  }\n}\n@media only screen and (min-width: 601px) {\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-item > a.mega-menu-link:hover {\n    background: rgba(255, 255, 255, 0.1);\n    color: #fff;\n    font-weight: normal;\n    text-decoration: none;\n    border-color: #fff;\n  }\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-item.mega-toggle-on > a.mega-menu-link {\n  background: rgba(255, 255, 255, 0.1);\n  color: #fff;\n  font-weight: normal;\n  text-decoration: none;\n  border-color: #fff;\n}\n@media only screen and (max-width: 600px) {\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-item.mega-toggle-on > a.mega-menu-link {\n    color: #fff;\n    background: #333;\n  }\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-item.mega-current-menu-item > a.mega-menu-link, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-item.mega-current-menu-ancestor > a.mega-menu-link, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-item.mega-current-page-ancestor > a.mega-menu-link {\n  background: rgba(255, 255, 255, 0.1);\n  color: #fff;\n  font-weight: normal;\n  text-decoration: none;\n  border-color: #fff;\n}\n@media only screen and (max-width: 600px) {\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-item.mega-current-menu-item > a.mega-menu-link, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-item.mega-current-menu-ancestor > a.mega-menu-link, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-item.mega-current-page-ancestor > a.mega-menu-link {\n    color: #fff;\n    background: #333;\n  }\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-item > a.mega-menu-link {\n  line-height: 40px;\n  height: 40px;\n  padding: 0px 10px 0px 10px;\n  vertical-align: baseline;\n  width: auto;\n  display: block;\n  color: #fff;\n  text-transform: none;\n  text-decoration: none;\n  text-align: left;\n  text-decoration: none;\n  background: rgba(0, 0, 0, 0);\n  border: 0;\n  border-radius: 0;\n  font-family: inherit;\n  font-size: 14px;\n  font-weight: normal;\n  outline: none;\n}\n@media only screen and (min-width: 601px) {\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-item.mega-multi-line > a.mega-menu-link {\n    line-height: inherit;\n    display: table-cell;\n    vertical-align: middle;\n  }\n}\n@media only screen and (max-width: 600px) {\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-item.mega-multi-line > a.mega-menu-link br {\n    display: none;\n  }\n}\n@media only screen and (max-width: 600px) {\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-item {\n    display: list-item;\n    margin: 0;\n    clear: both;\n    border: 0;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-item.mega-item-align-right {\n    float: none;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-item > a.mega-menu-link {\n    border-radius: 0;\n    border: 0;\n    margin: 0;\n    line-height: 40px;\n    height: 40px;\n    padding: 0 10px;\n    background: transparent;\n    text-align: left;\n    color: #fff;\n    font-size: 14px;\n  }\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row {\n  width: 100%;\n  float: left;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row .mega-menu-column {\n  float: left;\n  min-height: 1px;\n}\n@media only screen and (min-width: 601px) {\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-1-of-1 {\n    width: 100%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-1-of-2 {\n    width: 50%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-2-of-2 {\n    width: 100%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-1-of-3 {\n    width: 33.33333%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-2-of-3 {\n    width: 66.66667%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-3-of-3 {\n    width: 100%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-1-of-4 {\n    width: 25%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-2-of-4 {\n    width: 50%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-3-of-4 {\n    width: 75%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-4-of-4 {\n    width: 100%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-1-of-5 {\n    width: 20%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-2-of-5 {\n    width: 40%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-3-of-5 {\n    width: 60%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-4-of-5 {\n    width: 80%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-5-of-5 {\n    width: 100%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-1-of-6 {\n    width: 16.66667%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-2-of-6 {\n    width: 33.33333%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-3-of-6 {\n    width: 50%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-4-of-6 {\n    width: 66.66667%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-5-of-6 {\n    width: 83.33333%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-6-of-6 {\n    width: 100%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-1-of-7 {\n    width: 14.28571%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-2-of-7 {\n    width: 28.57143%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-3-of-7 {\n    width: 42.85714%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-4-of-7 {\n    width: 57.14286%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-5-of-7 {\n    width: 71.42857%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-6-of-7 {\n    width: 85.71429%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-7-of-7 {\n    width: 100%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-1-of-8 {\n    width: 12.5%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-2-of-8 {\n    width: 25%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-3-of-8 {\n    width: 37.5%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-4-of-8 {\n    width: 50%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-5-of-8 {\n    width: 62.5%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-6-of-8 {\n    width: 75%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-7-of-8 {\n    width: 87.5%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-8-of-8 {\n    width: 100%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-1-of-9 {\n    width: 11.11111%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-2-of-9 {\n    width: 22.22222%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-3-of-9 {\n    width: 33.33333%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-4-of-9 {\n    width: 44.44444%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-5-of-9 {\n    width: 55.55556%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-6-of-9 {\n    width: 66.66667%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-7-of-9 {\n    width: 77.77778%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-8-of-9 {\n    width: 88.88889%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-9-of-9 {\n    width: 100%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-1-of-10 {\n    width: 10%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-2-of-10 {\n    width: 20%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-3-of-10 {\n    width: 30%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-4-of-10 {\n    width: 40%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-5-of-10 {\n    width: 50%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-6-of-10 {\n    width: 60%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-7-of-10 {\n    width: 70%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-8-of-10 {\n    width: 80%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-9-of-10 {\n    width: 90%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-10-of-10 {\n    width: 100%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-1-of-11 {\n    width: 9.09091%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-2-of-11 {\n    width: 18.18182%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-3-of-11 {\n    width: 27.27273%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-4-of-11 {\n    width: 36.36364%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-5-of-11 {\n    width: 45.45455%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-6-of-11 {\n    width: 54.54545%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-7-of-11 {\n    width: 63.63636%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-8-of-11 {\n    width: 72.72727%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-9-of-11 {\n    width: 81.81818%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-10-of-11 {\n    width: 90.90909%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-11-of-11 {\n    width: 100%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-1-of-12 {\n    width: 8.33333%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-2-of-12 {\n    width: 16.66667%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-3-of-12 {\n    width: 25%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-4-of-12 {\n    width: 33.33333%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-5-of-12 {\n    width: 41.66667%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-6-of-12 {\n    width: 50%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-7-of-12 {\n    width: 58.33333%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-8-of-12 {\n    width: 66.66667%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-9-of-12 {\n    width: 75%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-10-of-12 {\n    width: 83.33333%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-11-of-12 {\n    width: 91.66667%;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-columns-12-of-12 {\n    width: 100%;\n  }\n}\n@media only screen and (max-width: 600px) {\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row > ul.mega-sub-menu > li.mega-menu-column {\n    width: 100%;\n    clear: both;\n  }\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-row .mega-menu-column > ul.mega-sub-menu > li.mega-menu-item {\n  padding: 15px 15px 15px 15px;\n  width: 100%;\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu {\n  z-index: 999;\n  border-radius: 0;\n  background: #f1f1f1;\n  border: 0;\n  padding: 0px 0px 0px 0px;\n  position: absolute;\n  width: 100%;\n  max-width: none;\n  left: 0;\n}\n@media only screen and (max-width: 600px) {\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu {\n    float: left;\n    position: static;\n    width: 100%;\n  }\n}\n@media only screen and (min-width: 601px) {\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-1-of-1 {\n    width: 100%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-1-of-2 {\n    width: 50%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-2-of-2 {\n    width: 100%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-1-of-3 {\n    width: 33.33333%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-2-of-3 {\n    width: 66.66667%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-3-of-3 {\n    width: 100%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-1-of-4 {\n    width: 25%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-2-of-4 {\n    width: 50%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-3-of-4 {\n    width: 75%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-4-of-4 {\n    width: 100%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-1-of-5 {\n    width: 20%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-2-of-5 {\n    width: 40%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-3-of-5 {\n    width: 60%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-4-of-5 {\n    width: 80%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-5-of-5 {\n    width: 100%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-1-of-6 {\n    width: 16.66667%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-2-of-6 {\n    width: 33.33333%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-3-of-6 {\n    width: 50%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-4-of-6 {\n    width: 66.66667%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-5-of-6 {\n    width: 83.33333%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-6-of-6 {\n    width: 100%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-1-of-7 {\n    width: 14.28571%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-2-of-7 {\n    width: 28.57143%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-3-of-7 {\n    width: 42.85714%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-4-of-7 {\n    width: 57.14286%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-5-of-7 {\n    width: 71.42857%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-6-of-7 {\n    width: 85.71429%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-7-of-7 {\n    width: 100%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-1-of-8 {\n    width: 12.5%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-2-of-8 {\n    width: 25%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-3-of-8 {\n    width: 37.5%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-4-of-8 {\n    width: 50%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-5-of-8 {\n    width: 62.5%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-6-of-8 {\n    width: 75%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-7-of-8 {\n    width: 87.5%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-8-of-8 {\n    width: 100%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-1-of-9 {\n    width: 11.11111%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-2-of-9 {\n    width: 22.22222%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-3-of-9 {\n    width: 33.33333%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-4-of-9 {\n    width: 44.44444%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-5-of-9 {\n    width: 55.55556%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-6-of-9 {\n    width: 66.66667%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-7-of-9 {\n    width: 77.77778%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-8-of-9 {\n    width: 88.88889%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-columns-9-of-9 {\n    width: 100%;\n  }\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu .mega-description-group .mega-menu-description {\n  margin: 5px 0;\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item ul.mega-sub-menu {\n  clear: both;\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item ul.mega-sub-menu li.mega-menu-item ul.mega-sub-menu {\n  margin-left: 10px;\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-column > ul.mega-sub-menu ul.mega-sub-menu ul.mega-sub-menu {\n  margin-left: 10px;\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-column > ul.mega-sub-menu > li.mega-menu-item {\n  color: #666;\n  font-family: inherit;\n  font-size: 14px;\n  display: block;\n  float: left;\n  clear: none;\n  padding: 15px 15px 15px 15px;\n  vertical-align: top;\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item.mega-menu-clear, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-column > ul.mega-sub-menu > li.mega-menu-item.mega-menu-clear {\n  clear: left;\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item h4.mega-block-title, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-column > ul.mega-sub-menu > li.mega-menu-item h4.mega-block-title {\n  color: #555;\n  font-family: inherit;\n  font-size: 16px;\n  text-transform: uppercase;\n  text-decoration: none;\n  font-weight: bold;\n  text-align: left;\n  margin: 0px 0px 0px 0px;\n  padding: 0px 0px 5px 0px;\n  vertical-align: top;\n  display: block;\n  visibility: inherit;\n  border: 0;\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item h4.mega-block-title:hover, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-column > ul.mega-sub-menu > li.mega-menu-item h4.mega-block-title:hover {\n  border-color: rgba(0, 0, 0, 0);\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item > a.mega-menu-link, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-column > ul.mega-sub-menu > li.mega-menu-item > a.mega-menu-link {\n  /* Mega Menu > Menu Themes > Mega Menus > Second Level Menu Items */\n  color: #555;\n  font-family: inherit;\n  font-size: 16px;\n  text-transform: uppercase;\n  text-decoration: none;\n  font-weight: bold;\n  text-align: left;\n  margin: 0px 0px 0px 0px;\n  padding: 0px 0px 0px 0px;\n  vertical-align: top;\n  display: block;\n  border: 0;\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item > a.mega-menu-link:hover, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-column > ul.mega-sub-menu > li.mega-menu-item > a.mega-menu-link:hover {\n  border-color: rgba(0, 0, 0, 0);\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item > a.mega-menu-link:hover, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-column > ul.mega-sub-menu > li.mega-menu-item > a.mega-menu-link:hover, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item > a.mega-menu-link:focus, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-column > ul.mega-sub-menu > li.mega-menu-item > a.mega-menu-link:focus {\n  /* Mega Menu > Menu Themes > Mega Menus > Second Level Menu Items (Hover) */\n  color: #555;\n  font-weight: bold;\n  text-decoration: none;\n  background: rgba(0, 0, 0, 0);\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item li.mega-menu-item > a.mega-menu-link, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-column > ul.mega-sub-menu > li.mega-menu-item li.mega-menu-item > a.mega-menu-link {\n  /* Mega Menu > Menu Themes > Mega Menus > Third Level Menu Items */\n  color: #666;\n  font-family: inherit;\n  font-size: 14px;\n  text-transform: none;\n  text-decoration: none;\n  font-weight: normal;\n  text-align: left;\n  margin: 0px 0px 0px 0px;\n  padding: 0px 0px 0px 0px;\n  vertical-align: top;\n  display: block;\n  border: 0;\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item li.mega-menu-item > a.mega-menu-link:hover, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-column > ul.mega-sub-menu > li.mega-menu-item li.mega-menu-item > a.mega-menu-link:hover {\n  border-color: rgba(0, 0, 0, 0);\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item li.mega-menu-item.mega-icon-left.mega-has-description.mega-has-icon > a.mega-menu-link, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-column > ul.mega-sub-menu > li.mega-menu-item li.mega-menu-item.mega-icon-left.mega-has-description.mega-has-icon > a.mega-menu-link {\n  display: flex;\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item li.mega-menu-item > a.mega-menu-link:hover, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-column > ul.mega-sub-menu > li.mega-menu-item li.mega-menu-item > a.mega-menu-link:hover, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item li.mega-menu-item > a.mega-menu-link:focus, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-column > ul.mega-sub-menu > li.mega-menu-item li.mega-menu-item > a.mega-menu-link:focus {\n  /* Mega Menu > Menu Themes > Mega Menus > Third Level Menu Items (Hover) */\n  color: #666;\n  font-weight: normal;\n  text-decoration: none;\n  background: rgba(0, 0, 0, 0);\n}\n@media only screen and (max-width: 600px) {\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu {\n    border: 0;\n    padding: 10px;\n    border-radius: 0;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item {\n    width: 100%;\n    clear: both;\n  }\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu.mega-no-headers > ul.mega-sub-menu > li.mega-menu-item > a.mega-menu-link, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu.mega-no-headers > ul.mega-sub-menu li.mega-menu-column > ul.mega-sub-menu > li.mega-menu-item > a.mega-menu-link {\n  color: #666;\n  font-family: inherit;\n  font-size: 14px;\n  text-transform: none;\n  text-decoration: none;\n  font-weight: normal;\n  margin: 0;\n  border: 0;\n  padding: 0px 0px 0px 0px;\n  vertical-align: top;\n  display: block;\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu.mega-no-headers > ul.mega-sub-menu > li.mega-menu-item > a.mega-menu-link:hover, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu.mega-no-headers > ul.mega-sub-menu > li.mega-menu-item > a.mega-menu-link:focus, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu.mega-no-headers > ul.mega-sub-menu li.mega-menu-column > ul.mega-sub-menu > li.mega-menu-item > a.mega-menu-link:hover, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu.mega-no-headers > ul.mega-sub-menu li.mega-menu-column > ul.mega-sub-menu > li.mega-menu-item > a.mega-menu-link:focus {\n  color: #666;\n  font-weight: normal;\n  text-decoration: none;\n  background: rgba(0, 0, 0, 0);\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-flyout ul.mega-sub-menu {\n  z-index: 999;\n  position: absolute;\n  width: 250px;\n  border: 0;\n  padding: 0px 0px 0px 0px;\n  background: #f1f1f1;\n  max-width: none;\n}\n@media only screen and (max-width: 600px) {\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-flyout ul.mega-sub-menu {\n    float: left;\n    position: static;\n    width: 100%;\n    padding: 0;\n    border: 0;\n  }\n}\n@media only screen and (max-width: 600px) {\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-flyout ul.mega-sub-menu li.mega-menu-item {\n    clear: both;\n  }\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-flyout ul.mega-sub-menu li.mega-menu-item a.mega-menu-link {\n  display: block;\n  background: #f1f1f1;\n  color: #666;\n  font-family: inherit;\n  font-size: 14px;\n  font-weight: normal;\n  padding: 0px 10px 0px 10px;\n  line-height: 35px;\n  text-decoration: none;\n  text-transform: none;\n  vertical-align: baseline;\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-flyout ul.mega-sub-menu li.mega-menu-item:first-child > a.mega-menu-link {\n  border-top-left-radius: 0px;\n  border-top-right-radius: 0px;\n}\n@media only screen and (max-width: 600px) {\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-flyout ul.mega-sub-menu li.mega-menu-item:first-child > a.mega-menu-link {\n    border-top-left-radius: 0;\n    border-top-right-radius: 0;\n  }\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-flyout ul.mega-sub-menu li.mega-menu-item:last-child > a.mega-menu-link {\n  border-bottom-right-radius: 0px;\n  border-bottom-left-radius: 0px;\n}\n@media only screen and (max-width: 600px) {\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-flyout ul.mega-sub-menu li.mega-menu-item:last-child > a.mega-menu-link {\n    border-bottom-right-radius: 0;\n    border-bottom-left-radius: 0;\n  }\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-flyout ul.mega-sub-menu li.mega-menu-item a.mega-menu-link:hover, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-flyout ul.mega-sub-menu li.mega-menu-item a.mega-menu-link:focus {\n  background: #ddd;\n  font-weight: normal;\n  text-decoration: none;\n  color: #666;\n}\n#mega-menu-wrap-top #mega-menu-top > li.mega-menu-flyout ul.mega-sub-menu li.mega-menu-item ul.mega-sub-menu {\n  position: absolute;\n  left: 100%;\n  top: 0;\n}\n@media only screen and (max-width: 600px) {\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-flyout ul.mega-sub-menu li.mega-menu-item ul.mega-sub-menu {\n    position: static;\n    left: 0;\n    width: 100%;\n  }\n  #mega-menu-wrap-top #mega-menu-top > li.mega-menu-flyout ul.mega-sub-menu li.mega-menu-item ul.mega-sub-menu a.mega-menu-link {\n    padding-left: 20px;\n  }\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-item-has-children > a.mega-menu-link > span.mega-indicator:after {\n  content: \'\\f140\';\n  font-family: dashicons;\n  font-weight: normal;\n  display: inline-block;\n  margin: 0 0 0 6px;\n  vertical-align: top;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n  transform: rotate(0);\n  color: inherit;\n  position: relative;\n  background: transparent;\n  height: auto;\n  width: auto;\n  right: auto;\n  line-height: inherit;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-item-has-children > a.mega-menu-link > span.mega-indicator {\n  display: inline;\n  height: auto;\n  width: auto;\n  background: transparent;\n  position: relative;\n  pointer-events: auto;\n  left: auto;\n  min-width: auto;\n  line-height: inherit;\n  color: inherit;\n  font-size: inherit;\n  padding: 0;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-item-has-children li.mega-menu-item-has-children > a.mega-menu-link > span.mega-indicator {\n  float: right;\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-item-has-children.mega-collapse-children.mega-toggle-on > a.mega-menu-link > span.mega-indicator:after {\n  content: \'\\f142\';\n}\n@media only screen and (max-width: 600px) {\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-item-has-children > a.mega-menu-link > span.mega-indicator {\n    float: right;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-item-has-children.mega-toggle-on > a.mega-menu-link > span.mega-indicator:after {\n    content: \'\\f142\';\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-item-has-children.mega-hide-sub-menu-on-mobile > a.mega-menu-link > span.mega-indicator {\n    display: none;\n  }\n}\n#mega-menu-wrap-top #mega-menu-top li.mega-menu-megamenu:not(.mega-menu-tabbed) li.mega-menu-item-has-children:not(.mega-collapse-children) > a.mega-menu-link > span.mega-indicator, #mega-menu-wrap-top #mega-menu-top li.mega-menu-item-has-children.mega-hide-arrow > a.mega-menu-link > span.mega-indicator {\n  display: none;\n}\n@media only screen and (min-width: 601px) {\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-flyout li.mega-menu-item a.mega-menu-link > span.mega-indicator:after {\n    content: \'\\f139\';\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-flyout.mega-align-bottom-right li.mega-menu-item a.mega-menu-link {\n    text-align: right;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-flyout.mega-align-bottom-right li.mega-menu-item a.mega-menu-link > span.mega-indicator {\n    float: left;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-flyout.mega-align-bottom-right li.mega-menu-item a.mega-menu-link > span.mega-indicator:after {\n    content: \'\\f141\';\n    margin: 0 6px 0 0;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-flyout.mega-align-bottom-right li.mega-menu-item a.mega-menu-link:before {\n    float: right;\n    margin: 0 0 0 6px;\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-flyout.mega-align-bottom-right ul.mega-sub-menu li.mega-menu-item ul.mega-sub-menu {\n    left: -100%;\n    top: 0;\n  }\n}\n@media only screen and (min-width: 601px) {\n  #mega-menu-wrap-top #mega-menu-top.mega-menu-vertical li.mega-align-bottom-right.mega-menu-item-has-children > a.mega-menu-link > span.mega-indicator, #mega-menu-wrap-top #mega-menu-top.mega-menu-vertical li.mega-align-bottom-right.mega-menu-flyout li.mega-menu-item-has-children > a.mega-menu-link > span.mega-indicator {\n    float: right;\n  }\n  #mega-menu-wrap-top #mega-menu-top.mega-menu-vertical li.mega-align-bottom-right.mega-menu-item-has-children > a.mega-menu-link > span.mega-indicator:after, #mega-menu-wrap-top #mega-menu-top.mega-menu-vertical li.mega-align-bottom-right.mega-menu-flyout li.mega-menu-item-has-children > a.mega-menu-link > span.mega-indicator:after {\n    content: \'\\f139\';\n    margin: 0;\n  }\n  #mega-menu-wrap-top #mega-menu-top.mega-menu-vertical li.mega-align-bottom-left.mega-menu-item-has-children > a.mega-menu-link > span.mega-indicator, #mega-menu-wrap-top #mega-menu-top.mega-menu-vertical li.mega-align-bottom-left.mega-menu-flyout li.mega-menu-item-has-children > a.mega-menu-link > span.mega-indicator {\n    float: left;\n  }\n  #mega-menu-wrap-top #mega-menu-top.mega-menu-vertical li.mega-align-bottom-left.mega-menu-item-has-children > a.mega-menu-link > span.mega-indicator:after, #mega-menu-wrap-top #mega-menu-top.mega-menu-vertical li.mega-align-bottom-left.mega-menu-flyout li.mega-menu-item-has-children > a.mega-menu-link > span.mega-indicator:after {\n    content: \'\\f141\';\n    margin: 0;\n  }\n  #mega-menu-wrap-top #mega-menu-top.mega-menu-accordion > li.mega-menu-item-has-children > a.mega-menu-link > span.mega-indicator {\n    float: right;\n  }\n  #mega-menu-wrap-top #mega-menu-top.mega-menu-accordion li.mega-menu-item-has-children.mega-toggle-on > a.mega-menu-link > span.mega-indicator:after {\n    content: \'\\f142\';\n  }\n  #mega-menu-wrap-top #mega-menu-top li.mega-menu-tabbed > ul.mega-sub-menu > li.mega-menu-item.mega-menu-item-has-children > a.mega-menu-link > span.mega-indicator:after {\n    content: \'\\f139\';\n  }\n}\n#mega-menu-wrap-top #mega-menu-top li[class^=\'mega-lang-item\'] > a.mega-menu-link > img {\n  display: inline;\n}\n#mega-menu-wrap-top #mega-menu-top a.mega-menu-link > img.wpml-ls-flag, #mega-menu-wrap-top #mega-menu-top a.mega-menu-link > img.iclflag {\n  display: inline;\n  margin-right: 8px;\n}\n@media only screen and (max-width: 600px) {\n  #mega-menu-wrap-top #mega-menu-top li.mega-hide-on-mobile, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-hide-on-mobile, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-column > ul.mega-sub-menu > li.mega-menu-item.mega-hide-on-mobile {\n    display: none;\n  }\n}\n@media only screen and (min-width: 601px) {\n  #mega-menu-wrap-top #mega-menu-top li.mega-hide-on-desktop, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-hide-on-desktop, #mega-menu-wrap-top #mega-menu-top > li.mega-menu-megamenu > ul.mega-sub-menu li.mega-menu-column > ul.mega-sub-menu > li.mega-menu-item.mega-hide-on-desktop {\n    display: none;\n  }\n}\n@media only screen and (max-width: 600px) {\n  #mega-menu-wrap-top:after {\n    content: \"\";\n    display: table;\n    clear: both;\n  }\n}\n#mega-menu-wrap-top .mega-menu-toggle {\n  display: none;\n  z-index: 1;\n  cursor: pointer;\n  background: #222;\n  border-radius: 2px 2px 2px 2px;\n  line-height: 40px;\n  height: 40px;\n  text-align: left;\n  -webkit-touch-callout: none;\n  -webkit-user-select: none;\n  -khtml-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  -webkit-tap-highlight-color: transparent;\n  outline: none;\n  white-space: nowrap;\n}\n#mega-menu-wrap-top .mega-menu-toggle img {\n  max-width: 100%;\n  padding: 0;\n}\n@media only screen and (max-width: 600px) {\n  #mega-menu-wrap-top .mega-menu-toggle {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: -webkit-flex;\n    display: flex;\n  }\n}\n#mega-menu-wrap-top .mega-menu-toggle .mega-toggle-blocks-left, #mega-menu-wrap-top .mega-menu-toggle .mega-toggle-blocks-center, #mega-menu-wrap-top .mega-menu-toggle .mega-toggle-blocks-right {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: -webkit-flex;\n  display: flex;\n  -ms-flex-preferred-size: 33.33%;\n  -webkit-flex-basis: 33.33%;\n  flex-basis: 33.33%;\n}\n#mega-menu-wrap-top .mega-menu-toggle .mega-toggle-blocks-left {\n  -webkit-box-flex: 1;\n  -ms-flex: 1;\n  -webkit-flex: 1;\n  flex: 1;\n  -webkit-box-pack: start;\n  -ms-flex-pack: start;\n  -webkit-justify-content: flex-start;\n  justify-content: flex-start;\n}\n#mega-menu-wrap-top .mega-menu-toggle .mega-toggle-blocks-left .mega-toggle-block {\n  margin-left: 6px;\n}\n#mega-menu-wrap-top .mega-menu-toggle .mega-toggle-blocks-center {\n  -webkit-box-pack: center;\n  -ms-flex-pack: center;\n  -webkit-justify-content: center;\n  justify-content: center;\n}\n#mega-menu-wrap-top .mega-menu-toggle .mega-toggle-blocks-center .mega-toggle-block {\n  margin-left: 3px;\n  margin-right: 3px;\n}\n#mega-menu-wrap-top .mega-menu-toggle .mega-toggle-blocks-right {\n  -webkit-box-flex: 1;\n  -ms-flex: 1;\n  -webkit-flex: 1;\n  flex: 1;\n  -webkit-box-pack: end;\n  -ms-flex-pack: end;\n  -webkit-justify-content: flex-end;\n  justify-content: flex-end;\n}\n#mega-menu-wrap-top .mega-menu-toggle .mega-toggle-blocks-right .mega-toggle-block {\n  margin-right: 6px;\n}\n#mega-menu-wrap-top .mega-menu-toggle .mega-toggle-block {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: -webkit-flex;\n  display: flex;\n  height: 100%;\n  outline: 0;\n  -webkit-align-self: center;\n  -ms-flex-item-align: center;\n  align-self: center;\n  -ms-flex-negative: 0;\n  -webkit-flex-shrink: 0;\n  flex-shrink: 0;\n}\n@media only screen and (max-width: 600px) {\n  #mega-menu-wrap-top .mega-menu-toggle + #mega-menu-top {\n    background: #222;\n    padding: 0px 0px 0px 0px;\n    display: none;\n  }\n  #mega-menu-wrap-top .mega-menu-toggle.mega-menu-open + #mega-menu-top {\n    display: block;\n  }\n}\n#mega-menu-wrap-top .mega-menu-toggle {\n  /** Push menu onto new line **/\n}\n#mega-menu-wrap-top .mega-menu-toggle .mega-toggle-block-1 {\n  cursor: pointer;\n}\n#mega-menu-wrap-top .mega-menu-toggle .mega-toggle-block-1:after {\n  content: \'\\f333\';\n  font-family: \'dashicons\';\n  font-size: 24px;\n  color: #ddd;\n  margin: 0 0 0 5px;\n}\n#mega-menu-wrap-top .mega-menu-toggle .mega-toggle-block-1 .mega-toggle-label {\n  color: #ddd;\n  font-size: 14px;\n}\n#mega-menu-wrap-top .mega-menu-toggle .mega-toggle-block-1 .mega-toggle-label .mega-toggle-label-open {\n  display: none;\n}\n#mega-menu-wrap-top .mega-menu-toggle .mega-toggle-block-1 .mega-toggle-label .mega-toggle-label-closed {\n  display: inline;\n}\n#mega-menu-wrap-top .mega-menu-toggle.mega-menu-open .mega-toggle-block-1:after {\n  content: \'\\f153\';\n}\n#mega-menu-wrap-top .mega-menu-toggle.mega-menu-open .mega-toggle-block-1 .mega-toggle-label-open {\n  display: inline;\n}\n#mega-menu-wrap-top .mega-menu-toggle.mega-menu-open .mega-toggle-block-1 .mega-toggle-label-closed {\n  display: none;\n}\n#mega-menu-wrap-top {\n  clear: both;\n}\n', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(758, '_transient_timeout__quadmenu_first_rating', '1578963459', 'no'),
(759, '_transient__quadmenu_first_rating', '1', 'no'),
(760, 'redux_version_upgraded_from', '3.6.15', 'yes'),
(761, 'widget_quadmenu_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(762, 'quadmenu_schoolsportact', 'a:113:{s:8:\"last_tab\";s:0:\"\";s:8:\"viewport\";s:1:\"0\";s:6:\"styles\";s:1:\"0\";s:16:\"styles_normalize\";s:1:\"1\";s:14:\"styles_widgets\";s:1:\"1\";s:17:\"styles_pscrollbar\";s:1:\"0\";s:18:\"styles_owlcarousel\";s:1:\"0\";s:12:\"styles_icons\";s:9:\"dashicons\";s:15:\"top_integration\";s:1:\"1\";s:10:\"top_unwrap\";s:1:\"0\";s:9:\"top_theme\";s:13:\"default_theme\";s:6:\"gutter\";s:2:\"30\";s:15:\"screen_sm_width\";s:3:\"768\";s:15:\"screen_md_width\";s:3:\"992\";s:15:\"screen_lg_width\";s:4:\"1200\";s:25:\"default_theme_theme_title\";s:13:\"Default Theme\";s:20:\"default_theme_layout\";s:5:\"embed\";s:36:\"default_theme_layout_offcanvas_float\";s:5:\"right\";s:26:\"default_theme_layout_align\";s:5:\"right\";s:31:\"default_theme_layout_breakpoint\";s:3:\"768\";s:26:\"default_theme_layout_width\";s:1:\"0\";s:32:\"default_theme_layout_width_inner\";s:1:\"0\";s:41:\"default_theme_layout_width_inner_selector\";s:0:\"\";s:29:\"default_theme_layout_lazyload\";s:0:\"\";s:28:\"default_theme_layout_current\";s:0:\"\";s:28:\"default_theme_layout_divider\";s:4:\"hide\";s:26:\"default_theme_layout_caret\";s:4:\"show\";s:28:\"default_theme_layout_classes\";s:0:\"\";s:28:\"default_theme_layout_trigger\";s:11:\"hoverintent\";s:39:\"default_theme_layout_dropdown_maxheight\";s:1:\"1\";s:31:\"default_theme_navbar_background\";s:5:\"color\";s:37:\"default_theme_navbar_background_color\";s:7:\"#333333\";s:34:\"default_theme_navbar_background_to\";s:7:\"#000000\";s:35:\"default_theme_navbar_background_deg\";s:2:\"17\";s:25:\"default_theme_navbar_text\";s:7:\"#aaaaaa\";s:27:\"default_theme_navbar_height\";s:2:\"60\";s:26:\"default_theme_navbar_width\";s:3:\"260\";s:30:\"default_theme_navbar_logo_link\";s:33:\"http://localhost:8888/ssact/\";s:25:\"default_theme_navbar_logo\";a:9:{s:3:\"url\";s:92:\"http://localhost:8888/ssact/wp-content/plugins/quadmenu/assets/frontend/images/logo.png\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";s:5:\"title\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:32:\"default_theme_navbar_logo_height\";s:2:\"25\";s:28:\"default_theme_navbar_logo_bg\";s:11:\"transparent\";s:32:\"default_theme_navbar_link_margin\";a:6:{s:10:\"border-top\";s:3:\"0px\";s:12:\"border-right\";s:3:\"0px\";s:13:\"border-bottom\";s:3:\"0px\";s:11:\"border-left\";s:3:\"0px\";s:12:\"border-style\";s:0:\"\";s:12:\"border-color\";s:7:\"#000000\";}s:32:\"default_theme_navbar_link_radius\";a:6:{s:10:\"border-top\";s:3:\"0px\";s:12:\"border-right\";s:3:\"0px\";s:13:\"border-bottom\";s:3:\"0px\";s:11:\"border-left\";s:3:\"0px\";s:12:\"border-style\";s:0:\"\";s:12:\"border-color\";s:0:\"\";}s:25:\"default_theme_navbar_link\";s:7:\"#f1f1f1\";s:28:\"default_theme_navbar_link_bg\";s:11:\"transparent\";s:31:\"default_theme_navbar_link_hover\";s:7:\"#ffffff\";s:34:\"default_theme_navbar_link_bg_hover\";s:7:\"#111111\";s:28:\"default_theme_navbar_divider\";s:21:\"rgba(255,255,255,0.5)\";s:38:\"default_theme_navbar_link_hover_effect\";s:21:\"rgba(255,255,255,0.3)\";s:30:\"default_theme_navbar_link_icon\";s:7:\"#eeeeee\";s:36:\"default_theme_navbar_link_icon_hover\";s:7:\"#ffffff\";s:34:\"default_theme_navbar_link_subtitle\";s:7:\"#eeeeee\";s:40:\"default_theme_navbar_link_subtitle_hover\";s:7:\"#ffffff\";s:32:\"default_theme_navbar_badge_color\";s:7:\"#ffffff\";s:26:\"default_theme_navbar_badge\";s:7:\"#fb88dd\";s:27:\"default_theme_navbar_button\";s:7:\"#ffffff\";s:38:\"default_theme_navbar_button_background\";s:7:\"#fb88dd\";s:33:\"default_theme_navbar_button_hover\";s:7:\"#383838\";s:44:\"default_theme_navbar_button_hover_background\";s:7:\"#eeeeee\";s:34:\"default_theme_navbar_button_radius\";a:6:{s:10:\"border-top\";s:3:\"2px\";s:12:\"border-right\";s:3:\"2px\";s:13:\"border-bottom\";s:3:\"2px\";s:11:\"border-left\";s:3:\"2px\";s:12:\"border-style\";s:0:\"\";s:12:\"border-color\";s:7:\"#000000\";}s:30:\"default_theme_navbar_scrollbar\";s:7:\"#fb88dd\";s:35:\"default_theme_navbar_scrollbar_rail\";s:7:\"#ffffff\";s:27:\"default_theme_layout_sticky\";s:1:\"0\";s:34:\"default_theme_layout_sticky_offset\";s:1:\"0\";s:31:\"default_theme_sticky_background\";s:16:\"rgba(0,0,0,0.95)\";s:27:\"default_theme_sticky_height\";s:2:\"60\";s:32:\"default_theme_sticky_logo_height\";s:2:\"25\";s:27:\"default_theme_mobile_shadow\";s:4:\"show\";s:34:\"default_theme_navbar_mobile_border\";s:21:\"rgba(255,255,255,0.1)\";s:32:\"default_theme_navbar_toggle_open\";s:7:\"#ffffff\";s:33:\"default_theme_navbar_toggle_close\";s:7:\"#fb88dd\";s:33:\"default_theme_mobile_link_padding\";a:6:{s:10:\"border-top\";s:4:\"15px\";s:12:\"border-right\";s:4:\"30px\";s:13:\"border-bottom\";s:4:\"15px\";s:11:\"border-left\";s:4:\"30px\";s:12:\"border-style\";s:0:\"\";s:12:\"border-color\";s:0:\"\";}s:32:\"default_theme_mobile_link_border\";a:6:{s:10:\"border-top\";s:3:\"0px\";s:12:\"border-right\";s:0:\"\";s:13:\"border-bottom\";s:0:\"\";s:11:\"border-left\";s:0:\"\";s:12:\"border-style\";s:4:\"none\";s:12:\"border-color\";s:11:\"transparent\";}s:29:\"default_theme_dropdown_shadow\";s:4:\"show\";s:29:\"default_theme_dropdown_margin\";s:1:\"0\";s:29:\"default_theme_dropdown_radius\";a:6:{s:10:\"border-top\";s:3:\"0px\";s:12:\"border-right\";s:3:\"0px\";s:13:\"border-bottom\";s:3:\"0px\";s:11:\"border-left\";s:3:\"0px\";s:12:\"border-style\";s:0:\"\";s:12:\"border-color\";s:7:\"#000000\";}s:29:\"default_theme_dropdown_border\";a:6:{s:10:\"border-top\";s:3:\"0px\";s:12:\"border-right\";s:3:\"0px\";s:13:\"border-bottom\";s:3:\"0px\";s:11:\"border-left\";s:3:\"0px\";s:12:\"border-style\";s:0:\"\";s:12:\"border-color\";s:7:\"#000000\";}s:33:\"default_theme_dropdown_background\";s:7:\"#ffffff\";s:27:\"default_theme_dropdown_link\";s:7:\"#444444\";s:33:\"default_theme_dropdown_link_hover\";s:7:\"#333333\";s:36:\"default_theme_dropdown_link_bg_hover\";s:7:\"#f4f4f4\";s:34:\"default_theme_dropdown_link_border\";a:6:{s:10:\"border-top\";s:3:\"1px\";s:12:\"border-right\";s:0:\"\";s:13:\"border-bottom\";s:0:\"\";s:11:\"border-left\";s:0:\"\";s:12:\"border-style\";s:5:\"solid\";s:12:\"border-color\";s:7:\"#f4f4f4\";}s:28:\"default_theme_dropdown_title\";s:7:\"#444444\";s:35:\"default_theme_dropdown_title_border\";a:6:{s:10:\"border-top\";s:3:\"1px\";s:12:\"border-right\";s:0:\"\";s:13:\"border-bottom\";s:0:\"\";s:11:\"border-left\";s:0:\"\";s:12:\"border-style\";s:5:\"solid\";s:12:\"border-color\";s:7:\"#fb88dd\";}s:32:\"default_theme_dropdown_link_icon\";s:7:\"#fb88dd\";s:38:\"default_theme_dropdown_link_icon_hover\";s:7:\"#a9a9a9\";s:36:\"default_theme_dropdown_link_subtitle\";s:7:\"#a0a0a0\";s:42:\"default_theme_dropdown_link_subtitle_hover\";s:7:\"#cccccc\";s:29:\"default_theme_dropdown_button\";s:7:\"#ffffff\";s:32:\"default_theme_dropdown_button_bg\";s:7:\"#fb88dd\";s:35:\"default_theme_dropdown_button_hover\";s:7:\"#ffffff\";s:38:\"default_theme_dropdown_button_bg_hover\";s:7:\"#000000\";s:29:\"default_theme_dropdown_tab_bg\";s:16:\"rgba(0,0,0,0.05)\";s:35:\"default_theme_dropdown_tab_bg_hover\";s:15:\"rgba(0,0,0,0.1)\";s:32:\"default_theme_dropdown_scrollbar\";s:7:\"#fb88dd\";s:37:\"default_theme_dropdown_scrollbar_rail\";s:7:\"#ffffff\";s:18:\"default_theme_font\";a:8:{s:11:\"font-family\";s:27:\"Verdana, Geneva, sans-serif\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:3:\"400\";s:10:\"font-style\";s:6:\"normal\";s:7:\"subsets\";s:0:\"\";s:9:\"font-size\";s:2:\"11\";s:14:\"letter-spacing\";s:7:\"inherit\";}s:25:\"default_theme_navbar_font\";a:8:{s:11:\"font-family\";s:27:\"Verdana, Geneva, sans-serif\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:3:\"400\";s:10:\"font-style\";s:6:\"normal\";s:7:\"subsets\";s:0:\"\";s:9:\"font-size\";s:2:\"11\";s:14:\"letter-spacing\";s:7:\"inherit\";}s:35:\"default_theme_navbar_link_transform\";s:9:\"uppercase\";s:27:\"default_theme_dropdown_font\";a:8:{s:11:\"font-family\";s:27:\"Verdana, Geneva, sans-serif\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:3:\"400\";s:10:\"font-style\";s:6:\"normal\";s:7:\"subsets\";s:0:\"\";s:9:\"font-size\";s:2:\"11\";s:14:\"letter-spacing\";s:7:\"inherit\";}s:37:\"default_theme_dropdown_link_transform\";s:4:\"none\";s:33:\"default_theme_layout_hover_effect\";s:21:\"quadmenu-hover-ripple\";s:35:\"default_theme_navbar_animation_text\";a:3:{s:7:\"options\";s:0:\"\";s:6:\"action\";s:5:\"hover\";s:5:\"speed\";s:6:\"t_1000\";}s:39:\"default_theme_navbar_animation_subtitle\";a:3:{s:7:\"options\";s:0:\"\";s:6:\"action\";s:5:\"hover\";s:5:\"speed\";s:6:\"t_1000\";}s:35:\"default_theme_navbar_animation_icon\";a:3:{s:7:\"options\";s:0:\"\";s:6:\"action\";s:5:\"hover\";s:5:\"speed\";s:6:\"t_1000\";}s:36:\"default_theme_navbar_animation_badge\";a:3:{s:7:\"options\";s:14:\"quadmenu_swing\";s:6:\"action\";s:5:\"hover\";s:5:\"speed\";s:6:\"t_1000\";}s:35:\"default_theme_navbar_animation_cart\";a:2:{s:7:\"options\";s:15:\"quadmenu_bounce\";s:5:\"speed\";s:5:\"t_500\";}s:30:\"default_theme_layout_animation\";a:2:{s:7:\"options\";s:12:\"quadmenu_btt\";s:5:\"speed\";s:5:\"t_300\";}s:37:\"default_theme_dropdown_animation_text\";a:3:{s:7:\"options\";s:0:\"\";s:6:\"action\";s:5:\"hover\";s:5:\"speed\";s:6:\"t_1000\";}s:41:\"default_theme_dropdown_animation_subtitle\";a:3:{s:7:\"options\";s:0:\"\";s:6:\"action\";s:5:\"hover\";s:5:\"speed\";s:6:\"t_1000\";}s:37:\"default_theme_dropdown_animation_icon\";a:3:{s:7:\"options\";s:0:\"\";s:6:\"action\";s:5:\"hover\";s:5:\"speed\";s:6:\"t_1000\";}s:38:\"default_theme_dropdown_animation_badge\";a:3:{s:7:\"options\";s:14:\"quadmenu_swing\";s:6:\"action\";s:4:\"loop\";s:5:\"speed\";s:6:\"t_1000\";}s:3:\"css\";s:0:\"\";}', 'yes'),
(763, 'quadmenu_schoolsportact-transients', 'a:2:{s:14:\"changed_values\";a:1:{s:9:\"top_theme\";s:0:\"\";}s:9:\"last_save\";i:1576371761;}', 'yes'),
(764, 'quadmenu_schoolsportact_locations', 'a:2:{s:3:\"top\";a:1:{s:4:\"name\";s:8:\"Top menu\";}s:12:\"quadmenu_dev\";a:1:{s:4:\"name\";s:12:\"QuadMenu Dev\";}}', 'yes'),
(774, 'options_banners_1_image', '231', 'no'),
(775, '_options_banners_1_image', 'field_5df13e941bca6', 'no'),
(776, 'options_banners_1_content', '2', 'no'),
(777, '_options_banners_1_content', 'field_5df13e9d1bca7', 'no'),
(784, 'widget_search_filter_register_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(785, 'search-filter-cache', 'a:13:{s:6:\"status\";s:8:\"finished\";s:11:\"last_update\";i:1576387474;s:7:\"restart\";b:0;s:10:\"cache_list\";a:0:{}s:18:\"current_post_index\";i:22;s:16:\"progress_percent\";d:100;s:6:\"locked\";b:0;s:11:\"error_count\";i:0;s:9:\"rc_status\";s:15:\"connect_success\";s:10:\"process_id\";i:0;s:10:\"run_method\";s:9:\"automatic\";s:12:\"caching_data\";a:2:{s:10:\"post_types\";a:1:{i:0;s:10:\"attachment\";}s:9:\"meta_keys\";a:3:{i:0;s:4:\"type\";i:1;s:8:\"category\";i:2;s:6:\"sports\";}}s:16:\"total_post_index\";i:22;}', 'no'),
(786, 'search_filter_transient_keys', 'a:0:{}', 'yes'),
(827, 'school_sport_register', '272', 'yes'),
(834, 'options_title', '‘Participation, Potential, Pathways’', 'no'),
(835, '_options_title', 'field_5df13f81d99bc', 'no'),
(836, 'options_content', '<p style=\"text-align: center;\"><span class=\"font-weight-bold\">School Sport ACT (SSACT)</span> is the peak body for School Sport delivery in the ACT.\r\nSSACT actively promotes school sport for all ACT students through the support of regional, state and national representative opportunities and pathways.</p>\r\n<p style=\"text-align: center;\">Our motto of <span class=\"font-weight-bold\">‘Participation, Potential, Pathways’</span> perfectly encapsulates the role SSACT plays in the development and progression of ACT students to achieve their sporting goals.</p>', 'no'),
(837, '_options_content', 'field_5df13f89d99bd', 'no'),
(838, 'options_registration', 'Representative sports is an excellent pathway to becoming a professiona athlete. Being part of a team that represents your state helps develops a child’s confidence, temwork and leadership skills.\r\n\r\nTake your first step and look up an upcoming trial or register your child now:', 'no'),
(839, '_options_registration', 'field_5df58cf720c24', 'no'),
(840, 'options_registration_image', '296', 'no'),
(841, '_options_registration_image', 'field_5df58d70a0b8a', 'no'),
(842, 'options_help_organise', 'We rely on an extensive network of volunteer coaches, team managers and officials to help select, organise and teach the children.\r\n\r\nEach year in January we take an intake of new volunteers. Register your interest today or contact us to find out more.', 'no'),
(843, '_options_help_organise', 'field_5df58d82a0b8b', 'no'),
(844, 'options_help_organise_image', '297', 'no'),
(845, '_options_help_organise_image', 'field_5df58da8a0b8c', 'no'),
(846, 'options_banners_0_content_0_title', 'Supporting the sporting particiaption', 'no'),
(847, '_options_banners_0_content_0_title', 'field_5df6d6d8eb7db', 'no'),
(848, 'options_banners_0_content_1_title', 'for over 50.000 students across the ACT', 'no'),
(849, '_options_banners_0_content_1_title', 'field_5df6d6d8eb7db', 'no'),
(850, 'options_banners_1_content_0_title', 'Supporting the sporting particiaption', 'no'),
(851, '_options_banners_1_content_0_title', 'field_5df6d6d8eb7db', 'no'),
(852, 'options_banners_1_content_1_title', 'for over 50.000 students across the ACT', 'no'),
(853, '_options_banners_1_content_1_title', 'field_5df6d6d8eb7db', 'no'),
(957, 'options_calendar', '322', 'no'),
(958, '_options_calendar', 'field_5dfbc17b73707', 'no'),
(971, '_site_transient_timeout_browser_237aa6249591b6a7ad6962bc73492c77', '1577590119', 'no'),
(972, '_site_transient_browser_237aa6249591b6a7ad6962bc73492c77', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"79.0.3945.88\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(975, '_site_transient_timeout_php_check_8706d9e16ec2aa6542c624d1e3c9facd', '1577590119', 'no'),
(976, '_site_transient_php_check_8706d9e16ec2aa6542c624d1e3c9facd', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:0;s:13:\"is_acceptable\";b:1;}', 'no'),
(1018, '_site_transient_timeout_browser_a9cfc38c8c27d8aade7b19701097beda', '1577750932', 'no'),
(1019, '_site_transient_browser_a9cfc38c8c27d8aade7b19701097beda', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"79.0.3945.88\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(1046, '_site_transient_timeout_browser_5b21a4f36fd10e23154b80cac949aedf', '1577832983', 'no'),
(1047, '_site_transient_browser_5b21a4f36fd10e23154b80cac949aedf', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"79.0.3945.88\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(1067, '_transient_timeout_feed_9bbd59226dc36b9b26cd43f15694c5c3', '1577366004', 'no'),
(1070, '_transient_timeout_feed_d117b5738fbd35bd8c0391cda1f2b5d9', '1577366005', 'no'),
(1124, '_site_transient_timeout_community-events-119daafa6330df63c759ef5206166ab7', '1577585134', 'no'),
(1125, '_site_transient_community-events-119daafa6330df63c759ef5206166ab7', 'a:3:{s:9:\"sandboxed\";b:0;s:8:\"location\";a:1:{s:2:\"ip\";s:11:\"190.22.39.0\";}s:6:\"events\";a:1:{i:0;a:8:{s:4:\"type\";s:8:\"wordcamp\";s:5:\"title\";s:13:\"WordCamp Asia\";s:3:\"url\";s:31:\"https://2020.asia.wordcamp.org/\";s:6:\"meetup\";s:0:\"\";s:10:\"meetup_url\";s:0:\"\";s:4:\"date\";s:19:\"2020-02-21 00:00:00\";s:8:\"end_date\";s:19:\"2020-02-23 00:00:00\";s:8:\"location\";a:4:{s:8:\"location\";s:17:\"Bangkok, Thailand\";s:7:\"country\";s:2:\"TH\";s:8:\"latitude\";d:13.7248933999999991328877513296902179718017578125;s:9:\"longitude\";d:100.49268299999999953797669149935245513916015625;}}}}', 'no'),
(1127, '_transient_timeout_acf_plugin_updates', '1577729138', 'no'),
(1128, '_transient_acf_plugin_updates', 'a:3:{s:7:\"plugins\";a:1:{s:41:\"advanced-custom-fields-pro-master/acf.php\";a:8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:41:\"advanced-custom-fields-pro-master/acf.php\";s:11:\"new_version\";s:5:\"5.8.7\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:5:\"5.3.0\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;}', 'no'),
(1135, '_transient_timeout_plugin_slugs', '1577654055', 'no'),
(1136, '_transient_plugin_slugs', 'a:11:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:29:\"acf-repeater/acf-repeater.php\";i:2;s:41:\"advanced-custom-fields-pro-master/acf.php\";i:3;s:19:\"akismet/akismet.php\";i:4;s:9:\"hello.php\";i:5;s:63:\"limit-login-attempts-reloaded/limit-login-attempts-reloaded.php\";i:6;s:19:\"members/members.php\";i:7;s:57:\"reveal-ids-for-wp-admin-25/reveal-ids-for-wp-admin-25.php\";i:8;s:21:\"schoolsport/index.php\";i:9;s:39:\"search-filter-pro/search-filter-pro.php\";i:10;s:37:\"wp-sendgrid-smtp/wp-sendgrid-smtp.php\";}', 'no'),
(1137, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1577578416', 'no'),
(1138, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'O:8:\"stdClass\":100:{s:6:\"widget\";a:3:{s:4:\"name\";s:6:\"widget\";s:4:\"slug\";s:6:\"widget\";s:5:\"count\";i:4654;}s:11:\"woocommerce\";a:3:{s:4:\"name\";s:11:\"woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:5:\"count\";i:3826;}s:4:\"post\";a:3:{s:4:\"name\";s:4:\"post\";s:4:\"slug\";s:4:\"post\";s:5:\"count\";i:2653;}s:5:\"admin\";a:3:{s:4:\"name\";s:5:\"admin\";s:4:\"slug\";s:5:\"admin\";s:5:\"count\";i:2531;}s:5:\"posts\";a:3:{s:4:\"name\";s:5:\"posts\";s:4:\"slug\";s:5:\"posts\";s:5:\"count\";i:1951;}s:9:\"shortcode\";a:3:{s:4:\"name\";s:9:\"shortcode\";s:4:\"slug\";s:9:\"shortcode\";s:5:\"count\";i:1782;}s:8:\"comments\";a:3:{s:4:\"name\";s:8:\"comments\";s:4:\"slug\";s:8:\"comments\";s:5:\"count\";i:1764;}s:7:\"twitter\";a:3:{s:4:\"name\";s:7:\"twitter\";s:4:\"slug\";s:7:\"twitter\";s:5:\"count\";i:1477;}s:6:\"images\";a:3:{s:4:\"name\";s:6:\"images\";s:4:\"slug\";s:6:\"images\";s:5:\"count\";i:1462;}s:6:\"google\";a:3:{s:4:\"name\";s:6:\"google\";s:4:\"slug\";s:6:\"google\";s:5:\"count\";i:1453;}s:8:\"facebook\";a:3:{s:4:\"name\";s:8:\"facebook\";s:4:\"slug\";s:8:\"facebook\";s:5:\"count\";i:1444;}s:5:\"image\";a:3:{s:4:\"name\";s:5:\"image\";s:4:\"slug\";s:5:\"image\";s:5:\"count\";i:1400;}s:3:\"seo\";a:3:{s:4:\"name\";s:3:\"seo\";s:4:\"slug\";s:3:\"seo\";s:5:\"count\";i:1374;}s:7:\"sidebar\";a:3:{s:4:\"name\";s:7:\"sidebar\";s:4:\"slug\";s:7:\"sidebar\";s:5:\"count\";i:1296;}s:7:\"gallery\";a:3:{s:4:\"name\";s:7:\"gallery\";s:4:\"slug\";s:7:\"gallery\";s:5:\"count\";i:1165;}s:5:\"email\";a:3:{s:4:\"name\";s:5:\"email\";s:4:\"slug\";s:5:\"email\";s:5:\"count\";i:1151;}s:4:\"page\";a:3:{s:4:\"name\";s:4:\"page\";s:4:\"slug\";s:4:\"page\";s:5:\"count\";i:1114;}s:6:\"social\";a:3:{s:4:\"name\";s:6:\"social\";s:4:\"slug\";s:6:\"social\";s:5:\"count\";i:1079;}s:9:\"ecommerce\";a:3:{s:4:\"name\";s:9:\"ecommerce\";s:4:\"slug\";s:9:\"ecommerce\";s:5:\"count\";i:1072;}s:5:\"login\";a:3:{s:4:\"name\";s:5:\"login\";s:4:\"slug\";s:5:\"login\";s:5:\"count\";i:974;}s:5:\"links\";a:3:{s:4:\"name\";s:5:\"links\";s:4:\"slug\";s:5:\"links\";s:5:\"count\";i:863;}s:7:\"widgets\";a:3:{s:4:\"name\";s:7:\"widgets\";s:4:\"slug\";s:7:\"widgets\";s:5:\"count\";i:854;}s:5:\"video\";a:3:{s:4:\"name\";s:5:\"video\";s:4:\"slug\";s:5:\"video\";s:5:\"count\";i:853;}s:8:\"security\";a:3:{s:4:\"name\";s:8:\"security\";s:4:\"slug\";s:8:\"security\";s:5:\"count\";i:832;}s:4:\"spam\";a:3:{s:4:\"name\";s:4:\"spam\";s:4:\"slug\";s:4:\"spam\";s:5:\"count\";i:770;}s:7:\"content\";a:3:{s:4:\"name\";s:7:\"content\";s:4:\"slug\";s:7:\"content\";s:5:\"count\";i:757;}s:6:\"slider\";a:3:{s:4:\"name\";s:6:\"slider\";s:4:\"slug\";s:6:\"slider\";s:5:\"count\";i:743;}s:10:\"e-commerce\";a:3:{s:4:\"name\";s:10:\"e-commerce\";s:4:\"slug\";s:10:\"e-commerce\";s:5:\"count\";i:743;}s:10:\"buddypress\";a:3:{s:4:\"name\";s:10:\"buddypress\";s:4:\"slug\";s:10:\"buddypress\";s:5:\"count\";i:739;}s:9:\"analytics\";a:3:{s:4:\"name\";s:9:\"analytics\";s:4:\"slug\";s:9:\"analytics\";s:5:\"count\";i:726;}s:3:\"rss\";a:3:{s:4:\"name\";s:3:\"rss\";s:4:\"slug\";s:3:\"rss\";s:5:\"count\";i:709;}s:5:\"media\";a:3:{s:4:\"name\";s:5:\"media\";s:4:\"slug\";s:5:\"media\";s:5:\"count\";i:694;}s:5:\"pages\";a:3:{s:4:\"name\";s:5:\"pages\";s:4:\"slug\";s:5:\"pages\";s:5:\"count\";i:692;}s:4:\"form\";a:3:{s:4:\"name\";s:4:\"form\";s:4:\"slug\";s:4:\"form\";s:5:\"count\";i:685;}s:6:\"search\";a:3:{s:4:\"name\";s:6:\"search\";s:4:\"slug\";s:6:\"search\";s:5:\"count\";i:671;}s:6:\"jquery\";a:3:{s:4:\"name\";s:6:\"jquery\";s:4:\"slug\";s:6:\"jquery\";s:5:\"count\";i:657;}s:4:\"feed\";a:3:{s:4:\"name\";s:4:\"feed\";s:4:\"slug\";s:4:\"feed\";s:5:\"count\";i:640;}s:4:\"menu\";a:3:{s:4:\"name\";s:4:\"menu\";s:4:\"slug\";s:4:\"menu\";s:5:\"count\";i:638;}s:8:\"category\";a:3:{s:4:\"name\";s:8:\"category\";s:4:\"slug\";s:8:\"category\";s:5:\"count\";i:631;}s:4:\"ajax\";a:3:{s:4:\"name\";s:4:\"ajax\";s:4:\"slug\";s:4:\"ajax\";s:5:\"count\";i:622;}s:6:\"editor\";a:3:{s:4:\"name\";s:6:\"editor\";s:4:\"slug\";s:6:\"editor\";s:5:\"count\";i:620;}s:5:\"embed\";a:3:{s:4:\"name\";s:5:\"embed\";s:4:\"slug\";s:5:\"embed\";s:5:\"count\";i:607;}s:3:\"css\";a:3:{s:4:\"name\";s:3:\"css\";s:4:\"slug\";s:3:\"css\";s:5:\"count\";i:580;}s:10:\"javascript\";a:3:{s:4:\"name\";s:10:\"javascript\";s:4:\"slug\";s:10:\"javascript\";s:5:\"count\";i:577;}s:4:\"link\";a:3:{s:4:\"name\";s:4:\"link\";s:4:\"slug\";s:4:\"link\";s:5:\"count\";i:569;}s:7:\"youtube\";a:3:{s:4:\"name\";s:7:\"youtube\";s:4:\"slug\";s:7:\"youtube\";s:5:\"count\";i:567;}s:12:\"contact-form\";a:3:{s:4:\"name\";s:12:\"contact form\";s:4:\"slug\";s:12:\"contact-form\";s:5:\"count\";i:564;}s:5:\"share\";a:3:{s:4:\"name\";s:5:\"share\";s:4:\"slug\";s:5:\"share\";s:5:\"count\";i:548;}s:5:\"theme\";a:3:{s:4:\"name\";s:5:\"theme\";s:4:\"slug\";s:5:\"theme\";s:5:\"count\";i:541;}s:7:\"comment\";a:3:{s:4:\"name\";s:7:\"comment\";s:4:\"slug\";s:7:\"comment\";s:5:\"count\";i:538;}s:7:\"payment\";a:3:{s:4:\"name\";s:7:\"payment\";s:4:\"slug\";s:7:\"payment\";s:5:\"count\";i:537;}s:10:\"responsive\";a:3:{s:4:\"name\";s:10:\"responsive\";s:4:\"slug\";s:10:\"responsive\";s:5:\"count\";i:532;}s:9:\"dashboard\";a:3:{s:4:\"name\";s:9:\"dashboard\";s:4:\"slug\";s:9:\"dashboard\";s:5:\"count\";i:528;}s:6:\"custom\";a:3:{s:4:\"name\";s:6:\"custom\";s:4:\"slug\";s:6:\"custom\";s:5:\"count\";i:524;}s:9:\"affiliate\";a:3:{s:4:\"name\";s:9:\"affiliate\";s:4:\"slug\";s:9:\"affiliate\";s:5:\"count\";i:524;}s:3:\"ads\";a:3:{s:4:\"name\";s:3:\"ads\";s:4:\"slug\";s:3:\"ads\";s:5:\"count\";i:516;}s:10:\"categories\";a:3:{s:4:\"name\";s:10:\"categories\";s:4:\"slug\";s:10:\"categories\";s:5:\"count\";i:507;}s:4:\"user\";a:3:{s:4:\"name\";s:4:\"user\";s:4:\"slug\";s:4:\"user\";s:5:\"count\";i:490;}s:7:\"contact\";a:3:{s:4:\"name\";s:7:\"contact\";s:4:\"slug\";s:7:\"contact\";s:5:\"count\";i:489;}s:3:\"api\";a:3:{s:4:\"name\";s:3:\"api\";s:4:\"slug\";s:3:\"api\";s:5:\"count\";i:487;}s:4:\"tags\";a:3:{s:4:\"name\";s:4:\"tags\";s:4:\"slug\";s:4:\"tags\";s:5:\"count\";i:483;}s:6:\"button\";a:3:{s:4:\"name\";s:6:\"button\";s:4:\"slug\";s:6:\"button\";s:5:\"count\";i:482;}s:15:\"payment-gateway\";a:3:{s:4:\"name\";s:15:\"payment gateway\";s:4:\"slug\";s:15:\"payment-gateway\";s:5:\"count\";i:470;}s:5:\"users\";a:3:{s:4:\"name\";s:5:\"users\";s:4:\"slug\";s:5:\"users\";s:5:\"count\";i:467;}s:6:\"mobile\";a:3:{s:4:\"name\";s:6:\"mobile\";s:4:\"slug\";s:6:\"mobile\";s:5:\"count\";i:462;}s:6:\"events\";a:3:{s:4:\"name\";s:6:\"events\";s:4:\"slug\";s:6:\"events\";s:5:\"count\";i:452;}s:5:\"photo\";a:3:{s:4:\"name\";s:5:\"photo\";s:4:\"slug\";s:5:\"photo\";s:5:\"count\";i:434;}s:9:\"slideshow\";a:3:{s:4:\"name\";s:9:\"slideshow\";s:4:\"slug\";s:9:\"slideshow\";s:5:\"count\";i:428;}s:9:\"marketing\";a:3:{s:4:\"name\";s:9:\"marketing\";s:4:\"slug\";s:9:\"marketing\";s:5:\"count\";i:425;}s:9:\"gutenberg\";a:3:{s:4:\"name\";s:9:\"gutenberg\";s:4:\"slug\";s:9:\"gutenberg\";s:5:\"count\";i:424;}s:4:\"chat\";a:3:{s:4:\"name\";s:4:\"chat\";s:4:\"slug\";s:4:\"chat\";s:5:\"count\";i:420;}s:6:\"photos\";a:3:{s:4:\"name\";s:6:\"photos\";s:4:\"slug\";s:6:\"photos\";s:5:\"count\";i:420;}s:10:\"navigation\";a:3:{s:4:\"name\";s:10:\"navigation\";s:4:\"slug\";s:10:\"navigation\";s:5:\"count\";i:420;}s:5:\"stats\";a:3:{s:4:\"name\";s:5:\"stats\";s:4:\"slug\";s:5:\"stats\";s:5:\"count\";i:417;}s:8:\"calendar\";a:3:{s:4:\"name\";s:8:\"calendar\";s:4:\"slug\";s:8:\"calendar\";s:5:\"count\";i:414;}s:10:\"statistics\";a:3:{s:4:\"name\";s:10:\"statistics\";s:4:\"slug\";s:10:\"statistics\";s:5:\"count\";i:406;}s:5:\"popup\";a:3:{s:4:\"name\";s:5:\"popup\";s:4:\"slug\";s:5:\"popup\";s:5:\"count\";i:405;}s:10:\"newsletter\";a:3:{s:4:\"name\";s:10:\"newsletter\";s:4:\"slug\";s:10:\"newsletter\";s:5:\"count\";i:399;}s:10:\"shortcodes\";a:3:{s:4:\"name\";s:10:\"shortcodes\";s:4:\"slug\";s:10:\"shortcodes\";s:5:\"count\";i:389;}s:4:\"news\";a:3:{s:4:\"name\";s:4:\"news\";s:4:\"slug\";s:4:\"news\";s:5:\"count\";i:388;}s:5:\"forms\";a:3:{s:4:\"name\";s:5:\"forms\";s:4:\"slug\";s:5:\"forms\";s:5:\"count\";i:385;}s:12:\"social-media\";a:3:{s:4:\"name\";s:12:\"social media\";s:4:\"slug\";s:12:\"social-media\";s:5:\"count\";i:381;}s:4:\"code\";a:3:{s:4:\"name\";s:4:\"code\";s:4:\"slug\";s:4:\"code\";s:5:\"count\";i:376;}s:8:\"redirect\";a:3:{s:4:\"name\";s:8:\"redirect\";s:4:\"slug\";s:8:\"redirect\";s:5:\"count\";i:372;}s:7:\"plugins\";a:3:{s:4:\"name\";s:7:\"plugins\";s:4:\"slug\";s:7:\"plugins\";s:5:\"count\";i:370;}s:14:\"contact-form-7\";a:3:{s:4:\"name\";s:14:\"contact form 7\";s:4:\"slug\";s:14:\"contact-form-7\";s:5:\"count\";i:370;}s:9:\"multisite\";a:3:{s:4:\"name\";s:9:\"multisite\";s:4:\"slug\";s:9:\"multisite\";s:5:\"count\";i:366;}s:3:\"url\";a:3:{s:4:\"name\";s:3:\"url\";s:4:\"slug\";s:3:\"url\";s:5:\"count\";i:361;}s:4:\"meta\";a:3:{s:4:\"name\";s:4:\"meta\";s:4:\"slug\";s:4:\"meta\";s:5:\"count\";i:353;}s:11:\"performance\";a:3:{s:4:\"name\";s:11:\"performance\";s:4:\"slug\";s:11:\"performance\";s:5:\"count\";i:353;}s:4:\"list\";a:3:{s:4:\"name\";s:4:\"list\";s:4:\"slug\";s:4:\"list\";s:5:\"count\";i:351;}s:12:\"notification\";a:3:{s:4:\"name\";s:12:\"notification\";s:4:\"slug\";s:12:\"notification\";s:5:\"count\";i:345;}s:8:\"tracking\";a:3:{s:4:\"name\";s:8:\"tracking\";s:4:\"slug\";s:8:\"tracking\";s:5:\"count\";i:332;}s:16:\"custom-post-type\";a:3:{s:4:\"name\";s:16:\"custom post type\";s:4:\"slug\";s:16:\"custom-post-type\";s:5:\"count\";i:331;}s:16:\"google-analytics\";a:3:{s:4:\"name\";s:16:\"google analytics\";s:4:\"slug\";s:16:\"google-analytics\";s:5:\"count\";i:329;}s:11:\"advertising\";a:3:{s:4:\"name\";s:11:\"advertising\";s:4:\"slug\";s:11:\"advertising\";s:5:\"count\";i:328;}s:5:\"cache\";a:3:{s:4:\"name\";s:5:\"cache\";s:4:\"slug\";s:5:\"cache\";s:5:\"count\";i:322;}s:6:\"simple\";a:3:{s:4:\"name\";s:6:\"simple\";s:4:\"slug\";s:6:\"simple\";s:5:\"count\";i:320;}s:4:\"html\";a:3:{s:4:\"name\";s:4:\"html\";s:4:\"slug\";s:4:\"html\";s:5:\"count\";i:320;}s:6:\"author\";a:3:{s:4:\"name\";s:6:\"author\";s:4:\"slug\";s:6:\"author\";s:5:\"count\";i:317;}}', 'no'),
(1140, '_site_transient_timeout_theme_roots', '1577569445', 'no'),
(1141, '_site_transient_theme_roots', 'a:5:{s:14:\"schoolsportact\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";s:12:\"twentytwenty\";s:7:\"/themes\";}', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1142, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1577567654;s:7:\"checked\";a:11:{s:30:\"advanced-custom-fields/acf.php\";s:5:\"5.8.7\";s:29:\"acf-repeater/acf-repeater.php\";s:5:\"2.1.0\";s:41:\"advanced-custom-fields-pro-master/acf.php\";s:5:\"5.8.3\";s:19:\"akismet/akismet.php\";s:5:\"4.1.3\";s:9:\"hello.php\";s:5:\"1.7.2\";s:63:\"limit-login-attempts-reloaded/limit-login-attempts-reloaded.php\";s:6:\"2.10.0\";s:19:\"members/members.php\";s:5:\"2.2.0\";s:57:\"reveal-ids-for-wp-admin-25/reveal-ids-for-wp-admin-25.php\";s:5:\"1.5.4\";s:21:\"schoolsport/index.php\";s:5:\"0.0.1\";s:39:\"search-filter-pro/search-filter-pro.php\";s:5:\"2.4.6\";s:37:\"wp-sendgrid-smtp/wp-sendgrid-smtp.php\";s:5:\"1.0.6\";}s:8:\"response\";a:2:{s:41:\"advanced-custom-fields-pro-master/acf.php\";O:8:\"stdClass\":8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:41:\"advanced-custom-fields-pro-master/acf.php\";s:11:\"new_version\";s:5:\"5.8.7\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:5:\"5.3.0\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}s:39:\"search-filter-pro/search-filter-pro.php\";O:8:\"stdClass\":13:{s:11:\"new_version\";s:5:\"2.5.0\";s:14:\"stable_version\";s:5:\"2.5.0\";s:4:\"name\";s:19:\"Search & Filter Pro\";s:4:\"slug\";s:17:\"search-filter-pro\";s:3:\"url\";s:68:\"https://searchandfilter.com/downloads/search-filter-pro/?changelog=1\";s:12:\"last_updated\";s:10:\"1 week ago\";s:8:\"homepage\";s:28:\"https://searchandfilter.com/\";s:8:\"sections\";a:3:{s:11:\"description\";s:864:\"<p>Search &amp; Filter Pro is a advanced search and filtering plugin for WordPress.  It allows you to Search &amp; Filter your posts / custom posts / products by any number of parameters allowing your users to easily find what they are looking for on your site, whether it be a blog post, a product in an online shop and much more.</p>\n<p>Users can filter by Categories, Tags, Taxonomies, Custom Fields, Post Meta, Post Dates, Post Types and Authors, or any combination of these easily.</p>\n<p>Great for searching in your online shop, tested with: WooCommerce and WP eCommerce, Easy Digital Downloads</p>\n<h4> Field types include: </h4>\n<ul>\n<li>dropdown selects</li>\n<li>checkboxes</li>\n<li>radio buttons</li>\n<li>multi selects</li>\n<li>range slider</li>\n<li>number range</li>\n<li>date picker</li>\n<li>single or multiselect comboboxes with autocomplete</li>\n</ul>\";s:9:\"changelog\";s:41658:\"<h4> 2.5.0 </h4>\n<ul>\n<li>Fix - issues with number range fields not setting the &quot;max&quot; value by default</li>\n<li>Fix - some errors were being thrown when checking if a term exists</li>\n<li>Fix - some php warnings related to an object being countable</li>\n<li>Fix - issues with forming the URL for taxonomy archives in certain circumstances</li>\n<li>Fix - an issue with the current author being detected when enabling this feature on author archives</li>\n<li>Fix - issues with multiple date pickers and auto submit activating properly when selecting a date</li>\n<li>Fix - a warning about an undefined variable</li>\n<li>Fix - an issue with reset form not working properly on taxonomy archives</li>\n<li>Fix - allow <code>update_post_cache</code> action in admin</li>\n<li>Fix - an issue with URL encoding in pagination</li>\n<li>Fix - an issue with whitespace being removed from user choices in choice fields</li>\n<li>Fix - a Polylang issue with the wrong language form being loaded, when auto submit is off</li>\n<li>Fix - an issue with URL encoding in sort order fields</li>\n<li>Fix - an issue where our meta queries (in query settings) were not respecting WordPress Time Zone when &quot;current date&quot; was used</li>\n<li>Fix - an issue when using the OR comparison inside a field, and non latin characters</li>\n<li>Fix - an issue with searches not working when pressing the back button on iOS Safari</li>\n<li>Fix - an issue with stock status not being stored on the parent product in a variable product</li>\n<li>Fix - an issue with WooCommerce shop page, where it was not registering as <code>filtered</code> when using the search input box</li>\n<li>Fix - issues detecting post meta for WC variations</li>\n<li>Fix - added date and datetime meta type options for ordering by meta values</li>\n<li>Fix - re-fix mobile Safari back button issue</li>\n<li>New - added &quot;Relevance&quot; to default order by and sort fields</li>\n<li>New - an issue where multiple meta keys with the same name (but different cases) were not being correctly detected</li>\n<li>Improvement - updates to license page</li>\n</ul>\n<h4> 2.4.6 </h4>\n<ul>\n<li>Fix - properly disable <code>maintain search form state</code> as this was causing potential security issues</li>\n<li>Fix - a character encoding issue when checking if ajax can be enabled on a particular page</li>\n<li>Fix - an issue with the sf-option-active class not being removed when using the reset button and submit form is disabled</li>\n<li>Fix - some issue with sf-option-active not being set correctly on radio buttons in certain circumstances</li>\n<li>Improvement - add support for pagination without the <code>page</code> prefix, ie, the updated Elementor Pro Posts widget uses /%postname%/%pageno%</li>\n<li>Improvement - set <code>paged</code> using <code>set_query_var</code> for better compatibility with other plugins</li>\n</ul>\n<h4> 2.4.5 </h4>\n<ul>\n<li>Fix - an issue with noUiSlider when &quot;Display values as&quot; is set to &quot;text&quot; in range fields</li>\n<li>Fix - an issue with Beaver Builder Themer auto scrolling to results on page load (when using our display method &quot;archive&quot;)</li>\n<li>Fix - an issue with Ajax requests and Polylang</li>\n<li>Fix - some issues with filtering WC shop in some themes</li>\n</ul>\n<h4> 2.4.4 </h4>\n<ul>\n<li>Fix - an error being thrown when creating new sites in wpmu</li>\n<li>Fix - return the original IDs of taxonomy terms, when no translated term is found (when using translation plugins) - this allows for taxonomies that are not translated to retain their settings</li>\n<li>Fix - an issue where some of our 3rd party integrations were not working in ajax requests (very rare)</li>\n<li>Fix - an issue where the <code>filter_next_query</code> shortcode was being ignored in ajax requests</li>\n<li>Fix - an issue with Ajax URLs not always being set correctly when using PolyLang</li>\n<li>Updated - noUiSlider to v11.1.0</li>\n<li>Updated - chosen to v1.8.7</li>\n<li>New - added a <code>skip</code> argument for our <code>filter_next_query</code> shortcode, to access those tricky queries</li>\n</ul>\n<h4> 2.4.3 </h4>\n<ul>\n<li>Fix - refix enable_taxonomy_archives variable warnings</li>\n<li>Fix - an issue with Beaver Builder Themer scrolling to the results on page load (this occured when pagination was set)</li>\n<li>Fix - silenced (@)set_time_limit as this was throwing warnings on some hosts</li>\n<li>Update - update the plugin to point to our new domain for auth and updates, searchandfilter.com :)</li>\n</ul>\n<h4> 2.4.2 </h4>\n<ul>\n<li>Fix - removed an unwanted <code>exit</code> causing various and seemingly unrelated issues</li>\n</ul>\n<h4> 2.4.1 </h4>\n<ul>\n<li>New - added JS events <code>sf:ajaxformstart</code> and <code>sf:ajaxformfinish</code> to detect when updating the form has started/finished</li>\n<li>Improvement - speed improvements to the cache, when saving posts and when rebuilding the entire cache</li>\n<li>Fix - an issue where filtering on taxonomy archives was not working with WooCommerce</li>\n<li>Fix - WooCommerce variations were not being taking into consideration in the batch size when rebuilding the cache</li>\n<li>Fix - an issue with WC not showing category/taxonomy descriptions or sub categories on archives</li>\n<li>Fix - exclude products from results that are &quot;not in catalog&quot; for WC</li>\n<li>Fix - an issue where the count was incorrect when using the private publish option with WooCommerce products</li>\n<li>Fix - changing a search form settings to include product variations, or not, didn\'t trigger a rebuild of the cache in some cases</li>\n<li>Fix - some WC issues when converting child IDs to parent IDs</li>\n<li>Fix - an issue with pagination on taxonomy archives</li>\n<li>Fix - an issue with ACF where option labels were not being correctly detected</li>\n<li>Fix - an issue with uninstall not working correctly sometimes</li>\n<li>Fix - an issue with infinite scroll not activating when the <code>Only use Ajax on the results page</code> setting is off</li>\n<li>Fix - an issue with Polylang when searching posts that are not managed by Polylang</li>\n</ul>\n<h4> 2.4.0 </h4>\n<ul>\n<li>NOTICE - If you are using S&amp;F with Woocommerce Variations and experiencing issues, you may need to rebuild the S&amp;F cache</li>\n<li>New - change the &quot;no results&quot; message for comboboxes</li>\n<li>Fix - WooCommerce deprecated <code>woocommerce_get_page_id</code> in 3.0</li>\n<li>Fix - various WooCommerce issues relating to Variations - Woocommerce users\' who use variations may need to rebuild S&amp;F cache</li>\n<li>Fix - correctly set the <code>sf-option-active</code> class on multi select items (this includes checkboxes)</li>\n<li>Fix - properly escape some strings</li>\n<li>Fix - destroy noUiSlider (if it exists) before init, in case it has been init by another plugin (improved compatibility)</li>\n<li>Fix - some issues with levels / nesting of hierarchical taxonomies</li>\n<li>Fix - some issues with polylang and ajax requests</li>\n<li>Fix - an issue with a number range field not resetting properly</li>\n<li>Fix - an issue with the range slider in firefox, when ajax was disabled and auto submit was on</li>\n<li>Fix - an issue with <code>enable on taxonomy archives</code> when taxonomies were shared between multiple post types</li>\n<li>Fix - a PHP error when using multiple date pickers with post meta</li>\n<li>Fix - the infinite scroll loader will now check the parent it is attached to and use the correct html tag for the loader</li>\n<li>Fix - an issue with the icon not loading for available fields</li>\n<li>Fix - an issue with &quot;enable on taxonomy archives&quot; and pagination not working correctly</li>\n<li>Fix - an issue with min / max values being correctly autodetected for range fields</li>\n<li>Fix - some issues with rounding &amp; formatting on numeric and slider range fields</li>\n<li>Fix - range dropdown &amp; radio fields were not respecting the step value when it came to the last / max option</li>\n<li>Fix - some layout issues in the admin</li>\n<li>Fix - issues with the later versions of Relevanssi</li>\n<li>Fix - some issues with refocusing the search box after a search is performed</li>\n<li>Fix - issues with taxonomy rewrites when using <code>enable on taxonomy archives</code></li>\n<li>Fix - an issue with the date range fields being auto submitted when only 1 has been selected</li>\n<li>Fix - an issue with ACF using <code>get_field_object</code> - and returning the wrong options depending on language</li>\n<li>Fix - some issues with the cache building in the background</li>\n<li>Fix - some issues with ajax filtering with fragment urls</li>\n<li>Fix - a PHP warning when creating the first search form after install</li>\n<li>Fix - a PHP warning - incorrect usage of <code>count</code>, displaying warnings when saving posts that are to be cached</li>\n<li>Update - update chosen to v1.8.2</li>\n<li>Update - update select2 to v4.0.5</li>\n</ul>\n<h4> 2.3.4 </h4>\n<ul>\n<li>Fix - issues in some environments where infinite scroll wasn\'t activating after a performing search, or getting the page var wrong</li>\n<li>Fix - infinite scroll offset was not being applied correctly</li>\n<li>Improvement - changed scope of some CSS classes in admin ui for better compatibility with other plugins</li>\n<li>Fix - some bugs causing issues with 3rd part plugin compatibility</li>\n<li>Fix - a bug where S&amp;F wouldn\'t cache new items added to media</li>\n</ul>\n<h4> 2.3.3 </h4>\n<ul>\n<li>New - added action <code>search_filter_api_header</code>, to allow for modification of the headers that are sent with our ajax requests</li>\n<li>New - added offset for activation of infinite scroll in the display results tab</li>\n<li>New - added new shortcode action <code>filter_next_query</code> - this will apply filtering to the next <code>WP_Query</code> found</li>\n<li>Fix - an issue with infinite scroll activating multiple times, if you have multiple instances of a search form on a page</li>\n<li>Fix - speed issues with WPML when using media library grid view (and S&amp;F is set to search media)</li>\n<li>Fix - incorrect type cast of a settings variable causing settings not to be loaded correctly in some circumstances</li>\n</ul>\n<h4> 2.3.2 </h4>\n<ul>\n<li>Fix - PHP warnings &amp; errors when using WooCommerce &amp; Taxonomy Archive display mode</li>\n<li>Fix - Some issues with the correct fields appearing in the &quot;display results&quot; tab</li>\n</ul>\n<h4> 2.3.1 </h4>\n<ul>\n<li>New - Plugin data (such as saved search forms &amp; cache) will no longer be deleted when uninstalling - to remove all data use the new option in the settings page</li>\n<li>New - Search &amp; Filter can now be used to filter your taxonomy archives - currently only works with &quot;Post Type Archive&quot; and &quot;WooCommerce&quot; display methods</li>\n<li>Fix - WPML issue was re-introduced in 2.3.0</li>\n<li>Fix - A Polylang issue when using the shortcode display method &amp; ajax</li>\n<li>Fix - <code>sf-option-active</code> class was not updating when using ajax, with autocount off (as the form no longer gets refreshed)</li>\n<li>Fix - issue with &quot;include children in parents&quot; for taxonomy fields</li>\n<li>Fix - an issue with <code>?sf_data</code> being appended to pagination in ajax requests</li>\n<li>Fix - issue with Visual Composer plugin only working after first interaction (ajax)</li>\n<li>Fix - an issue with infinite scroll triggering on incorrect pages</li>\n<li>Fix - an issue where the <code>sf_results_url</code> filter was not being applied to pagination</li>\n<li>Fix - an issue with Archive display method &amp; polylang</li>\n<li>Compatibility - store the results URL in its own custom field for better compatibility with migration tools which search/replace urls. <em>Notice</em>, you will need to edit and hit &quot;save&quot; in your search forms before migrating your sites (so the url can be copied in to the correct custom field)</li>\n</ul>\n<h4> 2.3.0 </h4>\n<ul>\n<li>New - Added support for visual composer post grids (free addon plugin required) - create results layouts using visual composer!</li>\n<li>New - Infinite scroll for all display methods - how to setup - <a href=\"https://searchandfilter.com/documentation/search-results/infinite-scroll/\">https://searchandfilter.com/documentation/search-results/infinite-scroll/</a></li>\n<li>New - Added support for ACF relationship fields</li>\n<li>New - added <code>none</code> sort order option for choice meta fields, allowing preservation of the order of options (if set from external plugins)</li>\n<li>New - added option to specify decimal seperator for number range fields</li>\n<li>Update - Select2 JS library to 4.0.3</li>\n<li>Update - Chosen JS library to 1.6.2</li>\n<li>Update - noUiSlider JS library to 8.5.1</li>\n<li>Performance - improvements when generating forms with many options</li>\n<li>Performance - do not reload the S&amp;F form (ajax) if auto count is not enabled - speed improvement</li>\n<li>Performance - store search related data in transients so that search forms are rendered quickly when used outside of Results Pages (enable via settings page)</li>\n<li>Performance - improved cache building speeds</li>\n<li>Fix - combobox issues on touch devices</li>\n<li>Fix - thousands seperator was not displaying for certain input types</li>\n<li>Fix - some issues with Polylang plugin after Polylang updates</li>\n<li>Fix - issues with the post type field not being set</li>\n<li>Fix - IDs for input fields are now generated randomly based on current timestamp - using the same search form multiple times on a page caused errors with labels &amp; IDs (clicking a label in one form would update the other instance)</li>\n<li>Fix - an issue when using <code>update_post_cache</code> filter on already deleted posts</li>\n<li>Fix - PHP notices when using Woocommerce with a static homepage</li>\n<li>Fix - variatons not working correctly in woocommerce, S&amp;F was returning matches for attribute combinations (of variations) which did not exist, but did exist within a particular product</li>\n<li>Fix - potential infinite loop when results contain results shortcodes :/</li>\n<li>Fix - &quot;Only use Ajax on the results page&quot; was not working correctly on post type archives when the taxonomy archives were based of the post type archive URL</li>\n<li>Fix - S&amp;F pagination was taking over taxonomy archive pagination when the display method was set to <code>post type archive</code> and the taxonomy archives path had the post type as base rewrite</li>\n<li>Fix - <code>hide empty</code> and <code>show count</code> options no longer have any effect when auto count is disabled.</li>\n<li>Fix - default sorting by numeric meta keys was not working when they were decimals / floating point numbers.. all numeric sorting is now converted to decimal sorting to 4 decimal places which also works for standard numeric sorting</li>\n<li>Fix - an issue where numerical ranges using the &quot;overlap&quot; comparison were not returning  the correct results</li>\n<li>Fix - an issue where numerical ranges were not auto detecing the max value correctly, when using different start/end meta keys</li>\n<li>Fix - issues with WPML &amp; PolyLang</li>\n<li>Fix - some issues with Ajax when using ajax in certain display modes</li>\n<li>Fix - issues when search forms are within the results area, and replaced with an ajax request</li>\n<li>Removed - help tab on admin screens as it was unused</li>\n</ul>\n<h4> 2.2.0 </h4>\n<ul>\n<li>New - field - posts per page</li>\n<li>New - added system status screen</li>\n<li>New - Added Select2 JS as an alternative to Chosen for comboboxes - seems to have better mobile support - change this in main S&amp;F settings screen</li>\n<li>New - added support for custom post stati (statuses)</li>\n<li>New - use slugs instead of IDs in shortcodes (check the shortcodes metabox)</li>\n<li>Improved - allow post meta keys and values to contain spaces &amp; special characters</li>\n<li>Improved - updated Chosen JS to - v1.5.2</li>\n<li>Improved - reset button - choose if the reset button submits the form after resetting it</li>\n<li>Improved - new admin notices for settings that can potentially cause errors</li>\n<li>Improved - admin UI tweaks</li>\n<li>Improved - added <code>stop()</code> to scrolling animations before starting to scroll the page</li>\n<li>Fix - issues with PHP 7</li>\n<li>Fix - properly escape some input fields</li>\n<li>Fix - S&amp;F incorrectly warning network activated plugins are not enabled</li>\n<li>Fix - refocus input fields after the form has been auto submitted</li>\n<li>Fix - an error with the range slider and decimals</li>\n<li>Fix - JS event &quot;sf:ajaxfinish&quot; now fired after all S&amp;F process have completed (such as updating the search form)</li>\n<li>Fix - various pagination issues</li>\n<li>Fix - an issue with pagination and the new <code>custom</code> display method</li>\n<li>Fix - renamed a global function to prevent conflicts</li>\n<li>Fix - Display problems when using WooCommerce shop on the homepage with S&amp;F</li>\n<li>Fix - Issues with the woocommerce orderby dropdown</li>\n<li>Fix - an issue with the action <code>search_filter_query_posts</code> not working correctly</li>\n<li>Fix - a bug sometimes causing tag and category fields to be detected as undefined causing search issues</li>\n<li>Fix - issues with detecting when attachments were updated and rebuilding the cache</li>\n<li>Fix - pressing enter in the search box reset the timer for autosubmit</li>\n<li>Fix - added EDD prep_query shortcode to shortcodes box</li>\n<li>Fix - fix some compatibility issues with WPML where WPML was converting taxonomy term IDs into the current language rather than post language</li>\n<li>Fix - use global function <code>get_queried_object</code> rather than <code>$wp_query-&gt;queried_object</code> for consistency</li>\n<li>Fix - issues with the author field and detecting defaults</li>\n<li>Notice - WP 4.6 tested &amp; compatible + PHP 7</li>\n</ul>\n<h4> 2.1.2 </h4>\n<ul>\n<li>New - Sort order can be displayed as radio buttons</li>\n<li>New - filters for all URLs used in S&amp;F - this allows for dynamically changing the various URLs for example to force https or similar - <a href=\"https://searchandfilter.com/documentation/action-filter-reference/\">https://searchandfilter.com/documentation/action-filter-reference/</a></li>\n<li>Fix - an issue with <code>include_children</code> and allowing the AND operator to be used</li>\n<li>Fix - an issue with hierarchical lists not being display correctly</li>\n</ul>\n<h4> 2.1.1 </h4>\n<ul>\n<li>New - added <code>data-sf-count</code> attributes to inputs which have count variables</li>\n<li>Improvement - default cache speed is set to slow</li>\n<li>Fix - an issue with pagination filters</li>\n<li>Fix - issues with PolyLang - should now be working again with PolyLang v1.7.12</li>\n<li>Fix - minify issues with CSS &amp; JS files</li>\n<li>Fix - issues with depth in hierarchical fields</li>\n<li>Fix - an issue where S&amp;F was hijacking pagination when it wasn\'t supposed to</li>\n<li>Fix - a couple of minor issues with the author field</li>\n<li>Fix - S&amp;F <code>sf:init</code> was incorrectly firing after each ajax request, it is now fired only on page load &amp; once initialised</li>\n<li>Fix - An issue where post date fields were not being set correctly in the front end</li>\n<li>Fix - a PHP/pass by reference overload issue</li>\n<li>Fix - an issue with <code>number_format</code> &amp; PHP warnings in admin</li>\n<li>Fix - an issue with undefined taxonomy slugs in the S&amp;F cache</li>\n<li>Fix - an issue with <code>wp_json_encode</code></li>\n</ul>\n<h4> 2.1.0 </h4>\n<ul>\n<li>Notice - depth classes for hierarchical fields fields have renamed to avoid conflicts - from <code>.level-0</code> to <code>.sf-level-0</code></li>\n<li>Notice - properly prefix range &amp; min / max classes = from <code>.range-min</code>, <code>.range-max</code> and <code>.meta-range</code>  to <code>.sf-range-min</code>, <code>.sf-range-max</code> and <code>.sf-meta-range</code></li>\n<li>New - sync meta fields - when using &quot;number&quot; or &quot;choice&quot; type meta fields, the values can now be auto detected - values can also be sorted</li>\n<li>New - sync ACF fields - use above in choice fields with auto detection - S&amp;F can now retreive built in ACF labels for values too</li>\n<li>New - added support for ordering by multiple fields (the default posts order)</li>\n<li>New - added support for ordering posts by Post Type</li>\n<li>New - lots of improvements to post meta fields (number) more UI options and input types</li>\n<li>New - added support for decimals and number formatting in post meta (number) fields</li>\n<li>New - new compare options for date range and number fields - great for date ranges and bookings/promotional systems</li>\n<li>New - added sort by relevance option for Relevanssi under the Advanced tab</li>\n<li>New - added options to control the display of sticky posts (under the Posts tab)</li>\n<li>New - Settings page</li>\n<li>New - Settings - control caching speed &amp; background processes</li>\n<li>New - Settings - added settings to lazyload JS and an option to load jQuery language files for the datepicker</li>\n<li>New - accessibility - WCAG 2.0 compliant - some html restructuring (mostly adding in labels) and added screen reader text option to all text/number input fields and selects</li>\n<li>New - filter - allows users to filter any field in the search form and most of the options</li>\n<li>New - added counts to the active query class</li>\n<li>Improvement - authors now use slugs instead of IDs in the URL</li>\n<li>Improvement - updated Chosen &amp; noUiSlider to their latest versions</li>\n<li>Improvement - show internal taxonomy names as well as labels throughout admin UI</li>\n<li>Improvement - support for WooCommerce shop when it is set to category display</li>\n<li>Improvement - speed updates/optimisations to the cache and auto count</li>\n<li>Improvement - better admin notices &amp; warnings</li>\n<li>Fix - issues with WPML and loading the correct taxonomies etc</li>\n<li>Fix - issues with caching and the attachment post type</li>\n<li>Fix - an issue where getting counts for taxonomies was occuring twice</li>\n<li>Fix - URL Encoding issue with meta fields</li>\n<li>Fix - an issue when using multiple search forms on a single page &amp; pagination not working correctly</li>\n<li>Fix - removed an error message which was showing whenever the cache was restarting - it was unnecessary</li>\n<li>Fix - whitespace being trimmed from textareas in certain field types</li>\n<li>Fix - some pagination issues when using post type archive</li>\n<li>Fix - do not enable auto count by default</li>\n<li>Fix - Post Type archive display method now properly uses the Posts Page (as defined under <code>settings</code> -&gt; <code>reading</code>) where applicable when the post type is set to <code>post</code></li>\n<li>Fix - a bug where html entities were matched when searching in chosen comboboxes such as <code>nbsp</code></li>\n<li>Fix - an admin bug where selecting Post Type Archive as the results method would show the wrong options after saving</li>\n<li>Fix - a few admin UI bugs</li>\n<li>Fix - a bug with the ajax start/end events</li>\n<li>WP 4.4 compat - tested with 2016 theme</li>\n</ul>\n<h4> 2.0.3 </h4>\n<ul>\n<li>New - update search form (auto count) without submitting the form</li>\n<li>New - added variable <code>search_filter_id</code> to all queries to easily identify which S&amp;F form your queries are being modified by - use <code>$query[\'search_filter_id\'];</code></li>\n<li>New - added RTL support for all JS plugins - chosen comboxbox, jQuery datepicker and noUiSlider</li>\n<li>New - added action to trigger the rebuild of the cache for a specific post (<code>do_action(\'search_filter_update_post_cache\', 1984);</code> where 1984 is the post ID)</li>\n<li>Fix - issue with Firefox &quot;rembering&quot; disabled state on soft refresh - now a soft page refresh in FF also forces all inputs to be enabled to overcome this issue</li>\n<li>Fix - issue with comboboxes finding child terms (hierarchical enabled)</li>\n<li>Fix - issue with URI encoding in search field</li>\n<li>Fix - issues with multiple results shortcodes when meta data defaults are set</li>\n<li>Fix - issues with WP Types plugin and nested post meta values</li>\n<li>Fix - caching issues with post meta when there are multiple values</li>\n<li>Fix - issue with search term &amp; stripslashes</li>\n<li>Fix - compatibility issue with Relevanssi</li>\n<li>Fix - correctly show count numbers when &quot;detect defaults from current page&quot; is selected</li>\n<li>Fix - re-implement <code>save_post</code> filter outside of <code>is_admin</code> for rebuilding the cache from the front end</li>\n</ul>\n<h4> 2.0.2 </h4>\n<ul>\n<li>New - use S&amp;F with even more templates (Archive Mode) by adding a shortcode/action before your loop</li>\n<li>Fix - set priority of Ajax (with results shortcode) search to <code>200</code> on <code>init</code> hook - it was being fired sometimes before taxonomies had been declared</li>\n<li>Fix - <code>array_merge</code> errors when using hierarchical taxonomies and including children in parents</li>\n<li>Fix - JS errors with multiple search forms on the same page at the same time</li>\n<li>Fix - JS error error in Firefox where refreshing the page sometimes caused a disabled state on the search form</li>\n<li>Fix - an issue in Avada + woocommerce, when setting up the query, and only using 1 post type S&amp;F now passes a string instead of an array</li>\n<li>Fix - a PHP error and delimiters in the Active Query class</li>\n<li>Fix - an issue with maintain search form state passing <code>page_id</code> when permalinks are disabled</li>\n<li>Fix - undefined variable notice in author walker</li>\n<li>Fix - undefined variable notice in edit search form screen</li>\n</ul>\n<h4> 2.0.1 </h4>\n<ul>\n<li>NOTICE - DO NOT UPDATE UNTIL YOU HAVE READ THE RELEASE NOTES: <a href=\"https://searchandfilter.com/documentation/2-0-upgrade-notes/\">https://searchandfilter.com/documentation/2-0-upgrade-notes/</a></li>\n<li>Version bump so all beta testers get the latest update via the dashboard</li>\n</ul>\n<h4> 2.0 </h4>\n<ul>\n<li>New - caching of results for fast speeds even on large databases</li>\n<li>New - direct support for the WooCommerce shop page</li>\n<li>New - direct support for WooCommerce product variations</li>\n<li>New - integration with Easy Digital Downloads (EDD) shortcodes - just add the S&amp;F prep_query shortcode directly before the EDD shortcode ie - <code>[searchandfilter id=\"14\" action=\"prep_query\"]</code></li>\n<li>New - use post type archives to display your results (single post type only)</li>\n<li>New - huge speed and accuracy improvements for meta queries - no more <code>%like%</code> queries for serialised meta</li>\n<li>New - auto count - dynamically display counts next to field options based on the current search &amp; settings</li>\n<li>New - auto count - drill down fields - hide options which yield no results</li>\n<li>New - allow for multiple meta keys to be queried when doing ranges</li>\n<li>New - prepolutate search form based on current archive - works for post types, tags, categories, taxonomies and authors</li>\n<li>New - datepicker - supports jQuery UI i18n, dropdown for years &amp; months option, placeholder text customisation</li>\n<li>New - methods for accessing what has been searched</li>\n<li>Improvement - moved all Ajax logic to front end for better compatibility with other plugins (esp shortcode based)</li>\n<li>Improvement - huge amount of refactoring - some parts completely rewritten and optimized, JS rewrite</li>\n<li>Improvement - show which meta keys are selected in widget title</li>\n<li>Improvement - change labels on checkbox and radio fields - don\'t wrap the inputs inside the labels</li>\n<li>Fix - some problems with pagination links sometimes pointing to the ajax URL</li>\n<li>Fix - Fix an issue with <code>include_children</code> now working</li>\n<li>New - relationships can now be defined across taxonomy and meta fields</li>\n<li>Fix - Issues with pagination</li>\n<li>fix - removed references to CSS images that were not being used</li>\n<li>Fix - localised some sloppy CSS rules for compatibility</li>\n<li>Fix - some issues with currencies and decimals when using number ranges</li>\n<li>Fix - an issue with exclude post IDs not working correctly</li>\n<li>Fix - UTF characters in taxonomy term names</li>\n<li>Fix - <code>orderby</code> getting added to the URL on non WooCommerce search forms</li>\n<li>Fix - IE8 JS error - Object.keys() compatibility</li>\n<li>Fix - IE10 JS error / reload error - the <code>input</code> event was triggering when it was not supposed to causing an ajax request to be performed</li>\n<li>Fix - Admin - function definition in wrong scope causing errors in strict mode on some browsers</li>\n<li>Removed - .postform classes that have crept back into build - but added classes and IDs on every input element</li>\n<li>Removed - the global $sf_form_data - changed to $searchandfilter</li>\n<li>Notice - you should no longer use <code>pre_get_posts</code> to modify queries, there is a new filter which takes an array of arguments <code>sf_edit_query_args</code> which must be used to also update count number and other non main queries</li>\n<li>In progress - support for PolyLang - testing so far seems good</li>\n</ul>\n<h4> 1.4.3.1 </h4>\n<ul>\n<li>Fix - add serialised tick box to post meta fields</li>\n<li>Fix - added a &quot;data is serialised&quot; checkbox to meta fields</li>\n<li>Dropped - built in pagination functions - <code>sf_pagination_numbers</code> and <code>sf_pagination_prev_next</code> are now redundant</li>\n</ul>\n<h4> 1.4.1 </h4>\n<ul>\n<li>New - Added IDs to search forms for easy css targeting - also renamed ID on results container to keep in line with naming conventions</li>\n<li>New - added reset button</li>\n<li>New - dropdown number range</li>\n<li>New - added options to use timestamps in post meta</li>\n<li>fix - a bug when sanitizing keys from post meta</li>\n<li>fix - a bug with autosuggest &amp; encoding</li>\n<li>fix - issues with searching serialised post meta</li>\n<li>fix - throwing an error when trying to access the <code>all_items</code> label of a taxonomy when it does not exist</li>\n<li>fix - some dependencies with JS/CSS allowing them to be removed more easily</li>\n<li>fix - some tweaks to automatic updates</li>\n<li>fix - layout issues with search form UI and WP 4.1</li>\n<li>fix - various fixes and improvements with compatibility and WPML</li>\n</ul>\n<h4> 1.4.0 </h4>\n<ul>\n<li>New - search media/attachments</li>\n<li>New - added post meta defaults - now you can add constraints for meta data such as searching only products in stock, excluding featured posts or restricting all searches to specific meta data values</li>\n<li>New - scroll to top of page when updating results with ajax</li>\n<li>New - use the shortcode to display results without ajax too (results shortcode only worked with ajax setups previously)</li>\n<li>New - allow regular pagination when using a shortcode for results - (use wp next_posts_link &amp; previous_posts_link, plus added support for wp_pagenavi plugin)</li>\n<li>New - added AND / OR operator to define relationships between tag, category and taxonomy fields</li>\n<li>New - optionally include children in parent searches (categories, hierarchical taxonomies)</li>\n<li>New - improvded UI - add taxonomy browser to help find IDs easily</li>\n<li>New - improved ajax/template UI</li>\n<li>New - minify CSS &amp; JS - finally integrated grunt ;) - non minified versions still available</li>\n<li>New - duplicate search form - a link has been added to the main S&amp;F admin screen underneath each form for easy duplicating!</li>\n<li>New - added support for Relevanssi when using shortcodes to display results</li>\n<li>New - add &quot;today&quot; for date comparisons in meta queries in post meta defaults</li>\n<li>Updated - the default results template (shortcode) to include new pagination options</li>\n<li>Fixed - an error when users are not using permalinks, and submitting the search form</li>\n<li>Fixed - &quot;OR&quot; operator for checkboxes with taxonomies was broken</li>\n<li>Fixed - a JS error when no terms were being shown for a checkbox/radio field</li>\n<li>Fixed - an error when using <code>maintain state</code> and getting 404 on results</li>\n<li>Fixed - an error when detecting if a meta field was serialised or not</li>\n<li>Fixed - an error when saving a post meta field with a poorly formatted name</li>\n<li>Fixed - ajax pagination without shortcode</li>\n<li>Fixed - meta fields with the value <code>0</code> being ignored</li>\n<li>Fixed - some updates to the plugin auto updater - some users weren\'t seeing udpates in the dashboard even when activated</li>\n</ul>\n<h4> 1.3.0 </h4>\n<ul>\n<li>New - JavaScript rewrite - refactored - faster cleaner code</li>\n<li>New - add setting to allow the default sort order of results - check settings panel -&gt; posts</li>\n<li>New - Speed improvements - searching usually caused 2 header requests (a POST and a redirect) - now uses only a single GET request</li>\n<li>New - play nice with other scripts - can now initialise the search form via JS if the form/html is loaded in dynamically</li>\n<li>New - mulitple search forms on the same page!</li>\n<li>New - add data to JS events for targeting individual forms on the same page</li>\n<li>New - maintain search state - keep user search settings while looking at results pages</li>\n<li>New - for Ajax w/ Shortcodes - Added results URL - this allows the widget to be placed anywhere in your site</li>\n<li>New - shortcode meta box - for easier access to shortcodes within the Search Form editor</li>\n<li>New - allow auto submit when ajax is not enabled</li>\n<li>New - shareable/bookmarkable URLs when using shortcodes (this was already available without)</li>\n<li>Fixed - an issue with auto submit</li>\n<li>Fixed - an issue with a significant delay to fetch initial results when using ajax (with shortcode) - initial results are now loaded server side on page load</li>\n<li>Fixed - bad html and &quot;hide_empty&quot; was not working as expected - it was disabling inputs rather than hiding them</li>\n<li>Fixed - i18n for &quot;prev&quot; and &quot;next&quot; in pagination</li>\n<li>Fixed - post date field was not working correctly when using ajax w/ shortcodes</li>\n<li>Improved - integration with WPML - better URLs and works fully with shortcodes</li>\n<li>Removed - <em>Beta</em> Auto Count - this feature is likely to be even more broken (it had plenty of bugs already) - it is recommended you disable this for now.  The next major update will inlcude a revised &amp; working version of this.</li>\n</ul>\n<h4> 1.2.7 </h4>\n<ul>\n<li>Fixed an issue with array_replace_recursive for older PHP version</li>\n</ul>\n<h4> 1.2.6 </h4>\n<ul>\n<li>Fixed an issue with headers in admin when publishing a post</li>\n</ul>\n<h4> 1.2.5 </h4>\n<ul>\n<li>Fixed a JS error in IE8</li>\n<li>Added new settings panel - set defaults search parameters</li>\n<li>Settings Panel - include/exclude categories</li>\n<li>Settings Panel - exclude posts by ID</li>\n<li>Settings Panel - choose to search by Post Status</li>\n<li>Settings Panel - Added Results Per Page for controlling the number of results you see</li>\n<li>Settings Panel - UI refinements</li>\n<li>Settings Panel - more to come (meta)!</li>\n<li>Category, Tag &amp; Taxonomy fields - new option (advanced) to sync included/excluded posts with new settings parameters</li>\n</ul>\n<h4> 1.2.4 </h4>\n<ul>\n<li>DO NOT UPGRADE IF YOU WERE HAVING ISSUES WITH AJAX FUNCTIONALITY AND WAITING FOR A PATCH, ONLY THE TWO UPDATES BELOW ARE INCLUDED IN THIS UPDATE:</li>\n<li>Fix - ajax shortcode functionality - search field is now working again!</li>\n<li>Fix - ajax shortcode functionality - fixed custom field/meta search</li>\n<li>Fix - ajax shortcode functionality - fixed a bug with categories</li>\n</ul>\n<h4> 1.2.3 </h4>\n<ul>\n<li>DO NOT UPGRADE IF YOU WERE HAVING ISSUES WITH AJAX FUNCTIONALITY AND WAITING FOR A PATCH, ONLY THE TWO UPDATES BELOW ARE INCLUDED IN THIS UPDATE:</li>\n<li>Fix - ajax shortcode functionality - only displays published posts (it was also fetching drafts)</li>\n<li>Fix - ajax shortcode functionality - auto submit now working</li>\n</ul>\n<h4> 1.2.2 </h4>\n<ul>\n<li>Fix - stopped using short syntax array in php (<code>[]</code>) which is only supported in php version 5.4+</li>\n</ul>\n<h4> 1.2.1 </h4>\n<ul>\n<li>Fix - a JS error for older Ajax setups</li>\n</ul>\n<h4> 1.2.0 </h4>\n<ul>\n<li>NEW - completely reworked how to use Ajax - simply use a shortcode to place where you want the results to display and you\'re set to go!</li>\n<li>Fix - allow paths in template names - S&amp;F was previously stripping out slashes so couldn\'t access templates in sub directories</li>\n<li>Fix - various small bug fixes</li>\n</ul>\n<h4> 1.1.8 </h4>\n<ul>\n<li>New - add new way to modify the main search query for individual forms</li>\n<li>New - added a new JS init event</li>\n</ul>\n<h4> 1.1.7 </h4>\n<ul>\n<li>New - <em>beta</em> - Auto count for taxonomies - when using tag, category and taxonomies only in a search form, you can now enable a live update of fields, which means as users make filter selections, unavailable combinations will be hidden (this is beta and would love feedback especially from users with high numbers of posts/taxonomies)</li>\n<li>New - date picker for custom fields / post meta - dates must be stored as YYYYMMDD or as timestamps in order to use this field</li>\n<li>New - added JS events to capture start / end of ajax loading so you can add in your own custom loaders</li>\n<li>Fix - prefixed taxonomy and meta field names properly - there were collisions on the set defaults function, for example if a tax and meta share the same key there would be a collision</li>\n<li>Fix - errors with number ranges &amp; range slider</li>\n<li>Fix - an error with detecting if a meta value is serialized</li>\n<li>Fix - scope issue with date fields auto submitting correctly</li>\n</ul>\n<h4> 1.1.6 </h4>\n<ul>\n<li><strong>Notice</strong> - dropped - <code>.postform</code> css class  this was redundant and left in by error - any users using this should update their CSS to use the new and improved options provided:</li>\n<li>New - class names added to all field list items for easy CSS styling + added classes to all options for form inputs for easy targeting of specific field values</li>\n<li>New - added a <code>&lt;span class=\"sf-count\"&gt;</code> wrapper to all fields where a count was being shown for easy styling</li>\n<li>Fix - removed all reference to <code>__DIR__</code> for PHP versions &lt; 5.3</li>\n<li>Fix - Some general tweaks for WPML</li>\n<li>Fix - a bug when choosing all post types still adding &quot;post_types&quot; to the url</li>\n</ul>\n<h4> 1.1.5 </h4>\n<ul>\n<li><strong>Notice</strong> - this update breaks previous Sort Order fields, so make sure if you have a Sort Order Field to rebuild it once you\'ve updated!</li>\n<li>New - Sort Order - in addition to sorting by Meta Value, users can now sort their results by ID, author, title, name, date, date modified, parent ID, random, comment count and menu order, users can also choose whether they they want only ASC or DESC directions - both are optional.</li>\n<li>New - Autocomplete Comboboxes - user friendly select boxes powered by Chosen - text input with auto-complete for selects and multiple selects - just tick the box when choosing a select or multiselect input type</li>\n<li>Fix - add a lower priority to <code>init</code> hook when parsing taxonomies - this helps ensure S&amp;F runs after your custom taxonomies have been created</li>\n<li>Fix - add a lower priority to <code>pre_get_posts</code> - helps with modifying the main query after other plugins/custom code have run</li>\n<li>Fix - a problem with meta values having spaces</li>\n</ul>\n<h4> 1.1.4 </h4>\n<ul>\n<li>New - Meta Suggestions - auto detect values for your custom fields / post meta</li>\n<li>Enhancement - improved Post Meta UI (admin)</li>\n<li>Fix - an error with displaying templates (there was a PHP error being thrown in some environments)</li>\n<li>Fix - an error where ajax enabled search forms were causing a refresh loop on some mobile browsers</li>\n</ul>\n<h4> 1.1.3 </h4>\n<ul>\n<li>New - display meta data as dropdowns, checkboxes, radio buttons and multi selects</li>\n<li>New - added date formats to date field</li>\n<li>fix - auto submit &amp; date picker issues</li>\n<li>fix - widget titles not displaying</li>\n<li>fix - missed a history.pushstate check for AJAX enabled search forms</li>\n<li>fix - dashboard menu conflict with other plugins</li>\n<li>fix - submit label was not updating</li>\n<li>fix - post count for authors was showing only for posts - now works with all post types</li>\n<li>compat - add fallback for <code>array_replace</code> for &lt;= PHP 5.3 users</li>\n</ul>\n<h4> 1.1.2 </h4>\n<ul>\n<li>New - customsise results URL - add a slug for your search results to display on (eg yousite.com/product-search)</li>\n<li>fix - js error when Ajax pagination links are undefined</li>\n<li>fix - date picker dom structure updated to match that of all other fields</li>\n<li>fix - scope issue when using auto submit on Ajax search forms</li>\n</ul>\n<h4> 1.1.1 </h4>\n<ul>\n<li>fix - fixed an error where JS would hide the submit button :/</li>\n<li>fix - fixed an error where parent categories/taxonomies weren\'t showing their results</li>\n</ul>\n<h4> 1.1.0 </h4>\n<ul>\n<li>New - AJAX - searches can be performed using Ajax</li>\n<li>fix - removed redundant js/css calls</li>\n</ul>\n<h4> 1.0.0 </h4>\n<ul>\n<li>Initial release</li>\n</ul>\";s:12:\"installation\";s:634:\"<p>= Uploading in WordPress Dashboard =</p>\n<ol>\n<li>Navigate to the \'Add New\' in the plugins dashboard</li>\n<li>Navigate to the \'Upload\' area</li>\n<li>Select <code>search-filter-pro.zip</code> from your computer</li>\n<li>Click \'Install Now\'</li>\n<li>Activate the plugin in the Plugin dashboard</li>\n</ol>\n<p>= Using FTP =</p>\n<ol>\n<li>Download <code>search-filter-pro.zip</code></li>\n<li>Extract the <code>search-filter-pro</code> directory to your computer</li>\n<li>Upload the <code>search-filter-pro</code> directory to the <code>/wp-content/plugins/</code> directory</li>\n<li>Activate the plugin in the Plugin dashboard</li>\n</ol>\";}s:7:\"banners\";s:41:\"a:2:{s:4:\"high\";s:0:\"\";s:3:\"low\";s:0:\"\";}\";s:5:\"icons\";s:6:\"a:0:{}\";s:10:\"stable_tag\";s:5:\"2.5.0\";s:12:\"contributors\";O:8:\"stdClass\":2:{s:14:\"DesignsAndCode\";O:8:\"stdClass\":3:{s:12:\"display_name\";s:14:\"DesignsAndCode\";s:7:\"profile\";s:39:\"//profiles.wordpress.org/DesignsAndCode\";s:6:\"avatar\";s:59:\"https://wordpress.org/grav-redirect.php?user=DesignsAndCode\";}s:7:\"CodeAmp\";O:8:\"stdClass\":3:{s:12:\"display_name\";s:7:\"CodeAmp\";s:7:\"profile\";s:32:\"//profiles.wordpress.org/CodeAmp\";s:6:\"avatar\";s:52:\"https://wordpress.org/grav-redirect.php?user=CodeAmp\";}}s:5:\"added\";s:10:\"2014-05-10\";}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:7:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:5:\"5.8.7\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.8.7.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}}s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.3\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}s:63:\"limit-login-attempts-reloaded/limit-login-attempts-reloaded.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:43:\"w.org/plugins/limit-login-attempts-reloaded\";s:4:\"slug\";s:29:\"limit-login-attempts-reloaded\";s:6:\"plugin\";s:63:\"limit-login-attempts-reloaded/limit-login-attempts-reloaded.php\";s:11:\"new_version\";s:6:\"2.10.0\";s:3:\"url\";s:60:\"https://wordpress.org/plugins/limit-login-attempts-reloaded/\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/plugin/limit-login-attempts-reloaded.2.10.0.zip\";s:5:\"icons\";a:2:{s:2:\"1x\";s:74:\"https://ps.w.org/limit-login-attempts-reloaded/assets/icon.svg?rev=1472250\";s:3:\"svg\";s:74:\"https://ps.w.org/limit-login-attempts-reloaded/assets/icon.svg?rev=1472250\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:19:\"members/members.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/members\";s:4:\"slug\";s:7:\"members\";s:6:\"plugin\";s:19:\"members/members.php\";s:11:\"new_version\";s:5:\"2.2.0\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/members/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/members.2.2.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:60:\"https://ps.w.org/members/assets/icon-256x256.png?rev=2126126\";s:2:\"1x\";s:60:\"https://ps.w.org/members/assets/icon-128x128.png?rev=2126126\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/members/assets/banner-1544x500.png?rev=2126126\";s:2:\"1x\";s:62:\"https://ps.w.org/members/assets/banner-772x250.png?rev=2126126\";}s:11:\"banners_rtl\";a:0:{}}s:57:\"reveal-ids-for-wp-admin-25/reveal-ids-for-wp-admin-25.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:40:\"w.org/plugins/reveal-ids-for-wp-admin-25\";s:4:\"slug\";s:26:\"reveal-ids-for-wp-admin-25\";s:6:\"plugin\";s:57:\"reveal-ids-for-wp-admin-25/reveal-ids-for-wp-admin-25.php\";s:11:\"new_version\";s:5:\"1.5.4\";s:3:\"url\";s:57:\"https://wordpress.org/plugins/reveal-ids-for-wp-admin-25/\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/plugin/reveal-ids-for-wp-admin-25.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:79:\"https://ps.w.org/reveal-ids-for-wp-admin-25/assets/icon-256x256.png?rev=1162209\";s:2:\"1x\";s:79:\"https://ps.w.org/reveal-ids-for-wp-admin-25/assets/icon-128x128.png?rev=1163228\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:80:\"https://ps.w.org/reveal-ids-for-wp-admin-25/assets/banner-772x250.png?rev=479453\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"wp-sendgrid-smtp/wp-sendgrid-smtp.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:30:\"w.org/plugins/wp-sendgrid-smtp\";s:4:\"slug\";s:16:\"wp-sendgrid-smtp\";s:6:\"plugin\";s:37:\"wp-sendgrid-smtp/wp-sendgrid-smtp.php\";s:11:\"new_version\";s:5:\"1.0.6\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/wp-sendgrid-smtp/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/wp-sendgrid-smtp.1.0.6.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/wp-sendgrid-smtp/assets/icon-128x128.png?rev=1509863\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/wp-sendgrid-smtp/assets/banner-1544x500.png?rev=1509789\";s:2:\"1x\";s:71:\"https://ps.w.org/wp-sendgrid-smtp/assets/banner-772x250.png?rev=1509788\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1143, 'ridwpa_version', '1.5.4', 'yes'),
(1146, '_site_transient_timeout_community-events-93dffa33ab09171a25a28f9dff9e8453', '1577618684', 'no'),
(1147, '_site_transient_community-events-93dffa33ab09171a25a28f9dff9e8453', 'a:3:{s:9:\"sandboxed\";b:0;s:8:\"location\";a:1:{s:2:\"ip\";s:13:\"201.214.239.0\";}s:6:\"events\";a:1:{i:0;a:8:{s:4:\"type\";s:8:\"wordcamp\";s:5:\"title\";s:13:\"WordCamp Asia\";s:3:\"url\";s:31:\"https://2020.asia.wordcamp.org/\";s:6:\"meetup\";s:0:\"\";s:10:\"meetup_url\";s:0:\"\";s:4:\"date\";s:19:\"2020-02-21 00:00:00\";s:8:\"end_date\";s:19:\"2020-02-23 00:00:00\";s:8:\"location\";a:4:{s:8:\"location\";s:17:\"Bangkok, Thailand\";s:7:\"country\";s:2:\"TH\";s:8:\"latitude\";d:13.7248933999999991328877513296902179718017578125;s:9:\"longitude\";d:100.49268299999999953797669149935245513916015625;}}}}', 'no');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_postmeta`
--

DROP TABLE IF EXISTS `wp_postmeta`;
CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(3, 5, '_wp_attached_file', '2019/11/silla-principal.png'),
(4, 5, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:705;s:6:\"height\";i:639;s:4:\"file\";s:27:\"2019/11/silla-principal.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"silla-principal-300x272.png\";s:5:\"width\";i:300;s:6:\"height\";i:272;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"silla-principal-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(13, 30, '_edit_lock', '1575564334:1'),
(14, 30, '_edit_last', '1'),
(15, 31, '_edit_last', '1'),
(16, 31, '_edit_lock', '1575564419:1'),
(17, 32, '_edit_lock', '1575566103:1'),
(18, 32, '_edit_last', '1'),
(19, 32, 'field_5de936884495e', 'a:12:{s:3:\"key\";s:19:\"field_5de936884495e\";s:5:\"label\";s:11:\"Region Type\";s:4:\"name\";s:11:\"region_type\";s:4:\"type\";s:6:\"select\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:7:\"choices\";a:3:{s:17:\"high: High School\";s:17:\"high: High School\";s:23:\"primary: Primary School\";s:23:\"primary: Primary School\";s:0:\"\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:10:\"allow_null\";s:1:\"0\";s:8:\"multiple\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(21, 32, 'position', 'normal'),
(22, 32, 'layout', 'no_box'),
(23, 32, 'hide_on_screen', ''),
(24, 32, 'rule', 'a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"post\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(200, 116, '_edit_lock', '1576086681:1'),
(201, 112, '_edit_lock', '1576085566:1'),
(202, 116, '_edit_last', '1'),
(213, 123, '_edit_last', '1'),
(214, 123, '_edit_lock', '1576780048:1'),
(215, 144, '_edit_last', '1'),
(216, 144, '_edit_lock', '1576357429:1'),
(217, 145, '_edit_last', '1'),
(218, 145, '_edit_lock', '1577323542:31'),
(220, 159, '_edit_lock', '1576357324:1'),
(221, 159, '_edit_last', '1'),
(222, 160, '_edit_last', '1'),
(223, 160, '_edit_lock', '1576357346:1'),
(224, 161, '_edit_last', '1'),
(225, 161, '_edit_lock', '1576357358:1'),
(226, 162, '_edit_last', '1'),
(227, 162, '_edit_lock', '1576357366:1'),
(228, 163, '_edit_last', '1'),
(229, 163, '_edit_lock', '1576357373:1'),
(230, 164, '_edit_last', '1'),
(231, 164, '_edit_lock', '1576357379:1'),
(232, 165, '_edit_last', '1'),
(233, 165, '_edit_lock', '1576357386:1'),
(234, 166, '_edit_lock', '1577322669:31'),
(235, 166, '_edit_last', '1'),
(236, 167, '_edit_last', '1'),
(237, 167, '_edit_lock', '1577323035:31'),
(238, 168, '_edit_lock', '1576696899:1'),
(239, 168, '_edit_last', '1'),
(240, 169, '_edit_last', '1'),
(241, 169, '_edit_lock', '1576357561:1'),
(242, 170, '_edit_last', '1'),
(243, 170, '_edit_lock', '1576357572:1'),
(244, 171, '_edit_last', '1'),
(245, 171, '_edit_lock', '1576357606:1'),
(246, 172, '_edit_last', '1'),
(247, 172, '_edit_lock', '1576357619:1'),
(248, 173, '_edit_last', '1'),
(249, 173, '_edit_lock', '1577323041:31'),
(250, 115, '_edit_lock', '1577323648:31'),
(252, 175, '_edit_lock', '1576357904:1'),
(253, 175, '_edit_last', '1'),
(254, 177, '_edit_lock', '1576446395:31'),
(255, 177, '_edit_last', '1'),
(256, 179, '_edit_lock', '1576357980:1'),
(257, 179, '_edit_last', '1'),
(258, 181, '_edit_lock', '1576452814:31'),
(259, 181, '_edit_last', '31'),
(260, 183, '_edit_lock', '1576358042:1'),
(261, 183, '_edit_last', '1'),
(262, 185, '_edit_lock', '1576358063:1'),
(263, 185, '_edit_last', '1'),
(264, 187, '_edit_lock', '1576358076:1'),
(265, 187, '_edit_last', '1'),
(266, 189, '_edit_lock', '1576358425:1'),
(267, 189, '_edit_last', '1'),
(268, 193, '_wp_attached_file', '2019/12/School-Sport-Coordinator-Roles-and-Responsibilities.pdf'),
(269, 193, '_wp_attachment_metadata', 'a:1:{s:5:\"sizes\";a:4:{s:4:\"full\";a:4:{s:4:\"file\";s:59:\"School-Sport-Coordinator-Roles-and-Responsibilities-pdf.jpg\";s:5:\"width\";i:1058;s:6:\"height\";i:1497;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:67:\"School-Sport-Coordinator-Roles-and-Responsibilities-pdf-212x300.jpg\";s:5:\"width\";i:212;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:68:\"School-Sport-Coordinator-Roles-and-Responsibilities-pdf-724x1024.jpg\";s:5:\"width\";i:724;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:67:\"School-Sport-Coordinator-Roles-and-Responsibilities-pdf-106x150.jpg\";s:5:\"width\";i:106;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}}'),
(270, 194, '_wp_attached_file', '2019/12/2019-SSACT-Membership-Information.pdf'),
(271, 194, '_wp_attachment_metadata', 'a:1:{s:5:\"sizes\";a:4:{s:4:\"full\";a:4:{s:4:\"file\";s:41:\"2019-SSACT-Membership-Information-pdf.jpg\";s:5:\"width\";i:1058;s:6:\"height\";i:1497;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:49:\"2019-SSACT-Membership-Information-pdf-212x300.jpg\";s:5:\"width\";i:212;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:50:\"2019-SSACT-Membership-Information-pdf-724x1024.jpg\";s:5:\"width\";i:724;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:49:\"2019-SSACT-Membership-Information-pdf-106x150.jpg\";s:5:\"width\";i:106;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}}'),
(434, 213, '_menu_item_type', 'post_type'),
(435, 213, '_menu_item_menu_item_parent', '0'),
(436, 213, '_menu_item_object_id', '175'),
(437, 213, '_menu_item_object', 'page'),
(438, 213, '_menu_item_target', ''),
(439, 213, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(440, 213, '_menu_item_xfn', ''),
(441, 213, '_menu_item_url', ''),
(443, 214, '_menu_item_type', 'post_type'),
(444, 214, '_menu_item_menu_item_parent', '213'),
(445, 214, '_menu_item_object_id', '179'),
(446, 214, '_menu_item_object', 'page'),
(447, 214, '_menu_item_target', ''),
(448, 214, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(449, 214, '_menu_item_xfn', ''),
(450, 214, '_menu_item_url', ''),
(452, 215, '_menu_item_type', 'post_type'),
(453, 215, '_menu_item_menu_item_parent', '213'),
(454, 215, '_menu_item_object_id', '181'),
(455, 215, '_menu_item_object', 'page'),
(456, 215, '_menu_item_target', ''),
(457, 215, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(458, 215, '_menu_item_xfn', ''),
(459, 215, '_menu_item_url', ''),
(461, 216, '_menu_item_type', 'post_type'),
(462, 216, '_menu_item_menu_item_parent', '213'),
(463, 216, '_menu_item_object_id', '114'),
(464, 216, '_menu_item_object', 'page'),
(465, 216, '_menu_item_target', ''),
(466, 216, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(467, 216, '_menu_item_xfn', ''),
(468, 216, '_menu_item_url', ''),
(470, 217, '_menu_item_type', 'post_type'),
(471, 217, '_menu_item_menu_item_parent', '213'),
(472, 217, '_menu_item_object_id', '189'),
(473, 217, '_menu_item_object', 'page'),
(474, 217, '_menu_item_target', ''),
(475, 217, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(476, 217, '_menu_item_xfn', ''),
(477, 217, '_menu_item_url', ''),
(479, 218, '_menu_item_type', 'post_type'),
(480, 218, '_menu_item_menu_item_parent', '0'),
(481, 218, '_menu_item_object_id', '115'),
(482, 218, '_menu_item_object', 'page'),
(483, 218, '_menu_item_target', ''),
(484, 218, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(485, 218, '_menu_item_xfn', ''),
(486, 218, '_menu_item_url', ''),
(487, 218, '_menu_item_orphaned', '1576368398'),
(488, 219, '_menu_item_type', 'post_type'),
(489, 219, '_menu_item_menu_item_parent', '0'),
(490, 219, '_menu_item_object_id', '116'),
(491, 219, '_menu_item_object', 'page'),
(492, 219, '_menu_item_target', ''),
(493, 219, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(494, 219, '_menu_item_xfn', ''),
(495, 219, '_menu_item_url', ''),
(496, 219, '_menu_item_orphaned', '1576368398'),
(497, 220, '_menu_item_type', 'post_type'),
(498, 220, '_menu_item_menu_item_parent', '0'),
(499, 220, '_menu_item_object_id', '112'),
(500, 220, '_menu_item_object', 'page'),
(501, 220, '_menu_item_target', ''),
(502, 220, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(503, 220, '_menu_item_xfn', ''),
(504, 220, '_menu_item_url', ''),
(505, 220, '_menu_item_orphaned', '1576368398'),
(506, 221, '_menu_item_type', 'post_type'),
(507, 221, '_menu_item_menu_item_parent', '0'),
(508, 221, '_menu_item_object_id', '177'),
(509, 221, '_menu_item_object', 'page'),
(510, 221, '_menu_item_target', ''),
(511, 221, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(512, 221, '_menu_item_xfn', ''),
(513, 221, '_menu_item_url', ''),
(514, 221, '_menu_item_orphaned', '1576368398'),
(515, 222, '_menu_item_type', 'post_type'),
(516, 222, '_menu_item_menu_item_parent', '213'),
(517, 222, '_menu_item_object_id', '185'),
(518, 222, '_menu_item_object', 'page'),
(519, 222, '_menu_item_target', ''),
(520, 222, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(521, 222, '_menu_item_xfn', ''),
(522, 222, '_menu_item_url', ''),
(524, 223, '_menu_item_type', 'post_type'),
(525, 223, '_menu_item_menu_item_parent', '0'),
(526, 223, '_menu_item_object_id', '109'),
(527, 223, '_menu_item_object', 'page'),
(528, 223, '_menu_item_target', ''),
(529, 223, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(530, 223, '_menu_item_xfn', ''),
(531, 223, '_menu_item_url', ''),
(532, 223, '_menu_item_orphaned', '1576368398'),
(533, 224, '_menu_item_type', 'post_type'),
(534, 224, '_menu_item_menu_item_parent', '0'),
(535, 224, '_menu_item_object_id', '111'),
(536, 224, '_menu_item_object', 'page'),
(537, 224, '_menu_item_target', ''),
(538, 224, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(539, 224, '_menu_item_xfn', ''),
(540, 224, '_menu_item_url', ''),
(541, 224, '_menu_item_orphaned', '1576368398'),
(542, 225, '_menu_item_type', 'post_type'),
(543, 225, '_menu_item_menu_item_parent', '0'),
(544, 225, '_menu_item_object_id', '183'),
(545, 225, '_menu_item_object', 'page'),
(546, 225, '_menu_item_target', ''),
(547, 225, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(548, 225, '_menu_item_xfn', ''),
(549, 225, '_menu_item_url', ''),
(550, 225, '_menu_item_orphaned', '1576368398'),
(551, 226, '_menu_item_type', 'post_type'),
(552, 226, '_menu_item_menu_item_parent', '0'),
(553, 226, '_menu_item_object_id', '113'),
(554, 226, '_menu_item_object', 'page'),
(555, 226, '_menu_item_target', ''),
(556, 226, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(557, 226, '_menu_item_xfn', ''),
(558, 226, '_menu_item_url', ''),
(559, 226, '_menu_item_orphaned', '1576368398'),
(560, 227, '_menu_item_type', 'post_type'),
(561, 227, '_menu_item_menu_item_parent', '213'),
(562, 227, '_menu_item_object_id', '187'),
(563, 227, '_menu_item_object', 'page'),
(564, 227, '_menu_item_target', ''),
(565, 227, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(566, 227, '_menu_item_xfn', ''),
(567, 227, '_menu_item_url', ''),
(569, 228, '_menu_item_type', 'post_type'),
(570, 228, '_menu_item_menu_item_parent', '0'),
(571, 228, '_menu_item_object_id', '110'),
(572, 228, '_menu_item_object', 'page'),
(573, 228, '_menu_item_target', ''),
(574, 228, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(575, 228, '_menu_item_xfn', ''),
(576, 228, '_menu_item_url', ''),
(577, 228, '_menu_item_orphaned', '1576368398'),
(578, 213, '_megamenu', 'a:2:{s:4:\"type\";s:4:\"grid\";s:4:\"grid\";a:1:{i:0;a:2:{s:4:\"meta\";a:4:{s:5:\"class\";s:0:\"\";s:15:\"hide-on-desktop\";s:5:\"false\";s:14:\"hide-on-mobile\";s:5:\"false\";s:7:\"columns\";s:2:\"12\";}s:7:\"columns\";a:3:{i:0;a:2:{s:4:\"meta\";a:4:{s:4:\"span\";s:1:\"3\";s:5:\"class\";s:0:\"\";s:15:\"hide-on-desktop\";s:5:\"false\";s:14:\"hide-on-mobile\";s:5:\"false\";}s:5:\"items\";a:1:{i:0;a:2:{s:2:\"id\";s:3:\"215\";s:4:\"type\";s:4:\"item\";}}}i:1;a:2:{s:4:\"meta\";a:4:{s:4:\"span\";s:1:\"3\";s:5:\"class\";s:0:\"\";s:15:\"hide-on-desktop\";s:5:\"false\";s:14:\"hide-on-mobile\";s:5:\"false\";}s:5:\"items\";a:1:{i:0;a:2:{s:2:\"id\";s:3:\"214\";s:4:\"type\";s:4:\"item\";}}}i:2;a:2:{s:4:\"meta\";a:4:{s:4:\"span\";s:1:\"3\";s:5:\"class\";s:0:\"\";s:15:\"hide-on-desktop\";s:5:\"false\";s:14:\"hide-on-mobile\";s:5:\"false\";}s:5:\"items\";a:1:{i:0;a:2:{s:2:\"id\";s:3:\"216\";s:4:\"type\";s:4:\"item\";}}}}}}}'),
(579, 229, '_menu_item_type', 'post_type'),
(580, 229, '_menu_item_menu_item_parent', '0'),
(581, 229, '_menu_item_object_id', '189'),
(582, 229, '_menu_item_object', 'page'),
(583, 229, '_menu_item_target', ''),
(584, 229, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(585, 229, '_menu_item_xfn', ''),
(586, 229, '_menu_item_url', ''),
(587, 229, '_menu_item_orphaned', '1576371603'),
(597, 231, '_wp_attached_file', '2019/12/bg_home3.jpg'),
(598, 231, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1280;s:4:\"file\";s:20:\"2019/12/bg_home3.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"bg_home3-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"bg_home3-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"bg_home3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"bg_home3-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:22:\"bg_home3-1536x1024.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(599, 232, '_wp_attached_file', '2019/12/logo3.png'),
(600, 232, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:193;s:6:\"height\";i:146;s:4:\"file\";s:17:\"2019/12/logo3.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"logo3-150x146.png\";s:5:\"width\";i:150;s:6:\"height\";i:146;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(601, 233, '_wp_attached_file', '2019/12/logo4.png'),
(602, 233, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:184;s:6:\"height\";i:141;s:4:\"file\";s:17:\"2019/12/logo4.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"logo4-150x141.png\";s:5:\"width\";i:150;s:6:\"height\";i:141;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(603, 234, '_wp_attached_file', '2019/12/logo5.png'),
(604, 234, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:193;s:6:\"height\";i:146;s:4:\"file\";s:17:\"2019/12/logo5.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"logo5-150x146.png\";s:5:\"width\";i:150;s:6:\"height\";i:146;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(605, 235, '_wp_attached_file', '2019/12/logo1.png'),
(606, 235, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:168;s:6:\"height\";i:172;s:4:\"file\";s:17:\"2019/12/logo1.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"logo1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(607, 236, '_wp_attached_file', '2019/12/logo2.png'),
(608, 236, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:183;s:6:\"height\";i:163;s:4:\"file\";s:17:\"2019/12/logo2.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"logo2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(609, 244, '_edit_lock', '1576400021:31'),
(610, 244, '_edit_last', '31'),
(611, 244, '_search-filter-fields', 'a:4:{i:0;a:4:{s:4:\"type\";s:6:\"search\";s:7:\"heading\";s:6:\"Search\";s:11:\"placeholder\";s:10:\"Search …\";s:19:\"accessibility_label\";s:0:\"\";}i:1;a:60:{s:4:\"type\";s:9:\"post_meta\";s:9:\"meta_type\";s:6:\"choice\";s:17:\"number_input_type\";s:12:\"range-slider\";s:17:\"number_is_decimal\";i:1;s:17:\"choice_input_type\";s:8:\"checkbox\";s:15:\"date_input_type\";s:4:\"date\";s:22:\"meta_key_manual_toggle\";i:0;s:9:\"combo_box\";i:0;s:18:\"no_results_message\";s:0:\"\";s:10:\"show_count\";i:1;s:10:\"hide_empty\";i:0;s:10:\"input_type\";s:0:\"\";s:15:\"choice_meta_key\";s:4:\"type\";s:26:\"choice_accessibility_label\";s:0:\"\";s:22:\"choice_get_option_mode\";s:4:\"auto\";s:22:\"choice_order_option_by\";s:5:\"value\";s:23:\"choice_order_option_dir\";s:3:\"asc\";s:24:\"choice_order_option_type\";s:10:\"alphabetic\";s:13:\"choice_is_acf\";i:1;s:24:\"date_accessibility_label\";s:0:\"\";s:13:\"date_meta_key\";s:0:\"\";s:19:\"date_start_meta_key\";s:8:\"category\";s:17:\"date_end_meta_key\";s:0:\"\";s:20:\"date_use_same_toggle\";i:1;s:26:\"number_accessibility_label\";s:0:\"\";s:21:\"number_start_meta_key\";s:8:\"category\";s:19:\"number_end_meta_key\";s:0:\"\";s:22:\"number_use_same_toggle\";i:1;s:7:\"heading\";s:4:\"Type\";s:8:\"meta_key\";s:4:\"type\";s:16:\"date_from_prefix\";s:0:\"\";s:17:\"date_from_postfix\";s:0:\"\";s:21:\"date_from_placeholder\";s:0:\"\";s:14:\"date_to_prefix\";s:0:\"\";s:15:\"date_to_postfix\";s:0:\"\";s:19:\"date_to_placeholder\";s:0:\"\";s:23:\"date_use_dropdown_month\";i:0;s:22:\"date_use_dropdown_year\";i:0;s:14:\"decimal_places\";i:0;s:21:\"number_decimal_places\";i:2;s:18:\"thousand_seperator\";s:0:\"\";s:17:\"decimal_seperator\";s:1:\".\";s:23:\"number_values_seperator\";s:3:\" - \";s:24:\"number_display_values_as\";s:9:\"textinput\";s:23:\"number_display_input_as\";s:11:\"singlefield\";s:16:\"range_min_detect\";i:0;s:16:\"range_max_detect\";i:0;s:9:\"range_min\";s:1:\"0\";s:9:\"range_max\";s:4:\"1000\";s:10:\"range_step\";s:2:\"10\";s:18:\"range_value_prefix\";s:0:\"\";s:19:\"range_value_postfix\";s:0:\"\";s:17:\"date_input_format\";s:9:\"timestamp\";s:17:\"date_compare_mode\";s:9:\"userrange\";s:19:\"number_compare_mode\";s:9:\"userrange\";s:18:\"date_output_format\";s:5:\"d/m/Y\";s:15:\"all_items_label\";s:0:\"\";s:22:\"all_items_label_number\";s:0:\"\";s:8:\"operator\";s:3:\"and\";s:12:\"meta_options\";a:0:{}}i:2;a:60:{s:4:\"type\";s:9:\"post_meta\";s:9:\"meta_type\";s:6:\"choice\";s:17:\"number_input_type\";s:12:\"range-slider\";s:17:\"number_is_decimal\";i:1;s:17:\"choice_input_type\";s:8:\"checkbox\";s:15:\"date_input_type\";s:4:\"date\";s:22:\"meta_key_manual_toggle\";i:0;s:9:\"combo_box\";i:0;s:18:\"no_results_message\";s:0:\"\";s:10:\"show_count\";i:1;s:10:\"hide_empty\";i:0;s:10:\"input_type\";s:0:\"\";s:15:\"choice_meta_key\";s:8:\"category\";s:26:\"choice_accessibility_label\";s:0:\"\";s:22:\"choice_get_option_mode\";s:4:\"auto\";s:22:\"choice_order_option_by\";s:5:\"value\";s:23:\"choice_order_option_dir\";s:3:\"asc\";s:24:\"choice_order_option_type\";s:10:\"alphabetic\";s:13:\"choice_is_acf\";i:1;s:24:\"date_accessibility_label\";s:0:\"\";s:13:\"date_meta_key\";s:0:\"\";s:19:\"date_start_meta_key\";s:8:\"category\";s:17:\"date_end_meta_key\";s:0:\"\";s:20:\"date_use_same_toggle\";i:1;s:26:\"number_accessibility_label\";s:0:\"\";s:21:\"number_start_meta_key\";s:8:\"category\";s:19:\"number_end_meta_key\";s:0:\"\";s:22:\"number_use_same_toggle\";i:1;s:7:\"heading\";s:8:\"Category\";s:8:\"meta_key\";s:8:\"category\";s:16:\"date_from_prefix\";s:0:\"\";s:17:\"date_from_postfix\";s:0:\"\";s:21:\"date_from_placeholder\";s:0:\"\";s:14:\"date_to_prefix\";s:0:\"\";s:15:\"date_to_postfix\";s:0:\"\";s:19:\"date_to_placeholder\";s:0:\"\";s:23:\"date_use_dropdown_month\";i:0;s:22:\"date_use_dropdown_year\";i:0;s:14:\"decimal_places\";i:0;s:21:\"number_decimal_places\";i:2;s:18:\"thousand_seperator\";s:0:\"\";s:17:\"decimal_seperator\";s:1:\".\";s:23:\"number_values_seperator\";s:3:\" - \";s:24:\"number_display_values_as\";s:9:\"textinput\";s:23:\"number_display_input_as\";s:11:\"singlefield\";s:16:\"range_min_detect\";i:0;s:16:\"range_max_detect\";i:0;s:9:\"range_min\";s:1:\"0\";s:9:\"range_max\";s:4:\"1000\";s:10:\"range_step\";s:2:\"10\";s:18:\"range_value_prefix\";s:0:\"\";s:19:\"range_value_postfix\";s:0:\"\";s:17:\"date_input_format\";s:9:\"timestamp\";s:17:\"date_compare_mode\";s:9:\"userrange\";s:19:\"number_compare_mode\";s:9:\"userrange\";s:18:\"date_output_format\";s:5:\"d/m/Y\";s:15:\"all_items_label\";s:0:\"\";s:22:\"all_items_label_number\";s:0:\"\";s:8:\"operator\";s:3:\"and\";s:12:\"meta_options\";a:0:{}}i:3;a:60:{s:4:\"type\";s:9:\"post_meta\";s:9:\"meta_type\";s:6:\"choice\";s:17:\"number_input_type\";s:12:\"range-slider\";s:17:\"number_is_decimal\";i:1;s:17:\"choice_input_type\";s:8:\"checkbox\";s:15:\"date_input_type\";s:4:\"date\";s:22:\"meta_key_manual_toggle\";i:0;s:9:\"combo_box\";i:0;s:18:\"no_results_message\";s:0:\"\";s:10:\"show_count\";i:1;s:10:\"hide_empty\";i:0;s:10:\"input_type\";s:0:\"\";s:15:\"choice_meta_key\";s:6:\"sports\";s:26:\"choice_accessibility_label\";s:0:\"\";s:22:\"choice_get_option_mode\";s:4:\"auto\";s:22:\"choice_order_option_by\";s:5:\"value\";s:23:\"choice_order_option_dir\";s:3:\"asc\";s:24:\"choice_order_option_type\";s:10:\"alphabetic\";s:13:\"choice_is_acf\";i:1;s:24:\"date_accessibility_label\";s:0:\"\";s:13:\"date_meta_key\";s:0:\"\";s:19:\"date_start_meta_key\";s:8:\"category\";s:17:\"date_end_meta_key\";s:0:\"\";s:20:\"date_use_same_toggle\";i:1;s:26:\"number_accessibility_label\";s:0:\"\";s:21:\"number_start_meta_key\";s:8:\"category\";s:19:\"number_end_meta_key\";s:0:\"\";s:22:\"number_use_same_toggle\";i:1;s:7:\"heading\";s:5:\"Sport\";s:8:\"meta_key\";s:6:\"sports\";s:16:\"date_from_prefix\";s:0:\"\";s:17:\"date_from_postfix\";s:0:\"\";s:21:\"date_from_placeholder\";s:0:\"\";s:14:\"date_to_prefix\";s:0:\"\";s:15:\"date_to_postfix\";s:0:\"\";s:19:\"date_to_placeholder\";s:0:\"\";s:23:\"date_use_dropdown_month\";i:0;s:22:\"date_use_dropdown_year\";i:0;s:14:\"decimal_places\";i:0;s:21:\"number_decimal_places\";i:2;s:18:\"thousand_seperator\";s:0:\"\";s:17:\"decimal_seperator\";s:1:\".\";s:23:\"number_values_seperator\";s:3:\" - \";s:24:\"number_display_values_as\";s:9:\"textinput\";s:23:\"number_display_input_as\";s:11:\"singlefield\";s:16:\"range_min_detect\";i:0;s:16:\"range_max_detect\";i:0;s:9:\"range_min\";s:1:\"0\";s:9:\"range_max\";s:4:\"1000\";s:10:\"range_step\";s:2:\"10\";s:18:\"range_value_prefix\";s:0:\"\";s:19:\"range_value_postfix\";s:0:\"\";s:17:\"date_input_format\";s:9:\"timestamp\";s:17:\"date_compare_mode\";s:9:\"userrange\";s:19:\"number_compare_mode\";s:9:\"userrange\";s:18:\"date_output_format\";s:5:\"d/m/Y\";s:15:\"all_items_label\";s:0:\"\";s:22:\"all_items_label_number\";s:0:\"\";s:8:\"operator\";s:3:\"and\";s:12:\"meta_options\";a:0:{}}}'),
(612, 244, '_search-filter-settings', 'a:37:{s:26:\"use_template_manual_toggle\";i:1;s:24:\"enable_taxonomy_archives\";i:0;s:17:\"enable_auto_count\";i:1;s:23:\"auto_count_refresh_mode\";i:1;s:25:\"auto_count_deselect_emtpy\";i:0;s:20:\"template_name_manual\";s:10:\"search.php\";s:9:\"page_slug\";s:0:\"\";s:10:\"post_types\";a:1:{s:10:\"attachment\";i:1;}s:11:\"post_status\";a:1:{s:7:\"publish\";i:1;}s:18:\"settings_post_meta\";a:1:{i:0;a:6:{s:8:\"meta_key\";s:4:\"type\";s:9:\"meta_type\";s:4:\"CHAR\";s:12:\"meta_compare\";s:2:\"ne\";s:10:\"meta_value\";s:0:\"\";s:20:\"meta_date_value_date\";s:0:\"\";s:25:\"meta_date_value_timestamp\";s:0:\"\";}}s:15:\"use_ajax_toggle\";i:1;s:13:\"scroll_to_pos\";s:1:\"0\";s:15:\"pagination_type\";s:6:\"normal\";s:16:\"custom_scroll_to\";s:0:\"\";s:11:\"auto_submit\";i:1;s:18:\"display_results_as\";s:9:\"shortcode\";s:15:\"update_ajax_url\";i:1;s:17:\"only_results_ajax\";i:1;s:11:\"ajax_target\";s:5:\"#main\";s:19:\"ajax_links_selector\";s:13:\".pagination a\";s:25:\"infinite_scroll_container\";s:0:\"\";s:23:\"infinite_scroll_trigger\";s:4:\"-100\";s:28:\"infinite_scroll_result_class\";s:0:\"\";s:27:\"show_infinite_scroll_loader\";i:1;s:16:\"results_per_page\";i:200;s:16:\"exclude_post_ids\";s:0:\"\";s:14:\"field_relation\";s:3:\"and\";s:15:\"default_sort_by\";s:1:\"0\";s:12:\"sticky_posts\";s:0:\"\";s:16:\"default_sort_dir\";s:4:\"desc\";s:16:\"default_meta_key\";s:19:\"field_5de936884495e\";s:17:\"default_sort_type\";s:7:\"numeric\";s:17:\"secondary_sort_by\";s:1:\"0\";s:18:\"secondary_sort_dir\";s:4:\"desc\";s:18:\"secondary_meta_key\";s:19:\"field_5de936884495e\";s:19:\"secondary_sort_type\";s:7:\"numeric\";s:19:\"taxonomies_settings\";a:6:{s:8:\"category\";a:2:{s:15:\"include_exclude\";s:7:\"include\";s:3:\"ids\";s:0:\"\";}s:8:\"post_tag\";a:2:{s:15:\"include_exclude\";s:7:\"include\";s:3:\"ids\";s:0:\"\";}s:11:\"post_format\";a:2:{s:15:\"include_exclude\";s:7:\"include\";s:3:\"ids\";s:0:\"\";}s:6:\"states\";a:2:{s:15:\"include_exclude\";s:7:\"include\";s:3:\"ids\";s:0:\"\";}s:7:\"regions\";a:2:{s:15:\"include_exclude\";s:7:\"include\";s:3:\"ids\";s:0:\"\";}s:4:\"fees\";a:2:{s:15:\"include_exclude\";s:7:\"include\";s:3:\"ids\";s:0:\"\";}}}'),
(614, 245, '_edit_lock', '1577230214:1'),
(615, 245, '_edit_last', '31'),
(616, 244, '_search-filter-results-url', 'http://localhost:8888/ssact/resources/'),
(617, 249, '_edit_lock', '1576448711:31'),
(618, 249, '_edit_last', '31'),
(619, 194, 'category', 'a:5:{i:0;s:16:\"Primary regional\";i:1;s:18:\"Secondary regional\";i:2;s:16:\"College regional\";i:3;s:14:\"Representative\";i:4;s:21:\"Education Directorate\";}'),
(620, 194, '_category', 'field_5df5b52845b46'),
(621, 194, 'type', 'a:8:{i:0;s:11:\"Information\";i:1;s:4:\"Form\";i:2;s:10:\"Governance\";i:3;s:5:\"Guide\";i:4;s:6:\"Policy\";i:5;s:9:\"Procedure\";i:6;s:7:\"Program\";i:7;s:5:\"Venue\";}'),
(622, 194, '_type', 'field_5df5b86d15ba8'),
(623, 193, 'type', 'a:2:{i:0;s:11:\"Information\";i:1;s:6:\"Policy\";}'),
(624, 193, '_type', 'field_5df5b86d15ba8'),
(625, 193, 'category', 'a:5:{i:0;s:16:\"Primary regional\";i:1;s:18:\"Secondary regional\";i:2;s:16:\"College regional\";i:3;s:14:\"Representative\";i:4;s:21:\"Education Directorate\";}'),
(626, 193, '_category', 'field_5df5b52845b46'),
(627, 194, 'sports', 'a:17:{i:0;s:19:\"Australian Football\";i:1;s:8:\"Baseball\";i:2;s:10:\"Basketball\";i:3;s:7:\"Cricket\";i:4;s:13:\"Cross Country\";i:5;s:6:\"Diving\";i:6;s:17:\"Football (Soccer)\";i:7;s:4:\"Golf\";i:8;s:6:\"Hockey\";i:9;s:7:\"Netball\";i:10;s:12:\"Rugby League\";i:11;s:8:\"Softball\";i:12;s:8:\"Swimming\";i:13;s:6:\"Tennis\";i:14;s:5:\"Touch\";i:15;s:15:\"Track and Field\";i:16;s:10:\"Volleyball\";}'),
(628, 194, '_sports', 'field_5df5bc67d8bc8'),
(629, 193, 'sports', 'a:17:{i:0;s:19:\"Australian Football\";i:1;s:8:\"Baseball\";i:2;s:10:\"Basketball\";i:3;s:7:\"Cricket\";i:4;s:13:\"Cross Country\";i:5;s:6:\"Diving\";i:6;s:17:\"Football (Soccer)\";i:7;s:4:\"Golf\";i:8;s:6:\"Hockey\";i:9;s:7:\"Netball\";i:10;s:12:\"Rugby League\";i:11;s:8:\"Softball\";i:12;s:8:\"Swimming\";i:13;s:6:\"Tennis\";i:14;s:5:\"Touch\";i:15;s:15:\"Track and Field\";i:16;s:10:\"Volleyball\";}'),
(630, 193, '_sports', 'field_5df5bc67d8bc8'),
(631, 254, '_menu_item_type', 'post_type'),
(632, 254, '_menu_item_menu_item_parent', '0'),
(633, 254, '_menu_item_object_id', '245'),
(634, 254, '_menu_item_object', 'page'),
(635, 254, '_menu_item_target', ''),
(636, 254, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(637, 254, '_menu_item_xfn', ''),
(638, 254, '_menu_item_url', ''),
(640, 255, '_menu_item_type', 'custom'),
(641, 255, '_menu_item_menu_item_parent', '0'),
(642, 255, '_menu_item_object_id', '255'),
(643, 255, '_menu_item_object', 'custom'),
(644, 255, '_menu_item_target', ''),
(645, 255, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(646, 255, '_menu_item_xfn', ''),
(647, 255, '_menu_item_url', '#'),
(649, 256, '_wp_attached_file', '2019/12/2019-SSACT-Request-for-Inclusion-ACT-Cross-Country.doc'),
(650, 257, '_wp_attached_file', '2019/12/Event-Total-Participation-Numbers.xlsx'),
(651, 258, '_wp_attached_file', '2019/12/Cross-Country-Championship-Required-Officials.docx'),
(652, 259, '_wp_attached_file', '2019/12/Cross-Country-Entry-Form.xlsx'),
(653, 260, '_wp_attached_file', '2019/12/Stromlo-Forest-Park-Course-Maps.docx'),
(654, 261, '_wp_attached_file', '2019/12/ACT-Combined-Cross-Country-Championship-Program.docx'),
(655, 262, '_wp_attached_file', '2019/12/Woden-Park-Conditions-of-Hire-Active-Canberra.pdf'),
(656, 262, '_wp_attachment_metadata', 'a:1:{s:5:\"sizes\";a:4:{s:4:\"full\";a:4:{s:4:\"file\";s:53:\"Woden-Park-Conditions-of-Hire-Active-Canberra-pdf.jpg\";s:5:\"width\";i:1058;s:6:\"height\";i:1497;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"Woden-Park-Conditions-of-Hire-Active-Canberra-pdf-212x300.jpg\";s:5:\"width\";i:212;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"Woden-Park-Conditions-of-Hire-Active-Canberra-pdf-724x1024.jpg\";s:5:\"width\";i:724;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"Woden-Park-Conditions-of-Hire-Active-Canberra-pdf-106x150.jpg\";s:5:\"width\";i:106;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}}'),
(657, 263, '_wp_attached_file', '2019/12/ACT-Cross-Country-Championship-Info-Consent-Form.docx'),
(658, 264, '_wp_attached_file', '2019/12/Regional-and-ACT-Cross-Country-Event-Coordinator-Checklist.pdf'),
(659, 264, '_wp_attachment_metadata', 'a:1:{s:5:\"sizes\";a:4:{s:4:\"full\";a:4:{s:4:\"file\";s:66:\"Regional-and-ACT-Cross-Country-Event-Coordinator-Checklist-pdf.jpg\";s:5:\"width\";i:1088;s:6:\"height\";i:1408;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:74:\"Regional-and-ACT-Cross-Country-Event-Coordinator-Checklist-pdf-232x300.jpg\";s:5:\"width\";i:232;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:75:\"Regional-and-ACT-Cross-Country-Event-Coordinator-Checklist-pdf-791x1024.jpg\";s:5:\"width\";i:791;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:74:\"Regional-and-ACT-Cross-Country-Event-Coordinator-Checklist-pdf-116x150.jpg\";s:5:\"width\";i:116;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}}'),
(660, 265, '_wp_attached_file', '2019/12/SSACT-Cross-Country-Age-Eligibility-Event-Specifications-and-Qualifying-Numbers.pdf'),
(661, 265, '_wp_attachment_metadata', 'a:1:{s:5:\"sizes\";a:4:{s:4:\"full\";a:4:{s:4:\"file\";s:87:\"SSACT-Cross-Country-Age-Eligibility-Event-Specifications-and-Qualifying-Numbers-pdf.jpg\";s:5:\"width\";i:1497;s:6:\"height\";i:1058;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:95:\"SSACT-Cross-Country-Age-Eligibility-Event-Specifications-and-Qualifying-Numbers-pdf-300x212.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:212;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:96:\"SSACT-Cross-Country-Age-Eligibility-Event-Specifications-and-Qualifying-Numbers-pdf-1024x724.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:724;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:95:\"SSACT-Cross-Country-Age-Eligibility-Event-Specifications-and-Qualifying-Numbers-pdf-150x106.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:106;s:9:\"mime-type\";s:10:\"image/jpeg\";}}}'),
(662, 266, '_wp_attached_file', '2019/12/AIS-Emergency-Procedures.pdf'),
(663, 266, '_wp_attachment_metadata', 'a:1:{s:5:\"sizes\";a:4:{s:4:\"full\";a:4:{s:4:\"file\";s:32:\"AIS-Emergency-Procedures-pdf.jpg\";s:5:\"width\";i:1848;s:6:\"height\";i:953;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:40:\"AIS-Emergency-Procedures-pdf-300x155.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:155;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:41:\"AIS-Emergency-Procedures-pdf-1024x528.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:528;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:39:\"AIS-Emergency-Procedures-pdf-150x77.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:77;s:9:\"mime-type\";s:10:\"image/jpeg\";}}}'),
(664, 267, '_wp_attached_file', '2019/12/AIS-Aquatic-Centre-Risk-Management-Plan.pdf'),
(665, 267, '_wp_attachment_metadata', 'a:1:{s:5:\"sizes\";a:4:{s:4:\"full\";a:4:{s:4:\"file\";s:47:\"AIS-Aquatic-Centre-Risk-Management-Plan-pdf.jpg\";s:5:\"width\";i:1497;s:6:\"height\";i:1058;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:55:\"AIS-Aquatic-Centre-Risk-Management-Plan-pdf-300x212.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:212;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:56:\"AIS-Aquatic-Centre-Risk-Management-Plan-pdf-1024x724.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:724;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:55:\"AIS-Aquatic-Centre-Risk-Management-Plan-pdf-150x106.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:106;s:9:\"mime-type\";s:10:\"image/jpeg\";}}}'),
(666, 268, '_wp_attached_file', '2019/12/ACT-Championship-Record-Holder-Pennant-Script.pdf'),
(667, 268, '_wp_attachment_metadata', 'a:1:{s:5:\"sizes\";a:4:{s:4:\"full\";a:4:{s:4:\"file\";s:53:\"ACT-Championship-Record-Holder-Pennant-Script-pdf.jpg\";s:5:\"width\";i:1058;s:6:\"height\";i:1497;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"ACT-Championship-Record-Holder-Pennant-Script-pdf-212x300.jpg\";s:5:\"width\";i:212;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"ACT-Championship-Record-Holder-Pennant-Script-pdf-724x1024.jpg\";s:5:\"width\";i:724;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"ACT-Championship-Record-Holder-Pennant-Script-pdf-106x150.jpg\";s:5:\"width\";i:106;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}}'),
(668, 268, '_edit_lock', '1576386942:31'),
(669, 268, 'type', 'a:1:{i:0;s:11:\"Information\";}'),
(670, 268, '_type', 'field_5df5b86d15ba8'),
(671, 268, 'category', ''),
(672, 268, '_category', 'field_5df5b52845b46'),
(673, 268, 'sports', 'a:2:{i:0;s:8:\"Swimming\";i:1;s:15:\"Track and Field\";}'),
(674, 268, '_sports', 'field_5df5bc67d8bc8'),
(675, 268, '_edit_last', '31'),
(676, 267, 'type', 'a:2:{i:0;s:11:\"Information\";i:1;s:5:\"Venue\";}'),
(677, 267, '_type', 'field_5df5b86d15ba8'),
(678, 267, 'category', ''),
(679, 267, '_category', 'field_5df5b52845b46'),
(680, 267, 'sports', 'a:1:{i:0;s:8:\"Swimming\";}'),
(681, 267, '_sports', 'field_5df5bc67d8bc8'),
(682, 266, 'type', 'a:2:{i:0;s:11:\"Information\";i:1;s:5:\"Venue\";}'),
(683, 266, '_type', 'field_5df5b86d15ba8'),
(684, 266, 'category', 'a:3:{i:0;s:16:\"Primary regional\";i:1;s:18:\"Secondary regional\";i:2;s:16:\"College regional\";}'),
(685, 266, '_category', 'field_5df5b52845b46'),
(686, 266, 'sports', 'a:2:{i:0;s:8:\"Swimming\";i:1;s:15:\"Track and Field\";}'),
(687, 266, '_sports', 'field_5df5bc67d8bc8'),
(688, 265, 'type', ''),
(689, 265, '_type', 'field_5df5b86d15ba8'),
(690, 265, 'category', 'a:3:{i:0;s:16:\"Primary regional\";i:1;s:18:\"Secondary regional\";i:2;s:16:\"College regional\";}'),
(691, 265, '_category', 'field_5df5b52845b46'),
(692, 265, 'sports', 'a:1:{i:0;s:13:\"Cross Country\";}'),
(693, 265, '_sports', 'field_5df5bc67d8bc8'),
(694, 264, 'type', 'a:2:{i:0;s:11:\"Information\";i:1;s:5:\"Guide\";}'),
(695, 264, '_type', 'field_5df5b86d15ba8'),
(696, 264, 'category', 'a:3:{i:0;s:16:\"Primary regional\";i:1;s:18:\"Secondary regional\";i:2;s:16:\"College regional\";}'),
(697, 264, '_category', 'field_5df5b52845b46'),
(698, 264, 'sports', 'a:1:{i:0;s:13:\"Cross Country\";}'),
(699, 264, '_sports', 'field_5df5bc67d8bc8'),
(700, 263, 'type', 'a:1:{i:0;s:4:\"Form\";}'),
(701, 263, '_type', 'field_5df5b86d15ba8'),
(702, 263, 'category', 'a:3:{i:0;s:16:\"Primary regional\";i:1;s:18:\"Secondary regional\";i:2;s:16:\"College regional\";}'),
(703, 263, '_category', 'field_5df5b52845b46'),
(704, 263, 'sports', 'a:1:{i:0;s:13:\"Cross Country\";}'),
(705, 263, '_sports', 'field_5df5bc67d8bc8'),
(706, 262, 'type', 'a:2:{i:0;s:11:\"Information\";i:1;s:5:\"Venue\";}'),
(707, 262, '_type', 'field_5df5b86d15ba8'),
(708, 262, 'category', 'a:3:{i:0;s:16:\"Primary regional\";i:1;s:18:\"Secondary regional\";i:2;s:16:\"College regional\";}'),
(709, 262, '_category', 'field_5df5b52845b46'),
(710, 262, 'sports', 'a:1:{i:0;s:15:\"Track and Field\";}'),
(711, 262, '_sports', 'field_5df5bc67d8bc8'),
(712, 260, 'type', 'a:2:{i:0;s:11:\"Information\";i:1;s:5:\"Venue\";}'),
(713, 260, '_type', 'field_5df5b86d15ba8'),
(714, 260, 'category', 'a:3:{i:0;s:16:\"Primary regional\";i:1;s:18:\"Secondary regional\";i:2;s:16:\"College regional\";}'),
(715, 260, '_category', 'field_5df5b52845b46'),
(716, 260, 'sports', 'a:1:{i:0;s:13:\"Cross Country\";}'),
(717, 260, '_sports', 'field_5df5bc67d8bc8'),
(718, 261, 'type', 'a:2:{i:0;s:11:\"Information\";i:1;s:7:\"Program\";}'),
(719, 261, '_type', 'field_5df5b86d15ba8'),
(720, 261, 'category', 'a:3:{i:0;s:16:\"Primary regional\";i:1;s:18:\"Secondary regional\";i:2;s:16:\"College regional\";}'),
(721, 261, '_category', 'field_5df5b52845b46'),
(722, 261, 'sports', 'a:1:{i:0;s:13:\"Cross Country\";}'),
(723, 261, '_sports', 'field_5df5bc67d8bc8'),
(724, 259, 'type', 'a:1:{i:0;s:4:\"Form\";}'),
(725, 259, '_type', 'field_5df5b86d15ba8'),
(726, 259, 'category', 'a:3:{i:0;s:16:\"Primary regional\";i:1;s:18:\"Secondary regional\";i:2;s:16:\"College regional\";}'),
(727, 259, '_category', 'field_5df5b52845b46'),
(728, 259, 'sports', 'a:1:{i:0;s:13:\"Cross Country\";}'),
(729, 259, '_sports', 'field_5df5bc67d8bc8'),
(730, 257, 'type', 'a:1:{i:0;s:4:\"Form\";}'),
(731, 257, '_type', 'field_5df5b86d15ba8'),
(732, 257, 'category', 'a:3:{i:0;s:16:\"Primary regional\";i:1;s:18:\"Secondary regional\";i:2;s:16:\"College regional\";}'),
(733, 257, '_category', 'field_5df5b52845b46'),
(734, 257, 'sports', 'a:1:{i:0;s:13:\"Cross Country\";}'),
(735, 257, '_sports', 'field_5df5bc67d8bc8'),
(736, 258, 'type', 'a:1:{i:0;s:11:\"Information\";}'),
(737, 258, '_type', 'field_5df5b86d15ba8'),
(738, 258, 'category', 'a:3:{i:0;s:16:\"Primary regional\";i:1;s:18:\"Secondary regional\";i:2;s:16:\"College regional\";}'),
(739, 258, '_category', 'field_5df5b52845b46'),
(740, 258, 'sports', 'a:1:{i:0;s:13:\"Cross Country\";}'),
(741, 258, '_sports', 'field_5df5bc67d8bc8'),
(742, 256, 'type', 'a:1:{i:0;s:4:\"Form\";}'),
(743, 256, '_type', 'field_5df5b86d15ba8'),
(744, 256, 'category', 'a:3:{i:0;s:16:\"Primary regional\";i:1;s:18:\"Secondary regional\";i:2;s:16:\"College regional\";}'),
(745, 256, '_category', 'field_5df5b52845b46'),
(746, 256, 'sports', 'a:1:{i:0;s:13:\"Cross Country\";}'),
(747, 256, '_sports', 'field_5df5bc67d8bc8'),
(748, 273, '_wp_attached_file', '2019/12/216_SSACT_Web_Report_02v4.pdf'),
(749, 273, '_edit_lock', '1576574020:31'),
(750, 273, 'type', 'a:2:{i:0;s:10:\"Governance\";i:1;s:5:\"Guide\";}'),
(751, 273, '_type', 'field_5df5b86d15ba8'),
(752, 273, 'category', 'a:1:{i:0;s:16:\"Primary regional\";}'),
(753, 273, '_category', 'field_5df5b52845b46'),
(754, 273, 'sports', 'a:17:{i:0;s:19:\"Australian Football\";i:1;s:8:\"Baseball\";i:2;s:10:\"Basketball\";i:3;s:7:\"Cricket\";i:4;s:13:\"Cross Country\";i:5;s:6:\"Diving\";i:6;s:17:\"Football (Soccer)\";i:7;s:4:\"Golf\";i:8;s:6:\"Hockey\";i:9;s:7:\"Netball\";i:10;s:12:\"Rugby League\";i:11;s:8:\"Softball\";i:12;s:8:\"Swimming\";i:13;s:6:\"Tennis\";i:14;s:5:\"Touch\";i:15;s:15:\"Track and Field\";i:16;s:10:\"Volleyball\";}'),
(755, 273, '_sports', 'field_5df5bc67d8bc8'),
(756, 273, '_edit_last', '31'),
(757, 272, '_edit_lock', '1576457748:1'),
(758, 272, '_edit_last', '1'),
(759, 276, '_edit_lock', '1576463542:1'),
(760, 276, '_edit_last', '1'),
(763, 282, '_wp_attached_file', '2019/12/reg_parents.jpg'),
(764, 282, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1283;s:4:\"file\";s:23:\"2019/12/reg_parents.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"reg_parents-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:24:\"reg_parents-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"reg_parents-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"reg_parents-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:25:\"reg_parents-1536x1026.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1026;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(765, 272, 'registration_0_title', 'Register as a Parent'),
(766, 272, '_registration_0_title', 'field_5df6bc7b94e68'),
(767, 272, 'registration_0_image', '282'),
(768, 272, '_registration_0_image', 'field_5df6bc8294e69'),
(769, 272, 'registration_0_link', '109'),
(770, 272, '_registration_0_link', 'field_5df6bccd94e6a'),
(771, 272, 'registration', '2'),
(772, 272, '_registration', 'field_5df6bc5794e67'),
(773, 283, 'registration_0_title', 'Register as a parent'),
(774, 283, '_registration_0_title', 'field_5df6bc7b94e68'),
(775, 283, 'registration_0_image', '282'),
(776, 283, '_registration_0_image', 'field_5df6bc8294e69'),
(777, 283, 'registration_0_link', ''),
(778, 283, '_registration_0_link', 'field_5df6bccd94e6a'),
(779, 283, 'registration', '1'),
(780, 283, '_registration', 'field_5df6bc5794e67'),
(781, 287, 'registration_0_title', 'Register as a parent'),
(782, 287, '_registration_0_title', 'field_5df6bc7b94e68'),
(783, 287, 'registration_0_image', '282'),
(784, 287, '_registration_0_image', 'field_5df6bc8294e69'),
(785, 287, 'registration_0_link', ''),
(786, 287, '_registration_0_link', 'field_5df6bccd94e6a'),
(787, 287, 'registration', '1'),
(788, 287, '_registration', 'field_5df6bc5794e67'),
(789, 288, 'registration_0_title', 'Register as a parent'),
(790, 288, '_registration_0_title', 'field_5df6bc7b94e68'),
(791, 288, 'registration_0_image', '282'),
(792, 288, '_registration_0_image', 'field_5df6bc8294e69'),
(793, 288, 'registration_0_link', '109'),
(794, 288, '_registration_0_link', 'field_5df6bccd94e6a'),
(795, 288, 'registration', '1'),
(796, 288, '_registration', 'field_5df6bc5794e67'),
(797, 289, '_wp_attached_file', '2019/12/reg_teams.jpg'),
(798, 289, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1280;s:4:\"file\";s:21:\"2019/12/reg_teams.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"reg_teams-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"reg_teams-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"reg_teams-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"reg_teams-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:23:\"reg_teams-1536x1024.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(799, 272, 'registration_1_title', 'Register as Team Official'),
(800, 272, '_registration_1_title', 'field_5df6bc7b94e68'),
(801, 272, 'registration_1_image', '289'),
(802, 272, '_registration_1_image', 'field_5df6bc8294e69'),
(803, 272, 'registration_1_link', '110'),
(804, 272, '_registration_1_link', 'field_5df6bccd94e6a'),
(805, 290, 'registration_0_title', 'Register as a parent'),
(806, 290, '_registration_0_title', 'field_5df6bc7b94e68'),
(807, 290, 'registration_0_image', '282'),
(808, 290, '_registration_0_image', 'field_5df6bc8294e69'),
(809, 290, 'registration_0_link', '109'),
(810, 290, '_registration_0_link', 'field_5df6bccd94e6a'),
(811, 290, 'registration', '2'),
(812, 290, '_registration', 'field_5df6bc5794e67'),
(813, 290, 'registration_1_title', 'Register as Team Official'),
(814, 290, '_registration_1_title', 'field_5df6bc7b94e68'),
(815, 290, 'registration_1_image', '289'),
(816, 290, '_registration_1_image', 'field_5df6bc8294e69'),
(817, 290, 'registration_1_link', '110'),
(818, 290, '_registration_1_link', 'field_5df6bccd94e6a'),
(819, 291, 'registration_0_title', 'Register as a Parent'),
(820, 291, '_registration_0_title', 'field_5df6bc7b94e68'),
(821, 291, 'registration_0_image', '282'),
(822, 291, '_registration_0_image', 'field_5df6bc8294e69'),
(823, 291, 'registration_0_link', '109'),
(824, 291, '_registration_0_link', 'field_5df6bccd94e6a'),
(825, 291, 'registration', '2'),
(826, 291, '_registration', 'field_5df6bc5794e67'),
(827, 291, 'registration_1_title', 'Register as Team Official'),
(828, 291, '_registration_1_title', 'field_5df6bc7b94e68'),
(829, 291, 'registration_1_image', '289'),
(830, 291, '_registration_1_image', 'field_5df6bc8294e69'),
(831, 291, 'registration_1_link', '110'),
(832, 291, '_registration_1_link', 'field_5df6bccd94e6a'),
(833, 294, '_edit_lock', '1576461194:1'),
(834, 294, '_edit_last', '1'),
(835, 295, '_edit_lock', '1576779887:1'),
(836, 295, '_edit_last', '1'),
(837, 295, '_thumbnail_id', '289'),
(838, 296, '_wp_attached_file', '2019/12/bg_regch.jpg'),
(839, 296, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:847;s:4:\"file\";s:20:\"2019/12/bg_regch.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"bg_regch-300x132.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:132;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"bg_regch-1024x452.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:452;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"bg_regch-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"bg_regch-768x339.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:339;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:21:\"bg_regch-1536x678.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:678;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(840, 297, '_wp_attached_file', '2019/12/bg_helpevnt.jpg'),
(841, 297, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:847;s:4:\"file\";s:23:\"2019/12/bg_helpevnt.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"bg_helpevnt-300x132.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:132;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:24:\"bg_helpevnt-1024x452.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:452;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"bg_helpevnt-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"bg_helpevnt-768x339.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:339;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:24:\"bg_helpevnt-1536x678.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:678;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(842, 298, '_edit_lock', '1576689450:1'),
(843, 298, '_edit_last', '1'),
(844, 298, '_wp_page_template', 'page-dashboard.php'),
(846, 303, '_edit_lock', '1577543278:1'),
(847, 303, '_edit_last', '1');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(848, 303, '_wp_page_template', 'page-dashboard.php'),
(849, 306, '_edit_lock', '1576696913:1'),
(850, 306, '_edit_last', '1'),
(851, 307, '_edit_lock', '1577323634:31'),
(852, 307, '_edit_last', '31'),
(853, 313, '_wp_attached_file', '2019/12/Product-37.jpg'),
(854, 313, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:900;s:6:\"height\";i:466;s:4:\"file\";s:22:\"2019/12/Product-37.jpg\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"Product-37-300x155.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:155;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"Product-37-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"Product-37-768x398.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:398;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:8:\"Accounts\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1464944042\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(855, 306, 'description', 'SSACT 12&U Basketball Jersey'),
(856, 306, '_description', 'field_5dfa7b0794a46'),
(857, 306, 'price', '43.00'),
(858, 306, '_price', 'field_5dfa7afc94a45'),
(859, 306, 'available_sizes', '4, 6, 8, 10, 12, 14, 16'),
(860, 306, '_available_sizes', 'field_5dfa7b1694a47'),
(861, 306, 'image', '313'),
(862, 306, '_image', 'field_5dfa7b6994a48'),
(863, 306, 'enabled', ''),
(864, 306, '_enabled', 'field_5dfa7b7f94a49'),
(865, 314, '_edit_lock', '1577074114:1'),
(866, 314, '_edit_last', '1'),
(867, 168, 'description', 'Home and Away Playing Jerseys, Playing Shorts and Long Playing Socks'),
(868, 168, '_description', 'field_5dfa7c1dd5e14'),
(869, 168, 'products', 'a:1:{i:0;s:3:\"306\";}'),
(870, 168, '_products', 'field_5dfa7c52d5e17'),
(871, 168, 'active', '0'),
(872, 168, '_active', 'field_5dfa7c3ed5e16'),
(873, 318, '_edit_lock', '1577323565:31'),
(874, 318, '_edit_last', '1'),
(875, 319, '_wp_attached_file', '2019/12/Product-32.jpg'),
(876, 319, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:520;s:6:\"height\";i:346;s:4:\"file\";s:22:\"2019/12/Product-32.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"Product-32-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"Product-32-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:8:\"Accounts\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1483532740\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(877, 318, 'description', 'SSACT Boys Hockey Playing Shorts'),
(878, 318, '_description', 'field_5dfa7b0794a46'),
(879, 318, 'price', '38.00'),
(880, 318, '_price', 'field_5dfa7afc94a45'),
(881, 318, 'available_sizes', '8, 10, 12, 14, 16, S, M, L, XL, XXL'),
(882, 318, '_available_sizes', 'field_5dfa7b1694a47'),
(883, 318, 'image', '319'),
(884, 318, '_image', 'field_5dfa7b6994a48'),
(885, 318, 'active', '1'),
(886, 318, '_active', 'field_5dfa7b7f94a49'),
(888, 322, '_wp_attached_file', '2019/12/SiteContentFiles-_School-Sport-ACT-Calendar.xlsx'),
(889, 323, '_edit_lock', '1576781804:1'),
(890, 323, '_edit_last', '1'),
(891, 327, '_edit_lock', '1577580147:1'),
(892, 327, '_edit_last', '1'),
(893, 336, '_edit_lock', '1577052225:1'),
(894, 337, '_edit_lock', '1577062514:1'),
(895, 339, '_edit_lock', '1577067440:1'),
(896, 339, '_edit_last', '1'),
(897, 341, '_edit_lock', '1577067600:1'),
(898, 339, '_wp_trash_meta_status', 'publish'),
(899, 339, '_wp_trash_meta_time', '1577067614'),
(900, 339, '_wp_desired_post_slug', 'group_5e0023c28b27a'),
(901, 340, '_wp_trash_meta_status', 'publish'),
(902, 340, '_wp_trash_meta_time', '1577067614'),
(903, 340, '_wp_desired_post_slug', 'field_5e0023e03036a'),
(904, 343, '_edit_lock', '1577116337:1'),
(905, 343, '_edit_last', '1'),
(906, 343, 'description', ''),
(907, 343, '_description', 'field_5dfa7c1dd5e14'),
(908, 343, 'products', 'a:2:{i:0;s:3:\"318\";i:1;s:3:\"306\";}'),
(909, 343, '_products', 'field_5dfa7c52d5e17'),
(910, 343, 'active', '0'),
(911, 343, '_active', 'field_5dfa7c3ed5e16'),
(912, 144, '_wp_trash_meta_status', 'publish'),
(913, 144, '_wp_trash_meta_time', '1577116521'),
(914, 144, '_wp_desired_post_slug', 'sda'),
(915, 344, 'year', '2019'),
(916, 344, '_year', 'field_5dffdaf957166'),
(917, 344, 'sport', '166'),
(918, 344, '_sport', 'field_5dffdb0557167'),
(919, 344, 'age', '11'),
(920, 344, '_age', 'field_5dffdb2157168'),
(921, 344, 'date_birth_restriction', '12/04/2019'),
(922, 344, '_date_birth_restriction', 'field_5dffdb6b5716a'),
(923, 344, 'date_of_trial', '12/17/2019'),
(924, 344, '_date_of_trial', 'field_5dffdb855716b'),
(925, 344, 'eligibility', 'dsadsa'),
(926, 344, '_eligibility', 'field_5dffe7185716c'),
(927, 344, 'championship_date', '12/05/2019'),
(928, 344, '_championship_date', 'field_5dffe71d5716d'),
(929, 345, 'year', '2019'),
(930, 345, '_year', 'field_5dffdaf957166'),
(931, 345, 'sport', '164'),
(932, 345, '_sport', 'field_5dffdb0557167'),
(933, 345, 'age', '111'),
(934, 345, '_age', 'field_5dffdb2157168'),
(935, 345, 'date_birth_restriction', '12/12/2019'),
(936, 345, '_date_birth_restriction', 'field_5dffdb6b5716a'),
(937, 345, 'date_of_trial', '12/02/2019'),
(938, 345, '_date_of_trial', 'field_5dffdb855716b'),
(939, 345, 'eligibility', 'dsadsa'),
(940, 345, '_eligibility', 'field_5dffe7185716c'),
(941, 345, 'championship_date', '12/04/2019'),
(942, 345, '_championship_date', 'field_5dffe71d5716d'),
(943, 346, 'year', '2019'),
(944, 346, '_year', 'field_5dffdaf957166'),
(945, 346, 'sport', '165'),
(946, 346, '_sport', 'field_5dffdb0557167'),
(947, 346, 'age', '111'),
(948, 346, '_age', 'field_5dffdb2157168'),
(949, 346, 'date_birth_restriction', '12/11/2019'),
(950, 346, '_date_birth_restriction', 'field_5dffdb6b5716a'),
(951, 346, 'date_of_trial', '12/02/2019'),
(952, 346, '_date_of_trial', 'field_5dffdb855716b'),
(953, 346, 'eligibility', 'dsadsa'),
(954, 346, '_eligibility', 'field_5dffe7185716c'),
(955, 346, 'championship_date', '12/11/2019'),
(956, 346, '_championship_date', 'field_5dffe71d5716d'),
(957, 347, 'year', '2019'),
(958, 347, '_year', 'field_5dffdaf957166'),
(959, 347, 'sport', '166'),
(960, 347, '_sport', 'field_5dffdb0557167'),
(961, 347, 'age', '111'),
(962, 347, '_age', 'field_5dffdb2157168'),
(963, 347, 'date_birth_restriction', '12/05/2019'),
(964, 347, '_date_birth_restriction', 'field_5dffdb6b5716a'),
(965, 347, 'date_of_trial', '12/10/2019'),
(966, 347, '_date_of_trial', 'field_5dffdb855716b'),
(967, 347, 'eligibility', 'adsadsa'),
(968, 347, '_eligibility', 'field_5dffe7185716c'),
(969, 347, 'championship_date', '12/05/2019'),
(970, 347, '_championship_date', 'field_5dffe71d5716d'),
(971, 348, 'year', '2019'),
(972, 348, '_year', 'field_5dffdaf957166'),
(973, 348, 'sport', '165'),
(974, 348, '_sport', 'field_5dffdb0557167'),
(975, 348, 'age', '111'),
(976, 348, '_age', 'field_5dffdb2157168'),
(977, 348, 'date_birth_restriction', '12/04/2019'),
(978, 348, '_date_birth_restriction', 'field_5dffdb6b5716a'),
(979, 348, 'date_of_trial', '12/02/2019'),
(980, 348, '_date_of_trial', 'field_5dffdb855716b'),
(981, 348, 'eligibility', 'dsadsa'),
(982, 348, '_eligibility', 'field_5dffe7185716c'),
(983, 348, 'championship_date', '12/04/2019'),
(984, 348, '_championship_date', 'field_5dffe71d5716d'),
(985, 349, 'year', '2019'),
(986, 349, '_year', 'field_5dffdaf957166'),
(987, 349, 'sport', '166'),
(988, 349, '_sport', 'field_5dffdb0557167'),
(989, 349, 'age', '111'),
(990, 349, '_age', 'field_5dffdb2157168'),
(991, 349, 'date_birth_restriction', '12/03/2019'),
(992, 349, '_date_birth_restriction', 'field_5dffdb6b5716a'),
(993, 349, 'date_of_trial', '12/17/2019'),
(994, 349, '_date_of_trial', 'field_5dffdb855716b'),
(995, 349, 'eligibility', 'dsadsa'),
(996, 349, '_eligibility', 'field_5dffe7185716c'),
(997, 349, 'championship_date', '12/04/2019'),
(998, 349, '_championship_date', 'field_5dffe71d5716d'),
(999, 350, 'year', '2019'),
(1000, 350, '_year', 'field_5dffdaf957166'),
(1001, 350, 'sport', '162'),
(1002, 350, '_sport', 'field_5dffdb0557167'),
(1003, 350, 'age', '111'),
(1004, 350, '_age', 'field_5dffdb2157168'),
(1005, 350, 'date_birth_restriction', '12/05/2019'),
(1006, 350, '_date_birth_restriction', 'field_5dffdb6b5716a'),
(1007, 350, 'date_of_trial', '12/10/2019'),
(1008, 350, '_date_of_trial', 'field_5dffdb855716b'),
(1009, 350, 'eligibility', 'sdadsa'),
(1010, 350, '_eligibility', 'field_5dffe7185716c'),
(1011, 350, 'championship_date', '12/11/2019'),
(1012, 350, '_championship_date', 'field_5dffe71d5716d'),
(1013, 351, 'year', '2019'),
(1014, 351, '_year', 'field_5dffdaf957166'),
(1015, 351, 'sport', '165'),
(1016, 351, '_sport', 'field_5dffdb0557167'),
(1017, 351, 'age', '111'),
(1018, 351, '_age', 'field_5dffdb2157168'),
(1019, 351, 'date_birth_restriction', '12/05/2019'),
(1020, 351, '_date_birth_restriction', 'field_5dffdb6b5716a'),
(1021, 351, 'date_of_trial', '12/02/2019'),
(1022, 351, '_date_of_trial', 'field_5dffdb855716b'),
(1023, 351, 'eligibility', 'dsadsa'),
(1024, 351, '_eligibility', 'field_5dffe7185716c'),
(1025, 351, 'championship_date', '12/03/2019'),
(1026, 351, '_championship_date', 'field_5dffe71d5716d'),
(1027, 352, 'year', '2019'),
(1028, 352, '_year', 'field_5dffdaf957166'),
(1029, 352, 'sport', '165'),
(1030, 352, '_sport', 'field_5dffdb0557167'),
(1031, 352, 'age', '111'),
(1032, 352, '_age', 'field_5dffdb2157168'),
(1033, 352, 'date_birth_restriction', '12/03/2019'),
(1034, 352, '_date_birth_restriction', 'field_5dffdb6b5716a'),
(1035, 352, 'date_of_trial', '01/01/2020'),
(1036, 352, '_date_of_trial', 'field_5dffdb855716b'),
(1037, 352, 'eligibility', 'dsadsa'),
(1038, 352, '_eligibility', 'field_5dffe7185716c'),
(1039, 352, 'championship_date', '12/04/2019'),
(1040, 352, '_championship_date', 'field_5dffe71d5716d'),
(1041, 353, 'year', '2019'),
(1042, 353, '_year', 'field_5dffdaf957166'),
(1043, 353, 'sport', '160'),
(1044, 353, '_sport', 'field_5dffdb0557167'),
(1045, 353, 'age', '111'),
(1046, 353, '_age', 'field_5dffdb2157168'),
(1047, 353, 'date_birth_restriction', '12/04/2019'),
(1048, 353, '_date_birth_restriction', 'field_5dffdb6b5716a'),
(1049, 353, 'date_of_trial', '12/10/2019'),
(1050, 353, '_date_of_trial', 'field_5dffdb855716b'),
(1051, 353, 'eligibility', 'dsadsa'),
(1052, 353, '_eligibility', 'field_5dffe7185716c'),
(1053, 353, 'championship_date', '12/10/2019'),
(1054, 353, '_championship_date', 'field_5dffe71d5716d'),
(1291, 368, 'year', '2019'),
(1292, 368, '_year', 'field_5dffdaf957166'),
(1293, 368, 'sport', '163'),
(1294, 368, '_sport', 'field_5dffdb0557167'),
(1295, 368, 'age', '444'),
(1296, 368, '_age', 'field_5dffdb2157168'),
(1297, 368, 'date_birth_restriction', '12/12/2019'),
(1298, 368, '_date_birth_restriction', 'field_5dffdb6b5716a'),
(1299, 368, 'date_of_trial', '12/10/2019'),
(1300, 368, '_date_of_trial', 'field_5dffdb855716b'),
(1301, 368, 'eligibility', 'dsadsa'),
(1302, 368, '_eligibility', 'field_5dffe7185716c'),
(1303, 368, 'championship_date', '12/04/2019'),
(1304, 368, '_championship_date', 'field_5dffe71d5716d'),
(1305, 368, 'product_set', 'a:2:{i:0;s:3:\"168\";i:1;s:3:\"167\";}'),
(1306, 368, '_product_set', 'field_5e0022168380e'),
(1321, 370, 'year', '2019'),
(1322, 370, '_year', 'field_5dffdaf957166'),
(1323, 370, 'sport', '164'),
(1324, 370, '_sport', 'field_5dffdb0557167'),
(1325, 370, 'age', '111'),
(1326, 370, '_age', 'field_5dffdb2157168'),
(1327, 370, 'date_birth_restriction', '12/06/2019'),
(1328, 370, '_date_birth_restriction', 'field_5dffdb6b5716a'),
(1329, 370, 'date_of_trial', '12/25/2019'),
(1330, 370, '_date_of_trial', 'field_5dffdb855716b'),
(1331, 370, 'eligibility', 'dsadsa'),
(1332, 370, '_eligibility', 'field_5dffe7185716c'),
(1333, 370, 'championship_date', '12/25/2019'),
(1334, 370, '_championship_date', 'field_5dffe71d5716d'),
(1335, 370, 'product_set', 'a:2:{i:0;s:3:\"168\";i:1;s:3:\"167\";}'),
(1336, 370, '_product_set', 'field_5e0022168380e'),
(1353, 372, 'year', '2019'),
(1354, 372, '_year', 'field_5dffdaf957166'),
(1355, 372, 'sport', '166'),
(1356, 372, '_sport', 'field_5dffdb0557167'),
(1357, 372, 'age', '111'),
(1358, 372, '_age', 'field_5dffdb2157168'),
(1359, 372, 'date_birth_restriction', '12/06/2019'),
(1360, 372, '_date_birth_restriction', 'field_5dffdb6b5716a'),
(1361, 372, 'date_of_trial', '12/02/2019'),
(1362, 372, '_date_of_trial', 'field_5dffdb855716b'),
(1363, 372, 'eligibility', 'dsadsa'),
(1364, 372, '_eligibility', 'field_5dffe7185716c'),
(1365, 372, 'championship_date', '12/26/2019'),
(1366, 372, '_championship_date', 'field_5dffe71d5716d'),
(1367, 372, 'product_set', 'a:1:{i:0;s:3:\"168\";}'),
(1368, 372, '_product_set', 'field_5e0022168380e'),
(1392, 374, 'year', '2019'),
(1393, 374, '_year', 'field_5dffdaf957166'),
(1394, 374, 'sport', '163'),
(1395, 374, '_sport', 'field_5dffdb0557167'),
(1396, 374, 'age', '34'),
(1397, 374, '_age', 'field_5dffdb2157168'),
(1398, 374, 'gender', 'female'),
(1399, 374, '_gender', 'field_5dffdb2c57169'),
(1400, 374, 'date_birth_restriction', '12/28/2019'),
(1401, 374, '_date_birth_restriction', 'field_5dffdb6b5716a'),
(1402, 374, 'date_of_trial', '12/29/2019'),
(1403, 374, '_date_of_trial', 'field_5dffdb855716b'),
(1404, 374, 'eligibility', 'blallal'),
(1405, 374, '_eligibility', 'field_5dffe7185716c'),
(1406, 374, 'championship_date', '12/22/2019'),
(1407, 374, '_championship_date', 'field_5dffe71d5716d'),
(1408, 374, 'product_set', 'a:2:{i:0;s:3:\"167\";i:1;s:3:\"168\";}'),
(1409, 374, '_product_set', 'field_5e0022168380e'),
(1437, 370, 'gender', 'male'),
(1438, 370, '_gender', 'field_5dffdb2c57169'),
(1464, 368, 'gender', 'male'),
(1465, 368, '_gender', 'field_5dffdb2c57169'),
(1481, 378, 'year', '2019'),
(1482, 378, '_year', 'field_5dffdaf957166'),
(1483, 378, 'sport', '165'),
(1484, 378, '_sport', 'field_5dffdb0557167'),
(1485, 378, 'age', '20'),
(1486, 378, '_age', 'field_5dffdb2157168'),
(1487, 378, 'gender', 'female'),
(1488, 378, '_gender', 'field_5dffdb2c57169'),
(1489, 378, 'date_birth_restriction', '01/04/2020'),
(1490, 378, '_date_birth_restriction', 'field_5dffdb6b5716a'),
(1491, 378, 'date_of_trial', '12/23/2019'),
(1492, 378, '_date_of_trial', 'field_5dffdb855716b'),
(1493, 378, 'eligibility', 'Hola Mundo!.'),
(1494, 378, '_eligibility', 'field_5dffe7185716c'),
(1495, 378, 'championship_date', '12/27/2019'),
(1496, 378, '_championship_date', 'field_5dffe71d5716d'),
(1497, 378, 'product_set', 'a:2:{i:0;s:3:\"168\";i:1;s:3:\"167\";}'),
(1498, 378, '_product_set', 'field_5e0022168380e'),
(1499, 378, '_edit_lock', '1577388916:1'),
(1500, 379, '_edit_lock', '1577545613:1'),
(1501, 379, '_wp_page_template', 'page-dashboard.php'),
(1502, 379, '_edit_last', '1'),
(1503, 383, '_edit_lock', '1577507371:1'),
(1504, 383, '_edit_last', '1'),
(1505, 388, 'school', '173'),
(1506, 388, '_school', 'field_5e050b5b86936'),
(1507, 388, 'date_of_birth', '20191218'),
(1508, 388, '_date_of_birth', 'field_5e050b1986934'),
(1509, 388, 'gender', 'male'),
(1510, 388, '_gender', 'field_5e050b3286935'),
(1511, 388, '_edit_lock', '1577578276:1'),
(1512, 389, 'parent', '38'),
(1513, 389, '_parent', 'field_5e050d406ea96'),
(1514, 389, 'school', '170'),
(1515, 389, '_school', 'field_5e050b5b86936'),
(1516, 389, 'date_of_birth', '12/11/2019'),
(1517, 389, '_date_of_birth', 'field_5e050b1986934'),
(1518, 389, 'gender', 'other'),
(1519, 389, '_gender', 'field_5e050b3286935'),
(1520, 390, 'parent', '38'),
(1521, 390, '_parent', 'field_5e050d406ea96'),
(1522, 391, 'parent', '38'),
(1523, 391, '_parent', 'field_5e050d406ea96'),
(1524, 391, '_wp_trash_meta_status', 'draft'),
(1525, 391, '_wp_trash_meta_time', '1577414249'),
(1526, 391, '_wp_desired_post_slug', ''),
(1527, 390, '_wp_trash_meta_status', 'draft'),
(1528, 390, '_wp_trash_meta_time', '1577414256'),
(1529, 390, '_wp_desired_post_slug', ''),
(1530, 389, '_wp_trash_meta_status', 'draft'),
(1531, 389, '_wp_trash_meta_time', '1577414256'),
(1532, 389, '_wp_desired_post_slug', ''),
(1533, 392, 'year', '2019'),
(1534, 392, '_year', 'field_5dffdaf957166'),
(1535, 392, 'product_set', 'a:3:{i:0;s:3:\"167\";i:1;s:3:\"168\";i:2;s:3:\"343\";}'),
(1536, 392, '_product_set', 'field_5e0022168380e'),
(1537, 392, '_edit_lock', '1577415588:1'),
(1538, 392, '_edit_last', '1'),
(1539, 392, 'sport', '165'),
(1540, 392, '_sport', 'field_5dffdb0557167'),
(1541, 392, 'age', '11'),
(1542, 392, '_age', 'field_5dffdb2157168'),
(1543, 392, 'gender', 'female'),
(1544, 392, '_gender', 'field_5dffdb2c57169'),
(1545, 392, 'date_birth_restriction', '12/04/2019'),
(1546, 392, '_date_birth_restriction', 'field_5dffdb6b5716a'),
(1547, 392, 'date_of_trial', '12/18/2019'),
(1548, 392, '_date_of_trial', 'field_5dffdb855716b'),
(1549, 392, 'eligibility', 'dsadsa'),
(1550, 392, '_eligibility', 'field_5dffe7185716c'),
(1551, 392, 'championship_date', '12/26/2019'),
(1552, 392, '_championship_date', 'field_5dffe71d5716d'),
(1553, 393, 'parent', '38'),
(1554, 393, '_parent', 'field_5e050d406ea96'),
(1555, 393, 'school', '172'),
(1556, 393, '_school', 'field_5e050b5b86936'),
(1557, 393, 'date_of_birth', '12/04/2019'),
(1558, 393, '_date_of_birth', 'field_5e050b1986934'),
(1559, 393, 'gender', 'female'),
(1560, 393, '_gender', 'field_5e050b3286935'),
(1561, 393, '_edit_lock', '1577415791:1'),
(1562, 394, 'parent', '38'),
(1563, 394, '_parent', 'field_5e050d406ea96'),
(1564, 394, 'school', '170'),
(1565, 394, '_school', 'field_5e050b5b86936'),
(1566, 394, 'date_of_birth', '12/10/2019'),
(1567, 394, '_date_of_birth', 'field_5e050b1986934'),
(1568, 394, 'gender', 'male'),
(1569, 394, '_gender', 'field_5e050b3286935'),
(1570, 394, '_edit_lock', '1577420045:1'),
(1571, 395, '_edit_lock', '1577507724:1'),
(1572, 395, '_edit_last', '1'),
(1573, 400, 'year', '2019'),
(1574, 400, '_year', 'field_5dffdaf957166'),
(1575, 400, 'sport', '163'),
(1576, 400, '_sport', 'field_5dffdb0557167'),
(1577, 400, 'age', '12'),
(1578, 400, '_age', 'field_5dffdb2157168'),
(1579, 400, 'gender', 'male'),
(1580, 400, '_gender', 'field_5e050b3286935'),
(1581, 400, 'date_birth_restriction', '12/26/2019'),
(1582, 400, '_date_birth_restriction', 'field_5dffdb6b5716a'),
(1583, 400, 'date_of_trial', '12/26/2019'),
(1584, 400, '_date_of_trial', 'field_5dffdb855716b'),
(1585, 400, 'eligibility', 'afsfdsd'),
(1586, 400, '_eligibility', 'field_5dffe7185716c'),
(1587, 400, 'championship_date', '12/12/2019'),
(1588, 400, '_championship_date', 'field_5dffe71d5716d'),
(1589, 400, 'product_set', 'a:1:{i:0;s:3:\"168\";}'),
(1590, 400, '_product_set', 'field_5e0022168380e'),
(1591, 402, 'year', '2019'),
(1592, 402, '_year', 'field_5dffdaf957166'),
(1593, 402, 'sport', '164'),
(1594, 402, '_sport', 'field_5dffdb0557167'),
(1595, 402, 'age', '100'),
(1596, 402, '_age', 'field_5dffdb2157168'),
(1597, 402, 'gender', 'female'),
(1598, 402, '_gender', 'field_5e050b3286935'),
(1599, 402, 'date_of_trial', '12/28/2019'),
(1600, 402, '_date_of_trial', 'field_5dffdb855716b'),
(1601, 402, 'eligibility', 'dadsdsadadsa'),
(1602, 402, '_eligibility', 'field_5dffe7185716c'),
(1603, 402, 'championship_date', '02/28/2020'),
(1604, 402, '_championship_date', 'field_5dffe71d5716d'),
(1605, 402, '_edit_lock', '1577505288:1'),
(1606, 402, 'date_birth_restriction_from', '12/11/2019'),
(1607, 402, '_date_birth_restriction_from', 'field_5dffdb6b5716a'),
(1608, 402, 'date_birth_restriction_to', '12/18/2019'),
(1609, 402, '_date_birth_restriction_to', 'field_5e06ccb195c7c'),
(1610, 402, 'venue', 'Pedro Correa 204'),
(1611, 402, '_venue', 'field_5e06d0a585597'),
(1612, 402, 'product_set', 'a:2:{i:0;s:3:\"167\";i:1;s:3:\"168\";}'),
(1613, 402, '_product_set', 'field_5e0022168380e'),
(1614, 405, '_edit_lock', '1577510027:1'),
(1615, 405, '_edit_last', '1'),
(1616, 411, '_edit_lock', '1577542213:1'),
(1617, 411, '_edit_last', '1'),
(1627, 415, '_edit_lock', '1577542861:1'),
(1628, 415, '_wp_page_template', 'page-new-student-step-1.php'),
(1629, 415, '_edit_last', '1'),
(1630, 417, '_edit_lock', '1577545557:1'),
(1631, 417, '_wp_page_template', 'page-new-student-step-2.php'),
(1632, 417, '_edit_last', '1'),
(1633, 419, '_edit_lock', '1577566600:1'),
(1634, 419, '_wp_page_template', 'page-new-student-step-3.php'),
(1635, 419, '_edit_last', '1'),
(1636, 394, '_wp_trash_meta_status', 'draft'),
(1637, 394, '_wp_trash_meta_time', '1577576249'),
(1638, 394, '_wp_desired_post_slug', ''),
(1639, 393, '_wp_trash_meta_status', 'draft'),
(1640, 393, '_wp_trash_meta_time', '1577576249'),
(1641, 393, '_wp_desired_post_slug', ''),
(1642, 388, '_edit_last', '1'),
(1643, 388, '_wp_old_date', '2019-12-26'),
(1644, 388, 'parent', ''),
(1645, 388, '_parent', 'field_5e050d406ea96'),
(1646, 388, 'medical_condition', ''),
(1647, 388, '_medical_condition', 'field_5e0585b0f702c'),
(1648, 388, 'allergies', ''),
(1649, 388, '_allergies', 'field_5e0585c7f702d'),
(1650, 388, 'date_last_tetanus', ''),
(1651, 388, '_date_last_tetanus', 'field_5e0585d4f702e'),
(1652, 388, 'medical_number', ''),
(1653, 388, '_medical_number', 'field_5e058618f702f'),
(1654, 388, 'name_of_preferred_emergency', ''),
(1655, 388, '_name_of_preferred_emergency', 'field_5e06dc29236f3'),
(1656, 388, 'relationship', ''),
(1657, 388, '_relationship', 'field_5e06dc69236f4'),
(1658, 388, 'mobile_emergency', ''),
(1659, 388, '_mobile_emergency', 'field_5e06dc87236f5'),
(1660, 388, 'student_email', ''),
(1661, 388, '_student_email', 'field_5e06dc98236f6'),
(1662, 388, 'student_mobile', ''),
(1663, 388, '_student_mobile', 'field_5e06dca5236f7');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_posts`
--

DROP TABLE IF EXISTS `wp_posts`;
CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(255) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL DEFAULT '',
  `to_ping` text NOT NULL,
  `pinged` text NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(5, 1, '2019-11-28 01:47:16', '2019-11-28 01:47:16', '', 'silla-principal', '', 'inherit', 'open', 'closed', '', 'silla-principal', '', '', '2019-11-28 01:47:16', '2019-11-28 01:47:16', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/11/silla-principal.png', 0, 'attachment', 'image/png', 0),
(30, 1, '2019-12-05 16:47:54', '2019-12-05 16:47:54', '', 'ACT', '', 'publish', 'closed', 'closed', '', 'act', '', '', '2019-12-05 16:47:54', '2019-12-05 16:47:54', '', 0, 'http://localhost:8888/ssact/?post_type=states&#038;p=30', 0, 'states', '', 0),
(31, 1, '2019-12-05 16:48:14', '2019-12-05 16:48:14', '', 'NSW', '', 'publish', 'closed', 'closed', '', 'nsw', '', '', '2019-12-05 16:48:14', '2019-12-05 16:48:14', '', 0, 'http://localhost:8888/ssact/?post_type=states&#038;p=31', 0, 'states', '', 0),
(32, 1, '2019-12-05 16:57:15', '2019-12-05 16:57:15', '', 'Region Type', '', 'publish', 'closed', 'closed', '', 'acf_region-type', '', '', '2019-12-05 16:57:33', '2019-12-05 16:57:33', '', 0, 'http://localhost:8888/ssact/?post_type=acf&#038;p=32', 0, 'acf', '', 0),
(109, 1, '2019-12-11 15:56:06', '2019-12-11 15:56:06', '[school_sport_register_parent]', 'Parents Registration', '', 'publish', 'closed', 'closed', '', 'parents-registration', '', '', '2019-12-11 15:56:06', '2019-12-11 15:56:06', '', 0, 'http://localhost:8888/ssact/parents-registration/', 0, 'page', '', 0),
(110, 1, '2019-12-11 15:56:06', '2019-12-11 15:56:06', '[school_sport_register_official]', 'Team Official Registration', '', 'publish', 'closed', 'closed', '', 'team-official-registration', '', '', '2019-12-11 15:56:06', '2019-12-11 15:56:06', '', 0, 'http://localhost:8888/ssact/team-official-registration/', 0, 'page', '', 0),
(111, 1, '2019-12-11 15:56:06', '2019-12-11 15:56:06', '[school_sport_recovery]', 'Recover your password', '', 'publish', 'closed', 'closed', '', 'recover-your-password', '', '', '2019-12-11 15:56:06', '2019-12-11 15:56:06', '', 0, 'http://localhost:8888/ssact/recover-your-password/', 0, 'page', '', 0),
(112, 1, '2019-12-11 15:56:06', '2019-12-11 15:56:06', '[school_sport_login]', 'Login', '', 'publish', 'closed', 'closed', '', 'login', '', '', '2019-12-11 15:56:06', '2019-12-11 15:56:06', '', 0, 'http://localhost:8888/ssact/login/', 0, 'page', '', 0),
(113, 1, '2019-12-11 15:56:06', '2019-12-11 15:56:06', '[school_sport_reset_password]', 'Reset my password', '', 'publish', 'closed', 'closed', '', 'reset-my-password', '', '', '2019-12-11 15:56:06', '2019-12-11 15:56:06', '', 0, 'http://localhost:8888/ssact/reset-my-password/', 0, 'page', '', 0),
(114, 1, '2019-12-11 15:56:06', '2019-12-11 15:56:06', '[school_sport_register_confirmation]', 'Confirmation Complete', '', 'publish', 'closed', 'closed', '', 'confirmation-complete', '', '', '2019-12-11 15:56:06', '2019-12-11 15:56:06', '', 0, 'http://localhost:8888/ssact/confirmation-complete/', 0, 'page', '', 0),
(115, 1, '2019-12-11 15:56:06', '2019-12-11 15:56:06', '[school_sport_dashboard]', 'Dashboard', '', 'publish', 'closed', 'closed', '', 'dashboard', '', '', '2019-12-11 15:56:06', '2019-12-11 15:56:06', '', 0, 'http://localhost:8888/ssact/dashboard/', 0, 'page', '', 0),
(116, 1, '2019-12-11 17:15:34', '2019-12-11 17:15:34', '<!-- wp:paragraph -->\n<p>An Email confirmation has been sent.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>To complete the registration process look for the email in your inbox that provides further instructions.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Didn\'t get the email? -&nbsp;<strong>Check your spam folder</strong>&nbsp;to make sure it didn\'t end up there.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Regards,</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>The School Sport ACT Team</p>\n<!-- /wp:paragraph -->', 'Email Sent', '', 'publish', 'closed', 'closed', '', 'email-sent', '', '', '2019-12-11 17:36:26', '2019-12-11 17:36:26', '', 0, 'http://localhost:8888/ssact/email-sent/', 0, 'page', '', 0),
(117, 1, '2019-12-11 17:36:25', '2019-12-11 17:36:25', '<!-- wp:paragraph -->\n<p>An Email confirmation has been sent.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>To complete the registration process look for the email in your inbox that provides further instructions.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Didn\'t get the email? -&nbsp;<strong>Check your spam folder</strong>&nbsp;to make sure it didn\'t end up there.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Regards,</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>The School Sport ACT Team</p>\n<!-- /wp:paragraph -->', 'Email Sent', '', 'inherit', 'closed', 'closed', '', '116-revision-v1', '', '', '2019-12-11 17:36:25', '2019-12-11 17:36:25', '', 116, 'http://localhost:8888/ssact/blog/2019/12/11/116-revision-v1/', 0, 'revision', '', 0),
(118, 1, '2019-12-11 17:36:29', '2019-12-11 17:36:29', '<!-- wp:paragraph -->\n<p>An Email confirmation has been sent.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>To complete the registration process look for the email in your inbox that provides further instructions.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Didn\'t get the email? -&nbsp;<strong>Check your spam folder</strong>&nbsp;to make sure it didn\'t end up there.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Regards,</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>The School Sport ACT Team</p>\n<!-- /wp:paragraph -->', 'Email Sent', '', 'inherit', 'closed', 'closed', '', '116-autosave-v1', '', '', '2019-12-11 17:36:29', '2019-12-11 17:36:29', '', 116, 'http://localhost:8888/ssact/blog/2019/12/11/116-autosave-v1/', 0, 'revision', '', 0),
(123, 1, '2019-12-11 19:00:25', '2019-12-11 19:00:25', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:13:\"home-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Group Home', 'group-home', 'publish', 'closed', 'closed', '', 'group_5df13c8918324', '', '', '2019-12-19 18:29:41', '2019-12-19 18:29:41', '', 0, 'http://localhost:8888/ssact/?post_type=acf-field-group&#038;p=123', 0, 'acf-field-group', '', 0),
(124, 1, '2019-12-11 19:00:25', '2019-12-11 19:00:25', 'a:18:{s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:6:\"insert\";s:6:\"append\";s:7:\"library\";s:3:\"all\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Sponsors', 'sponsors', 'publish', 'closed', 'closed', '', 'field_5df13c94bc10c', '', '', '2019-12-19 18:29:41', '2019-12-19 18:29:41', '', 123, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=124', 11, 'acf-field', '', 0),
(126, 1, '2019-12-11 19:03:16', '2019-12-11 19:03:16', 'a:18:{s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:6:\"insert\";s:6:\"append\";s:7:\"library\";s:3:\"all\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Partners', 'partners', 'publish', 'closed', 'closed', '', 'field_5df13d6354f26', '', '', '2019-12-19 18:29:41', '2019-12-19 18:29:41', '', 123, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=126', 12, 'acf-field', '', 0),
(131, 1, '2019-12-11 19:08:51', '2019-12-11 19:08:51', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";i:1;s:3:\"max\";s:0:\"\";s:6:\"layout\";s:3:\"row\";s:12:\"button_label\";s:0:\"\";}', 'Banners', 'banners', 'publish', 'closed', 'closed', '', 'field_5df13e5f1bca5', '', '', '2019-12-19 18:29:41', '2019-12-19 18:29:41', '', 123, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=131', 10, 'acf-field', '', 0),
(132, 1, '2019-12-11 19:08:51', '2019-12-11 19:08:51', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image', 'image', 'publish', 'closed', 'closed', '', 'field_5df13e941bca6', '', '', '2019-12-11 19:19:52', '2019-12-11 19:19:52', '', 131, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=132', 0, 'acf-field', '', 0),
(133, 1, '2019-12-11 19:08:51', '2019-12-11 19:08:51', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";i:1;s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_5df13e9d1bca7', '', '', '2019-12-16 00:59:28', '2019-12-16 00:59:28', '', 131, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=133', 1, 'acf-field', '', 0),
(137, 1, '2019-12-11 19:12:27', '2019-12-11 19:12:27', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'title', 'publish', 'closed', 'closed', '', 'field_5df13f81d99bc', '', '', '2019-12-15 01:32:10', '2019-12-15 01:32:10', '', 123, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=137', 1, 'acf-field', '', 0),
(138, 1, '2019-12-11 19:12:27', '2019-12-11 19:12:27', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_5df13f89d99bd', '', '', '2019-12-15 02:12:54', '2019-12-15 02:12:54', '', 123, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=138', 2, 'acf-field', '', 0),
(144, 1, '2019-12-11 19:45:41', '2019-12-11 19:45:41', 'dsada', 'sda', '', 'trash', 'closed', 'closed', '', 'sda__trashed', '', '', '2019-12-23 15:55:21', '2019-12-23 15:55:21', '', 0, 'http://localhost:8888/ssact/?post_type=product_sets&#038;p=144', 0, 'product_sets', '', 0),
(145, 1, '2019-12-12 01:59:15', '2019-12-12 01:59:15', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:15:\"school-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Group Setting', 'group-setting', 'publish', 'closed', 'closed', '', 'group_5df19e2755bc3', '', '', '2019-12-12 02:14:38', '2019-12-12 02:14:38', '', 0, 'http://localhost:8888/ssact/?post_type=acf-field-group&#038;p=145', 0, 'acf-field-group', '', 0),
(146, 1, '2019-12-12 01:59:15', '2019-12-12 01:59:15', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Footer', 'information', 'publish', 'closed', 'closed', '', 'field_5df19e94be374', '', '', '2019-12-12 01:59:15', '2019-12-12 01:59:15', '', 145, 'http://localhost:8888/ssact/?post_type=acf-field&p=146', 0, 'acf-field', '', 0),
(147, 1, '2019-12-12 01:59:15', '2019-12-12 01:59:15', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Address', 'address', 'publish', 'closed', 'closed', '', 'field_5df19ea9be375', '', '', '2019-12-12 01:59:15', '2019-12-12 01:59:15', '', 145, 'http://localhost:8888/ssact/?post_type=acf-field&p=147', 1, 'acf-field', '', 0),
(148, 1, '2019-12-12 01:59:15', '2019-12-12 01:59:15', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Phone', 'phone', 'publish', 'closed', 'closed', '', 'field_5df19eb7be376', '', '', '2019-12-12 01:59:15', '2019-12-12 01:59:15', '', 145, 'http://localhost:8888/ssact/?post_type=acf-field&p=148', 2, 'acf-field', '', 0),
(149, 1, '2019-12-12 01:59:15', '2019-12-12 01:59:15', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Fax', 'fax', 'publish', 'closed', 'closed', '', 'field_5df19ec1be377', '', '', '2019-12-12 01:59:15', '2019-12-12 01:59:15', '', 145, 'http://localhost:8888/ssact/?post_type=acf-field&p=149', 3, 'acf-field', '', 0),
(150, 1, '2019-12-12 01:59:15', '2019-12-12 01:59:15', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Email', 'email', 'publish', 'closed', 'closed', '', 'field_5df19edebe378', '', '', '2019-12-12 01:59:15', '2019-12-12 01:59:15', '', 145, 'http://localhost:8888/ssact/?post_type=acf-field&p=150', 4, 'acf-field', '', 0),
(151, 1, '2019-12-12 01:59:15', '2019-12-12 01:59:15', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'ABN', 'abn', 'publish', 'closed', 'closed', '', 'field_5df19ee4be379', '', '', '2019-12-12 01:59:15', '2019-12-12 01:59:15', '', 145, 'http://localhost:8888/ssact/?post_type=acf-field&p=151', 5, 'acf-field', '', 0),
(152, 1, '2019-12-12 01:59:15', '2019-12-12 01:59:15', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Social applications', 'social_conect', 'publish', 'closed', 'closed', '', 'field_5df19e73be373', '', '', '2019-12-12 02:01:06', '2019-12-12 02:01:06', '', 145, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=152', 6, 'acf-field', '', 0),
(153, 1, '2019-12-12 01:59:15', '2019-12-12 01:59:15', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Google Secret KEY', 'google_secret_key', 'publish', 'closed', 'closed', '', 'field_5df19e2ebe370', '', '', '2019-12-12 02:14:38', '2019-12-12 02:14:38', '', 145, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=153', 8, 'acf-field', '', 0),
(154, 1, '2019-12-12 01:59:15', '2019-12-12 01:59:15', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Facebook APP ID', 'facebook_app_id', 'publish', 'closed', 'closed', '', 'field_5df19e3dbe371', '', '', '2019-12-12 02:14:38', '2019-12-12 02:14:38', '', 145, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=154', 9, 'acf-field', '', 0),
(155, 1, '2019-12-12 01:59:15', '2019-12-12 01:59:15', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Facebook Url', 'facebook_url', 'publish', 'closed', 'closed', '', 'field_5df19e4bbe372', '', '', '2019-12-12 02:14:38', '2019-12-12 02:14:38', '', 145, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=155', 10, 'acf-field', '', 0),
(156, 1, '2019-12-12 02:14:38', '2019-12-12 02:14:38', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Google Site KEY', 'google_site_key', 'publish', 'closed', 'closed', '', 'field_5df1a27cb7dd1', '', '', '2019-12-12 02:14:38', '2019-12-12 02:14:38', '', 145, 'http://localhost:8888/ssact/?post_type=acf-field&p=156', 7, 'acf-field', '', 0),
(159, 1, '2019-12-14 21:04:24', '2019-12-14 21:04:24', '', 'Australian Football', '', 'publish', 'closed', 'closed', '', 'australian-football', '', '', '2019-12-14 21:04:24', '2019-12-14 21:04:24', '', 0, 'http://localhost:8888/ssact/?post_type=sports&#038;p=159', 0, 'sports', '', 0),
(160, 1, '2019-12-14 21:04:37', '2019-12-14 21:04:37', '', 'Baseball', '', 'publish', 'closed', 'closed', '', 'baseball', '', '', '2019-12-14 21:04:37', '2019-12-14 21:04:37', '', 0, 'http://localhost:8888/ssact/?post_type=sports&#038;p=160', 0, 'sports', '', 0),
(161, 1, '2019-12-14 21:05:00', '2019-12-14 21:05:00', '', 'Basketball', '', 'publish', 'closed', 'closed', '', 'basketball', '', '', '2019-12-14 21:05:00', '2019-12-14 21:05:00', '', 0, 'http://localhost:8888/ssact/?post_type=sports&#038;p=161', 0, 'sports', '', 0),
(162, 1, '2019-12-14 21:05:08', '2019-12-14 21:05:08', '', 'Cricket', '', 'publish', 'closed', 'closed', '', 'cricket', '', '', '2019-12-14 21:05:08', '2019-12-14 21:05:08', '', 0, 'http://localhost:8888/ssact/?post_type=sports&#038;p=162', 0, 'sports', '', 0),
(163, 1, '2019-12-14 21:05:16', '2019-12-14 21:05:16', '', 'Cross Country', '', 'publish', 'closed', 'closed', '', 'cross-country', '', '', '2019-12-14 21:05:16', '2019-12-14 21:05:16', '', 0, 'http://localhost:8888/ssact/?post_type=sports&#038;p=163', 0, 'sports', '', 0),
(164, 1, '2019-12-14 21:05:22', '2019-12-14 21:05:22', '', 'Diving', '', 'publish', 'closed', 'closed', '', 'diving', '', '', '2019-12-14 21:05:22', '2019-12-14 21:05:22', '', 0, 'http://localhost:8888/ssact/?post_type=sports&#038;p=164', 0, 'sports', '', 0),
(165, 1, '2019-12-14 21:05:29', '2019-12-14 21:05:29', '', 'Football (Soccer)', '', 'publish', 'closed', 'closed', '', 'football-soccer', '', '', '2019-12-14 21:05:29', '2019-12-14 21:05:29', '', 0, 'http://localhost:8888/ssact/?post_type=sports&#038;p=165', 0, 'sports', '', 0),
(166, 1, '2019-12-14 21:05:43', '2019-12-14 21:05:43', '', 'Golf', '', 'publish', 'closed', 'closed', '', 'golf', '', '', '2019-12-14 21:05:43', '2019-12-14 21:05:43', '', 0, 'http://localhost:8888/ssact/?post_type=sports&#038;p=166', 0, 'sports', '', 0),
(167, 1, '2019-12-14 21:06:22', '2019-12-14 21:06:22', '', 'Standard Product Set', '', 'publish', 'closed', 'closed', '', 'standard-product-set', '', '', '2019-12-14 21:06:22', '2019-12-14 21:06:22', '', 0, 'http://localhost:8888/ssact/?post_type=product_sets&#038;p=167', 0, 'product_sets', '', 0),
(168, 1, '2019-12-14 21:06:44', '2019-12-14 21:06:44', '', 'Standard Product Set (Winter)', '', 'publish', 'closed', 'closed', '', 'standard-product-set-winter', '', '', '2019-12-18 19:23:08', '2019-12-18 19:23:08', '', 0, 'http://localhost:8888/ssact/?post_type=product_sets&#038;p=168', 0, 'product_sets', '', 0),
(169, 1, '2019-12-14 21:08:23', '2019-12-14 21:08:23', '', 'Ainslie School', '', 'publish', 'closed', 'closed', '', 'ainslie-school', '', '', '2019-12-14 21:08:23', '2019-12-14 21:08:23', '', 0, 'http://localhost:8888/ssact/?post_type=schools&#038;p=169', 0, 'schools', '', 0),
(170, 1, '2019-12-14 21:08:35', '2019-12-14 21:08:35', '', 'Alfred Deakin High School', '', 'publish', 'closed', 'closed', '', 'alfred-deakin-high-school', '', '', '2019-12-14 21:08:35', '2019-12-14 21:08:35', '', 0, 'http://localhost:8888/ssact/?post_type=schools&#038;p=170', 0, 'schools', '', 0),
(171, 1, '2019-12-14 21:08:43', '2019-12-14 21:08:43', '', 'Amaroo School', '', 'publish', 'closed', 'closed', '', 'amaroo-school', '', '', '2019-12-14 21:09:09', '2019-12-14 21:09:09', '', 0, 'http://localhost:8888/ssact/?post_type=schools&#038;p=171', 0, 'schools', '', 0),
(172, 1, '2019-12-14 21:09:20', '2019-12-14 21:09:20', '', 'Arawang Primary School', '', 'publish', 'closed', 'closed', '', 'arawang-primary-school', '', '', '2019-12-14 21:09:20', '2019-12-14 21:09:20', '', 0, 'http://localhost:8888/ssact/?post_type=schools&#038;p=172', 0, 'schools', '', 0),
(173, 1, '2019-12-14 21:09:44', '2019-12-14 21:09:44', '', 'Aranda Primary School', '', 'publish', 'closed', 'closed', '', 'aranda-primary-school', '', '', '2019-12-14 21:09:44', '2019-12-14 21:09:44', '', 0, 'http://localhost:8888/ssact/?post_type=schools&#038;p=173', 0, 'schools', '', 0),
(175, 1, '2019-12-14 21:11:43', '2019-12-14 21:11:43', '<!-- wp:paragraph -->\n<p>School Sport ACT (SSACT) is responsible for the oversight of school sport as it relates to the School Sport Australia representative pathway. SSACT supports all ACT schools wishing to participate from the three sectors of education; ACT Department of Education &amp; Training, the Catholic Education Office and the Independent Schools Association.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>SSACT actively promotes sport for all ACT school students through:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li><strong>&nbsp;Participation&nbsp;</strong>- all students are encouraged to be active</li><li><strong>&nbsp;Potential&nbsp;</strong>- athletes have the chance to develop their potential and compete at their chosen level</li><li><strong>&nbsp;Pathways&nbsp;</strong>- students are encouraged to participate in community sport as well as school sport to develop lifelong habits, and therefore increase their health and well being</li></ul>\n<!-- /wp:list -->\n\n<!-- wp:heading -->\n<h2>School Sport ACT Objective:</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><em><strong>\" promote, coordinate, support and encourage ACT students’ participation in competitive sport to complement educational learning outcomes.\"</strong></em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>To achieve the objective SSACT will:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>provide regular sporting competition which complements the educational learning outcomes;</li><li>ensure competition meets the expectations of parents and school leaders in the spirit of good sportsmanship;</li><li>maximise opportunities for students to compete at a level suitable to their age and ability;</li><li>promote partnerships with community sports organisations;</li><li>provide strategic leadership;</li><li>advocating recognition for School Sport within the broader community, and</li><li>provide efficient administration of SSACT.</li></ul>\n<!-- /wp:list -->', 'About us', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2019-12-14 21:11:43', '2019-12-14 21:11:43', '', 0, 'http://localhost:8888/ssact/?page_id=175', 0, 'page', '', 0),
(176, 1, '2019-12-14 21:11:43', '2019-12-14 21:11:43', '<!-- wp:paragraph -->\n<p>School Sport ACT (SSACT) is responsible for the oversight of school sport as it relates to the School Sport Australia representative pathway. SSACT supports all ACT schools wishing to participate from the three sectors of education; ACT Department of Education &amp; Training, the Catholic Education Office and the Independent Schools Association.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>SSACT actively promotes sport for all ACT school students through:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li><strong>&nbsp;Participation&nbsp;</strong>- all students are encouraged to be active</li><li><strong>&nbsp;Potential&nbsp;</strong>- athletes have the chance to develop their potential and compete at their chosen level</li><li><strong>&nbsp;Pathways&nbsp;</strong>- students are encouraged to participate in community sport as well as school sport to develop lifelong habits, and therefore increase their health and well being</li></ul>\n<!-- /wp:list -->\n\n<!-- wp:heading -->\n<h2>School Sport ACT Objective:</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><em><strong>\" promote, coordinate, support and encourage ACT students’ participation in competitive sport to complement educational learning outcomes.\"</strong></em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>To achieve the objective SSACT will:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>provide regular sporting competition which complements the educational learning outcomes;</li><li>ensure competition meets the expectations of parents and school leaders in the spirit of good sportsmanship;</li><li>maximise opportunities for students to compete at a level suitable to their age and ability;</li><li>promote partnerships with community sports organisations;</li><li>provide strategic leadership;</li><li>advocating recognition for School Sport within the broader community, and</li><li>provide efficient administration of SSACT.</li></ul>\n<!-- /wp:list -->', 'About us', '', 'inherit', 'closed', 'closed', '', '175-revision-v1', '', '', '2019-12-14 21:11:43', '2019-12-14 21:11:43', '', 175, 'http://localhost:8888/ssact/blog/2019/12/14/175-revision-v1/', 0, 'revision', '', 0),
(177, 1, '2019-12-14 21:12:36', '2019-12-14 21:12:36', '<!-- wp:paragraph -->\n<p>School sport throughout Australia provides the pathways and encouragement for thousands of students to represent their schools, regions, state or territory and potentially Australia.&nbsp; School Sport ACT is honoured to facilitate these opportunities for the over 50,000 students across the ACT and is recognised as one of the leading and most effective and efficient organisations delivering school sport in Australia.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>The Council is very proud of the work done by our staff under the leadership of Executive Officer Liz Yuen in order to ensure we provide all our stakeholders, students, teachers, parents and officials, with the support required to participate in their chosen sport.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>I wish to also acknowledge and thank the Principals of the schools for endorsing school sport, the teachers who go “above and beyond” to provide these opportunities for students, the students for their enthusiastic engagement and the parents who provide the taxi services and supporting roles during trials, training and competition.&nbsp;</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Thanks also go to the many community sports who work closely with SSACT to maximise the sports specific learning outcomes through coaching. It is this collaborative approach to school sport that ensures ACT students are afforded the best opportunities for growth and development.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>I wish all our stakeholders an exciting year of participation and competition.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>Chris Nunn OAM</strong><br><strong>Chair</strong><br><strong>SSACT Council</strong></p>\n<!-- /wp:paragraph -->', 'Message from the Council Chair', '', 'publish', 'closed', 'closed', '', 'message-from-the-council-chair', '', '', '2019-12-14 21:12:36', '2019-12-14 21:12:36', '', 0, 'http://localhost:8888/ssact/?page_id=177', 0, 'page', '', 0),
(178, 1, '2019-12-14 21:12:36', '2019-12-14 21:12:36', '<!-- wp:paragraph -->\n<p>School sport throughout Australia provides the pathways and encouragement for thousands of students to represent their schools, regions, state or territory and potentially Australia.&nbsp; School Sport ACT is honoured to facilitate these opportunities for the over 50,000 students across the ACT and is recognised as one of the leading and most effective and efficient organisations delivering school sport in Australia.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>The Council is very proud of the work done by our staff under the leadership of Executive Officer Liz Yuen in order to ensure we provide all our stakeholders, students, teachers, parents and officials, with the support required to participate in their chosen sport.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>I wish to also acknowledge and thank the Principals of the schools for endorsing school sport, the teachers who go “above and beyond” to provide these opportunities for students, the students for their enthusiastic engagement and the parents who provide the taxi services and supporting roles during trials, training and competition.&nbsp;</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Thanks also go to the many community sports who work closely with SSACT to maximise the sports specific learning outcomes through coaching. It is this collaborative approach to school sport that ensures ACT students are afforded the best opportunities for growth and development.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>I wish all our stakeholders an exciting year of participation and competition.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>Chris Nunn OAM</strong><br><strong>Chair</strong><br><strong>SSACT Council</strong></p>\n<!-- /wp:paragraph -->', 'Message from the Council Chair', '', 'inherit', 'closed', 'closed', '', '177-revision-v1', '', '', '2019-12-14 21:12:36', '2019-12-14 21:12:36', '', 177, 'http://localhost:8888/ssact/blog/2019/12/14/177-revision-v1/', 0, 'revision', '', 0),
(179, 1, '2019-12-14 21:12:58', '2019-12-14 21:12:58', '<!-- wp:paragraph -->\n<p>The ACT School Sport Council is responsible for the efficient management of the School Sport ACT Office and oversees the operation of the School Sport Management Committee.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>The Council also develops and approves all major policies and provides strategic direction to the organisation.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>The Council consists of an independently appointed chair and seven other members who are highly experienced members of the SSACT stakeholder community. Representation includes the three sectors of education and community sport.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>The Council meets four times a year to discuss and resolve current issues affecting SSACT and holds an Annual General Meeting in the first term of each school year.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><a href=\"https://schoolsportsact.blob.core.windows.net/schoolsportsact-container-eas/SiteContentFiles-ACT%20Schools%20Sports%20Council%20-%20ACT%20Incorporated%20Associations%20Rules%2009012018.pdf\">ACT Schools Sports Council - ACT Incorporated Associations Rules.pdf</a></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>&nbsp;<strong>2019 Council Members</strong></h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Chris Nunn OAM - Chair</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Andrew Wrigley&nbsp;<br><em>Executive Director of Association of Independent Schools</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Julie Douglas<br><em>Chair RSSAG, Principal and CEO representative</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Mark Vanderstoep<br><em>President - Secondary South Region</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Jon Tucker<br><em>President - Primary Tuggeranong Region</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Anton Pemmer<br><em>Community Member</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Sharon Stekelenburg<em><br>Community Member</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Frank Arsego<br><em>Community Member</em></p>\n<!-- /wp:paragraph -->', 'ACT School Sport Council', '', 'publish', 'closed', 'closed', '', 'act-school-sport-council', '', '', '2019-12-14 21:12:59', '2019-12-14 21:12:59', '', 0, 'http://localhost:8888/ssact/?page_id=179', 0, 'page', '', 0),
(180, 1, '2019-12-14 21:12:58', '2019-12-14 21:12:58', '<!-- wp:paragraph -->\n<p>The ACT School Sport Council is responsible for the efficient management of the School Sport ACT Office and oversees the operation of the School Sport Management Committee.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>The Council also develops and approves all major policies and provides strategic direction to the organisation.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>The Council consists of an independently appointed chair and seven other members who are highly experienced members of the SSACT stakeholder community. Representation includes the three sectors of education and community sport.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>The Council meets four times a year to discuss and resolve current issues affecting SSACT and holds an Annual General Meeting in the first term of each school year.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><a href=\"https://schoolsportsact.blob.core.windows.net/schoolsportsact-container-eas/SiteContentFiles-ACT%20Schools%20Sports%20Council%20-%20ACT%20Incorporated%20Associations%20Rules%2009012018.pdf\">ACT Schools Sports Council - ACT Incorporated Associations Rules.pdf</a></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>&nbsp;<strong>2019 Council Members</strong></h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Chris Nunn OAM - Chair</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Andrew Wrigley&nbsp;<br><em>Executive Director of Association of Independent Schools</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Julie Douglas<br><em>Chair RSSAG, Principal and CEO representative</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Mark Vanderstoep<br><em>President - Secondary South Region</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Jon Tucker<br><em>President - Primary Tuggeranong Region</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Anton Pemmer<br><em>Community Member</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Sharon Stekelenburg<em><br>Community Member</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Frank Arsego<br><em>Community Member</em></p>\n<!-- /wp:paragraph -->', 'ACT School Sport Council', '', 'inherit', 'closed', 'closed', '', '179-revision-v1', '', '', '2019-12-14 21:12:58', '2019-12-14 21:12:58', '', 179, 'http://localhost:8888/ssact/blog/2019/12/14/179-revision-v1/', 0, 'revision', '', 0),
(181, 1, '2019-12-14 21:13:15', '2019-12-14 21:13:15', '<!-- wp:paragraph -->\n<p>In 2011, after many years of operating independently the ACT Secondary Schools Sports Association (ACTSSSA) and the ACT Primary Schools Sports Association (ACTPSSA) amalgamated to form the ACT School Sport Management Committee (ACTSSMC).</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":231,\"width\":585,\"height\":389,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large is-resized\"><img src=\"http://localhost:8888/ssact/wp-content/uploads/2019/12/bg_home3-1024x683.jpg\" alt=\"\" class=\"wp-image-231\" width=\"585\" height=\"389\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>To enable oversight of the management committee the ACT Schools Sports Council was established at the same time and remains responsible for the strategic direction and approach of SSACT.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Given these changes the Council was very aware of the need to capture historical records of both organisations and Mr Paul Andrews was therefore engaged to collate the historical data from the ACTSSSA and has collated this information into the “ACTSSSA Results History Project 1977 – 2011”.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading -->\n<h2>hjh</h2>\n<!-- /wp:heading --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:list -->\n<ul><li>66</li></ul>\n<!-- /wp:list --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p>hjkj</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:paragraph -->\n<p><a href=\"https://www.schoolsportact.asn.au/assets/documents/ACTSSSA_Results_History.pdf\">ACTSSSA Results History</a></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>This information is now available to the broader school community where students, parents and interested others can search on the School Sport ACT website and see who has come before them. The council would also like to hear from anyone who information which may be a useful addition to the history we currently have collated. Old photos, sports programs or personal reflections are most welcome.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Christopher Nunn OAM<br>Chair<br>ACT School Sport Council</p>\n<!-- /wp:paragraph -->', 'ACT School Sport History', '', 'publish', 'closed', 'closed', '', 'act-school-sport-history', '', '', '2019-12-15 23:24:47', '2019-12-15 23:24:47', '', 0, 'http://localhost:8888/ssact/?page_id=181', 0, 'page', '', 0),
(182, 1, '2019-12-14 21:13:15', '2019-12-14 21:13:15', '<!-- wp:paragraph -->\n<p>In 2011, after many years of operating independently the ACT Secondary Schools Sports Association (ACTSSSA) and the ACT Primary Schools Sports Association (ACTPSSA) amalgamated to form the ACT School Sport Management Committee (ACTSSMC).</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>To enable oversight of the management committee the ACT Schools Sports Council was established at the same time and remains responsible for the strategic direction and approach of SSACT.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Given these changes the Council was very aware of the need to capture historical records of both organisations and Mr Paul Andrews was therefore engaged to collate the historical data from the ACTSSSA and has collated this information into the “ACTSSSA Results History Project 1977 – 2011”.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><a href=\"https://www.schoolsportact.asn.au/assets/documents/ACTSSSA_Results_History.pdf\">ACTSSSA Results History</a></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>This information is now available to the broader school community where students, parents and interested others can search on the School Sport ACT website and see who has come before them. The council would also like to hear from anyone who information which may be a useful addition to the history we currently have collated. Old photos, sports programs or personal reflections are most welcome.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Christopher Nunn OAM<br>Chair<br>ACT School Sport Council</p>\n<!-- /wp:paragraph -->', 'ACT School Sport History', '', 'inherit', 'closed', 'closed', '', '181-revision-v1', '', '', '2019-12-14 21:13:15', '2019-12-14 21:13:15', '', 181, 'http://localhost:8888/ssact/blog/2019/12/14/181-revision-v1/', 0, 'revision', '', 0),
(183, 1, '2019-12-14 21:14:01', '2019-12-14 21:14:01', '<!-- wp:paragraph -->\n<p>Welcome!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>The Regional School Sport Advisory Group (RSSAG - formerly the School Sport Management Committee), was established in 2011 when the ACT Primary School Sport Association (ACTPSSA) and ACT Secondary School Sport Association (ACTSSSA) merged to be under the one umbrella overseen by representatives from each of the six regions. It is is an advisory group to the ACT School Sport Council.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Members of the RSSAG comprise of a School Principal as Chair , the six Regional Presidents and representatives for; Government Colleges, Association of Independent Schools, Catholic Education Office, Associated Southern Colleges, School Sport ACT and the Education Directorate.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>The group meets four times a year to discuss and resolve issues that arise from administering School Sport in the ACT. It is responsible for ensuring that all ACT regional and state events are organised and held in an appropriate manner that instils a correct code of conduct amongst all competitors, managers and spectators.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>&nbsp;<strong>Julie Douglas<br>Chair of the Regional School Sport Advisory Group</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>2019 RSSAG Members</h3>\n<!-- /wp:heading -->\n\n<!-- wp:table -->\n<figure class=\"wp-block-table\"><table class=\"\"><tbody><tr><td>Chair</td><td>Julie Douglas</td><td>St Bedes Primary School</td></tr><tr><td>Deputy Chair</td><td>Amanda Beresford</td><td>Curtin Primary School</td></tr><tr><td>Secretary</td><td>Amanda Beresford</td><td>Curtin Primary School</td></tr><tr><td>Secondary North Region President</td><td>Kate Bradley</td><td>Harrison School</td></tr><tr><td>Secondary South Region President</td><td>Mark Vanderstoep</td><td>Trinity Christian School</td></tr><tr><td>Primary Belconnen Region President</td><td>Emmalene Burriss</td><td>&nbsp;Mt Rogers Primary School</td></tr><tr><td>Primary North /Gungahlin Region President</td><td>Kellie Quirk</td><td>Holy Spirit Primary School</td></tr><tr><td>Primary South /Weston Region President</td><td>Amanda Beresford</td><td>Curtin Primary School</td></tr><tr><td>Primary Tuggeranong Region President</td><td>Jon Tucker</td><td>Charles Conder Primary School</td></tr><tr><td>College Sport Representative</td><td>John Forrest</td><td>Dickson College</td></tr><tr><td>ASC Representative</td><td>Jamaya Ferguson</td><td>Daramalan College</td></tr><tr><td>Independent Schools Representative</td><td>Mark Vanderstoep</td><td>Trinity Christian School</td></tr><tr><td>Catholic Education Office&nbsp;</td><td>Sherree Bush</td><td>Archdiocesan Sport Coordinator</td></tr><tr><td>SSACT Representative</td><td>Aaron Powell</td><td>School Sport ACT</td></tr><tr><td>ED Representative</td><td>Kylie Beer</td><td>Curriculum Support</td></tr></tbody></table></figure>\n<!-- /wp:table -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>2019 Regional Information</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><strong><a href=\"https://schoolsportsact.blob.core.windows.net/schoolsportsact-container-eas/SiteContentFiles-2019%20SSACT%20Membership%20Information.pdf\">2019 SSACT Membership Information.pdf</a></strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><a href=\"https://schoolsportsact.blob.core.windows.net/schoolsportsact-container-eas/SiteContentFiles-SSACT%20RSSAG%20Terms%20of%20Reference.pdf\"><strong>SSACT RSSAG Terms of Reference.pdf</strong></a></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong><a href=\"https://schoolsportsact.blob.core.windows.net/schoolsportsact-container-eas/SiteContentFiles-RSSAG%20Primary%20Regional%20Information%202019.pdf\">RSSAG Primary Regional Information 2019.pdf</a></strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong><a href=\"https://schoolsportsact.blob.core.windows.net/schoolsportsact-container-eas/SiteContentFiles-RSSAG%20Secondary%20Regional%20Information%202019.pdf\">RSSAG Secondary Regional Information 2019.pdf</a></strong></p>\n<!-- /wp:paragraph -->', 'Regional School Sport Advisory Group', '', 'publish', 'closed', 'closed', '', 'regional-school-sport-advisory-group', '', '', '2019-12-14 21:14:02', '2019-12-14 21:14:02', '', 0, 'http://localhost:8888/ssact/?page_id=183', 0, 'page', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(184, 1, '2019-12-14 21:14:01', '2019-12-14 21:14:01', '<!-- wp:paragraph -->\n<p>Welcome!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>The Regional School Sport Advisory Group (RSSAG - formerly the School Sport Management Committee), was established in 2011 when the ACT Primary School Sport Association (ACTPSSA) and ACT Secondary School Sport Association (ACTSSSA) merged to be under the one umbrella overseen by representatives from each of the six regions. It is is an advisory group to the ACT School Sport Council.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Members of the RSSAG comprise of a School Principal as Chair , the six Regional Presidents and representatives for; Government Colleges, Association of Independent Schools, Catholic Education Office, Associated Southern Colleges, School Sport ACT and the Education Directorate.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>The group meets four times a year to discuss and resolve issues that arise from administering School Sport in the ACT. It is responsible for ensuring that all ACT regional and state events are organised and held in an appropriate manner that instils a correct code of conduct amongst all competitors, managers and spectators.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>&nbsp;<strong>Julie Douglas<br>Chair of the Regional School Sport Advisory Group</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>2019 RSSAG Members</h3>\n<!-- /wp:heading -->\n\n<!-- wp:table -->\n<figure class=\"wp-block-table\"><table class=\"\"><tbody><tr><td>Chair</td><td>Julie Douglas</td><td>St Bedes Primary School</td></tr><tr><td>Deputy Chair</td><td>Amanda Beresford</td><td>Curtin Primary School</td></tr><tr><td>Secretary</td><td>Amanda Beresford</td><td>Curtin Primary School</td></tr><tr><td>Secondary North Region President</td><td>Kate Bradley</td><td>Harrison School</td></tr><tr><td>Secondary South Region President</td><td>Mark Vanderstoep</td><td>Trinity Christian School</td></tr><tr><td>Primary Belconnen Region President</td><td>Emmalene Burriss</td><td>&nbsp;Mt Rogers Primary School</td></tr><tr><td>Primary North /Gungahlin Region President</td><td>Kellie Quirk</td><td>Holy Spirit Primary School</td></tr><tr><td>Primary South /Weston Region President</td><td>Amanda Beresford</td><td>Curtin Primary School</td></tr><tr><td>Primary Tuggeranong Region President</td><td>Jon Tucker</td><td>Charles Conder Primary School</td></tr><tr><td>College Sport Representative</td><td>John Forrest</td><td>Dickson College</td></tr><tr><td>ASC Representative</td><td>Jamaya Ferguson</td><td>Daramalan College</td></tr><tr><td>Independent Schools Representative</td><td>Mark Vanderstoep</td><td>Trinity Christian School</td></tr><tr><td>Catholic Education Office&nbsp;</td><td>Sherree Bush</td><td>Archdiocesan Sport Coordinator</td></tr><tr><td>SSACT Representative</td><td>Aaron Powell</td><td>School Sport ACT</td></tr><tr><td>ED Representative</td><td>Kylie Beer</td><td>Curriculum Support</td></tr></tbody></table></figure>\n<!-- /wp:table -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>2019 Regional Information</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><strong><a href=\"https://schoolsportsact.blob.core.windows.net/schoolsportsact-container-eas/SiteContentFiles-2019%20SSACT%20Membership%20Information.pdf\">2019 SSACT Membership Information.pdf</a></strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><a href=\"https://schoolsportsact.blob.core.windows.net/schoolsportsact-container-eas/SiteContentFiles-SSACT%20RSSAG%20Terms%20of%20Reference.pdf\"><strong>SSACT RSSAG Terms of Reference.pdf</strong></a></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong><a href=\"https://schoolsportsact.blob.core.windows.net/schoolsportsact-container-eas/SiteContentFiles-RSSAG%20Primary%20Regional%20Information%202019.pdf\">RSSAG Primary Regional Information 2019.pdf</a></strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong><a href=\"https://schoolsportsact.blob.core.windows.net/schoolsportsact-container-eas/SiteContentFiles-RSSAG%20Secondary%20Regional%20Information%202019.pdf\">RSSAG Secondary Regional Information 2019.pdf</a></strong></p>\n<!-- /wp:paragraph -->', 'Regional School Sport Advisory Group', '', 'inherit', 'closed', 'closed', '', '183-revision-v1', '', '', '2019-12-14 21:14:01', '2019-12-14 21:14:01', '', 183, 'http://localhost:8888/ssact/blog/2019/12/14/183-revision-v1/', 0, 'revision', '', 0),
(185, 1, '2019-12-14 21:14:22', '2019-12-14 21:14:22', '<!-- wp:paragraph -->\n<p><strong>Education Training Directorate, Catholic Education Office and Independent Schools.</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>School Sport ACT can only continue to support the delivery of school sport in the ACT and provide our students with an opportunity to progress on their representative pathway through annual funding support from the three sectors of education. Education values SSACT contribution to the provision of physical activity and competitive representation, their ongoing support is greatly appreciated.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading -->\n<h2>Our Sponsors</h2>\n<!-- /wp:heading -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"https://www.schoolsportact.asn.au/Images/logos/Teachers-Mutual-Bank-Logo1.jpg\" alt=\"\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Teachers Mutual Bank</h3>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li>&nbsp;<a href=\"http://www.tmbank.com.au/\" target=\"_blank\" rel=\"noreferrer noopener\">www.tmbank.com.au/</a></li></ul>\n<!-- /wp:list -->\n\n<!-- wp:paragraph -->\n<p>Teachers Mutual Bank collaborate with a wide range of education community partners and provide financial support through sponsorships, partnerships, grants, scholarships, donations and charities Across the education community we aim to build long-term mutually beneficial relationships with our sponsorship recipients and the community</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>SSACT is fortunate to have Teachers Mutual Bank provide sponsorship to conduct the end of year Excellence Awards and Thanks Evening, their ongoing support is greatly appreciated.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"https://www.schoolsportact.asn.au/Images/logos/one-sport.jpg\" alt=\"\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>One Sportswear</h3>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li>&nbsp;<a href=\"http://www.onesportswear.com.au/\" target=\"_blank\" rel=\"noreferrer noopener\">www.onesportswear.com.au</a></li></ul>\n<!-- /wp:list -->\n\n<!-- wp:paragraph -->\n<p>One Sportswear and Uniforms supplies uniform clothing &amp; accessories to sports clubs, schools, business &amp; government bodies. Under its own label one sportswear manufactures custom made polo shirts, tracksuits, football jerseys &amp; shorts. One Sportswear is the uniform provider for SSACT team members and officials and provides ongoing sponsorship to the organisation, their support is greatly appreciated.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"https://www.schoolsportact.asn.au/Images/logos/r-star.jpg\" alt=\"\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Rising Stars</h3>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li>&nbsp;<a href=\"http://www.risingstars.com.au/\" target=\"_blank\" rel=\"noreferrer noopener\">www.risingstars.com.au</a></li></ul>\n<!-- /wp:list -->\n\n<!-- wp:paragraph -->\n<p>Rising Stars helps shape the growing professionalism of the educational travel sector by tailoring specialised travel programs for a number of our fellow states participating in School Sport Australia events. SSACT has taken on the services of Rising Stars to coordinate all our travel and accommodation needs, to alleviate the burden on our volunteer officials. Their ongoing support and sponsorship, especially in the preparation for the Pacific School Games, has been invaluable</p>\n<!-- /wp:paragraph -->', 'Our sponsors', '', 'publish', 'closed', 'closed', '', 'our-sponsors', '', '', '2019-12-14 21:14:22', '2019-12-14 21:14:22', '', 0, 'http://localhost:8888/ssact/?page_id=185', 0, 'page', '', 0),
(186, 1, '2019-12-14 21:14:22', '2019-12-14 21:14:22', '<!-- wp:paragraph -->\n<p><strong>Education Training Directorate, Catholic Education Office and Independent Schools.</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>School Sport ACT can only continue to support the delivery of school sport in the ACT and provide our students with an opportunity to progress on their representative pathway through annual funding support from the three sectors of education. Education values SSACT contribution to the provision of physical activity and competitive representation, their ongoing support is greatly appreciated.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading -->\n<h2>Our Sponsors</h2>\n<!-- /wp:heading -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"https://www.schoolsportact.asn.au/Images/logos/Teachers-Mutual-Bank-Logo1.jpg\" alt=\"\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Teachers Mutual Bank</h3>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li>&nbsp;<a href=\"http://www.tmbank.com.au/\" target=\"_blank\" rel=\"noreferrer noopener\">www.tmbank.com.au/</a></li></ul>\n<!-- /wp:list -->\n\n<!-- wp:paragraph -->\n<p>Teachers Mutual Bank collaborate with a wide range of education community partners and provide financial support through sponsorships, partnerships, grants, scholarships, donations and charities Across the education community we aim to build long-term mutually beneficial relationships with our sponsorship recipients and the community</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>SSACT is fortunate to have Teachers Mutual Bank provide sponsorship to conduct the end of year Excellence Awards and Thanks Evening, their ongoing support is greatly appreciated.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"https://www.schoolsportact.asn.au/Images/logos/one-sport.jpg\" alt=\"\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>One Sportswear</h3>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li>&nbsp;<a href=\"http://www.onesportswear.com.au/\" target=\"_blank\" rel=\"noreferrer noopener\">www.onesportswear.com.au</a></li></ul>\n<!-- /wp:list -->\n\n<!-- wp:paragraph -->\n<p>One Sportswear and Uniforms supplies uniform clothing &amp; accessories to sports clubs, schools, business &amp; government bodies. Under its own label one sportswear manufactures custom made polo shirts, tracksuits, football jerseys &amp; shorts. One Sportswear is the uniform provider for SSACT team members and officials and provides ongoing sponsorship to the organisation, their support is greatly appreciated.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"https://www.schoolsportact.asn.au/Images/logos/r-star.jpg\" alt=\"\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Rising Stars</h3>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li>&nbsp;<a href=\"http://www.risingstars.com.au/\" target=\"_blank\" rel=\"noreferrer noopener\">www.risingstars.com.au</a></li></ul>\n<!-- /wp:list -->\n\n<!-- wp:paragraph -->\n<p>Rising Stars helps shape the growing professionalism of the educational travel sector by tailoring specialised travel programs for a number of our fellow states participating in School Sport Australia events. SSACT has taken on the services of Rising Stars to coordinate all our travel and accommodation needs, to alleviate the burden on our volunteer officials. Their ongoing support and sponsorship, especially in the preparation for the Pacific School Games, has been invaluable</p>\n<!-- /wp:paragraph -->', 'Our sponsors', '', 'inherit', 'closed', 'closed', '', '185-revision-v1', '', '', '2019-12-14 21:14:22', '2019-12-14 21:14:22', '', 185, 'http://localhost:8888/ssact/blog/2019/12/14/185-revision-v1/', 0, 'revision', '', 0),
(187, 1, '2019-12-14 21:14:35', '2019-12-14 21:14:35', '<!-- wp:paragraph -->\n<p><strong>Liz Yuen</strong><br>Executive Officer<br><a href=\"mailto:operations@schoolsportact.org.au\">eo@schoolsportact.org.au</a></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>SImone&nbsp;Kuhlmann</strong><br>Finance Administration Officer<br><a href=\"mailto:info@schoolsportact.org.au\">business@schoolsportact.org.au</a></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>Aaron Powell</strong><br>Support Officer<br><a href=\"mailto:info@schoolsportact.org.au\">info@schoolsportact.org.au</a></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>SSACT State Team Official Vacant Positions&nbsp;</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Currently no 2019 team positions vacant</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong><br>For more information contact the SSACT Office and refer to this document;</strong>&nbsp;</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong><a href=\"https://schoolsportsact.blob.core.windows.net/schoolsportsact-container-eas/SiteContentFiles-SSACT%20Team%20Officials\'%20Responsibilities%20and%20Processes.pdf\">SSACT Team Officials\' Responsibilities and Processes.pdf</a></strong></p>\n<!-- /wp:paragraph -->', 'School Sport ACT team', '', 'publish', 'closed', 'closed', '', 'school-sport-act-team', '', '', '2019-12-14 21:14:35', '2019-12-14 21:14:35', '', 0, 'http://localhost:8888/ssact/?page_id=187', 0, 'page', '', 0),
(188, 1, '2019-12-14 21:14:35', '2019-12-14 21:14:35', '<!-- wp:paragraph -->\n<p><strong>Liz Yuen</strong><br>Executive Officer<br><a href=\"mailto:operations@schoolsportact.org.au\">eo@schoolsportact.org.au</a></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>SImone&nbsp;Kuhlmann</strong><br>Finance Administration Officer<br><a href=\"mailto:info@schoolsportact.org.au\">business@schoolsportact.org.au</a></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>Aaron Powell</strong><br>Support Officer<br><a href=\"mailto:info@schoolsportact.org.au\">info@schoolsportact.org.au</a></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>SSACT State Team Official Vacant Positions&nbsp;</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Currently no 2019 team positions vacant</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong><br>For more information contact the SSACT Office and refer to this document;</strong>&nbsp;</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong><a href=\"https://schoolsportsact.blob.core.windows.net/schoolsportsact-container-eas/SiteContentFiles-SSACT%20Team%20Officials\'%20Responsibilities%20and%20Processes.pdf\">SSACT Team Officials\' Responsibilities and Processes.pdf</a></strong></p>\n<!-- /wp:paragraph -->', 'School Sport ACT team', '', 'inherit', 'closed', 'closed', '', '187-revision-v1', '', '', '2019-12-14 21:14:35', '2019-12-14 21:14:35', '', 187, 'http://localhost:8888/ssact/blog/2019/12/14/187-revision-v1/', 0, 'revision', '', 0),
(189, 1, '2019-12-14 21:20:03', '2019-12-14 21:20:03', '<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns\"><!-- wp:column {\"width\":60} -->\n<div class=\"wp-block-column\" style=\"flex-basis:60%\"><!-- wp:html -->\n<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3258.2324039686596!2d149.15964631642555!3d-35.25047359951386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b165244d391fac3%3A0xa35a30c327313e7e!2sSchool%20Sport%20ACT!5e0!3m2!1sen!2sau!4v1576358236237!5m2!1sen!2sau\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>\n<!-- /wp:html -->\n\n<!-- wp:paragraph -->\n<p>We would love to hear from you and would be happy to assist with further details of our sporting programs</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Use the form below to contact School Sport ACT:</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column {\"width\":40} -->\n<div class=\"wp-block-column\" style=\"flex-basis:40%\"><!-- wp:heading -->\n<h2>Contacts</h2>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li><a href=\"https://www.schoolsportact.asn.au/Home/Contact#\">ACT Sports House 100 Maitland Street, Hackett, ACT 2602</a></li><li><a href=\"https://www.schoolsportact.asn.au/Home/Contact#\">info@schoolsportact.org.au</a></li><li><a href=\"https://www.schoolsportact.asn.au/Home/Contact#\">+61 2 6205 9174</a></li><li><a href=\"https://www.schoolsportact.asn.au/Home/Contact#\">+61 2 6205 7799</a></li></ul>\n<!-- /wp:list -->\n\n<!-- wp:heading -->\n<h2>Business Hours</h2>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li><strong>Monday-Friday:</strong>&nbsp;9am to 5pm</li><li><strong>Saturday:</strong>&nbsp;Closed</li><li><strong>Sunday:</strong>&nbsp;Closed</li></ul>\n<!-- /wp:list --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Contact us', '', 'publish', 'closed', 'closed', '', 'contact-us', '', '', '2019-12-14 21:20:25', '2019-12-14 21:20:25', '', 0, 'http://localhost:8888/ssact/?page_id=189', 0, 'page', '', 0),
(190, 1, '2019-12-14 21:20:03', '2019-12-14 21:20:03', '<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns\"><!-- wp:column {\"width\":80} -->\n<div class=\"wp-block-column\" style=\"flex-basis:80%\"><!-- wp:html -->\n<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3258.2324039686596!2d149.15964631642555!3d-35.25047359951386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b165244d391fac3%3A0xa35a30c327313e7e!2sSchool%20Sport%20ACT!5e0!3m2!1sen!2sau!4v1576358236237!5m2!1sen!2sau\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>\n<!-- /wp:html -->\n\n<!-- wp:paragraph -->\n<p>We would love to hear from you and would be happy to assist with further details of our sporting programs</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Use the form below to contact School Sport ACT:</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column {\"width\":20} -->\n<div class=\"wp-block-column\" style=\"flex-basis:20%\"><!-- wp:heading -->\n<h2>Contacts</h2>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li><a href=\"https://www.schoolsportact.asn.au/Home/Contact#\">ACT Sports House 100 Maitland Street, Hackett, ACT 2602</a></li><li><a href=\"https://www.schoolsportact.asn.au/Home/Contact#\">info@schoolsportact.org.au</a></li><li><a href=\"https://www.schoolsportact.asn.au/Home/Contact#\">+61 2 6205 9174</a></li><li><a href=\"https://www.schoolsportact.asn.au/Home/Contact#\">+61 2 6205 7799</a></li></ul>\n<!-- /wp:list -->\n\n<!-- wp:heading -->\n<h2>Business Hours</h2>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li><strong>Monday-Friday:</strong>&nbsp;9am to 5pm</li><li><strong>Saturday:</strong>&nbsp;Closed</li><li><strong>Sunday:</strong>&nbsp;Closed</li></ul>\n<!-- /wp:list --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Contact us', '', 'inherit', 'closed', 'closed', '', '189-revision-v1', '', '', '2019-12-14 21:20:03', '2019-12-14 21:20:03', '', 189, 'http://localhost:8888/ssact/blog/2019/12/14/189-revision-v1/', 0, 'revision', '', 0),
(191, 1, '2019-12-14 21:20:24', '2019-12-14 21:20:24', '<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns\"><!-- wp:column {\"width\":60} -->\n<div class=\"wp-block-column\" style=\"flex-basis:60%\"><!-- wp:html -->\n<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3258.2324039686596!2d149.15964631642555!3d-35.25047359951386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b165244d391fac3%3A0xa35a30c327313e7e!2sSchool%20Sport%20ACT!5e0!3m2!1sen!2sau!4v1576358236237!5m2!1sen!2sau\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>\n<!-- /wp:html -->\n\n<!-- wp:paragraph -->\n<p>We would love to hear from you and would be happy to assist with further details of our sporting programs</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Use the form below to contact School Sport ACT:</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column {\"width\":40} -->\n<div class=\"wp-block-column\" style=\"flex-basis:40%\"><!-- wp:heading -->\n<h2>Contacts</h2>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li><a href=\"https://www.schoolsportact.asn.au/Home/Contact#\">ACT Sports House 100 Maitland Street, Hackett, ACT 2602</a></li><li><a href=\"https://www.schoolsportact.asn.au/Home/Contact#\">info@schoolsportact.org.au</a></li><li><a href=\"https://www.schoolsportact.asn.au/Home/Contact#\">+61 2 6205 9174</a></li><li><a href=\"https://www.schoolsportact.asn.au/Home/Contact#\">+61 2 6205 7799</a></li></ul>\n<!-- /wp:list -->\n\n<!-- wp:heading -->\n<h2>Business Hours</h2>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li><strong>Monday-Friday:</strong>&nbsp;9am to 5pm</li><li><strong>Saturday:</strong>&nbsp;Closed</li><li><strong>Sunday:</strong>&nbsp;Closed</li></ul>\n<!-- /wp:list --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Contact us', '', 'inherit', 'closed', 'closed', '', '189-revision-v1', '', '', '2019-12-14 21:20:24', '2019-12-14 21:20:24', '', 189, 'http://localhost:8888/ssact/blog/2019/12/14/189-revision-v1/', 0, 'revision', '', 0),
(193, 31, '2019-12-14 22:02:12', '2019-12-14 22:02:12', '', 'School Sport Coordinator Roles and Responsibilities', 'Information for all Sport Coordinators within ACT schools.', 'inherit', 'open', 'closed', '', 'school-sport-coordinator-roles-and-responsibilities', '', '', '2019-12-15 04:58:57', '2019-12-15 04:58:57', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/School-Sport-Coordinator-Roles-and-Responsibilities.pdf', 0, 'attachment', 'application/pdf', 0),
(194, 31, '2019-12-14 22:02:31', '2019-12-14 22:02:31', '', '2019 SSACT Membership Information', 'Information for School Sport Coordinators, Principals, School Business Managers and Finance Officers.', 'inherit', 'open', 'closed', '', '2019-ssact-membership-information', '', '', '2019-12-15 05:06:02', '2019-12-15 05:06:02', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/2019-SSACT-Membership-Information.pdf', 0, 'attachment', 'application/pdf', 0),
(213, 1, '2019-12-15 00:07:28', '2019-12-15 00:07:28', ' ', '', '', 'publish', 'closed', 'closed', '', '213', '', '', '2019-12-15 05:11:50', '2019-12-15 05:11:50', '', 0, 'http://localhost:8888/ssact/?p=213', 3, 'nav_menu_item', '', 0),
(214, 1, '2019-12-15 00:07:28', '2019-12-15 00:07:28', ' ', '', '', 'publish', 'closed', 'closed', '', '214', '', '', '2019-12-15 05:11:50', '2019-12-15 05:11:50', '', 0, 'http://localhost:8888/ssact/?p=214', 4, 'nav_menu_item', '', 0),
(215, 1, '2019-12-15 00:07:28', '2019-12-15 00:07:28', ' ', '', '', 'publish', 'closed', 'closed', '', '215', '', '', '2019-12-15 05:11:50', '2019-12-15 05:11:50', '', 0, 'http://localhost:8888/ssact/?p=215', 5, 'nav_menu_item', '', 0),
(216, 1, '2019-12-15 00:07:28', '2019-12-15 00:07:28', ' ', '', '', 'publish', 'closed', 'closed', '', '216', '', '', '2019-12-15 05:11:50', '2019-12-15 05:11:50', '', 0, 'http://localhost:8888/ssact/?p=216', 6, 'nav_menu_item', '', 0),
(217, 1, '2019-12-15 00:07:28', '2019-12-15 00:07:28', ' ', '', '', 'publish', 'closed', 'closed', '', '217', '', '', '2019-12-15 05:11:50', '2019-12-15 05:11:50', '', 0, 'http://localhost:8888/ssact/?p=217', 7, 'nav_menu_item', '', 0),
(218, 1, '2019-12-15 00:06:38', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-12-15 00:06:38', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=218', 1, 'nav_menu_item', '', 0),
(219, 1, '2019-12-15 00:06:38', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-12-15 00:06:38', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=219', 1, 'nav_menu_item', '', 0),
(220, 1, '2019-12-15 00:06:38', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-12-15 00:06:38', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=220', 1, 'nav_menu_item', '', 0),
(221, 1, '2019-12-15 00:06:38', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-12-15 00:06:38', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=221', 1, 'nav_menu_item', '', 0),
(222, 1, '2019-12-15 00:07:28', '2019-12-15 00:07:28', ' ', '', '', 'publish', 'closed', 'closed', '', '222', '', '', '2019-12-15 05:11:50', '2019-12-15 05:11:50', '', 0, 'http://localhost:8888/ssact/?p=222', 8, 'nav_menu_item', '', 0),
(223, 1, '2019-12-15 00:06:38', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-12-15 00:06:38', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=223', 1, 'nav_menu_item', '', 0),
(224, 1, '2019-12-15 00:06:38', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-12-15 00:06:38', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=224', 1, 'nav_menu_item', '', 0),
(225, 1, '2019-12-15 00:06:38', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-12-15 00:06:38', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=225', 1, 'nav_menu_item', '', 0),
(226, 1, '2019-12-15 00:06:38', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-12-15 00:06:38', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=226', 1, 'nav_menu_item', '', 0),
(227, 1, '2019-12-15 00:07:28', '2019-12-15 00:07:28', ' ', '', '', 'publish', 'closed', 'closed', '', '227', '', '', '2019-12-15 05:11:50', '2019-12-15 05:11:50', '', 0, 'http://localhost:8888/ssact/?p=227', 9, 'nav_menu_item', '', 0),
(228, 1, '2019-12-15 00:06:38', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-12-15 00:06:38', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=228', 1, 'nav_menu_item', '', 0),
(229, 1, '2019-12-15 01:00:03', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-12-15 01:00:03', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=229', 1, 'nav_menu_item', '', 0),
(231, 1, '2019-12-15 01:24:10', '2019-12-15 01:24:10', '', 'bg_home3', '', 'inherit', 'open', 'closed', '', 'bg_home3', '', '', '2019-12-15 01:24:10', '2019-12-15 01:24:10', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/bg_home3.jpg', 0, 'attachment', 'image/jpeg', 0),
(232, 1, '2019-12-15 01:25:31', '2019-12-15 01:25:31', '', 'logo3', '', 'inherit', 'open', 'closed', '', 'logo3', '', '', '2019-12-15 01:25:31', '2019-12-15 01:25:31', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/logo3.png', 0, 'attachment', 'image/png', 0),
(233, 1, '2019-12-15 01:25:32', '2019-12-15 01:25:32', '', 'logo4', '', 'inherit', 'open', 'closed', '', 'logo4', '', '', '2019-12-15 01:25:32', '2019-12-15 01:25:32', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/logo4.png', 0, 'attachment', 'image/png', 0),
(234, 1, '2019-12-15 01:25:34', '2019-12-15 01:25:34', '', 'logo5', '', 'inherit', 'open', 'closed', '', 'logo5', '', '', '2019-12-15 01:25:34', '2019-12-15 01:25:34', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/logo5.png', 0, 'attachment', 'image/png', 0),
(235, 1, '2019-12-15 01:25:47', '2019-12-15 01:25:47', '', 'logo1', '', 'inherit', 'open', 'closed', '', 'logo1', '', '', '2019-12-15 01:25:47', '2019-12-15 01:25:47', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/logo1.png', 0, 'attachment', 'image/png', 0),
(236, 1, '2019-12-15 01:25:48', '2019-12-15 01:25:48', '', 'logo2', '', 'inherit', 'open', 'closed', '', 'logo2', '', '', '2019-12-15 01:25:48', '2019-12-15 01:25:48', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/logo2.png', 0, 'attachment', 'image/png', 0),
(237, 1, '2019-12-15 01:30:22', '2019-12-15 01:30:22', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_5df58c70d9a80', '', '', '2019-12-15 01:32:10', '2019-12-15 01:32:10', '', 123, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=237', 0, 'acf-field', '', 0),
(238, 1, '2019-12-15 01:30:22', '2019-12-15 01:30:22', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Media', 'media', 'publish', 'closed', 'closed', '', 'field_5df58c82d9a81', '', '', '2019-12-19 18:29:41', '2019-12-19 18:29:41', '', 123, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=238', 9, 'acf-field', '', 0),
(239, 1, '2019-12-15 01:32:10', '2019-12-15 01:32:10', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:24:\"Registration description\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Registration', 'registration', 'publish', 'closed', 'closed', '', 'field_5df58cf720c24', '', '', '2019-12-19 18:29:40', '2019-12-19 18:29:40', '', 123, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=239', 5, 'acf-field', '', 0),
(240, 1, '2019-12-15 01:35:02', '2019-12-15 01:35:02', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Registration & Help Event', 'box_registration_&_help_event', 'publish', 'closed', 'closed', '', 'field_5df58d2aa0b89', '', '', '2019-12-19 18:29:40', '2019-12-19 18:29:40', '', 123, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=240', 4, 'acf-field', '', 0),
(241, 1, '2019-12-15 01:35:02', '2019-12-15 01:35:02', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Registration Image', 'registration_image', 'publish', 'closed', 'closed', '', 'field_5df58d70a0b8a', '', '', '2019-12-19 18:29:41', '2019-12-19 18:29:41', '', 123, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=241', 6, 'acf-field', '', 0),
(242, 1, '2019-12-15 01:35:02', '2019-12-15 01:35:02', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:20:\"Organise description\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Help Organise', 'help_organise', 'publish', 'closed', 'closed', '', 'field_5df58d82a0b8b', '', '', '2019-12-19 18:29:41', '2019-12-19 18:29:41', '', 123, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=242', 7, 'acf-field', '', 0),
(243, 1, '2019-12-15 01:35:02', '2019-12-15 01:35:02', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Help Organise Image', 'help_organise_image', 'publish', 'closed', 'closed', '', 'field_5df58da8a0b8c', '', '', '2019-12-19 18:29:41', '2019-12-19 18:29:41', '', 123, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=243', 8, 'acf-field', '', 0),
(244, 31, '2019-12-15 04:18:16', '2019-12-15 04:18:16', '', 'Resources', '', 'publish', 'closed', 'closed', '', 'resources', '', '', '2019-12-15 06:06:06', '2019-12-15 06:06:06', '', 0, 'http://localhost:8888/ssact/?post_type=search-filter-widget&#038;p=244', 0, 'search-filter-widget', '', 0),
(245, 31, '2019-12-15 04:19:43', '2019-12-15 04:19:43', '<!-- wp:columns -->\n<div class=\"wp-block-columns\"><!-- wp:column {\"width\":25} -->\n<div class=\"wp-block-column\" style=\"flex-basis:25%\"><!-- wp:shortcode -->\n[searchandfilter id=\"244\"]\n<!-- /wp:shortcode --></div>\n<!-- /wp:column -->\n\n<!-- wp:column {\"width\":75} -->\n<div class=\"wp-block-column\" style=\"flex-basis:75%\"><!-- wp:shortcode -->\n[searchandfilter id=\"244\" show=\"results\"]\n<!-- /wp:shortcode --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Resources', '', 'publish', 'closed', 'closed', '', 'resources', '', '', '2019-12-15 05:57:53', '2019-12-15 05:57:53', '', 0, 'http://localhost:8888/ssact/?page_id=245', 0, 'page', '', 0),
(246, 31, '2019-12-15 04:19:43', '2019-12-15 04:19:43', '<!-- wp:paragraph -->\n<p>[searchandfilter id=\"244\" show=\"results\"]</p>\n<!-- /wp:paragraph -->', 'Resources', '', 'inherit', 'closed', 'closed', '', '245-revision-v1', '', '', '2019-12-15 04:19:43', '2019-12-15 04:19:43', '', 245, 'http://localhost:8888/ssact/blog/2019/12/15/245-revision-v1/', 0, 'revision', '', 0),
(247, 31, '2019-12-15 04:21:05', '2019-12-15 04:21:05', '<!-- wp:columns -->\n<div class=\"wp-block-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:shortcode -->\n[searchandfilter id=\"244\"]\n<!-- /wp:shortcode --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:shortcode -->\n[searchandfilter id=\"244\" show=\"results\"]\n<!-- /wp:shortcode --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Resources', '', 'inherit', 'closed', 'closed', '', '245-revision-v1', '', '', '2019-12-15 04:21:05', '2019-12-15 04:21:05', '', 245, 'http://localhost:8888/ssact/blog/2019/12/15/245-revision-v1/', 0, 'revision', '', 0),
(248, 31, '2019-12-15 04:21:18', '2019-12-15 04:21:18', '<!-- wp:columns -->\n<div class=\"wp-block-columns\"><!-- wp:column {\"width\":30} -->\n<div class=\"wp-block-column\" style=\"flex-basis:30%\"><!-- wp:shortcode -->\n[searchandfilter id=\"244\"]\n<!-- /wp:shortcode --></div>\n<!-- /wp:column -->\n\n<!-- wp:column {\"width\":70} -->\n<div class=\"wp-block-column\" style=\"flex-basis:70%\"><!-- wp:shortcode -->\n[searchandfilter id=\"244\" show=\"results\"]\n<!-- /wp:shortcode --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Resources', '', 'inherit', 'closed', 'closed', '', '245-revision-v1', '', '', '2019-12-15 04:21:18', '2019-12-15 04:21:18', '', 245, 'http://localhost:8888/ssact/blog/2019/12/15/245-revision-v1/', 0, 'revision', '', 0),
(249, 31, '2019-12-15 04:36:40', '2019-12-15 04:36:40', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:10:\"attachment\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"all\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Resource taxonomies', 'resource-taxonomies', 'publish', 'closed', 'closed', '', 'group_5df5b50615672', '', '', '2019-12-15 05:05:53', '2019-12-15 05:05:53', '', 0, 'http://localhost:8888/ssact/?post_type=acf-field-group&#038;p=249', 0, 'acf-field-group', '', 0),
(250, 31, '2019-12-15 04:36:40', '2019-12-15 04:36:40', 'a:12:{s:4:\"type\";s:8:\"checkbox\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"choices\";a:5:{s:16:\"Primary regional\";s:16:\"Primary regional\";s:18:\"Secondary regional\";s:18:\"Secondary regional\";s:16:\"College regional\";s:16:\"College regional\";s:14:\"Representative\";s:14:\"Representative\";s:21:\"Education Directorate\";s:21:\"Education Directorate\";}s:12:\"allow_custom\";i:0;s:13:\"default_value\";a:0:{}s:6:\"layout\";s:8:\"vertical\";s:6:\"toggle\";i:1;s:13:\"return_format\";s:5:\"value\";s:11:\"save_custom\";i:0;}', 'Category', 'category', 'publish', 'closed', 'closed', '', 'field_5df5b52845b46', '', '', '2019-12-15 04:52:23', '2019-12-15 04:52:23', '', 249, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=250', 1, 'acf-field', '', 0),
(251, 31, '2019-12-15 04:39:48', '2019-12-15 04:39:48', 'a:12:{s:4:\"type\";s:8:\"checkbox\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"choices\";a:8:{s:11:\"Information\";s:11:\"Information\";s:4:\"Form\";s:4:\"Form\";s:10:\"Governance\";s:10:\"Governance\";s:5:\"Guide\";s:5:\"Guide\";s:6:\"Policy\";s:6:\"Policy\";s:9:\"Procedure\";s:9:\"Procedure\";s:7:\"Program\";s:7:\"Program\";s:5:\"Venue\";s:5:\"Venue\";}s:12:\"allow_custom\";i:0;s:13:\"default_value\";a:0:{}s:6:\"layout\";s:8:\"vertical\";s:6:\"toggle\";i:1;s:13:\"return_format\";s:5:\"value\";s:11:\"save_custom\";i:0;}', 'Type', 'type', 'publish', 'closed', 'closed', '', 'field_5df5b86d15ba8', '', '', '2019-12-15 04:40:05', '2019-12-15 04:40:05', '', 249, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=251', 0, 'acf-field', '', 0),
(253, 31, '2019-12-15 04:55:32', '2019-12-15 04:55:32', 'a:12:{s:4:\"type\";s:8:\"checkbox\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"choices\";a:17:{s:19:\"Australian Football\";s:19:\"Australian Football\";s:8:\"Baseball\";s:8:\"Baseball\";s:10:\"Basketball\";s:10:\"Basketball\";s:7:\"Cricket\";s:7:\"Cricket\";s:13:\"Cross Country\";s:13:\"Cross Country\";s:6:\"Diving\";s:6:\"Diving\";s:17:\"Football (Soccer)\";s:17:\"Football (Soccer)\";s:4:\"Golf\";s:4:\"Golf\";s:6:\"Hockey\";s:6:\"Hockey\";s:7:\"Netball\";s:7:\"Netball\";s:12:\"Rugby League\";s:12:\"Rugby League\";s:8:\"Softball\";s:8:\"Softball\";s:8:\"Swimming\";s:8:\"Swimming\";s:6:\"Tennis\";s:6:\"Tennis\";s:5:\"Touch\";s:5:\"Touch\";s:15:\"Track and Field\";s:15:\"Track and Field\";s:10:\"Volleyball\";s:10:\"Volleyball\";}s:12:\"allow_custom\";i:0;s:13:\"default_value\";a:0:{}s:6:\"layout\";s:8:\"vertical\";s:6:\"toggle\";i:1;s:13:\"return_format\";s:5:\"value\";s:11:\"save_custom\";i:0;}', 'Sports', 'sports', 'publish', 'closed', 'closed', '', 'field_5df5bc67d8bc8', '', '', '2019-12-15 04:55:32', '2019-12-15 04:55:32', '', 249, 'http://localhost:8888/ssact/?post_type=acf-field&p=253', 2, 'acf-field', '', 0),
(254, 31, '2019-12-15 05:11:50', '2019-12-15 05:11:50', ' ', '', '', 'publish', 'closed', 'closed', '', '254', '', '', '2019-12-15 05:11:50', '2019-12-15 05:11:50', '', 0, 'http://localhost:8888/ssact/?p=254', 2, 'nav_menu_item', '', 0),
(255, 31, '2019-12-15 05:11:50', '2019-12-15 05:11:50', '', 'Sports', '', 'publish', 'closed', 'closed', '', 'sports', '', '', '2019-12-15 05:11:50', '2019-12-15 05:11:50', '', 0, 'http://localhost:8888/ssact/?p=255', 1, 'nav_menu_item', '', 0),
(256, 31, '2019-12-15 05:15:41', '2019-12-15 05:15:41', '', '2019 SSACT Request for Inclusion ACT Cross Country', '', 'inherit', 'open', 'closed', '', '2019-ssact-request-for-inclusion-act-cross-country', '', '', '2019-12-15 05:22:02', '2019-12-15 05:22:02', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/2019-SSACT-Request-for-Inclusion-ACT-Cross-Country.doc', 0, 'attachment', 'application/msword', 0),
(257, 31, '2019-12-15 05:15:42', '2019-12-15 05:15:42', '', 'Event Total Participation Numbers', '', 'inherit', 'open', 'closed', '', 'event-total-participation-numbers', '', '', '2019-12-15 05:21:32', '2019-12-15 05:21:32', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/Event-Total-Participation-Numbers.xlsx', 0, 'attachment', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 0),
(258, 31, '2019-12-15 05:15:42', '2019-12-15 05:15:42', '', 'Cross Country Championship Required Officials', '', 'inherit', 'open', 'closed', '', 'cross-country-championship-required-officials', '', '', '2019-12-15 05:21:48', '2019-12-15 05:21:48', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/Cross-Country-Championship-Required-Officials.docx', 0, 'attachment', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 0),
(259, 31, '2019-12-15 05:15:43', '2019-12-15 05:15:43', '', 'Cross Country Entry Form', '', 'inherit', 'open', 'closed', '', 'cross-country-entry-form', '', '', '2019-12-15 05:21:12', '2019-12-15 05:21:12', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/Cross-Country-Entry-Form.xlsx', 0, 'attachment', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 0),
(260, 31, '2019-12-15 05:15:44', '2019-12-15 05:15:44', '', 'Stromlo Forest Park Course Maps', '', 'inherit', 'open', 'closed', '', 'stromlo-forest-park-course-maps', '', '', '2019-12-15 05:20:24', '2019-12-15 05:20:24', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/Stromlo-Forest-Park-Course-Maps.docx', 0, 'attachment', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 0),
(261, 31, '2019-12-15 05:15:44', '2019-12-15 05:15:44', '', 'ACT Combined Cross Country Championship Program', '', 'inherit', 'open', 'closed', '', 'act-combined-cross-country-championship-program', '', '', '2019-12-15 05:21:06', '2019-12-15 05:21:06', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/ACT-Combined-Cross-Country-Championship-Program.docx', 0, 'attachment', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 0),
(262, 31, '2019-12-15 05:15:47', '2019-12-15 05:15:47', '', 'Woden Park Conditions of Hire - Active Canberra', '', 'inherit', 'open', 'closed', '', 'woden-park-conditions-of-hire-active-canberra', '', '', '2019-12-15 05:20:09', '2019-12-15 05:20:09', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/Woden-Park-Conditions-of-Hire-Active-Canberra.pdf', 0, 'attachment', 'application/pdf', 0),
(263, 31, '2019-12-15 05:16:02', '2019-12-15 05:16:02', '', 'ACT Cross Country Championship Info & Consent Form', '', 'inherit', 'open', 'closed', '', 'act-cross-country-championship-info-consent-form', '', '', '2019-12-15 05:19:28', '2019-12-15 05:19:28', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/ACT-Cross-Country-Championship-Info-Consent-Form.docx', 0, 'attachment', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 0),
(264, 31, '2019-12-15 05:16:03', '2019-12-15 05:16:03', '', 'Regional and ACT Cross Country Event Coordinator Checklist', '', 'inherit', 'open', 'closed', '', 'regional-and-act-cross-country-event-coordinator-checklist', '', '', '2019-12-15 05:19:17', '2019-12-15 05:19:17', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/Regional-and-ACT-Cross-Country-Event-Coordinator-Checklist.pdf', 0, 'attachment', 'application/pdf', 0),
(265, 31, '2019-12-15 05:16:05', '2019-12-15 05:16:05', '', 'SSACT Cross Country Age Eligibility Event Specifications and Qualifying Numbers', '', 'inherit', 'open', 'closed', '', 'ssact-cross-country-age-eligibility-event-specifications-and-qualifying-numbers', '', '', '2019-12-15 05:19:00', '2019-12-15 05:19:00', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/SSACT-Cross-Country-Age-Eligibility-Event-Specifications-and-Qualifying-Numbers.pdf', 0, 'attachment', 'application/pdf', 0),
(266, 31, '2019-12-15 05:16:07', '2019-12-15 05:16:07', '', 'AIS Emergency Procedures', '', 'inherit', 'open', 'closed', '', 'ais-emergency-procedures', '', '', '2019-12-15 05:18:51', '2019-12-15 05:18:51', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/AIS-Emergency-Procedures.pdf', 0, 'attachment', 'application/pdf', 0),
(267, 31, '2019-12-15 05:16:10', '2019-12-15 05:16:10', '', 'AIS Aquatic Centre - Risk Management Plan', '', 'inherit', 'open', 'closed', '', 'ais-aquatic-centre-risk-management-plan', '', '', '2019-12-15 05:18:25', '2019-12-15 05:18:25', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/AIS-Aquatic-Centre-Risk-Management-Plan.pdf', 0, 'attachment', 'application/pdf', 0),
(268, 31, '2019-12-15 05:16:12', '2019-12-15 05:16:12', '', 'ACT Championship Record Holder Pennant Script', '', 'inherit', 'open', 'closed', '', 'act-championship-record-holder-pennant-script', '', '', '2019-12-15 05:18:01', '2019-12-15 05:18:01', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/ACT-Championship-Record-Holder-Pennant-Script.pdf', 0, 'attachment', 'application/pdf', 0),
(269, 31, '2019-12-15 05:57:52', '2019-12-15 05:57:52', '<!-- wp:columns -->\n<div class=\"wp-block-columns\"><!-- wp:column {\"width\":25} -->\n<div class=\"wp-block-column\" style=\"flex-basis:25%\"><!-- wp:shortcode -->\n[searchandfilter id=\"244\"]\n<!-- /wp:shortcode --></div>\n<!-- /wp:column -->\n\n<!-- wp:column {\"width\":75} -->\n<div class=\"wp-block-column\" style=\"flex-basis:75%\"><!-- wp:shortcode -->\n[searchandfilter id=\"244\" show=\"results\"]\n<!-- /wp:shortcode --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Resources', '', 'inherit', 'closed', 'closed', '', '245-revision-v1', '', '', '2019-12-15 05:57:52', '2019-12-15 05:57:52', '', 245, 'http://localhost:8888/ssact/blog/2019/12/15/245-revision-v1/', 0, 'revision', '', 0),
(272, 1, '2019-12-15 22:24:38', '2019-12-15 22:24:38', '', 'Registration', '', 'publish', 'closed', 'closed', '', 'registration', '', '', '2019-12-16 00:04:48', '2019-12-16 00:04:48', '', 0, 'http://localhost:8888/ssact/registration/', 0, 'page', '', 0),
(273, 31, '2019-12-15 22:28:32', '2019-12-15 22:28:32', '', '216_SSACT_Web_Report_02v4', 'This is the report from NINPO', 'inherit', 'open', 'closed', '', '216_ssact_web_report_02v4', '', '', '2019-12-15 22:32:58', '2019-12-15 22:32:58', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/216_SSACT_Web_Report_02v4.pdf', 0, 'attachment', 'application/pdf', 0),
(274, 1, '2019-12-15 23:02:46', '2019-12-15 23:02:46', '', 'Registration', '', 'inherit', 'closed', 'closed', '', '272-revision-v1', '', '', '2019-12-15 23:02:46', '2019-12-15 23:02:46', '', 272, 'http://localhost:8888/ssact/blog/2019/12/15/272-revision-v1/', 0, 'revision', '', 0),
(276, 1, '2019-12-15 23:09:10', '2019-12-15 23:09:10', 'a:7:{s:8:\"location\";a:2:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"272\";}}i:1;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:21:\"page-registration.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:7:{i:0;s:11:\"the_content\";i:1;s:7:\"excerpt\";i:2;s:10:\"discussion\";i:3;s:8:\"comments\";i:4;s:9:\"revisions\";i:5;s:14:\"featured_image\";i:6;s:10:\"categories\";}s:11:\"description\";s:0:\"\";}', 'Group Registration', 'group-registration', 'publish', 'closed', 'closed', '', 'group_5df6bc1ec1d8a', '', '', '2019-12-16 00:00:15', '2019-12-16 00:00:15', '', 0, 'http://localhost:8888/ssact/?post_type=acf-field-group&#038;p=276', 0, 'acf-field-group', '', 0),
(277, 1, '2019-12-15 23:09:10', '2019-12-15 23:09:10', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";i:1;s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"block\";s:12:\"button_label\";s:0:\"\";}', 'Registration', 'registration', 'publish', 'closed', 'closed', '', 'field_5df6bc5794e67', '', '', '2019-12-15 23:09:41', '2019-12-15 23:09:41', '', 276, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=277', 0, 'acf-field', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(278, 1, '2019-12-15 23:09:10', '2019-12-15 23:09:10', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'title', 'publish', 'closed', 'closed', '', 'field_5df6bc7b94e68', '', '', '2019-12-15 23:09:10', '2019-12-15 23:09:10', '', 277, 'http://localhost:8888/ssact/?post_type=acf-field&p=278', 0, 'acf-field', '', 0),
(279, 1, '2019-12-15 23:09:10', '2019-12-15 23:09:10', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image', 'image', 'publish', 'closed', 'closed', '', 'field_5df6bc8294e69', '', '', '2019-12-15 23:09:41', '2019-12-15 23:09:41', '', 277, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=279', 1, 'acf-field', '', 0),
(280, 1, '2019-12-15 23:09:10', '2019-12-15 23:09:10', 'a:10:{s:4:\"type\";s:9:\"page_link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:4:\"page\";}s:8:\"taxonomy\";s:0:\"\";s:10:\"allow_null\";i:0;s:14:\"allow_archives\";i:0;s:8:\"multiple\";i:0;}', 'Link', 'link', 'publish', 'closed', 'closed', '', 'field_5df6bccd94e6a', '', '', '2019-12-16 00:00:15', '2019-12-16 00:00:15', '', 277, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=280', 2, 'acf-field', '', 0),
(282, 1, '2019-12-15 23:11:20', '2019-12-15 23:11:20', '', 'reg_parents', '', 'inherit', 'open', 'closed', '', 'reg_parents', '', '', '2019-12-15 23:11:20', '2019-12-15 23:11:20', '', 272, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/reg_parents.jpg', 0, 'attachment', 'image/jpeg', 0),
(283, 1, '2019-12-15 23:11:33', '2019-12-15 23:11:33', '', 'Registration', '', 'inherit', 'closed', 'closed', '', '272-revision-v1', '', '', '2019-12-15 23:11:33', '2019-12-15 23:11:33', '', 272, 'http://localhost:8888/ssact/blog/2019/12/15/272-revision-v1/', 0, 'revision', '', 0),
(285, 31, '2019-12-15 23:22:51', '2019-12-15 23:22:51', '<!-- wp:paragraph -->\n<p>In 2011, after many years of operating independently the ACT Secondary Schools Sports Association (ACTSSSA) and the ACT Primary Schools Sports Association (ACTPSSA) amalgamated to form the ACT School Sport Management Committee (ACTSSMC).</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":231,\"width\":585,\"height\":389,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large is-resized\"><img src=\"http://localhost:8888/ssact/wp-content/uploads/2019/12/bg_home3-1024x683.jpg\" alt=\"\" class=\"wp-image-231\" width=\"585\" height=\"389\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>To enable oversight of the management committee the ACT Schools Sports Council was established at the same time and remains responsible for the strategic direction and approach of SSACT.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Given these changes the Council was very aware of the need to capture historical records of both organisations and Mr Paul Andrews was therefore engaged to collate the historical data from the ACTSSSA and has collated this information into the “ACTSSSA Results History Project 1977 – 2011”.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><a href=\"https://www.schoolsportact.asn.au/assets/documents/ACTSSSA_Results_History.pdf\">ACTSSSA Results History</a></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>This information is now available to the broader school community where students, parents and interested others can search on the School Sport ACT website and see who has come before them. The council would also like to hear from anyone who information which may be a useful addition to the history we currently have collated. Old photos, sports programs or personal reflections are most welcome.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Christopher Nunn OAM<br>Chair<br>ACT School Sport Council</p>\n<!-- /wp:paragraph -->', 'ACT School Sport History', '', 'inherit', 'closed', 'closed', '', '181-revision-v1', '', '', '2019-12-15 23:22:51', '2019-12-15 23:22:51', '', 181, 'http://localhost:8888/ssact/blog/2019/12/15/181-revision-v1/', 0, 'revision', '', 0),
(286, 31, '2019-12-15 23:24:47', '2019-12-15 23:24:47', '<!-- wp:paragraph -->\n<p>In 2011, after many years of operating independently the ACT Secondary Schools Sports Association (ACTSSSA) and the ACT Primary Schools Sports Association (ACTPSSA) amalgamated to form the ACT School Sport Management Committee (ACTSSMC).</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":231,\"width\":585,\"height\":389,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large is-resized\"><img src=\"http://localhost:8888/ssact/wp-content/uploads/2019/12/bg_home3-1024x683.jpg\" alt=\"\" class=\"wp-image-231\" width=\"585\" height=\"389\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>To enable oversight of the management committee the ACT Schools Sports Council was established at the same time and remains responsible for the strategic direction and approach of SSACT.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Given these changes the Council was very aware of the need to capture historical records of both organisations and Mr Paul Andrews was therefore engaged to collate the historical data from the ACTSSSA and has collated this information into the “ACTSSSA Results History Project 1977 – 2011”.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading -->\n<h2>hjh</h2>\n<!-- /wp:heading --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:list -->\n<ul><li>66</li></ul>\n<!-- /wp:list --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p>hjkj</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:paragraph -->\n<p><a href=\"https://www.schoolsportact.asn.au/assets/documents/ACTSSSA_Results_History.pdf\">ACTSSSA Results History</a></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>This information is now available to the broader school community where students, parents and interested others can search on the School Sport ACT website and see who has come before them. The council would also like to hear from anyone who information which may be a useful addition to the history we currently have collated. Old photos, sports programs or personal reflections are most welcome.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Christopher Nunn OAM<br>Chair<br>ACT School Sport Council</p>\n<!-- /wp:paragraph -->', 'ACT School Sport History', '', 'inherit', 'closed', 'closed', '', '181-revision-v1', '', '', '2019-12-15 23:24:47', '2019-12-15 23:24:47', '', 181, 'http://localhost:8888/ssact/blog/2019/12/15/181-revision-v1/', 0, 'revision', '', 0),
(287, 1, '2019-12-15 23:57:58', '2019-12-15 23:57:58', '', 'Registration', '', 'inherit', 'closed', 'closed', '', '272-revision-v1', '', '', '2019-12-15 23:57:58', '2019-12-15 23:57:58', '', 272, 'http://localhost:8888/ssact/blog/2019/12/15/272-revision-v1/', 0, 'revision', '', 0),
(288, 1, '2019-12-16 00:00:35', '2019-12-16 00:00:35', '', 'Registration', '', 'inherit', 'closed', 'closed', '', '272-revision-v1', '', '', '2019-12-16 00:00:35', '2019-12-16 00:00:35', '', 272, 'http://localhost:8888/ssact/blog/2019/12/16/272-revision-v1/', 0, 'revision', '', 0),
(289, 1, '2019-12-16 00:01:15', '2019-12-16 00:01:15', '', 'reg_teams', '', 'inherit', 'open', 'closed', '', 'reg_teams', '', '', '2019-12-16 00:01:15', '2019-12-16 00:01:15', '', 272, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/reg_teams.jpg', 0, 'attachment', 'image/jpeg', 0),
(290, 1, '2019-12-16 00:01:29', '2019-12-16 00:01:29', '', 'Registration', '', 'inherit', 'closed', 'closed', '', '272-revision-v1', '', '', '2019-12-16 00:01:29', '2019-12-16 00:01:29', '', 272, 'http://localhost:8888/ssact/blog/2019/12/16/272-revision-v1/', 0, 'revision', '', 0),
(291, 1, '2019-12-16 00:04:48', '2019-12-16 00:04:48', '', 'Registration', '', 'inherit', 'closed', 'closed', '', '272-revision-v1', '', '', '2019-12-16 00:04:48', '2019-12-16 00:04:48', '', 272, 'http://localhost:8888/ssact/blog/2019/12/16/272-revision-v1/', 0, 'revision', '', 0),
(293, 1, '2019-12-16 00:59:28', '2019-12-16 00:59:28', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'title', 'publish', 'closed', 'closed', '', 'field_5df6d6d8eb7db', '', '', '2019-12-16 00:59:28', '2019-12-16 00:59:28', '', 133, 'http://localhost:8888/ssact/?post_type=acf-field&p=293', 0, 'acf-field', '', 0),
(294, 1, '2019-12-16 01:55:30', '2019-12-16 01:55:30', 'Further information regarding the award categories and nomination criteria, as well as the Nomination Form can be found in the Forms section of the SSACT website homepage.', 'SSACT 2019 Excellence in Sport Awards Nominations - Open', '', 'publish', 'closed', 'closed', '', 'ssact-2019-excellence-in-sport-awards-nominations-open', '', '', '2019-12-16 01:55:30', '2019-12-16 01:55:30', '', 0, 'http://localhost:8888/ssact/?post_type=news&#038;p=294', 0, 'news', '', 0),
(295, 1, '2019-12-16 01:56:20', '2019-12-16 01:56:20', 'Information regarding positions still vacant as of June 2019, after the first promotion round, can be found within the About Us, School Sport ACT Team section of website and application form in Forms section of the website.\r\n\r\nInformation regarding positions still vacant as of June 2019, after the first promotion round, can be found within the About Us, School Sport ACT Team section of website and application form in Forms section of the website.', 'SSACT State Team Official Vacant Positions', '', 'publish', 'closed', 'closed', '', 'ssact-state-team-official-vacant-positions', '', '', '2019-12-19 15:12:30', '2019-12-19 15:12:30', '', 0, 'http://localhost:8888/ssact/?post_type=news&#038;p=295', 0, 'news', '', 0),
(296, 1, '2019-12-16 03:30:46', '2019-12-16 03:30:46', '', 'bg_regch', '', 'inherit', 'open', 'closed', '', 'bg_regch', '', '', '2019-12-16 03:30:46', '2019-12-16 03:30:46', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/bg_regch.jpg', 0, 'attachment', 'image/jpeg', 0),
(297, 1, '2019-12-16 03:31:19', '2019-12-16 03:31:19', '', 'bg_helpevnt', '', 'inherit', 'open', 'closed', '', 'bg_helpevnt', '', '', '2019-12-16 03:31:19', '2019-12-16 03:31:19', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/bg_helpevnt.jpg', 0, 'attachment', 'image/jpeg', 0),
(298, 1, '2019-12-17 20:27:52', '2019-12-17 20:27:52', '<!-- wp:paragraph -->\n<p>[school_sport_user_details]</p>\n<!-- /wp:paragraph -->', 'Edit my details', '', 'publish', 'closed', 'closed', '', 'edit-my-details', '', '', '2019-12-17 20:49:04', '2019-12-17 20:49:04', '', 115, 'http://localhost:8888/ssact/?page_id=298', 0, 'page', '', 0),
(299, 1, '2019-12-17 20:27:52', '2019-12-17 20:27:52', '', 'Profile', '', 'inherit', 'closed', 'closed', '', '298-revision-v1', '', '', '2019-12-17 20:27:52', '2019-12-17 20:27:52', '', 298, 'http://localhost:8888/ssact/blog/2019/12/17/298-revision-v1/', 0, 'revision', '', 0),
(300, 1, '2019-12-17 20:32:45', '2019-12-17 20:32:45', '', 'Edit my details', '', 'inherit', 'closed', 'closed', '', '298-revision-v1', '', '', '2019-12-17 20:32:45', '2019-12-17 20:32:45', '', 298, 'http://localhost:8888/ssact/blog/2019/12/17/298-revision-v1/', 0, 'revision', '', 0),
(301, 1, '2019-12-17 20:49:03', '2019-12-17 20:49:03', '<!-- wp:paragraph -->\n<p>[school_sport_user_details]</p>\n<!-- /wp:paragraph -->', 'Edit my details', '', 'inherit', 'closed', 'closed', '', '298-revision-v1', '', '', '2019-12-17 20:49:03', '2019-12-17 20:49:03', '', 298, 'http://localhost:8888/ssact/blog/2019/12/17/298-revision-v1/', 0, 'revision', '', 0),
(303, 1, '2019-12-18 17:22:36', '2019-12-18 17:22:36', '<!-- wp:paragraph -->\n<p>[school_sport_create_trial]</p>\n<!-- /wp:paragraph -->', 'Create New Trial', '', 'publish', 'closed', 'closed', '', 'create-new-trial', '', '', '2019-12-18 17:30:50', '2019-12-18 17:30:50', '', 115, 'http://localhost:8888/ssact/?page_id=303', 0, 'page', '', 0),
(304, 1, '2019-12-18 17:22:36', '2019-12-18 17:22:36', '<!-- wp:paragraph -->\n<p>[school_sport_create_trial]</p>\n<!-- /wp:paragraph -->', 'Create New Trial', '', 'inherit', 'closed', 'closed', '', '303-revision-v1', '', '', '2019-12-18 17:22:36', '2019-12-18 17:22:36', '', 303, 'http://localhost:8888/ssact/blog/2019/12/18/303-revision-v1/', 0, 'revision', '', 0),
(306, 1, '2019-12-18 19:15:51', '2019-12-18 19:15:51', 'SSACT 12&amp;U Basketball Jersey', '12&U Basketball Jersey', '', 'publish', 'closed', 'closed', '', '12u-basketball-jersey', '', '', '2019-12-18 19:19:42', '2019-12-18 19:19:42', '', 0, 'http://localhost:8888/ssact/?post_type=products&#038;p=306', 0, 'products', '', 0),
(307, 1, '2019-12-18 19:18:41', '2019-12-18 19:18:41', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:8:\"products\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:14:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:7:\"excerpt\";i:3;s:10:\"discussion\";i:4;s:8:\"comments\";i:5;s:9:\"revisions\";i:6;s:4:\"slug\";i:7;s:6:\"author\";i:8;s:6:\"format\";i:9;s:15:\"page_attributes\";i:10;s:14:\"featured_image\";i:11;s:10:\"categories\";i:12;s:4:\"tags\";i:13;s:15:\"send-trackbacks\";}s:11:\"description\";s:0:\"\";}', 'Group Product', 'group-product', 'publish', 'closed', 'closed', '', 'group_5dfa7af34a14d', '', '', '2019-12-26 01:29:31', '2019-12-26 01:29:31', '', 0, 'http://localhost:8888/ssact/?post_type=acf-field-group&#038;p=307', 0, 'acf-field-group', '', 0),
(308, 1, '2019-12-18 19:18:41', '2019-12-18 19:18:41', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Description', 'description', 'publish', 'closed', 'closed', '', 'field_5dfa7b0794a46', '', '', '2019-12-18 19:23:19', '2019-12-18 19:23:19', '', 307, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=308', 0, 'acf-field', '', 0),
(309, 1, '2019-12-18 19:18:41', '2019-12-18 19:18:41', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Price', 'price', 'publish', 'closed', 'closed', '', 'field_5dfa7afc94a45', '', '', '2019-12-26 01:29:31', '2019-12-26 01:29:31', '', 307, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=309', 1, 'acf-field', '', 0),
(310, 1, '2019-12-18 19:18:41', '2019-12-18 19:18:41', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Available Sizes', 'available_sizes', 'publish', 'closed', 'closed', '', 'field_5dfa7b1694a47', '', '', '2019-12-18 19:18:41', '2019-12-18 19:18:41', '', 307, 'http://localhost:8888/ssact/?post_type=acf-field&p=310', 2, 'acf-field', '', 0),
(311, 1, '2019-12-18 19:18:41', '2019-12-18 19:18:41', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image', 'image', 'publish', 'closed', 'closed', '', 'field_5dfa7b6994a48', '', '', '2019-12-18 19:18:41', '2019-12-18 19:18:41', '', 307, 'http://localhost:8888/ssact/?post_type=acf-field&p=311', 3, 'acf-field', '', 0),
(312, 1, '2019-12-18 19:18:41', '2019-12-18 19:18:41', 'a:10:{s:4:\"type\";s:10:\"true_false\";s:12:\"instructions\";s:89:\"WARNING: Deactivating a Product will remove it from all of the above listed Product Sets.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"message\";s:0:\"\";s:13:\"default_value\";i:1;s:2:\"ui\";i:1;s:10:\"ui_on_text\";s:0:\"\";s:11:\"ui_off_text\";s:0:\"\";}', 'Active', 'active', 'publish', 'closed', 'closed', '', 'field_5dfa7b7f94a49', '', '', '2019-12-18 19:23:58', '2019-12-18 19:23:58', '', 307, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=312', 4, 'acf-field', '', 0),
(313, 1, '2019-12-18 19:19:30', '2019-12-18 19:19:30', '', 'Product-37', '', 'inherit', 'open', 'closed', '', 'product-37', '', '', '2019-12-18 19:19:30', '2019-12-18 19:19:30', '', 306, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/Product-37.jpg', 0, 'attachment', 'image/jpeg', 0),
(314, 1, '2019-12-18 19:22:39', '2019-12-18 19:22:39', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:12:\"product_sets\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:14:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:7:\"excerpt\";i:3;s:10:\"discussion\";i:4;s:8:\"comments\";i:5;s:9:\"revisions\";i:6;s:4:\"slug\";i:7;s:6:\"author\";i:8;s:6:\"format\";i:9;s:15:\"page_attributes\";i:10;s:14:\"featured_image\";i:11;s:10:\"categories\";i:12;s:4:\"tags\";i:13;s:15:\"send-trackbacks\";}s:11:\"description\";s:0:\"\";}', 'Group Product Set', 'group-product-set', 'publish', 'closed', 'closed', '', 'group_5dfa7c136992a', '', '', '2019-12-23 02:20:25', '2019-12-23 02:20:25', '', 0, 'http://localhost:8888/ssact/?post_type=acf-field-group&#038;p=314', 0, 'acf-field-group', '', 0),
(315, 1, '2019-12-18 19:22:39', '2019-12-18 19:22:39', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Description', 'description', 'publish', 'closed', 'closed', '', 'field_5dfa7c1dd5e14', '', '', '2019-12-18 19:22:39', '2019-12-18 19:22:39', '', 314, 'http://localhost:8888/ssact/?post_type=acf-field&p=315', 0, 'acf-field', '', 0),
(316, 1, '2019-12-18 19:22:39', '2019-12-18 19:22:39', 'a:11:{s:4:\"type\";s:11:\"post_object\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:8:\"products\";}s:8:\"taxonomy\";s:0:\"\";s:10:\"allow_null\";i:0;s:8:\"multiple\";i:1;s:13:\"return_format\";s:6:\"object\";s:2:\"ui\";i:1;}', 'Products', 'products', 'publish', 'closed', 'closed', '', 'field_5dfa7c52d5e17', '', '', '2019-12-18 19:22:39', '2019-12-18 19:22:39', '', 314, 'http://localhost:8888/ssact/?post_type=acf-field&p=316', 1, 'acf-field', '', 0),
(317, 1, '2019-12-18 19:22:39', '2019-12-18 19:22:39', 'a:10:{s:4:\"type\";s:10:\"true_false\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"message\";s:0:\"\";s:13:\"default_value\";i:0;s:2:\"ui\";i:1;s:10:\"ui_on_text\";s:0:\"\";s:11:\"ui_off_text\";s:0:\"\";}', 'Active', 'active', 'publish', 'closed', 'closed', '', 'field_5dfa7c3ed5e16', '', '', '2019-12-18 19:22:39', '2019-12-18 19:22:39', '', 314, 'http://localhost:8888/ssact/?post_type=acf-field&p=317', 2, 'acf-field', '', 0),
(318, 1, '2019-12-18 19:28:26', '2019-12-18 19:28:26', '', '2019 Hockey Playing Shorts', '', 'publish', 'closed', 'closed', '', '2019-hockey-playing-shorts', '', '', '2019-12-18 19:28:26', '2019-12-18 19:28:26', '', 0, 'http://localhost:8888/ssact/?post_type=products&#038;p=318', 0, 'products', '', 0),
(319, 1, '2019-12-18 19:28:13', '2019-12-18 19:28:13', '', 'Product-32', '', 'inherit', 'open', 'closed', '', 'product-32', '', '', '2019-12-18 19:28:13', '2019-12-18 19:28:13', '', 318, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/Product-32.jpg', 0, 'attachment', 'image/jpeg', 0),
(321, 1, '2019-12-19 18:29:40', '2019-12-19 18:29:40', 'a:10:{s:4:\"type\";s:4:\"file\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:7:\"library\";s:3:\"all\";s:8:\"min_size\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Calendar', 'calendar', 'publish', 'closed', 'closed', '', 'field_5dfbc17b73707', '', '', '2019-12-19 18:29:40', '2019-12-19 18:29:40', '', 123, 'http://localhost:8888/ssact/?post_type=acf-field&p=321', 3, 'acf-field', '', 0),
(322, 1, '2019-12-19 18:30:41', '2019-12-19 18:30:41', '', 'SiteContentFiles-_School Sport ACT Calendar', '', 'inherit', 'open', 'closed', '', 'sitecontentfiles-_school-sport-act-calendar', '', '', '2019-12-19 18:30:41', '2019-12-19 18:30:41', '', 0, 'http://localhost:8888/ssact/wp-content/uploads/2019/12/SiteContentFiles-_School-Sport-ACT-Calendar.xlsx', 0, 'attachment', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 0),
(323, 1, '2019-12-19 18:36:18', '2019-12-19 18:36:18', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', 'ACT Sport Bulletin #17 2019', '', 'publish', 'closed', 'closed', '', 'act-sport-bulletin-17-2019', '', '', '2019-12-19 18:41:42', '2019-12-19 18:41:42', '', 0, 'http://localhost:8888/ssact/?post_type=bulletins&#038;p=323', 0, 'bulletins', '', 0),
(324, 31, '2019-12-22 03:28:39', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-12-22 03:28:39', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=324', 0, 'post', '', 0),
(325, 31, '2019-12-22 03:29:14', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-12-22 03:29:14', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?post_type=teams&p=325', 0, 'teams', '', 0),
(326, 1, '2019-12-22 21:01:32', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-12-22 21:01:32', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=326', 0, 'post', '', 0),
(327, 1, '2019-12-22 21:59:12', '2019-12-22 21:59:12', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:6:\"trials\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:14:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:7:\"excerpt\";i:3;s:10:\"discussion\";i:4;s:8:\"comments\";i:5;s:9:\"revisions\";i:6;s:4:\"slug\";i:7;s:6:\"author\";i:8;s:6:\"format\";i:9;s:15:\"page_attributes\";i:10;s:14:\"featured_image\";i:11;s:10:\"categories\";i:12;s:4:\"tags\";i:13;s:15:\"send-trackbacks\";}s:11:\"description\";s:0:\"\";}', 'Group Trials', 'group-trials', 'publish', 'closed', 'closed', '', 'group_5dffdaf37ef8b', '', '', '2019-12-29 00:26:02', '2019-12-29 00:26:02', '', 0, 'http://localhost:8888/ssact/?post_type=acf-field-group&#038;p=327', 0, 'acf-field-group', '', 0),
(328, 1, '2019-12-22 21:59:12', '2019-12-22 21:59:12', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Year', 'year', 'publish', 'closed', 'closed', '', 'field_5dffdaf957166', '', '', '2019-12-22 21:59:12', '2019-12-22 21:59:12', '', 327, 'http://localhost:8888/ssact/?post_type=acf-field&p=328', 0, 'acf-field', '', 0),
(329, 1, '2019-12-22 21:59:12', '2019-12-22 21:59:12', 'a:11:{s:4:\"type\";s:11:\"post_object\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:6:\"sports\";}s:8:\"taxonomy\";s:0:\"\";s:10:\"allow_null\";i:0;s:8:\"multiple\";i:0;s:13:\"return_format\";s:6:\"object\";s:2:\"ui\";i:1;}', 'Sport', 'sport', 'publish', 'closed', 'closed', '', 'field_5dffdb0557167', '', '', '2019-12-22 22:08:14', '2019-12-22 22:08:14', '', 327, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=329', 1, 'acf-field', '', 0),
(330, 1, '2019-12-22 21:59:12', '2019-12-22 21:59:12', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Age', 'age', 'publish', 'closed', 'closed', '', 'field_5dffdb2157168', '', '', '2019-12-22 21:59:12', '2019-12-22 21:59:12', '', 327, 'http://localhost:8888/ssact/?post_type=acf-field&p=330', 2, 'acf-field', '', 0),
(331, 1, '2019-12-22 21:59:12', '2019-12-22 21:59:12', 'a:13:{s:4:\"type\";s:6:\"select\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"choices\";a:3:{s:4:\"male\";s:4:\"Male\";s:6:\"female\";s:6:\"Female\";s:5:\"other\";s:5:\"Other\";}s:13:\"default_value\";a:0:{}s:10:\"allow_null\";i:0;s:8:\"multiple\";i:0;s:2:\"ui\";i:0;s:13:\"return_format\";s:5:\"value\";s:4:\"ajax\";i:0;s:11:\"placeholder\";s:0:\"\";}', 'Gender', 'gender', 'publish', 'closed', 'closed', '', 'field_5dffdb2c57169', '', '', '2019-12-22 21:59:12', '2019-12-22 21:59:12', '', 327, 'http://localhost:8888/ssact/?post_type=acf-field&p=331', 3, 'acf-field', '', 0),
(332, 1, '2019-12-22 21:59:12', '2019-12-22 21:59:12', 'a:8:{s:4:\"type\";s:11:\"date_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:14:\"display_format\";s:5:\"m/d/Y\";s:13:\"return_format\";s:5:\"m/d/Y\";s:9:\"first_day\";i:1;}', 'Date birth Restriction from', 'date_birth_restriction_from', 'publish', 'closed', 'closed', '', 'field_5dffdb6b5716a', '', '', '2019-12-28 03:24:43', '2019-12-28 03:24:43', '', 327, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=332', 4, 'acf-field', '', 0),
(333, 1, '2019-12-22 21:59:12', '2019-12-22 21:59:12', 'a:8:{s:4:\"type\";s:11:\"date_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:14:\"display_format\";s:5:\"m/d/Y\";s:13:\"return_format\";s:5:\"m/d/Y\";s:9:\"first_day\";i:1;}', 'Date of Trial', 'date_of_trial', 'publish', 'closed', 'closed', '', 'field_5dffdb855716b', '', '', '2019-12-28 03:33:24', '2019-12-28 03:33:24', '', 327, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=333', 6, 'acf-field', '', 0),
(334, 1, '2019-12-22 21:59:12', '2019-12-22 21:59:12', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Eligibility', 'eligibility', 'publish', 'closed', 'closed', '', 'field_5dffe7185716c', '', '', '2019-12-28 03:33:24', '2019-12-28 03:33:24', '', 327, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=334', 7, 'acf-field', '', 0),
(335, 1, '2019-12-22 21:59:12', '2019-12-22 21:59:12', 'a:8:{s:4:\"type\";s:11:\"date_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:14:\"display_format\";s:5:\"m/d/Y\";s:13:\"return_format\";s:5:\"m/d/Y\";s:9:\"first_day\";i:1;}', 'Championship date', 'championship_date', 'publish', 'closed', 'closed', '', 'field_5dffe71d5716d', '', '', '2019-12-28 03:33:24', '2019-12-28 03:33:24', '', 327, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=335', 8, 'acf-field', '', 0),
(336, 1, '2019-12-22 22:00:01', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-12-22 22:00:01', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?post_type=trials&p=336', 0, 'trials', '', 0),
(337, 1, '2019-12-22 22:08:24', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-12-22 22:08:24', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?post_type=trials&p=337', 0, 'trials', '', 0),
(338, 1, '2019-12-23 02:11:08', '2019-12-23 02:11:08', 'a:11:{s:4:\"type\";s:11:\"post_object\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:12:\"product_sets\";}s:8:\"taxonomy\";s:0:\"\";s:10:\"allow_null\";i:0;s:8:\"multiple\";i:1;s:13:\"return_format\";s:6:\"object\";s:2:\"ui\";i:1;}', 'Product Set', 'product_set', 'publish', 'closed', 'closed', '', 'field_5e0022168380e', '', '', '2019-12-28 03:33:24', '2019-12-28 03:33:24', '', 327, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=338', 9, 'acf-field', '', 0),
(339, 1, '2019-12-23 02:18:48', '2019-12-23 02:18:48', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:12:\"product_sets\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:14:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:7:\"excerpt\";i:3;s:10:\"discussion\";i:4;s:8:\"comments\";i:5;s:9:\"revisions\";i:6;s:4:\"slug\";i:7;s:6:\"author\";i:8;s:6:\"format\";i:9;s:15:\"page_attributes\";i:10;s:14:\"featured_image\";i:11;s:10:\"categories\";i:12;s:4:\"tags\";i:13;s:15:\"send-trackbacks\";}s:11:\"description\";s:0:\"\";}', 'Grupo Product Set', 'grupo-product-set', 'trash', 'closed', 'closed', '', 'group_5e0023c28b27a__trashed', '', '', '2019-12-23 02:20:14', '2019-12-23 02:20:14', '', 0, 'http://localhost:8888/ssact/?post_type=acf-field-group&#038;p=339', 0, 'acf-field-group', '', 0),
(340, 1, '2019-12-23 02:18:48', '2019-12-23 02:18:48', 'a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:8:\"products\";}s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:1:{i:0;s:6:\"search\";}s:8:\"elements\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:13:\"return_format\";s:6:\"object\";}', 'products', 'products', 'trash', 'closed', 'closed', '', 'field_5e0023e03036a__trashed', '', '', '2019-12-23 02:20:14', '2019-12-23 02:20:14', '', 339, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=340', 0, 'acf-field', '', 0),
(341, 1, '2019-12-23 02:19:44', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-12-23 02:19:44', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?post_type=product_sets&p=341', 0, 'product_sets', '', 0),
(342, 1, '2019-12-23 15:50:44', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-12-23 15:50:44', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?post_type=trials&p=342', 0, 'trials', '', 0),
(343, 1, '2019-12-23 15:52:02', '2019-12-23 15:52:02', '', '2018 ACT 18&U Girls Basketball', '', 'publish', 'closed', 'closed', '', '2018-act-18u-girls-basketball', '', '', '2019-12-23 15:52:02', '2019-12-23 15:52:02', '', 0, 'http://localhost:8888/ssact/?post_type=product_sets&#038;p=343', 0, 'product_sets', '', 0),
(344, 38, '2019-12-24 00:54:20', '0000-00-00 00:00:00', '', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-12-24 00:54:20', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=344', 0, 'trial', '', 0),
(345, 38, '2019-12-24 02:14:32', '0000-00-00 00:00:00', '', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-12-24 02:14:32', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=345', 0, 'trial', '', 0),
(346, 38, '2019-12-24 02:15:25', '0000-00-00 00:00:00', '', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-12-24 02:15:25', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=346', 0, 'trial', '', 0),
(347, 38, '2019-12-24 02:21:17', '0000-00-00 00:00:00', '', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-12-24 02:21:17', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=347', 0, 'trial', '', 0),
(348, 38, '2019-12-24 02:23:03', '0000-00-00 00:00:00', '', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-12-24 02:23:03', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=348', 0, 'trial', '', 0),
(349, 38, '2019-12-24 02:23:41', '0000-00-00 00:00:00', '', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-12-24 02:23:41', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=349', 0, 'trial', '', 0),
(350, 38, '2019-12-24 02:25:42', '0000-00-00 00:00:00', '', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-12-24 02:25:42', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=350', 0, 'trial', '', 0),
(351, 38, '2019-12-24 02:27:55', '0000-00-00 00:00:00', '', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-12-24 02:27:55', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=351', 0, 'trial', '', 0),
(352, 38, '2019-12-24 02:28:42', '0000-00-00 00:00:00', '', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-12-24 02:28:42', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=352', 0, 'trial', '', 0),
(353, 38, '2019-12-24 02:29:16', '0000-00-00 00:00:00', '', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-12-24 02:29:16', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/ssact/?p=353', 0, 'trial', '', 0),
(368, 38, '2019-12-26 00:27:42', '0000-00-00 00:00:00', '', 'Guillermo', '', 'pending', 'closed', 'closed', '', '', '', '', '2019-12-26 00:27:42', '2019-12-26 00:27:42', '', 0, 'http://localhost:8888/ssact/?post_type=trials&#038;p=368', 0, 'trials', '', 0),
(370, 38, '2019-12-26 00:21:07', '0000-00-00 00:00:00', '', 'Name of the Trial', '', 'pending', 'closed', 'closed', '', '', '', '', '2019-12-26 00:21:07', '2019-12-26 00:21:07', '', 0, 'http://localhost:8888/ssact/?post_type=trials&#038;p=370', 0, 'trials', '', 0),
(372, 38, '2019-12-24 14:12:12', '0000-00-00 00:00:00', '', 'Name of the Trial', '', 'pending', 'closed', 'closed', '', '', '', '', '2019-12-24 14:12:12', '2019-12-24 14:12:12', '', 0, 'http://localhost:8888/ssact/?post_type=trials&#038;p=372', 0, 'trials', '', 0),
(374, 38, '2019-12-26 00:11:05', '0000-00-00 00:00:00', '', 'Trial Prueba', '', 'pending', 'closed', 'closed', '', '', '', '', '2019-12-26 00:11:05', '2019-12-26 00:11:05', '', 0, 'http://localhost:8888/ssact/?post_type=trials&#038;p=374', 0, 'trials', '', 0),
(378, 38, '2019-12-26 00:29:57', '0000-00-00 00:00:00', '', 'Trial 25 dic', '', 'pending', 'closed', 'closed', '', '', '', '', '2019-12-26 00:29:57', '2019-12-26 00:29:57', '', 0, 'http://localhost:8888/ssact/?post_type=trials&#038;p=378', 0, 'trials', '', 0),
(379, 1, '2019-12-26 02:20:03', '2019-12-26 02:20:03', '<!-- wp:paragraph -->\n<p>[school_sport_create_student]</p>\n<!-- /wp:paragraph -->', 'Add new Student', '', 'publish', 'closed', 'closed', '', 'add-new-student', '', '', '2019-12-26 02:55:47', '2019-12-26 02:55:47', '', 115, 'http://localhost:8888/ssact/?page_id=379', 0, 'page', '', 0),
(380, 1, '2019-12-26 02:20:03', '2019-12-26 02:20:03', '', 'Add new Student', '', 'inherit', 'closed', 'closed', '', '379-revision-v1', '', '', '2019-12-26 02:20:03', '2019-12-26 02:20:03', '', 379, 'http://localhost:8888/ssact/blog/2019/12/26/379-revision-v1/', 0, 'revision', '', 0),
(381, 1, '2019-12-26 02:55:46', '2019-12-26 02:55:46', '<!-- wp:paragraph -->\n<p>[school_sport_create_student]</p>\n<!-- /wp:paragraph -->', 'Add new Student', '', 'inherit', 'closed', 'closed', '', '379-revision-v1', '', '', '2019-12-26 02:55:46', '2019-12-26 02:55:46', '', 379, 'http://localhost:8888/ssact/blog/2019/12/26/379-revision-v1/', 0, 'revision', '', 0),
(382, 1, '2019-12-26 02:55:58', '2019-12-26 02:55:58', '<!-- wp:paragraph -->\n<p>[school_sport_create_student]</p>\n<!-- /wp:paragraph -->', 'Add new Student', '', 'inherit', 'closed', 'closed', '', '379-autosave-v1', '', '', '2019-12-26 02:55:58', '2019-12-26 02:55:58', '', 379, 'http://localhost:8888/ssact/blog/2019/12/26/379-autosave-v1/', 0, 'revision', '', 0),
(383, 1, '2019-12-26 19:34:58', '2019-12-26 19:34:58', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:8:\"students\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:13:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:7:\"excerpt\";i:3;s:10:\"discussion\";i:4;s:8:\"comments\";i:5;s:9:\"revisions\";i:6;s:4:\"slug\";i:7;s:6:\"format\";i:8;s:15:\"page_attributes\";i:9;s:14:\"featured_image\";i:10;s:10:\"categories\";i:11;s:4:\"tags\";i:12;s:15:\"send-trackbacks\";}s:11:\"description\";s:0:\"\";}', 'Group Student', 'group-student', 'publish', 'closed', 'closed', '', 'group_5e050b05070ce', '', '', '2019-12-26 19:43:10', '2019-12-26 19:43:10', '', 0, 'http://localhost:8888/ssact/?post_type=acf-field-group&#038;p=383', 0, 'acf-field-group', '', 0),
(384, 1, '2019-12-26 19:34:58', '2019-12-26 19:34:58', 'a:8:{s:4:\"type\";s:11:\"date_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:14:\"display_format\";s:5:\"m/d/Y\";s:13:\"return_format\";s:5:\"m/d/Y\";s:9:\"first_day\";i:1;}', 'Date of Birth', 'date_of_birth', 'publish', 'closed', 'closed', '', 'field_5e050b1986934', '', '', '2019-12-26 19:43:10', '2019-12-26 19:43:10', '', 383, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=384', 1, 'acf-field', '', 0),
(385, 1, '2019-12-26 19:34:58', '2019-12-26 19:34:58', 'a:13:{s:4:\"type\";s:6:\"select\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"choices\";a:3:{s:4:\"male\";s:4:\"Male\";s:6:\"female\";s:6:\"Female\";s:5:\"other\";s:5:\"Other\";}s:13:\"default_value\";a:0:{}s:10:\"allow_null\";i:0;s:8:\"multiple\";i:0;s:2:\"ui\";i:0;s:13:\"return_format\";s:5:\"value\";s:4:\"ajax\";i:0;s:11:\"placeholder\";s:0:\"\";}', 'Gender', 'gender', 'publish', 'closed', 'closed', '', 'field_5e050b3286935', '', '', '2019-12-26 19:43:10', '2019-12-26 19:43:10', '', 383, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=385', 2, 'acf-field', '', 0),
(386, 1, '2019-12-26 19:34:58', '2019-12-26 19:34:58', 'a:11:{s:4:\"type\";s:11:\"post_object\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:7:\"schools\";}s:8:\"taxonomy\";s:0:\"\";s:10:\"allow_null\";i:0;s:8:\"multiple\";i:0;s:13:\"return_format\";s:6:\"object\";s:2:\"ui\";i:1;}', 'School', 'school', 'publish', 'closed', 'closed', '', 'field_5e050b5b86936', '', '', '2019-12-26 19:43:10', '2019-12-26 19:43:10', '', 383, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=386', 3, 'acf-field', '', 0),
(387, 1, '2019-12-26 19:43:10', '2019-12-26 19:43:10', 'a:9:{s:4:\"type\";s:4:\"user\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:4:\"role\";s:0:\"\";s:10:\"allow_null\";i:0;s:8:\"multiple\";i:0;s:13:\"return_format\";s:6:\"object\";}', 'Parent', 'parent', 'publish', 'closed', 'closed', '', 'field_5e050d406ea96', '', '', '2019-12-26 19:43:10', '2019-12-26 19:43:10', '', 383, 'http://localhost:8888/ssact/?post_type=acf-field&p=387', 0, 'acf-field', '', 0),
(388, 38, '2019-12-28 23:39:12', '2019-12-28 23:39:12', '', 'Sofia Fuentes', '', 'publish', 'closed', 'closed', '', 'sofia-fuentes', '', '', '2019-12-28 23:39:12', '2019-12-28 23:39:12', '', 0, 'http://localhost:8888/ssact/?post_type=students&#038;p=388', 0, 'students', '', 0),
(389, 38, '2019-12-27 02:37:36', '2019-12-27 02:37:36', '', 'Guillermo', '', 'trash', 'closed', 'closed', '', '__trashed-3', '', '', '2019-12-27 02:37:36', '2019-12-27 02:37:36', '', 0, 'http://localhost:8888/ssact/?post_type=students&#038;p=389', 0, 'students', '', 0),
(390, 38, '2019-12-27 02:37:36', '2019-12-27 02:37:36', '', 'Guillermo', '', 'trash', 'closed', 'closed', '', '__trashed-2', '', '', '2019-12-27 02:37:36', '2019-12-27 02:37:36', '', 0, 'http://localhost:8888/ssact/?post_type=students&#038;p=390', 0, 'students', '', 0),
(391, 38, '2019-12-27 02:37:29', '2019-12-27 02:37:29', '', '', '', 'trash', 'closed', 'closed', '', '__trashed', '', '', '2019-12-27 02:37:29', '2019-12-27 02:37:29', '', 0, 'http://localhost:8888/ssact/?post_type=students&#038;p=391', 0, 'students', '', 0),
(392, 1, '2019-12-27 03:01:14', '0000-00-00 00:00:00', '', 'Guillermo', '', 'pending', 'closed', 'closed', '', '', '', '', '2019-12-27 03:01:14', '2019-12-27 03:01:14', '', 0, 'http://localhost:8888/ssact/?post_type=trials&#038;p=392', 0, 'trials', '', 0),
(393, 38, '2019-12-28 23:37:29', '2019-12-28 23:37:29', '', 'Emilia Fuentes Contreras', '', 'trash', 'closed', 'closed', '', '__trashed-5', '', '', '2019-12-28 23:37:29', '2019-12-28 23:37:29', '', 0, 'http://localhost:8888/ssact/?post_type=students&#038;p=393', 0, 'students', '', 0),
(394, 38, '2019-12-28 23:37:29', '2019-12-28 23:37:29', '', 'Emilia Fuentes Contreras', '', 'trash', 'closed', 'closed', '', '__trashed-4', '', '', '2019-12-28 23:37:29', '2019-12-28 23:37:29', '', 0, 'http://localhost:8888/ssact/?post_type=students&#038;p=394', 0, 'students', '', 0),
(395, 1, '2019-12-27 04:18:49', '2019-12-27 04:18:49', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:8:\"students\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:13:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:7:\"excerpt\";i:3;s:10:\"discussion\";i:4;s:8:\"comments\";i:5;s:9:\"revisions\";i:6;s:4:\"slug\";i:7;s:6:\"format\";i:8;s:15:\"page_attributes\";i:9;s:14:\"featured_image\";i:10;s:10:\"categories\";i:11;s:4:\"tags\";i:12;s:15:\"send-trackbacks\";}s:11:\"description\";s:0:\"\";}', 'Group Medical', 'group-medical', 'publish', 'closed', 'closed', '', 'group_5e0585a9d3d1b', '', '', '2019-12-27 04:18:49', '2019-12-27 04:18:49', '', 0, 'http://localhost:8888/ssact/?post_type=acf-field-group&#038;p=395', 0, 'acf-field-group', '', 0),
(396, 1, '2019-12-27 04:18:49', '2019-12-27 04:18:49', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Medical Condition', 'medical_condition', 'publish', 'closed', 'closed', '', 'field_5e0585b0f702c', '', '', '2019-12-27 04:18:49', '2019-12-27 04:18:49', '', 395, 'http://localhost:8888/ssact/?post_type=acf-field&p=396', 0, 'acf-field', '', 0),
(397, 1, '2019-12-27 04:18:49', '2019-12-27 04:18:49', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Allergies', 'allergies', 'publish', 'closed', 'closed', '', 'field_5e0585c7f702d', '', '', '2019-12-27 04:18:49', '2019-12-27 04:18:49', '', 395, 'http://localhost:8888/ssact/?post_type=acf-field&p=397', 1, 'acf-field', '', 0),
(398, 1, '2019-12-27 04:18:49', '2019-12-27 04:18:49', 'a:8:{s:4:\"type\";s:11:\"date_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:14:\"display_format\";s:5:\"m/d/Y\";s:13:\"return_format\";s:5:\"m/d/Y\";s:9:\"first_day\";i:1;}', 'Date of last tetanus shot', 'date_last_tetanus', 'publish', 'closed', 'closed', '', 'field_5e0585d4f702e', '', '', '2019-12-27 04:18:49', '2019-12-27 04:18:49', '', 395, 'http://localhost:8888/ssact/?post_type=acf-field&p=398', 2, 'acf-field', '', 0),
(399, 1, '2019-12-27 04:18:49', '2019-12-27 04:18:49', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Medical Number', 'medical_number', 'publish', 'closed', 'closed', '', 'field_5e058618f702f', '', '', '2019-12-27 04:18:49', '2019-12-27 04:18:49', '', 395, 'http://localhost:8888/ssact/?post_type=acf-field&p=399', 3, 'acf-field', '', 0),
(400, 39, '2019-12-27 22:09:45', '0000-00-00 00:00:00', '', '111', '', 'pending', 'closed', 'closed', '', '', '', '', '2019-12-27 22:09:45', '2019-12-27 22:09:45', '', 0, 'http://localhost:8888/ssact/?post_type=trials&#038;p=400', 0, 'trials', '', 0),
(402, 1, '2019-12-28 03:56:29', '0000-00-00 00:00:00', '', 'Guillermo', '', 'pending', 'closed', 'closed', '', '', '', '', '2019-12-28 03:56:29', '2019-12-28 03:56:29', '', 0, 'http://localhost:8888/ssact/?post_type=trials&#038;p=402', 0, 'trials', '', 0),
(403, 1, '2019-12-28 03:32:30', '2019-12-28 03:32:30', 'a:8:{s:4:\"type\";s:11:\"date_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:14:\"display_format\";s:5:\"m/d/Y\";s:13:\"return_format\";s:5:\"m/d/Y\";s:9:\"first_day\";i:1;}', 'Date birth Restriction to', 'date_birth_restriction_to', 'publish', 'closed', 'closed', '', 'field_5e06ccb195c7c', '', '', '2019-12-28 03:33:24', '2019-12-28 03:33:24', '', 327, 'http://localhost:8888/ssact/?post_type=acf-field&#038;p=403', 5, 'acf-field', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(404, 1, '2019-12-28 03:49:06', '2019-12-28 03:49:06', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Venue of Trial', 'venue', 'publish', 'closed', 'closed', '', 'field_5e06d0a585597', '', '', '2019-12-28 03:49:06', '2019-12-28 03:49:06', '', 327, 'http://localhost:8888/ssact/?post_type=acf-field&p=404', 10, 'acf-field', '', 0),
(405, 1, '2019-12-28 04:41:01', '2019-12-28 04:41:01', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:8:\"students\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:13:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:7:\"excerpt\";i:3;s:10:\"discussion\";i:4;s:8:\"comments\";i:5;s:9:\"revisions\";i:6;s:4:\"slug\";i:7;s:6:\"format\";i:8;s:15:\"page_attributes\";i:9;s:14:\"featured_image\";i:10;s:10:\"categories\";i:11;s:4:\"tags\";i:12;s:15:\"send-trackbacks\";}s:11:\"description\";s:0:\"\";}', 'Group Emergency', 'group-emergency', 'publish', 'closed', 'closed', '', 'group_5e06dc1fa1fbc', '', '', '2019-12-28 04:41:01', '2019-12-28 04:41:01', '', 0, 'http://localhost:8888/ssact/?post_type=acf-field-group&#038;p=405', 0, 'acf-field-group', '', 0),
(406, 1, '2019-12-28 04:41:01', '2019-12-28 04:41:01', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Name of Preferred emergency', 'name_of_preferred_emergency', 'publish', 'closed', 'closed', '', 'field_5e06dc29236f3', '', '', '2019-12-28 04:41:01', '2019-12-28 04:41:01', '', 405, 'http://localhost:8888/ssact/?post_type=acf-field&p=406', 0, 'acf-field', '', 0),
(407, 1, '2019-12-28 04:41:01', '2019-12-28 04:41:01', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Relationship', 'relationship', 'publish', 'closed', 'closed', '', 'field_5e06dc69236f4', '', '', '2019-12-28 04:41:01', '2019-12-28 04:41:01', '', 405, 'http://localhost:8888/ssact/?post_type=acf-field&p=407', 1, 'acf-field', '', 0),
(408, 1, '2019-12-28 04:41:01', '2019-12-28 04:41:01', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Mobile Emergency', 'mobile_emergency', 'publish', 'closed', 'closed', '', 'field_5e06dc87236f5', '', '', '2019-12-28 04:41:01', '2019-12-28 04:41:01', '', 405, 'http://localhost:8888/ssact/?post_type=acf-field&p=408', 2, 'acf-field', '', 0),
(409, 1, '2019-12-28 04:41:01', '2019-12-28 04:41:01', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Student Email', 'student_email', 'publish', 'closed', 'closed', '', 'field_5e06dc98236f6', '', '', '2019-12-28 04:41:01', '2019-12-28 04:41:01', '', 405, 'http://localhost:8888/ssact/?post_type=acf-field&p=409', 3, 'acf-field', '', 0),
(410, 1, '2019-12-28 04:41:01', '2019-12-28 04:41:01', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Student Mobile', 'student_mobile', 'publish', 'closed', 'closed', '', 'field_5e06dca5236f7', '', '', '2019-12-28 04:41:01', '2019-12-28 04:41:01', '', 405, 'http://localhost:8888/ssact/?post_type=acf-field&p=410', 4, 'acf-field', '', 0),
(411, 1, '2019-12-28 14:10:12', '2019-12-28 14:10:12', '', 'Add Student to Trial', '', 'publish', 'closed', 'closed', '', 'add-student-to-trial', '', '', '2019-12-28 14:10:13', '2019-12-28 14:10:13', '', 0, 'http://localhost:8888/ssact/?page_id=411', 0, 'page', '', 0),
(412, 1, '2019-12-28 14:10:12', '2019-12-28 14:10:12', '', 'Add Student to Trial', '', 'inherit', 'closed', 'closed', '', '411-revision-v1', '', '', '2019-12-28 14:10:12', '2019-12-28 14:10:12', '', 411, 'http://localhost:8888/ssact/blog/2019/12/28/411-revision-v1/', 0, 'revision', '', 0),
(415, 1, '2019-12-28 14:16:16', '2019-12-28 14:16:16', '', 'Basic Information', '', 'publish', 'closed', 'closed', '', 'basic-information', '', '', '2019-12-28 14:16:17', '2019-12-28 14:16:17', '', 411, 'http://localhost:8888/ssact/?page_id=415', 0, 'page', '', 0),
(416, 1, '2019-12-28 14:16:16', '2019-12-28 14:16:16', '', 'Basic Information', '', 'inherit', 'closed', 'closed', '', '415-revision-v1', '', '', '2019-12-28 14:16:16', '2019-12-28 14:16:16', '', 415, 'http://localhost:8888/ssact/blog/2019/12/28/415-revision-v1/', 0, 'revision', '', 0),
(417, 1, '2019-12-28 14:18:40', '2019-12-28 14:18:40', '', 'Additional Information', '', 'publish', 'closed', 'closed', '', 'additional-information', '', '', '2019-12-28 14:18:41', '2019-12-28 14:18:41', '', 411, 'http://localhost:8888/ssact/?page_id=417', 0, 'page', '', 0),
(418, 1, '2019-12-28 14:18:40', '2019-12-28 14:18:40', '', 'Additional Information', '', 'inherit', 'closed', 'closed', '', '417-revision-v1', '', '', '2019-12-28 14:18:40', '2019-12-28 14:18:40', '', 417, 'http://localhost:8888/ssact/blog/2019/12/28/417-revision-v1/', 0, 'revision', '', 0),
(419, 1, '2019-12-28 14:19:10', '2019-12-28 14:19:10', '', 'Payment', '', 'publish', 'closed', 'closed', '', 'payment', '', '', '2019-12-28 14:19:11', '2019-12-28 14:19:11', '', 411, 'http://localhost:8888/ssact/?page_id=419', 0, 'page', '', 0),
(420, 1, '2019-12-28 14:19:10', '2019-12-28 14:19:10', '', 'Payment', '', 'inherit', 'closed', 'closed', '', '419-revision-v1', '', '', '2019-12-28 14:19:10', '2019-12-28 14:19:10', '', 419, 'http://localhost:8888/ssact/blog/2019/12/28/419-revision-v1/', 0, 'revision', '', 0),
(421, 1, '2019-12-29 00:26:02', '2019-12-29 00:26:02', 'a:9:{s:4:\"type\";s:4:\"user\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:4:\"role\";s:0:\"\";s:10:\"allow_null\";i:0;s:8:\"multiple\";i:1;s:13:\"return_format\";s:5:\"array\";}', 'Students pending school approval', 'students_pending', 'publish', 'closed', 'closed', '', 'field_5e07efc8c356f', '', '', '2019-12-29 00:26:02', '2019-12-29 00:26:02', '', 327, 'http://localhost:8888/ssact/?post_type=acf-field&p=421', 11, 'acf-field', '', 0),
(422, 1, '2019-12-29 00:26:02', '2019-12-29 00:26:02', 'a:9:{s:4:\"type\";s:4:\"user\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:4:\"role\";s:0:\"\";s:10:\"allow_null\";i:0;s:8:\"multiple\";i:1;s:13:\"return_format\";s:5:\"array\";}', 'Students', 'students', 'publish', 'closed', 'closed', '', 'field_5e07eff0c3570', '', '', '2019-12-29 00:26:02', '2019-12-29 00:26:02', '', 327, 'http://localhost:8888/ssact/?post_type=acf-field&p=422', 12, 'acf-field', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_registration_log`
--

DROP TABLE IF EXISTS `wp_registration_log`;
CREATE TABLE `wp_registration_log` (
  `ID` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `IP` varchar(30) NOT NULL DEFAULT '',
  `blog_id` bigint(20) NOT NULL DEFAULT '0',
  `date_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_sciact_schools`
--

DROP TABLE IF EXISTS `wp_sciact_schools`;
CREATE TABLE `wp_sciact_schools` (
  `school_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `high_id` int(11) NOT NULL,
  `primary_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `address` varchar(250) NOT NULL,
  `suburb` varchar(150) NOT NULL,
  `postcode` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_sciact_students`
--

DROP TABLE IF EXISTS `wp_sciact_students`;
CREATE TABLE `wp_sciact_students` (
  `student_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `birthday` date NOT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `medical_condition` text,
  `allergies` text,
  `date_last_tetanus` date DEFAULT NULL,
  `medicare_number` varchar(50) DEFAULT NULL,
  `emergency_relationship` varchar(150) DEFAULT NULL,
  `emergency_name` varchar(150) DEFAULT NULL,
  `emergency_phone` varchar(150) DEFAULT NULL,
  `emergency_mobile` varchar(150) DEFAULT NULL,
  `alternate_phone` varchar(150) DEFAULT NULL,
  `alternate_mobile` varchar(150) DEFAULT NULL,
  `alternate_email` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_search_filter_cache`
--

DROP TABLE IF EXISTS `wp_search_filter_cache`;
CREATE TABLE `wp_search_filter_cache` (
  `id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `post_parent_id` bigint(20) NOT NULL,
  `field_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_value_num` bigint(20) DEFAULT NULL,
  `field_parent_num` bigint(20) DEFAULT NULL,
  `term_parent_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `wp_search_filter_cache`
--

INSERT INTO `wp_search_filter_cache` (`id`, `post_id`, `post_parent_id`, `field_name`, `field_value`, `field_value_num`, `field_parent_num`, `term_parent_id`) VALUES
(1, 5, 0, '', '', NULL, NULL, NULL),
(2, 193, 0, '_sfm_type', 'Information', 0, NULL, 0),
(3, 193, 0, '_sfm_type', 'Policy', 0, NULL, 0),
(4, 193, 0, '_sfm_category', 'Primary regional', 0, NULL, 0),
(5, 193, 0, '_sfm_category', 'Secondary regional', 0, NULL, 0),
(6, 193, 0, '_sfm_category', 'College regional', 0, NULL, 0),
(7, 193, 0, '_sfm_category', 'Representative', 0, NULL, 0),
(8, 193, 0, '_sfm_category', 'Education Directorate', 0, NULL, 0),
(9, 193, 0, '_sfm_sports', 'Australian Football', 0, NULL, 0),
(10, 193, 0, '_sfm_sports', 'Baseball', 0, NULL, 0),
(11, 193, 0, '_sfm_sports', 'Basketball', 0, NULL, 0),
(12, 193, 0, '_sfm_sports', 'Cricket', 0, NULL, 0),
(13, 193, 0, '_sfm_sports', 'Cross Country', 0, NULL, 0),
(14, 193, 0, '_sfm_sports', 'Diving', 0, NULL, 0),
(15, 193, 0, '_sfm_sports', 'Football (Soccer)', 0, NULL, 0),
(16, 193, 0, '_sfm_sports', 'Golf', 0, NULL, 0),
(17, 193, 0, '_sfm_sports', 'Hockey', 0, NULL, 0),
(18, 193, 0, '_sfm_sports', 'Netball', 0, NULL, 0),
(19, 193, 0, '_sfm_sports', 'Rugby League', 0, NULL, 0),
(20, 193, 0, '_sfm_sports', 'Softball', 0, NULL, 0),
(21, 193, 0, '_sfm_sports', 'Swimming', 0, NULL, 0),
(22, 193, 0, '_sfm_sports', 'Tennis', 0, NULL, 0),
(23, 193, 0, '_sfm_sports', 'Touch', 0, NULL, 0),
(24, 193, 0, '_sfm_sports', 'Track and Field', 0, NULL, 0),
(25, 193, 0, '_sfm_sports', 'Volleyball', 0, NULL, 0),
(26, 194, 0, '_sfm_type', 'Information', 0, NULL, 0),
(27, 194, 0, '_sfm_type', 'Form', 0, NULL, 0),
(28, 194, 0, '_sfm_type', 'Governance', 0, NULL, 0),
(29, 194, 0, '_sfm_type', 'Guide', 0, NULL, 0),
(30, 194, 0, '_sfm_type', 'Policy', 0, NULL, 0),
(31, 194, 0, '_sfm_type', 'Procedure', 0, NULL, 0),
(32, 194, 0, '_sfm_type', 'Program', 0, NULL, 0),
(33, 194, 0, '_sfm_type', 'Venue', 0, NULL, 0),
(34, 194, 0, '_sfm_category', 'Primary regional', 0, NULL, 0),
(35, 194, 0, '_sfm_category', 'Secondary regional', 0, NULL, 0),
(36, 194, 0, '_sfm_category', 'College regional', 0, NULL, 0),
(37, 194, 0, '_sfm_category', 'Representative', 0, NULL, 0),
(38, 194, 0, '_sfm_category', 'Education Directorate', 0, NULL, 0),
(39, 194, 0, '_sfm_sports', 'Australian Football', 0, NULL, 0),
(40, 194, 0, '_sfm_sports', 'Baseball', 0, NULL, 0),
(41, 194, 0, '_sfm_sports', 'Basketball', 0, NULL, 0),
(42, 194, 0, '_sfm_sports', 'Cricket', 0, NULL, 0),
(43, 194, 0, '_sfm_sports', 'Cross Country', 0, NULL, 0),
(44, 194, 0, '_sfm_sports', 'Diving', 0, NULL, 0),
(45, 194, 0, '_sfm_sports', 'Football (Soccer)', 0, NULL, 0),
(46, 194, 0, '_sfm_sports', 'Golf', 0, NULL, 0),
(47, 194, 0, '_sfm_sports', 'Hockey', 0, NULL, 0),
(48, 194, 0, '_sfm_sports', 'Netball', 0, NULL, 0),
(49, 194, 0, '_sfm_sports', 'Rugby League', 0, NULL, 0),
(50, 194, 0, '_sfm_sports', 'Softball', 0, NULL, 0),
(51, 194, 0, '_sfm_sports', 'Swimming', 0, NULL, 0),
(52, 194, 0, '_sfm_sports', 'Tennis', 0, NULL, 0),
(53, 194, 0, '_sfm_sports', 'Touch', 0, NULL, 0),
(54, 194, 0, '_sfm_sports', 'Track and Field', 0, NULL, 0),
(55, 194, 0, '_sfm_sports', 'Volleyball', 0, NULL, 0),
(56, 231, 0, '', '', NULL, NULL, NULL),
(57, 232, 0, '', '', NULL, NULL, NULL),
(58, 233, 0, '', '', NULL, NULL, NULL),
(59, 234, 0, '', '', NULL, NULL, NULL),
(60, 235, 0, '', '', NULL, NULL, NULL),
(61, 236, 0, '', '', NULL, NULL, NULL),
(62, 256, 0, '_sfm_type', 'Form', 0, NULL, 0),
(63, 256, 0, '_sfm_category', 'Primary regional', 0, NULL, 0),
(64, 256, 0, '_sfm_category', 'Secondary regional', 0, NULL, 0),
(65, 256, 0, '_sfm_category', 'College regional', 0, NULL, 0),
(66, 256, 0, '_sfm_sports', 'Cross Country', 0, NULL, 0),
(67, 257, 0, '_sfm_type', 'Form', 0, NULL, 0),
(68, 257, 0, '_sfm_category', 'Primary regional', 0, NULL, 0),
(69, 257, 0, '_sfm_category', 'Secondary regional', 0, NULL, 0),
(70, 257, 0, '_sfm_category', 'College regional', 0, NULL, 0),
(71, 257, 0, '_sfm_sports', 'Cross Country', 0, NULL, 0),
(72, 258, 0, '_sfm_type', 'Information', 0, NULL, 0),
(73, 258, 0, '_sfm_category', 'Primary regional', 0, NULL, 0),
(74, 258, 0, '_sfm_category', 'Secondary regional', 0, NULL, 0),
(75, 258, 0, '_sfm_category', 'College regional', 0, NULL, 0),
(76, 258, 0, '_sfm_sports', 'Cross Country', 0, NULL, 0),
(77, 259, 0, '_sfm_type', 'Form', 0, NULL, 0),
(78, 259, 0, '_sfm_category', 'Primary regional', 0, NULL, 0),
(79, 259, 0, '_sfm_category', 'Secondary regional', 0, NULL, 0),
(80, 259, 0, '_sfm_category', 'College regional', 0, NULL, 0),
(81, 259, 0, '_sfm_sports', 'Cross Country', 0, NULL, 0),
(82, 260, 0, '_sfm_type', 'Information', 0, NULL, 0),
(83, 260, 0, '_sfm_type', 'Venue', 0, NULL, 0),
(84, 260, 0, '_sfm_category', 'Primary regional', 0, NULL, 0),
(85, 260, 0, '_sfm_category', 'Secondary regional', 0, NULL, 0),
(86, 260, 0, '_sfm_category', 'College regional', 0, NULL, 0),
(87, 260, 0, '_sfm_sports', 'Cross Country', 0, NULL, 0),
(88, 261, 0, '_sfm_type', 'Information', 0, NULL, 0),
(89, 261, 0, '_sfm_type', 'Program', 0, NULL, 0),
(90, 261, 0, '_sfm_category', 'Primary regional', 0, NULL, 0),
(91, 261, 0, '_sfm_category', 'Secondary regional', 0, NULL, 0),
(92, 261, 0, '_sfm_category', 'College regional', 0, NULL, 0),
(93, 261, 0, '_sfm_sports', 'Cross Country', 0, NULL, 0),
(94, 262, 0, '_sfm_type', 'Information', 0, NULL, 0),
(95, 262, 0, '_sfm_type', 'Venue', 0, NULL, 0),
(96, 262, 0, '_sfm_category', 'Primary regional', 0, NULL, 0),
(97, 262, 0, '_sfm_category', 'Secondary regional', 0, NULL, 0),
(98, 262, 0, '_sfm_category', 'College regional', 0, NULL, 0),
(99, 262, 0, '_sfm_sports', 'Track and Field', 0, NULL, 0),
(100, 263, 0, '_sfm_type', 'Form', 0, NULL, 0),
(101, 263, 0, '_sfm_category', 'Primary regional', 0, NULL, 0),
(102, 263, 0, '_sfm_category', 'Secondary regional', 0, NULL, 0),
(103, 263, 0, '_sfm_category', 'College regional', 0, NULL, 0),
(104, 263, 0, '_sfm_sports', 'Cross Country', 0, NULL, 0),
(105, 264, 0, '_sfm_type', 'Information', 0, NULL, 0),
(106, 264, 0, '_sfm_type', 'Guide', 0, NULL, 0),
(107, 264, 0, '_sfm_category', 'Primary regional', 0, NULL, 0),
(108, 264, 0, '_sfm_category', 'Secondary regional', 0, NULL, 0),
(109, 264, 0, '_sfm_category', 'College regional', 0, NULL, 0),
(110, 264, 0, '_sfm_sports', 'Cross Country', 0, NULL, 0),
(111, 265, 0, '_sfm_type', '', 0, NULL, 0),
(112, 265, 0, '_sfm_category', 'Primary regional', 0, NULL, 0),
(113, 265, 0, '_sfm_category', 'Secondary regional', 0, NULL, 0),
(114, 265, 0, '_sfm_category', 'College regional', 0, NULL, 0),
(115, 265, 0, '_sfm_sports', 'Cross Country', 0, NULL, 0),
(116, 266, 0, '_sfm_type', 'Information', 0, NULL, 0),
(117, 266, 0, '_sfm_type', 'Venue', 0, NULL, 0),
(118, 266, 0, '_sfm_category', 'Primary regional', 0, NULL, 0),
(119, 266, 0, '_sfm_category', 'Secondary regional', 0, NULL, 0),
(120, 266, 0, '_sfm_category', 'College regional', 0, NULL, 0),
(121, 266, 0, '_sfm_sports', 'Swimming', 0, NULL, 0),
(122, 266, 0, '_sfm_sports', 'Track and Field', 0, NULL, 0),
(123, 267, 0, '_sfm_type', 'Information', 0, NULL, 0),
(124, 267, 0, '_sfm_type', 'Venue', 0, NULL, 0),
(125, 267, 0, '_sfm_category', '', 0, NULL, 0),
(126, 267, 0, '_sfm_sports', 'Swimming', 0, NULL, 0),
(127, 268, 0, '_sfm_type', 'Information', 0, NULL, 0),
(128, 268, 0, '_sfm_category', '', 0, NULL, 0),
(129, 268, 0, '_sfm_sports', 'Swimming', 0, NULL, 0),
(130, 268, 0, '_sfm_sports', 'Track and Field', 0, NULL, 0),
(132, 273, 0, '_sfm_type', 'Governance', 0, NULL, 0),
(133, 273, 0, '_sfm_type', 'Guide', 0, NULL, 0),
(134, 273, 0, '_sfm_category', 'Primary regional', 0, NULL, 0),
(135, 273, 0, '_sfm_sports', 'Australian Football', 0, NULL, 0),
(136, 273, 0, '_sfm_sports', 'Baseball', 0, NULL, 0),
(137, 273, 0, '_sfm_sports', 'Basketball', 0, NULL, 0),
(138, 273, 0, '_sfm_sports', 'Cricket', 0, NULL, 0),
(139, 273, 0, '_sfm_sports', 'Cross Country', 0, NULL, 0),
(140, 273, 0, '_sfm_sports', 'Diving', 0, NULL, 0),
(141, 273, 0, '_sfm_sports', 'Football (Soccer)', 0, NULL, 0),
(142, 273, 0, '_sfm_sports', 'Golf', 0, NULL, 0),
(143, 273, 0, '_sfm_sports', 'Hockey', 0, NULL, 0),
(144, 273, 0, '_sfm_sports', 'Netball', 0, NULL, 0),
(145, 273, 0, '_sfm_sports', 'Rugby League', 0, NULL, 0),
(146, 273, 0, '_sfm_sports', 'Softball', 0, NULL, 0),
(147, 273, 0, '_sfm_sports', 'Swimming', 0, NULL, 0),
(148, 273, 0, '_sfm_sports', 'Tennis', 0, NULL, 0),
(149, 273, 0, '_sfm_sports', 'Touch', 0, NULL, 0),
(150, 273, 0, '_sfm_sports', 'Track and Field', 0, NULL, 0),
(151, 273, 0, '_sfm_sports', 'Volleyball', 0, NULL, 0),
(152, 273, 0, '', '', NULL, NULL, NULL),
(154, 281, 0, '', '', NULL, NULL, NULL),
(156, 282, 0, '', '', NULL, NULL, NULL),
(158, 289, 0, '', '', NULL, NULL, NULL),
(160, 296, 0, '', '', NULL, NULL, NULL),
(162, 297, 0, '', '', NULL, NULL, NULL),
(164, 313, 0, '', '', NULL, NULL, NULL),
(166, 319, 0, '', '', NULL, NULL, NULL),
(167, 322, 0, '', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_search_filter_term_results`
--

DROP TABLE IF EXISTS `wp_search_filter_term_results`;
CREATE TABLE `wp_search_filter_term_results` (
  `id` bigint(20) NOT NULL,
  `field_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_value_num` bigint(20) DEFAULT NULL,
  `result_ids` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `wp_search_filter_term_results`
--

INSERT INTO `wp_search_filter_term_results` (`id`, `field_name`, `field_value`, `field_value_num`, `result_ids`) VALUES
(26, '_sfm_type', 'Policy', NULL, '193,194'),
(30, '_sfm_type', 'Procedure', NULL, '194'),
(36, '_sfm_category', 'Representative', NULL, '193,194'),
(37, '_sfm_category', 'Education Directorate', NULL, '193,194'),
(82, '_sfm_type', 'Program', NULL, '194,261'),
(97, '_sfm_type', 'Form', NULL, '194,256,257,259,263'),
(108, '_sfm_type', '', NULL, '265'),
(112, '_sfm_category', 'Secondary regional', NULL, '193,194,256,257,258,259,260,261,262,263,264,265,266'),
(113, '_sfm_category', 'College regional', NULL, '193,194,256,257,258,259,260,261,262,263,264,265,266'),
(117, '_sfm_type', 'Venue', NULL, '194,260,262,266,267'),
(120, '_sfm_type', 'Information', NULL, '193,194,258,260,261,262,264,266,267,268'),
(123, '_sfm_category', '', NULL, '267,268'),
(124, '_sfm_category', 'Primary regional', NULL, '193,194,256,257,258,259,260,261,262,263,264,265,266,273'),
(125, '_sfm_sports', 'Australian Football', NULL, '193,194,273'),
(126, '_sfm_sports', 'Baseball', NULL, '193,194,273'),
(127, '_sfm_sports', 'Basketball', NULL, '193,194,273'),
(128, '_sfm_sports', 'Cricket', NULL, '193,194,273'),
(129, '_sfm_sports', 'Cross Country', NULL, '193,194,256,257,258,259,260,261,263,264,265,273'),
(130, '_sfm_sports', 'Diving', NULL, '193,194,273'),
(131, '_sfm_sports', 'Football (Soccer)', NULL, '193,194,273'),
(132, '_sfm_sports', 'Golf', NULL, '193,194,273'),
(133, '_sfm_sports', 'Hockey', NULL, '193,194,273'),
(134, '_sfm_sports', 'Netball', NULL, '193,194,273'),
(135, '_sfm_sports', 'Rugby League', NULL, '193,194,273'),
(136, '_sfm_sports', 'Softball', NULL, '193,194,273'),
(137, '_sfm_sports', 'Swimming', NULL, '193,194,266,267,268,273'),
(138, '_sfm_sports', 'Tennis', NULL, '193,194,273'),
(139, '_sfm_sports', 'Touch', NULL, '193,194,273'),
(140, '_sfm_sports', 'Track and Field', NULL, '193,194,262,266,268,273'),
(141, '_sfm_sports', 'Volleyball', NULL, '193,194,273'),
(142, '_sfm_type', 'Governance', NULL, '194,273'),
(143, '_sfm_type', 'Guide', NULL, '194,264,273');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_signups`
--

DROP TABLE IF EXISTS `wp_signups`;
CREATE TABLE `wp_signups` (
  `signup_id` bigint(20) NOT NULL,
  `domain` varchar(200) NOT NULL DEFAULT '',
  `path` varchar(100) NOT NULL DEFAULT '',
  `title` longtext NOT NULL,
  `user_login` varchar(60) NOT NULL DEFAULT '',
  `user_email` varchar(100) NOT NULL DEFAULT '',
  `registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `activation_key` varchar(50) NOT NULL DEFAULT '',
  `meta` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_site`
--

DROP TABLE IF EXISTS `wp_site`;
CREATE TABLE `wp_site` (
  `id` bigint(20) NOT NULL,
  `domain` varchar(200) NOT NULL DEFAULT '',
  `path` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `wp_site`
--

INSERT INTO `wp_site` (`id`, `domain`, `path`) VALUES
(1, 'www.ninpodesign.com', '/ssact/');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_sitemeta`
--

DROP TABLE IF EXISTS `wp_sitemeta`;
CREATE TABLE `wp_sitemeta` (
  `meta_id` bigint(20) NOT NULL,
  `site_id` bigint(20) NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `wp_sitemeta`
--

INSERT INTO `wp_sitemeta` (`meta_id`, `site_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'site_name', 'My WordPress Site'),
(2, 1, 'admin_email', 'devnull@installatron.com'),
(3, 1, 'admin_user_id', '1'),
(4, 1, 'registration', 'none'),
(5, 1, 'upload_filetypes', 'jpg jpeg png gif mov avi mpg 3gp 3g2 midi mid pdf doc ppt odt pptx docx pps ppsx xls xlsx key mp3 ogg flac m4a wav mp4 m4v webm ogv flv'),
(6, 1, 'blog_upload_space', '100'),
(7, 1, 'fileupload_maxk', '1500'),
(8, 1, 'site_admins', 'a:1:{i:0;s:8:\"123admin\";}'),
(9, 1, 'allowedthemes', 'a:1:{s:14:\"twentynineteen\";b:1;}'),
(10, 1, 'illegal_names', 'a:9:{i:0;s:3:\"www\";i:1;s:3:\"web\";i:2;s:4:\"root\";i:3;s:5:\"admin\";i:4;s:4:\"main\";i:5;s:6:\"invite\";i:6;s:13:\"administrator\";i:7;s:5:\"files\";i:8;s:4:\"blog\";}'),
(11, 1, 'wpmu_upgrade_site', '44719'),
(12, 1, 'welcome_email', 'Howdy USERNAME,\n\nYour new SITE_NAME site has been successfully set up at:\nBLOG_URL\n\nYou can log in to the administrator account with the following information:\n\nUsername: USERNAME\nPassword: PASSWORD\nLog in here: BLOG_URLwp-login.php\n\nWe hope you enjoy your new site. Thanks!\n\n--The Team @ SITE_NAME'),
(13, 1, 'first_post', 'Welcome to %s. This is your first post. Edit or delete it, then start writing!'),
(14, 1, 'siteurl', 'http://localhost:8888/ssact/'),
(15, 1, 'add_new_users', '0'),
(16, 1, 'upload_space_check_disabled', '1'),
(17, 1, 'subdomain_install', ''),
(18, 1, 'global_terms_enabled', '0'),
(19, 1, 'ms_files_rewriting', '0'),
(20, 1, 'initial_db_version', '44719'),
(21, 1, 'active_sitewide_plugins', 'a:0:{}'),
(22, 1, 'WPLANG', 'en_US'),
(23, 1, 'nonce_key', ']re[cH3wy 7Y%] 7y~c-(N<00v?KSI8a<Cu*Rbj!4tG8jXE4z_3IxmKrhQEj(}wM'),
(24, 1, 'nonce_salt', '?Q2&ic5Y|1-RL||5>`%u7B@b:f%2T`RUYl{_hTrUU{GWPnpmTxVVyFrUjqf},J0h'),
(25, 1, 'auth_key', ')Gs$-].1<*/Z=xh acY]>_>ns KzU&/ag.;@D|hk<DV^t_pqC(~Ek_ YREX^rs6X'),
(26, 1, 'auth_salt', '~.#+}z$3FsM+jkZDWPRE``!?w70:JlM$HA-EGWBbbHP!O;B1m>=}9/?e$?`No9%v'),
(27, 1, 'logged_in_key', 'NtrOitp[Q@EJrg|tWR%tU*ZG*,*sa}aZj=uy3VXY?c!oOToZ|W_0P>!oM2n6pYSb'),
(28, 1, 'logged_in_salt', 'JT0)cM[6xv _Qdc!.!&C&WvKT:#x:&@JC#j4=FVq[|sm&7Tb_VL+K{w#n{<Y#~3x'),
(29, 1, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.1-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.1\";s:7:\"version\";s:5:\"5.2.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1560594859;s:15:\"version_checked\";s:5:\"5.2.1\";s:12:\"translations\";a:0:{}}'),
(30, 1, '_site_transient_update_plugins', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1560594859;s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:2:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.2\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}}}'),
(31, 1, '_site_transient_timeout_theme_roots', '1560596659'),
(32, 1, '_site_transient_theme_roots', 'a:3:{s:14:\"twentynineteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}'),
(33, 1, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1560594859;s:7:\"checked\";a:3:{s:14:\"twentynineteen\";s:3:\"1.4\";s:15:\"twentyseventeen\";s:3:\"2.2\";s:13:\"twentysixteen\";s:3:\"2.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}'),
(34, 1, '_site_transient_timeout_browser_d9d5c10d928d6d9ccaa9bfcb50526967', '1561199660'),
(35, 1, '_site_transient_browser_d9d5c10d928d6d9ccaa9bfcb50526967', 'a:10:{s:4:\"name\";s:7:\"Firefox\";s:7:\"version\";s:4:\"67.0\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:24:\"https://www.firefox.com/\";s:7:\"img_src\";s:44:\"http://s.w.org/images/browsers/firefox.png?1\";s:11:\"img_src_ssl\";s:45:\"https://s.w.org/images/browsers/firefox.png?1\";s:15:\"current_version\";s:2:\"56\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}'),
(36, 1, '_site_transient_timeout_php_check_03b757470e94c3bf37ab9d7408701106', '1561199660'),
(37, 1, '_site_transient_php_check_03b757470e94c3bf37ab9d7408701106', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}'),
(38, 1, 'user_count', '1'),
(39, 1, 'blog_count', '1'),
(40, 1, '_site_transient_timeout_community-events-b7ee23b6686b71e54afda7797532d4e1', '1560638061'),
(41, 1, '_site_transient_community-events-b7ee23b6686b71e54afda7797532d4e1', 'a:2:{s:8:\"location\";a:1:{s:2:\"ip\";s:13:\"144.136.218.0\";}s:6:\"events\";a:1:{i:0;a:7:{s:4:\"type\";s:8:\"wordcamp\";s:5:\"title\";s:15:\"WordCamp Europe\";s:3:\"url\";s:33:\"https://2019.europe.wordcamp.org/\";s:6:\"meetup\";s:0:\"\";s:10:\"meetup_url\";s:0:\"\";s:4:\"date\";s:19:\"2019-06-20 00:00:00\";s:8:\"location\";a:4:{s:8:\"location\";s:15:\"Berlin, Germany\";s:7:\"country\";s:2:\"DE\";s:8:\"latitude\";d:52.506970000000003;s:9:\"longitude\";d:13.2843064;}}}}'),
(42, 1, 'can_compress_scripts', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_termmeta`
--

DROP TABLE IF EXISTS `wp_termmeta`;
CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_terms`
--

DROP TABLE IF EXISTS `wp_terms`;
CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `slug` varchar(200) NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'ACT', 'act', 0),
(3, 'NSW', 'nsw', 0),
(4, 'High School North', 'high-school-north', 0),
(5, 'High School South', 'high-school-south', 0),
(6, 'Belconnen', 'belconnen', 0),
(7, 'North Gungahlin', 'north-gungahlin', 0),
(8, 'South Weston', 'south-weston', 0),
(9, 'Tuggeranong', 'tuggeranong', 0),
(11, 'Menu Top', 'menu-top', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_term_relationships`
--

DROP TABLE IF EXISTS `wp_term_relationships`;
CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(213, 11, 0),
(214, 11, 0),
(215, 11, 0),
(216, 11, 0),
(217, 11, 0),
(222, 11, 0),
(227, 11, 0),
(254, 11, 0),
(255, 11, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_term_taxonomy`
--

DROP TABLE IF EXISTS `wp_term_taxonomy`;
CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'states', '', 0, 0),
(3, 3, 'states', '', 0, 0),
(4, 4, 'regions', '', 0, 0),
(5, 5, 'regions', '', 0, 0),
(6, 6, 'regions', '', 0, 0),
(7, 7, 'regions', '', 0, 0),
(8, 8, 'regions', '', 0, 0),
(9, 9, 'regions', '', 0, 0),
(11, 11, 'nav_menu', '', 0, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_usermeta`
--

DROP TABLE IF EXISTS `wp_usermeta`;
CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', '123admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'theme_editor_notice'),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:5:{s:64:\"60b5cfdd734e4040d2f4b74bc95682c3e6f24d0def286a88958a8ebc5d379525\";a:4:{s:10:\"expiration\";i:1578619973;s:2:\"ip\";s:15:\"201.214.239.235\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36\";s:5:\"login\";i:1577410373;}s:64:\"e0394f86dade5b3d9cbbe949aa610c0a6d7327163b0c3b0d72a47d6768b81f76\";a:4:{s:10:\"expiration\";i:1577656839;s:2:\"ip\";s:15:\"201.241.210.147\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36\";s:5:\"login\";i:1577484039;}s:64:\"769ca6156b99d9e6913d4be19d317a2f56a3400e58ed1f7ad6139efb41cae9f1\";a:4:{s:10:\"expiration\";i:1577675738;s:2:\"ip\";s:15:\"201.214.239.235\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36\";s:5:\"login\";i:1577502938;}s:64:\"2c878d0484671a7302db71634069de0189b6a75024adffc7302bc961050028d6\";a:4:{s:10:\"expiration\";i:1577714719;s:2:\"ip\";s:12:\"190.22.39.99\";s:2:\"ua\";s:113:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36\";s:5:\"login\";i:1577541919;}s:64:\"df112d153e57b5797152bdf14f7b5e765548933fa5e063b775d4f924e2c32d42\";a:4:{s:10:\"expiration\";i:1577748260;s:2:\"ip\";s:15:\"201.214.239.235\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36\";s:5:\"login\";i:1577575460;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '326'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:13:\"201.214.239.0\";}'),
(19, 1, 'source_domain', 'www.ninpodesign.com'),
(20, 1, 'primary_blog', '1'),
(21, 1, 'wp_user-settings', 'editor=tinymce&libraryContent=browse&uploader=1'),
(22, 1, 'wp_user-settings-time', '1576461523'),
(23, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(24, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:\"add-post_tag\";}'),
(41, 1, 'closedpostboxes_acf', 'a:1:{i:0;s:12:\"acf_location\";}'),
(42, 1, 'metaboxhidden_acf', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(43, 1, 'closedpostboxes_dashboard', 'a:2:{i:0;s:19:\"dashboard_right_now\";i:1;s:17:\"dashboard_primary\";}'),
(44, 1, 'metaboxhidden_dashboard', 'a:0:{}'),
(195, 1, 'closedpostboxes_page', 'a:1:{i:0;s:10:\"members-cp\";}'),
(196, 1, 'metaboxhidden_page', 'a:0:{}'),
(678, 31, 'nickname', 'andrew.pearson2@gmail.com'),
(679, 31, 'first_name', 'Andrew'),
(680, 31, 'last_name', 'Pearson'),
(681, 31, 'description', ''),
(682, 31, 'rich_editing', 'true'),
(683, 31, 'syntax_highlighting', 'true'),
(684, 31, 'comment_shortcuts', 'false'),
(685, 31, 'admin_color', 'fresh'),
(686, 31, 'use_ssl', '0'),
(687, 31, 'show_admin_bar_front', 'true'),
(688, 31, 'locale', ''),
(689, 31, 'wp_capabilities', 'a:2:{s:10:\"subscriber\";b:1;s:13:\"administrator\";b:1;}'),
(690, 31, 'wp_user_level', '10'),
(691, 31, 'dismissed_wp_pointers', 'theme_editor_notice'),
(693, 31, 'wp_dashboard_quick_press_last_post_id', '324'),
(694, 31, 'community-events-location', 'a:1:{s:2:\"ip\";s:10:\"119.18.3.0\";}'),
(695, 31, 'closedpostboxes_acf-field-group', 'a:0:{}'),
(696, 31, 'metaboxhidden_acf-field-group', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(719, 1, 'nav_menu_recently_edited', '11'),
(721, 1, 'wp_r_tru_u_x', 'a:2:{s:2:\"id\";s:0:\"\";s:7:\"expires\";i:86400;}'),
(722, 31, 'search-filter-show-welcome-notice', '1'),
(723, 31, 'nav_menu_recently_edited', '11'),
(724, 31, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(725, 31, 'metaboxhidden_nav-menus', 'a:10:{i:0;s:22:\"add-post-type-students\";i:1;s:21:\"add-post-type-schools\";i:2;s:20:\"add-post-type-sports\";i:3;s:19:\"add-post-type-teams\";i:4;s:23:\"add-post-type-bulletins\";i:5;s:22:\"add-post-type-products\";i:6;s:12:\"add-post_tag\";i:7;s:10:\"add-states\";i:8;s:11:\"add-regions\";i:9;s:8:\"add-fees\";}'),
(726, 31, 'wp_media_library_mode', 'list'),
(727, 1, 'search-filter-show-welcome-notice', '1'),
(774, 1, 'wp_email_tracking_ignore_notice', 'true'),
(775, 31, 'wp_user-settings', 'libraryContent=browse'),
(776, 31, 'wp_user-settings-time', '1576452168'),
(777, 1, 'meta-box-order_page', 'a:4:{s:6:\"normal\";s:23:\"acf-group_5df6bc1ec1d8a\";s:8:\"advanced\";s:10:\"members-cp\";s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:0:\"\";}'),
(778, 1, 'closedpostboxes_news', 'a:0:{}'),
(779, 1, 'metaboxhidden_news', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(847, 38, 'nickname', 'fuentesguillermo@live.com'),
(848, 38, 'first_name', 'Guillermo'),
(849, 38, 'last_name', 'Fuentes'),
(850, 38, 'description', ''),
(851, 38, 'rich_editing', 'true'),
(852, 38, 'syntax_highlighting', 'true'),
(853, 38, 'comment_shortcuts', 'false'),
(854, 38, 'admin_color', 'fresh'),
(855, 38, 'use_ssl', '0'),
(856, 38, 'show_admin_bar_front', 'true'),
(857, 38, 'locale', ''),
(858, 38, 'wp_capabilities', 'a:1:{s:6:\"parent\";b:1;}'),
(859, 38, 'wp_user_level', '0'),
(860, 38, 'dismissed_wp_pointers', ''),
(863, 38, 'phone', '921737'),
(864, 38, 'mobile', '983000'),
(865, 38, 'address', 'Pedro Correa 204, Peñaflor'),
(866, 38, 'suburb', 'Peñaflor'),
(867, 38, 'post_code', '9750000'),
(868, 38, 'state', ''),
(869, 38, 'session_tokens', 'a:2:{s:64:\"b168a2624e4336cf0584de13a15fe5ae207931a8151f54d37d687935ae9ee431\";a:4:{s:10:\"expiration\";i:1578520206;s:2:\"ip\";s:15:\"201.214.239.235\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36\";s:5:\"login\";i:1577310606;}s:64:\"09e39da25d21e94594de109a1fdacd3100a2253a88053112ee054976daab3a86\";a:4:{s:10:\"expiration\";i:1578599725;s:2:\"ip\";s:12:\"170.18.1.103\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36\";s:5:\"login\";i:1577390125;}}'),
(870, 38, 'search-filter-show-welcome-notice', '1'),
(871, 38, 'community-events-location', 'a:1:{s:2:\"ip\";s:11:\"186.11.97.0\";}'),
(872, 38, 'city', 'Peñaflor'),
(873, 39, 'nickname', 'tarkonnen@gmail.com'),
(874, 39, 'first_name', 'Juan Pablo'),
(875, 39, 'last_name', 'Contreras Valdés'),
(876, 39, 'description', ''),
(877, 39, 'rich_editing', 'true'),
(878, 39, 'syntax_highlighting', 'true'),
(879, 39, 'comment_shortcuts', 'false'),
(880, 39, 'admin_color', 'fresh'),
(881, 39, 'use_ssl', '0'),
(882, 39, 'show_admin_bar_front', 'true'),
(883, 39, 'locale', ''),
(884, 39, 'wp_capabilities', 'a:1:{s:8:\"official\";b:1;}'),
(885, 39, 'wp_user_level', '0'),
(886, 39, 'dismissed_wp_pointers', ''),
(889, 39, 'phone', '12345678'),
(890, 39, 'mobile', '12345678'),
(891, 39, 'address', 'Calle 123'),
(892, 39, 'suburb', ''),
(893, 39, 'post_code', ''),
(894, 39, 'state', '2'),
(897, 38, 'token', '5e03bdc067186'),
(900, 38, 'trials', '1'),
(901, 38, 'trial', '1'),
(902, 38, 'approve', '1'),
(903, 31, 'closedpostboxes_trials', 'a:1:{i:0;s:10:\"members-cp\";}'),
(904, 31, 'metaboxhidden_trials', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(906, 31, 'session_tokens', 'a:1:{s:64:\"11de765dbbc082509bc6a92cc3c1d9ae2be6f530c29475a194b788536a0a3d04\";a:4:{s:10:\"expiration\";i:1578533524;s:2:\"ip\";s:11:\"119.18.3.78\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36\";s:5:\"login\";i:1577323924;}}'),
(907, 39, 'session_tokens', 'a:1:{s:64:\"8e68d80b828a7652feb549436cadf861f795e69d7f5fee889f37cbba2513962f\";a:4:{s:10:\"expiration\";i:1578693694;s:2:\"ip\";s:15:\"201.241.210.147\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36\";s:5:\"login\";i:1577484094;}}'),
(908, 1, 'edit_page_per_page', '100'),
(909, 1, 'meta-box-order_students', 'a:4:{s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:9:\"submitdiv\";s:6:\"normal\";s:79:\"acf-group_5e050b05070ce,acf-group_5e0585a9d3d1b,acf-group_5e06dc1fa1fbc,slugdiv\";s:8:\"advanced\";s:10:\"members-cp\";}'),
(910, 1, 'screen_layout_students', '2'),
(911, 1, 'closedpostboxes_students', 'a:0:{}'),
(912, 1, 'metaboxhidden_students', 'a:1:{i:0;s:7:\"slugdiv\";}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_users`
--

DROP TABLE IF EXISTS `wp_users`;
CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) NOT NULL DEFAULT '',
  `user_pass` varchar(255) NOT NULL DEFAULT '',
  `user_nicename` varchar(50) NOT NULL DEFAULT '',
  `user_email` varchar(100) NOT NULL DEFAULT '',
  `user_url` varchar(100) NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) NOT NULL DEFAULT '',
  `spam` tinyint(2) NOT NULL DEFAULT '0',
  `deleted` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`, `spam`, `deleted`) VALUES
(1, 'bs4csh', '$P$BfNL4IxEKpRZUND0G.ihaaU96ZRS.21', 'admin', 'ninpodesigncrew@gmail.com', '', '2019-11-27 02:12:45', '', 0, 'admin', 0, 0),
(31, 'andrew.pearson2@gmail.com', '$P$BfDlAlTM46lOoZvmDCWOon6ZUCMMNL/', 'andrew-pearson2gmail-com', 'andrew.pearson2@gmail.com', '', '2019-12-14 21:33:22', '1576359202:$P$BWolQGdBQ9F5yq4c2XQJ//34MuUmLt.', 0, 'Andrew Pearson', 0, 0),
(38, 'fuentesguillermo@live.com', '$P$BR8Sw2.NcqOfE7VvUjuBE.XxfSwI6M.', 'fuentesguillermolive-com', 'fuentesguillermo@live.com', '', '2019-12-17 19:37:23', '', 0, 'Guillermo Fuentes', 0, 0),
(39, 'tarkonnen@gmail.com', '$P$BQPxx8KKJBoZReJbEN8qFba/VOUiKS1', 'tarkonnengmail-com', 'tarkonnen@gmail.com', '', '2019-12-24 14:03:03', '', 0, 'Juan Pablo Contreras Valdés', 0, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `wp_blogmeta`
--
ALTER TABLE `wp_blogmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `meta_key` (`meta_key`(191)),
  ADD KEY `blog_id` (`blog_id`);

--
-- Indices de la tabla `wp_blogs`
--
ALTER TABLE `wp_blogs`
  ADD PRIMARY KEY (`blog_id`),
  ADD KEY `domain` (`domain`(50),`path`(5)),
  ADD KEY `lang_id` (`lang_id`);

--
-- Indices de la tabla `wp_blog_versions`
--
ALTER TABLE `wp_blog_versions`
  ADD PRIMARY KEY (`blog_id`),
  ADD KEY `db_version` (`db_version`);

--
-- Indices de la tabla `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indices de la tabla `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indices de la tabla `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indices de la tabla `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indices de la tabla `wp_registration_log`
--
ALTER TABLE `wp_registration_log`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `IP` (`IP`);

--
-- Indices de la tabla `wp_sciact_schools`
--
ALTER TABLE `wp_sciact_schools`
  ADD PRIMARY KEY (`school_id`);

--
-- Indices de la tabla `wp_sciact_students`
--
ALTER TABLE `wp_sciact_students`
  ADD PRIMARY KEY (`student_id`);

--
-- Indices de la tabla `wp_search_filter_cache`
--
ALTER TABLE `wp_search_filter_cache`
  ADD PRIMARY KEY (`id`),
  ADD KEY `field_name_index` (`field_name`(191)),
  ADD KEY `field_value_index` (`field_value`(191)),
  ADD KEY `field_value_num_index` (`field_value_num`);

--
-- Indices de la tabla `wp_search_filter_term_results`
--
ALTER TABLE `wp_search_filter_term_results`
  ADD PRIMARY KEY (`id`),
  ADD KEY `field_name_index` (`field_name`(191)),
  ADD KEY `field_value_index` (`field_value`(191)),
  ADD KEY `field_value_num_index` (`field_value`(191));

--
-- Indices de la tabla `wp_signups`
--
ALTER TABLE `wp_signups`
  ADD PRIMARY KEY (`signup_id`),
  ADD KEY `activation_key` (`activation_key`),
  ADD KEY `user_email` (`user_email`),
  ADD KEY `user_login_email` (`user_login`,`user_email`),
  ADD KEY `domain_path` (`domain`(140),`path`(51));

--
-- Indices de la tabla `wp_site`
--
ALTER TABLE `wp_site`
  ADD PRIMARY KEY (`id`),
  ADD KEY `domain` (`domain`(140),`path`(51));

--
-- Indices de la tabla `wp_sitemeta`
--
ALTER TABLE `wp_sitemeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `meta_key` (`meta_key`(191)),
  ADD KEY `site_id` (`site_id`);

--
-- Indices de la tabla `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indices de la tabla `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indices de la tabla `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indices de la tabla `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `wp_blogmeta`
--
ALTER TABLE `wp_blogmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `wp_blogs`
--
ALTER TABLE `wp_blogs`
  MODIFY `blog_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1149;

--
-- AUTO_INCREMENT de la tabla `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1664;

--
-- AUTO_INCREMENT de la tabla `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=423;

--
-- AUTO_INCREMENT de la tabla `wp_registration_log`
--
ALTER TABLE `wp_registration_log`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `wp_sciact_schools`
--
ALTER TABLE `wp_sciact_schools`
  MODIFY `school_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `wp_sciact_students`
--
ALTER TABLE `wp_sciact_students`
  MODIFY `student_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `wp_search_filter_cache`
--
ALTER TABLE `wp_search_filter_cache`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;

--
-- AUTO_INCREMENT de la tabla `wp_search_filter_term_results`
--
ALTER TABLE `wp_search_filter_term_results`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT de la tabla `wp_signups`
--
ALTER TABLE `wp_signups`
  MODIFY `signup_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `wp_site`
--
ALTER TABLE `wp_site`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `wp_sitemeta`
--
ALTER TABLE `wp_sitemeta`
  MODIFY `meta_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=913;

--
-- AUTO_INCREMENT de la tabla `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
