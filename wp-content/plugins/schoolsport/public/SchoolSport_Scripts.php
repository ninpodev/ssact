<?php

class SchoolSport_Scripts {
    public function __construct() {
        add_action( 'wp_enqueue_scripts', array($this,'schoolsport_enqueue_script') );
      add_action( 'wp_enqueue_scripts',  array($this,'schoolsport_enqueue_styles') );
        add_action( 'wp_head', array($this,'wp_ajax_url')); 
    }

    public function schoolsport_enqueue_script(){
        wp_enqueue_script( 'iact-google-captcha', '//www.google.com/recaptcha/api.js');
        wp_enqueue_script( 'schoolsport-select2-script', SCHOOLSPORT_URL. '/public/assets/js/plugins/select2.min.js', array( 'jquery' ),'1.0', true);     
        wp_enqueue_script( 'schoolsport-boostrap-script', SCHOOLSPORT_URL. '/public/assets/js/plugins/bootstrap.min.js', array( 'jquery' ),'1.0', true);
        wp_enqueue_script( 'schoolsport-bootstrap-validation-script', SCHOOLSPORT_URL. '/public/assets/js/plugins/jqBootstrapValidation.js', array( 'jquery' ),'1.0', true);
        wp_enqueue_script( 'schoolsport-app-script', SCHOOLSPORT_URL. '/public/assets/js/app.js', array( 'jquery' ),'1.0', true);
        wp_enqueue_script( 'schoolsport-user-script', SCHOOLSPORT_URL. '/public/assets/js/controller/user.js', array( 'jquery' ),'1.0', true);
    }

    public function wp_ajax_url() {
        echo '<script type="text/javascript">var ajax_url="'.admin_url( 'admin-ajax.php' ).'";</script>';
    }
  
    public function schoolsport_enqueue_styles() {
      	wp_enqueue_style( 'schoolsport-app-style', SCHOOLSPORT_URL . '/public/assets/css/app.css', array(), '1.2', 'all');
        wp_enqueue_style( 'schoolsport-select2-style', SCHOOLSPORT_URL . '/public/assets/css/select2.min.css', array(), '1.1', 'all');
    }
}

$schoolsport_scripts = new SchoolSport_Scripts;