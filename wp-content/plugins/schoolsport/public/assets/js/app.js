jQuery(function($) {
  
    $.fn.schoolFormValidation = function(callback) {
        var element = $(this);
        
        element.find("input, textarea, select").jqBootstrapValidation({
            preventSubmit: true,
            submitError: function($form, event, errors) {
                // additional error messages or events
            },
            submitSuccess: function($form, event) {
              
                event.preventDefault();
                callback($form);
            },
            filter: function() {
                return $(this).is(":visible");
            },
        });
    };

    $.fn.buttonDisabled = function(state) {
        var element = $(this);
        if( state === 'disabled'){
            element.find("[type=submit]").attr("disabled", true);
            return;
        }
        element.find("[type=submit]").removeAttr('disabled');
        return;
    }


    $.fn.notify = function(response, time){
        var element = $(this);
        var notice = element.find('[data-notice]');
        if(typeof response.class !== 'undefined' && typeof response.message !== 'undefined' && notice){
            var icon = notice.find('[data-icon]');
            var alert =  notice.find('[role=alert]');
            var message = notice.find('[data-message]');
            if(response.status == 'success'){
                icon.attr('class', 'icon_info icon-checkmark');
                alert.attr('class', 'alert alert-success');
            } else {
                alert.attr('class', 'alert alert-danger');
                icon.attr('class', 'icon_info icon-error');
            }
            if(typeof time == 'undefined') {
                time = 20000;
            }
            message.html(response.message);
            notice.fadeIn('normal', function () {
                notice.delay(time).fadeOut('normal');
            });
        }
    }

    $.fn.exStep = function(step) {
        var element = $(this);
        var currentStep = (currentStep !== 'undefined') ? step - 1 : 0;
        var nextStep = step;
        var steps = element.find('.step');
        var circle = element.find('.step_progress .circle'); 
        steps.eq(currentStep).delay(2000).fadeOut('normal', function () {
            $('html, body').animate({ scrollTop: $('header').offset().top })
            steps.eq(nextStep).fadeIn('normal');
            circle.eq(nextStep).addClass('active');
        });
    }

    $.fn.step = function(step, callback) {
        var element = $(this);
      
        var progress = element.find('[data-step="progress"]');
        var buttonGo = element.find('[data-step-go]');
        
        var goStep = function( step ) {
            var nextStep = (step > 0 ) ? step - 1 : 0;
            var steps = element.find('.step');
            steps.filter(":visible").stop(true,true).fadeOut('normal', function () {
                $('html, body').animate({ scrollTop: $('header').offset().top });
                steps.eq(nextStep).stop(true, true).fadeIn('normal');
                progress.attr('class', 'circle');
                progress.slice(0, step).addClass('active');
            });
        }
      
        buttonGo.click(function(e){
            e.preventDefault();
            var step = $(this).data('step-go');
            goStep(step);
        });
       
        if(callback !== 'undefined') {
            setTimeout(function() {
               callback();    
            }, 500);
        }
      
        goStep(step);
    }
     
    $.fn.loading = function(state) {
        var self = this;
        var destroyer = function() {
           $('.loading-school').remove();
        }
        var layout = $('<div/>', {
          class: 'loading-school'
        });
        var spinner = $('<div/>', {
          class: 'spinner'
        });
      
        destroyer();
      
        if(state == 'hide') {
          this.find(layout).remove();
        } else {
          this.append(layout.append(spinner));
        }
      
        
    }
    

});