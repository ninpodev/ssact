;(function ($) { $.fn.datepicker.language['en'] = {
    days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    daysMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
    months: ['January','February','March','April','May','June', 'July','August','September','October','November','December'],
    monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    today: 'Today',
    clear: 'Clear',
    dateFormat: 'mm/dd/yyyy',
    timeFormat: 'hh:ii aa',
    firstDay: 0
}; })(jQuery);

(function($){
    $.fn.serializeObject = function(){
        var self = this,
            json = {},
            push_counters = {},
            patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push":     /^$/,
                "fixed":    /^\d+$/,
                "named":    /^[a-zA-Z0-9_]+$/
            };
        this.build = function(base, key, value){
            base[key] = value;
            return base;
        };
        this.push_counter = function(key){
            if(push_counters[key] === undefined){
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };

        $.each($(this).serializeArray(), function(){

            // Skip invalid keys
            if(!patterns.validate.test(this.name)){
                return;
            }

            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;

            while((k = keys.pop()) !== undefined){
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');
                if(k.match(patterns.push)){
                    merge = self.build([], self.push_counter(reverse_key), merge);
                }
                else if(k.match(patterns.fixed)){
                    merge = self.build([], k, merge);
                }
                else if(k.match(patterns.named)){
                    merge = self.build({}, k, merge);
                }
            }
            json = $.extend(true, json, merge);
        });
        return json;
    };
})(jQuery);

var Select2Cascade = ( function(window, $) {

    function Select2Cascade(parent, child, url) {
        var afterActions = [];
        var options =  {};

        // Register functions to be called after cascading data loading done
        this.then = function(callback) {
            afterActions.push(callback);
            return this;
        };

        parent.select2(options).on("change", function (e) {

            child.prop("disabled", true);

            var _this = this;
            $.getJSON(url+'&'+$(this).attr('name')+'='+$(this).val(), function(items) {
                var defaultLabel = child.find('option:first').text();
                var newOptions = '<option value="">'+defaultLabel+'</option>';
                for(var id in items) {
                    newOptions += '<option value="'+ id +'">'+ items[id] +'</option>';
                }

                child.select2('destroy').html(newOptions).prop("disabled", false)
                    .select2(options);
                
                afterActions.forEach(function (callback) {
                    callback(parent, child, items);
                });
            });
        });
    }
    return Select2Cascade;
})( window, $);


jQuery(document).ready(function($) {
	// cambiar ha funcion
	var dataTable = $('#example').DataTable({
		"bSort": false,
		"lengthChange": false,
		"bFilter": false,
		"pageLength": 5,
        "responsive": true
	});
  
    
    var SchoolSportFormUpdateUser =  $("form[rel='user-update']"); 
	var SchoolSportDatePicker = $(".datepicker-here");   
	var SchoolSportDropDown = $("body .dropdown-toggle");
	var SchoolAddProductSet = $("[rel='product-add-set']");
	var SchoolRemoveProductSet = $("[rel='product-remove-set']");
	var SchoolListPreProductSet = $("[name='pre_product_set']"); 
	var SchoolListProductSet = $("[name='product_set[]']");
	
    var dashboard =  {
        init: function(){
            this.trigger();
        },
		trigger: function () {
            var self = this;
            
            SchoolSportFormUpdateUser.schoolFormValidation(function($form) {
                self.update($form);
            });
			
			SchoolAddProductSet.click(function() {
				self.addProductSet();
			});
			
			SchoolRemoveProductSet.click(function() {
				self.removeProductSet();
			});
			
			self.loadDatePicker();
			self.loadDropDown();
        },
		update: function() {
			$.ajax({
                url: ajax_url,
                data: SchoolSportFormUpdateUser.serialize()+"&action=school_sport_update_user_ajax",
                method: "POST",
                beforeSend : function(){
                    SchoolSportFormUpdateUser.buttonDisabled('disabled');
                }
            }).done(function( json ) {
                var response = jQuery.parseJSON(json); 
                SchoolSportFormUpdateUser.notify(response);
                SchoolSportFormUpdateUser.buttonDisabled('enabled');
                if( typeof response.status !== 'undefined'){
                    if(response.status == 'success'){
                        if(typeof response.url !== 'undefined') {
                           window.top.location = response.url;
                        }
                    }
                }
            }).error(function(err){
				console.log(err);
			});
		},
		loadDatePicker: function() {
			SchoolSportDatePicker.each(function() {  
				var explodeDate = $(this).val().split('/');
				if( explodeDate && explodeDate.length == 3) {
					var month = explodeDate[0];
					var day = explodeDate[1];
					var year = explodeDate[2];
					var date = new Date(year+'/'+month+'/'+day);
					$(this).datepicker({ dateFormat: 'mm/dd/yyyy', }).data('datepicker').selectDate(date);
				} else {
					$(this).datepicker({ dateFormat: 'mm/dd/yyyy', });
				}
			});
		
		},
       
		loadDropDown: function() {
			SchoolSportDropDown.dropdown();
		},
		addProductSet: function() {
			var selectedItem = SchoolListPreProductSet.find('option:selected');
			SchoolListProductSet.append(selectedItem);
		},
		removeProductSet: function() {
			var selectedItem = SchoolListProductSet.find('option:selected');
			SchoolListPreProductSet.append(selectedItem);
           
		}
	}
	
	dashboard.init();
	
});