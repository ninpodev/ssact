jQuery(function($) {
    var SchoolSportFormCreateTrial =  $("form[rel='create-trial']"); 
    var SchoolSportFormProductSetTrial = $("form[rel='product-set-trial']");
    var SchoolSportFormSubmitTrial = $("form[rel='create-submit-trial']");
    var SchoolSportFormTeamSelectionTrial = $("form[rel='team-selection-trial']");
    var SchoolSportFormTeamTryOnDate = $("form[rel='team-try-on-date']");
    var SchoolSportFormTeamSubmit = $("form[rel='team-selection-submit']");
    var SchoolSportTrial = $('input[name="trial"]');
    var SchoolSportDateTrial = $('input[name="date_trial"]');
    var SchoolSportDateBirthFrom = $('input[name="date_birth_from"]');
    var SchoolSportDateBirthTo = $('input[name="date_birth_to"]');
    var SchooltSportTimePickerFrom =  $('input[name="time_trial_from"]');
    var SchooltSportTimePickerTo=  $('input[name="time_trial_to"]');    
    var SchoolSportChampDate = $('input[name="champ_date"]');
    var SchoolTableSelectTeamTrial = $("table#select-team-trial");
    var SchoolTableSelectTeamSubmit = $("table#select-team-submit-table");
    var SchoolSportSteps = $('.steps');
    var SchoolSportCheckboxs = $(':checkbox');

    var official =  {
        init: function(){
	        this.setTrial();	
            this.trigger();
        },
		trigger: function () {
            var self = this; 
            SchoolSportFormCreateTrial.schoolFormValidation(function($form) {
                self.createTrial($form);
            });
			SchoolSportFormProductSetTrial.schoolFormValidation(function($form) {
                self.productSetTrial($form);
            });
            SchoolSportFormSubmitTrial.schoolFormValidation(function($form){
                self.submitTrial($form);
            });
            SchoolSportFormTeamSelectionTrial.schoolFormValidation(function($form){
                self.teamSelectionTrial($form);
            });
            SchoolSportFormTeamTryOnDate.schoolFormValidation(function($form){
                self.teamTryOnDate($form);
            });
            SchoolSportFormTeamSubmit.schoolFormValidation(function($form){
                self.teamSubmit($form);
            });
            SchoolSportDateTrial.datepicker({
               minDate: new Date()
            });
            SchoolSportChampDate.datepicker({
                minDate: new Date()
            });
            SchoolSportDateBirthFrom.datepicker({
                maxDate: new Date()
            });
            SchoolSportDateBirthTo.datepicker({
                maxDate: new Date()
            });
            self.tableSelectTeamTrial();
            
            self.tableSelectTeamSubmit();
            
            self.onlyCheckbox();
            
            self.loadTimePicker();
        },
        setTrial: function() {
            this.trial = 0;
            if(SchoolSportTrial && SchoolSportTrial.length !== 0){
                this.trial = parseInt(SchoolSportTrial.val());
            }
        },
		createTrial: function() {          
            var self = this;
            var trial = this.trial;
			$.ajax({
                url: ajax_url,
                data: SchoolSportFormCreateTrial.serialize()+"&action=school_sport_create_trial_ajax&trial="+trial,
                method: "POST",
                beforeSend : function(){
                    SchoolSportFormCreateTrial.loading('show');
                }
            }).done(function( json ) {
                var response = jQuery.parseJSON(json); 
                //SchoolSportFormCreateTrial.notify(response);
                if(typeof response.data !== 'undefined') {
                    var data = response.data;
                    var trial = data.trial;
                    self.trial = parseInt(trial);       
                    SchoolSportSteps.step(2, function() {
                        SchoolSportFormCreateTrial.loading('hide');
                    });
                }
               //revisar por libreria
            }).fail(function(err){
				console.log(err);
			});
		},
		productSetTrial: function() {
            var trial = this.trial;
			$.ajax({
                url: ajax_url,
                data: SchoolSportFormProductSetTrial.serialize()+"&action=school_sport_create_product_set_ajax&trial="+trial,
                method: "POST",
                beforeSend : function(){
                    SchoolSportFormProductSetTrial.loading('show');
                }
            }).done(function( json ) {
                var response = jQuery.parseJSON(json); 
                //SchoolSportFormProductSetTrial.notify(response);
                if(typeof response.data !== 'undefined') {
                    SchoolSportSteps.step(3, function() {
                        SchoolSportFormProductSetTrial.loading('hide');
                    });
                }
               
            }).fail(function(err){
				console.log(err);
			});
		},
		submitTrial: function() {
			var trial = this.trial;
			$.ajax({
                url: ajax_url,
                data: SchoolSportFormSubmitTrial.serialize()+"&action=school_sport_submit_trial_ajax&trial="+trial,
                method: "POST",
                beforeSend : function(){
                    SchoolSportFormSubmitTrial.buttonDisabled('disabled');
                }
            }).done(function( json ) {
                var response = jQuery.parseJSON(json); 
                SchoolSportFormSubmitTrial.notify(response);
                if( typeof response.status !== 'undefined'){
                    if(response.status == 'success'){
                        if(typeof response.url !== 'undefined') {
                           window.top.location = response.url;
                        }
                    }
                }
            }).fail(function(err){
				console.log(err);
			});
		},
        tableSelectTeamTrial: function() {
            SchoolTableSelectTeamTrial.DataTable({
                "bSort": false,
                "lengthChange": false,
                "bFilter": false,
                "pageLength": 5,
                "responsive": true,
                "autoWidth": false,
                 buttons: [
                    'excel'
                ]
            });
        },
        teamSelectionTrial: function(SchoolSportForm) {
            var self = this;
			$.ajax({
                url: ajax_url,
                data: SchoolSportForm.serialize()+"&action=school_sport_team_selection_trial_ajax",
                method: "POST",
                beforeSend : function(){
                      SchoolSportForm.loading('loading');
                }
            }).done(function( json ) {
                var response = jQuery.parseJSON(json); 
                var data = response.data;
                var selection = data.selection;
                self.selection = selection.id; 
                self.teamStudent = selection.students;
                SchoolSportForm.notify(response);
                if( typeof response.status !== 'undefined'){
                    if(response.status == 'success'){
                        SchoolSportSteps.step(2, function() {
                            SchoolSportForm.loading('hide');
                        });
                    }
                }
            }).fail(function(err){
				console.log(err);
			});
        },
        teamTryOnDate: function(SchoolSportForm) {
            var selection = this.selection;
            var self = this;
			$.ajax({
                url: ajax_url,
                data: SchoolSportForm.serialize()+"&action=school_sport_team_try_on_date_ajax&selection="+selection,
                method: "POST",
                beforeSend : function(){
                      SchoolSportForm.loading('loading');
                }
            }).done(function( json ) {
                var response = jQuery.parseJSON(json); 
                var data = response.data; 
                SchoolSportForm.notify(response);
                if( typeof response.status !== 'undefined'){
                    if(response.status == 'success'){
                        var selectionLocal = data.selection;
                        SchoolSportSteps.step(3, function() {
                            SchoolSportForm.loading('hide');
                            self.fillSelectionSubmit(selectionLocal);
                            self.addRowTableSelectTeamSubmit();
                        });
                    }
                }
            }).fail(function(err){
				console.log(err);
			});
        },
        teamSubmit: function(SchoolSportForm) {
            var selection = this.selection;
			$.ajax({
                url: ajax_url,
                data: SchoolSportForm.serialize()+"&action=school_sport_team_selection_submit_ajax&selection="+selection,
                method: "POST",
                beforeSend : function(){
                      SchoolSportForm.loading('loading');
                }
            }).done(function( json ) {
                var response = jQuery.parseJSON(json); 
                SchoolSportForm.notify(response, 15000);
                if( typeof response.status !== 'undefined'){
                    if(response.status == 'success'){
                       if(typeof response.url !== 'undefined') {
                           SchoolSportForm.loading('hide');
                           setTimeout(function() {
                               window.top.location = response.url;                           
                           }, 15000);
                        }
                    }
                }
            }).fail(function(err){
				console.log(err);
			});
        },
        fillSelectionSubmit: function(selection) {
            if(selection) {
               $.each(selection, function(key, value){
                 var element = SchoolSportFormTeamSubmit.find('[name="'+key+'"]');
                 element.val(value);
               }); 
            }
        },
        tableSelectTeamSubmit: function() {
            SchoolTableSelectTeamSubmit.DataTable({
                "bSort": false,
                "lengthChange": false,
                "bFilter": false,
                "pageLength": 5,
                "responsive": true,
                "autoWidth": false,
                columnDefs: [
                    { className: "col-6 border-0 text-muted text-normal", targets: [0] },
                    { className: "col-6 border-0 text-muted text-normal text-right", targets: [1] },
                ],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    var class_table;
                    if(aData[1]) {
                        if(aData[1] == 'Selected') {
                            class_table = 'text-success';
                        } else if(aData[1] == 'Shadow') {
                            class_table = 'text-dark';
                        } else {
                             class_table = 'text-danger';
                        }
                       $('td:eq(1)', nRow).html( '<span class="'+class_table+'">'+aData[1]+'</span>' );
                    }
                 }
            })
        },
        addRowTableSelectTeamSubmit: function() {
            if(this.teamStudent) {
                var studentSelected = this.teamStudent; 
                SchoolTableSelectTeamSubmit.dataTable().fnClearTable();
                $.each(studentSelected, function(key, obj){
                    SchoolTableSelectTeamSubmit.dataTable().fnAddData([ obj.name, obj.status ]);
                });
            }
        },
        onlyCheckbox: function() {
            var self = this;
            SchoolSportCheckboxs.on('change',function(){
                var th = $(this), name = th.prop('name'); 
                if(th.is(':checked')){
                    $(':checkbox[name="'  + name + '"]').not($(this)).prop('checked',false);   
                }
            });
        },
        loadTimePicker: function() {
           SchooltSportTimePickerFrom.timepicker({
                timeFormat: 'h:mm p',
                interval: 60,
                dynamic: true,
                dropdown: true,
                scrollbar: true
            });
            SchooltSportTimePickerTo.timepicker({
                timeFormat: 'h:mm p',
                interval: 60,
                dynamic: true,
                dropdown: true,
                scrollbar: true
            });
        },
	}
	
	official.init();
	
});