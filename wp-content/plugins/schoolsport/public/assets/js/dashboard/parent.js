jQuery(document).ready(function($) {
    var SchoolSportFormCreateStudent =  $("form[rel='create-student']"); 
    var SchoolSportFormEditStudent =  $("form[rel='edit-student']"); 
    var SchoolSportFormMedicalStudent =  $("form[rel='medical-student']"); 
    var SchoolSportFormEmergencyStudent =  $("form[rel='emergency-student']"); 
    var SchoolSportFormStudentSubmit = $("form[rel='add-student']");
    var SchoolSportFormStudentTrial = $("form[rel='register-student']");
    var SchoolSportFormStudentInformationTrial = $("form[rel='register-information-student']");
    var SchoolSportFormStudentPayment = $("form[rel='payment-student']");
    var SchoolSportFormStudentPaymentFee = $("form[rel='payment-fee']");
    var SchoolSportFormStudentPaymentProducts = $("form[rel='payment-products']");
    var SchoolSportStudent = $('input[name="student"]');
    var SchoolSportStudentBirth = $('input[name="date_of_birth"]');
    var SchoolSportSelectTrial = $('select[name="trial"]');
    var SchoolSportSelectStudent = $('select[name="student"]');
    var SchoolSportStep = $('.steps');
    

	  var parent =  {
      init: function(){
          this.setStudent();
          this.trigger();
          this.orderProductsStudent();
      },
      setStudent: function() {
        this.student = 0;
        if(SchoolSportStudent && SchoolSportStudent.length !== 0){
          this.student = parseInt(SchoolSportStudent.val());
        }
      },
      orderProductsStudent: function() {
        var totalAmount = 0
        $('.products').each(function () {
          var id = '#qty-' + $(this).attr('data-id');
          var price = $(this).attr('data-price');
          var qty = $(id).val();

          totalAmount = totalAmount + price*qty
        });
      
        $('#price-amount').text('$' + totalAmount);
        $('#amount').attr('max', totalAmount);
        $('#amount').val(totalAmount);

        var currentCost = $('#amount').val();
        $('#amount-to-pay').text(currentCost);


        $('.product-qty').change(function() {
          totalAmount = 0
          $('.products').each(function () {
            var id = '#qty-' + $(this).attr('data-id');
            var price = $(this).attr('data-price');
            var qty = $(id).val();
  
            totalAmount = totalAmount + price*qty
          });
        
          $('#price-amount').text('$' + totalAmount);
          $('#amount').attr('max', totalAmount);
          $('#amount').val(totalAmount);
          var currentCost = $('#amount').val();
          $('#amount-to-pay').text(currentCost);


          $("#btnSubmitOrder").on('click', function(e) {
            e.preventDefault();
            $('#step1').removeClass('active');
            $('#step2').addClass('active');

            $('#step-container-1').css('display', 'none');
            $('#step-container-2').css('display', 'block');


          })

        })

        
        
        
      },
      trigger: function () {
        var self = this;
        
        SchoolSportFormCreateStudent.schoolFormValidation(function($form) {
            self.createStudent($form);
        });

        SchoolSportFormEditStudent.schoolFormValidation(function($form) {
          self.editStudent($form);
      });
        
        SchoolSportFormMedicalStudent.schoolFormValidation(function($form){
            self.medicalStudent($form);
        });

        SchoolSportFormEmergencyStudent.schoolFormValidation(function($form){
          self.emergencyStudent($form);
        });
        
        SchoolSportFormStudentSubmit.schoolFormValidation(function($form){
          self.addStudent($form);
        });

        SchoolSportFormStudentTrial.schoolFormValidation(function($form){
          self.trialStudent($form);
        });

        SchoolSportFormStudentInformationTrial.schoolFormValidation(function($form){
          self.trialInformationStudent($form);
        });
        
        SchoolSportFormStudentPayment.schoolFormValidation(function($form){
          self.trialPaymentStudent($form);
        });
        
        SchoolSportFormStudentPaymentFee.schoolFormValidation(function($form){
          self.studentPaymentFee($form);
        });
        SchoolSportStudentBirth.datepicker({
           maxDate: new Date()
        });
        SchoolSportSelectTrial.select2({
            width: '100%'
        });
        SchoolSportSelectStudent.select2({
            width: '100%'
        });
          
        self.selectTrialStudent();
      },
      createStudent: function(SchoolSportStudent) {
        var self = this;
        $.ajax({
          url: ajax_url,
          data: SchoolSportStudent.serialize()+"&action=school_sport_create_student_ajax",
          method: "POST",
          beforeSend : function(){
              SchoolSportStudent.loading('show');
          }      
        }).done(function( json ) {
            var response = $.parseJSON(json); 
            SchoolSportStudent.notify(response);
            if(typeof response.data !== 'undefined') {
              var data = response.data;
              var student = data.student;
              self.student = student;
              SchoolSportStep.step(2 ,function() {
                SchoolSportStudent.loading('hide');
              });
            }
        }).fail(function(err){
          console.log(err);
        });
      },
      editStudent: function(SchoolSportStudent) {
        var self = this;
        $.ajax({
          url: ajax_url,
          data: SchoolSportStudent.serialize()+"&action=school_sport_edit_student_ajax",
          method: "POST",
          beforeSend : function(){
              SchoolSportStudent.loading('show');
          }      
        }).done(function( json ) {
            var response = $.parseJSON(json); 
            SchoolSportStudent.notify(response);
            if(typeof response.data !== 'undefined') {
              var data = response.data;
              var student = data.student;
              self.student = student;
              SchoolSportStep.step(2 ,function() {
                SchoolSportStudent.loading('hide');
              });
            }
        }).fail(function(err){
          console.log(err);
        });
      },
      medicalStudent: function(SchoolSportStudent) {
        var student = this.student.id;
        $.ajax({
          url: ajax_url,
          data: SchoolSportStudent.serialize()+"&action=school_sport_medical_student_ajax&student="+student,
          method: "POST",
          beforeSend : function(){
             SchoolSportStudent.loading('show');
          }      
        }).done(function( json ) {
            var response = $.parseJSON(json); 
            SchoolSportStudent.notify(response);
            SchoolSportStudent.buttonDisabled('enabled');
            if(typeof response.data !== 'undefined') {
              SchoolSportStep.step(3 ,function() {
                SchoolSportStudent.loading('hide');
              });
            }
        }).fail(function(err){
          console.log(err);
        });
      
      },
      emergencyStudent: function(SchoolSportStudent) {
        var self = this; 
        var student = self.student.id;
       
        $.ajax({
          url: ajax_url,
          data: SchoolSportStudent.serialize()+"&action=school_sport_emergency_student_ajax&student="+student,
          method: "POST",
          beforeSend : function(){
             SchoolSportStudent.loading('show');
          }    
        }).done(function( json ) {
            var response = $.parseJSON(json); 
            SchoolSportStudent.notify(response);
            SchoolSportStudent.buttonDisabled('enabled');
            if(typeof response.data !== 'undefined') {
              var studenLocal = response.data.student;
              self.fillFormSchoolSportStudent(studenLocal);
              SchoolSportStep.step(4 ,function() {
                SchoolSportStudent.loading('hide');
              });
            }
        }).fail(function(err){
          console.log(err);
        });
      },
      fillFormSchoolSportStudent(student){
        if(student) {
           $.each(student, function(key, value){
             var element = SchoolSportFormStudentSubmit.find('[name="'+key+'"]');
             if(element && key == 'school') {
                if(student.school_name){
                  element.val(student.school_name);
               }
             } else if(element) {
               element.val(value);
             }
           }); 
        }
      },
      addStudent: function(SchoolSportStudent) {
        var student = this.student.id;
        $.ajax({
          url: ajax_url,
          data: SchoolSportStudent.serialize()+"&action=school_sport_add_student_ajax&student="+student,
          method: "POST",
          beforeSend : function(){
             SchoolSportStudent.loading('show');
          }    
        }).done(function( json ) {
            var response = $.parseJSON(json); 
            SchoolSportStudent.notify(response);
            if(typeof response.data !== 'undefined') {
              if(typeof response.url !== 'undefined') {
                  window.top.location = response.url;
                  SchoolSportStudent.loading('hide');
              }
            }
        }).fail(function(err){
          console.log(err);
        });
      },
      trialStudent: function(SchoolSportStudentTrial) {
        $.ajax({
          url: ajax_url,
          data: SchoolSportStudentTrial.serialize()+"&action=school_sport_trial_student_ajax",
          method: "POST",
          beforeSend : function(){
            SchoolSportStudentTrial.loading('show');
          }      
        }).done(function( json ) {
            var response = $.parseJSON(json); 
            SchoolSportStudentTrial.notify(response);
            SchoolSportStudentTrial.loading('hide');
            if(typeof response.data !== 'undefined') {
              SchoolSportStep.step(2 ,function() {
              });
            } 
        }).fail(function(err){
           console.log(err);
        });
      },
      trialInformationStudent: function(SchoolSportStudentTrial) {
        $.ajax({
          url: ajax_url,
          data: SchoolSportStudentTrial.serialize()+"&action=school_sport_trial_student_information_ajax",
          method: "POST",
          beforeSend : function(){
            SchoolSportStudentTrial.loading('show');
          }      
        }).done(function( json ) {
            var response = $.parseJSON(json); 
            SchoolSportStudentTrial.notify(response);
            if(typeof response.data !== 'undefined') {
                SchoolSportStep.step(3 ,function() {
                  SchoolSportStudentTrial.loading('hide');
                });
            }
        }).fail(function(err){
          console.log(err);
        });
      
      },
      trialPaymentStudent: function(SchoolSportStudentPayment) {
         var data = SchoolSportStudentPayment.serializeObject();
         data.action = 'school_sport_trial_student_payment_eway_ajax';
        if(typeof data.card_number != 'undefined' ) {
            data.card_number = eCrypt.encryptValue(data.card_number);
         }
         if(typeof data.card_cvv != 'undefined' ) { 
            data.card_cvv = eCrypt.encryptValue(data.card_cvv);
         }
         $.ajax({
          url: ajax_url,
          data: data,
          method: "POST",
          beforeSend : function(){
            SchoolSportStudentPayment.loading('show');
          }      
        }).done(function( json ) {
            var response = $.parseJSON(json);
            if(response.status == 'error') {
              SchoolSportStudentPayment.notify(response);
              SchoolSportStudentPayment.loading('hide');
            } else {
              SchoolSportStep.step(4 ,function() {
                SchoolSportStudentPayment.loading('hide');
              });
            }
        }).fail(function(err){
          console.log(err);
        });
      },
     studentPaymentFee: function(SchoolSportStudentPayment) {
         var data = SchoolSportStudentPayment.serializeObject();
         data.action = 'school_sport_student_payment_fee_eway_ajax';
         if(typeof data.card_number != 'undefined' ) {
            data.card_number = eCrypt.encryptValue(data.card_number);
         }
         if(typeof data.card_cvv != 'undefined' ) { 
            data.card_cvv = eCrypt.encryptValue(data.card_cvv);
         }
         $.ajax({
          url: ajax_url,
          data: data,
          method: "POST",
          beforeSend : function(){
            SchoolSportStudentPayment.loading('show');
          }      
        }).done(function( json ) {
            var response = $.parseJSON(json);
            SchoolSportStudentPayment.loading('hide'); 
            SchoolSportStudentPayment.notify(response);  
            SchoolSportStudentPayment[0].reset();
            if(typeof response.url !== 'undefined') {
                setTimeout(function() {
                    window.top.location = response.url;                           
                }, 5000);
            }

        }).fail(function(err){
          console.log(err);
        });
     },
     selectTrialStudent: function() {
        var apiUrl =  ajax_url +"?action=school_sport_select_trial_student";
        var cascadLoading = new Select2Cascade(SchoolSportSelectTrial, SchoolSportSelectStudent, apiUrl);
        cascadLoading.then( function(parent, child, items) {
            // Dump response data
            console.log(items);
        });
     }
     
  };


  parent.init();
   /*   
   */

});