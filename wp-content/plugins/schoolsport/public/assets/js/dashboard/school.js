jQuery(function($) {
    var SchoolSportTablePendingStudent = $("table#students-pending");
    var SchoolSportTableApprovedStudent = $("table#students-approved");
    var SchoolSportTables = $('table#students-pending, table#students-approved');
    
    var school =  {
        init: function(){
            this.trigger();
        },
		trigger: function () {
            var self = this;
            SchoolSportTablePendingStudent.on("click", 
                "[rel='school-approved-student']", function(e) {
                e.preventDefault();
                self.studentApprovedTrial($(this));
            });
            SchoolSportTablePendingStudent.on("click",
                "[rel='school-reject-student']", function(e) {
                e.preventDefault();
                self.studentRejectTrial($(this));
            });
            
            self.tableStudentsPendingTrial();
            self.tableStudentsApprovedTrial();
        },
        tableStudentsPendingTrial: function() {
            SchoolSportTablePendingStudent.DataTable( {
                "ajax": {
                    url: ajax_url,
                    data: {
                        "action": "school_sport_get_students_trial_ajax",
                        "status": "pending"
                    }
                },
                "columns": [
                    { "data": "date" },
                    { "data": "student" },
                    { "data": "trial" },
                    { 
                      "data": null,
                      "width": "80px",
                      "className": "itm_dash mr-0",
                      "mRender": function (data) { 
                         return '<a href="#" rel="school-approved-student" data-student="'+data.student_id+'" '+
                             '" data-trial="'+data.trial_id+'" class="btn btn-success btn-small">Approve</a>'
                       }
                    },
                    { 
                      "data": null,
                      "width": "80px !important",  
                      "className": "itm_dash",
                      "mRender": function (data) { 
                         return '<a href="#" rel="school-reject-student" data-student="'+data.student_id+'" '+
                             '" data-trial="'+data.trial_id+'" class="btn btn-danger btn-small">Not Approved</a>'
                       }
                    },
                    {
                        "data": "status"
                    }
                ],
                "bSort": false,
                "lengthChange": false,
                "bFilter": false,
                "pageLength": 5,
                "responsive": true
            });
        },
        tableStudentsApprovedTrial: function() {
            SchoolSportTableApprovedStudent.DataTable( {
                "ajax": {
                    url: ajax_url,
                    data: {
                        "action": "school_sport_get_students_trial_ajax",
                        "status": "approved"
                    }
                },
                "columns": [
                    { "data": "date" },
                    { "data": "student" },
                    { "data": "trial" },
                    { 
                        "data": null,   
                        "mRender": function (data) {  return '' } 
                    }
                ],
                "bSort": false,
                "lengthChange": false,
                "bFilter": false,
                "pageLength": 5,
                "responsive": true
            });
        },
        studentApprovedTrial: function(SchoolSportStudent) {
            var self = this;
            var trial = SchoolSportStudent.data('trial');
            var student = SchoolSportStudent.data('student');
            $.ajax({
                url: ajax_url,
                data: "action=school_sport_trial_status_student_ajax&trial="+trial+"&student="+student+"&status=approved",
                method: "POST",
                beforeSend : function(){
                    SchoolSportTables.loading('show');
                }
            }).done(function( json ) {
                var response = jQuery.parseJSON(json); 
                if(response.status != 'error') {
                    SchoolSportTablePendingStudent.DataTable().ajax.reload();
                    SchoolSportTableApprovedStudent.DataTable().ajax.reload();
                }
                SchoolSportTables.loading('hide');
            }).fail(function(err){
				console.log(err);
			});
        },
        studentRejectTrial: function(SchoolSportStudent) {
            var trial = SchoolSportStudent.data('trial');
            var student = SchoolSportStudent.data('student');
            $.ajax({
                url: ajax_url,
                data: "action=school_sport_trial_status_student_ajax&trial="+trial+"&student="+student+"&status=not_approved",
                method: "POST",
                beforeSend : function(){
                   SchoolSportTables.loading('show');
                }
            }).done(function( json ) {
                var response = jQuery.parseJSON(json); 
                if(response.status != 'error') {
                    SchoolSportTablePendingStudent.DataTable().ajax.reload();
                    SchoolSportTableApprovedStudent.DataTable().ajax.reload();
                }
                SchoolSportTables.loading('hide');
            }).fail(function(err){
				console.log(err);
			});
        }
    };
    school.init();
});