jQuery(document).ready(function($) {
    var SchoolSportFormRegister =  $("form[rel='user-register']"); 
    var SchoolSportFormLogin =  $("form[rel='user-login']"); 
    var SchoolSportFormRecovery = $("form[rel='user-recovery']");
    var SchoolSportFormReset = $("form[rel='user-reset']")

    var user =  {
        init: function(){
            this.trigger();
        },
        trigger: function () {
            var self = this;
            
            SchoolSportFormRegister.schoolFormValidation(function($form) {
                self.register($form);
            });

            SchoolSportFormLogin.schoolFormValidation(function($form) {
                self.login($form);
            });
            
            SchoolSportFormRecovery.schoolFormValidation(function($form){
                self.recovery($form);
            })

            SchoolSportFormReset.schoolFormValidation(function($form){
                self.reset($form);
            })
        },
        register: function(SchoolSportFormRegister) {
            $.ajax({
                url: ajax_url,
                data: SchoolSportFormRegister.serialize()+"&action=school_sport_register_ajax",
                method: "POST",
                beforeSend : function(){
                    SchoolSportFormRegister.buttonDisabled('disabled');
                    //   IactFormRegister.loading(true);
                }
            }).done(function( json ) {
                var response = jQuery.parseJSON(json); 
                SchoolSportFormRegister.notify(response);
                SchoolSportFormRegister.buttonDisabled('enabled');
                // IactFormRegister.loading(false);
                if( typeof response.status !== 'undefined'){
                    if(response.status == 'success'){
                        SchoolSportFormRegister[0].reset()
                        if(typeof response.url !== 'undefined') {
                           window.top.location = response.url;
                        }
                    }
                }
                grecaptcha.reset();
            });
        },
        login: function(SchoolSportFormLogin) {
            $.ajax({
                url: ajax_url,
                data: SchoolSportFormLogin.serialize()+"&action=school_sport_login_ajax",
                method: "POST",
                beforeSend : function(){
                    SchoolSportFormLogin.buttonDisabled('disabled');
                //   IactFormRegister.loading(true);
                }
            }).done(function( json ) {
                var response = jQuery.parseJSON(json); 
                SchoolSportFormLogin.notify(response);
                SchoolSportFormLogin.buttonDisabled('enabled');
                // IactFormRegister.loading(false);
                if( typeof response.status !== 'undefined'){
                    if(response.status == 'success'){
                        SchoolSportFormLogin[0].reset()
                        if(typeof response.url !== 'undefined') {
                            window.top.location = response.url;
                        }
                    }
                }
            });
        },
        recovery: function(SchoolSportFormRecovery) {
            $.ajax({
                url: ajax_url,
                data: SchoolSportFormRecovery.serialize()+"&action=school_sport_recovery_ajax",
                method: "POST",
                beforeSend : function(){
                    SchoolSportFormRecovery.buttonDisabled('disabled');
                //   IactFormRegister.loading(true);
                }
            }).done(function( json ) {
                var response = jQuery.parseJSON(json); 
                SchoolSportFormRecovery.notify(response);
                SchoolSportFormRecovery.buttonDisabled('enabled');
                // IactFormRegister.loading(false);
                if( typeof response.status !== 'undefined'){
                    if(response.status == 'success'){
                        SchoolSportFormRecovery[0].reset();
                    }
                }
            });
        },
        reset: function(SchoolSportFormReset) {
            $.ajax({
                url: ajax_url,
                data: SchoolSportFormReset.serialize()+"&action=school_sport_reset_ajax",
                method: "POST",
                beforeSend : function(){
                    SchoolSportFormReset.buttonDisabled('disabled');
                //   IactFormRegister.loading(true);
                }
            }).done(function( json ) {
                var response = jQuery.parseJSON(json); 
                SchoolSportFormReset.notify(response);
                SchoolSportFormReset.buttonDisabled('enabled');
                // IactFormRegister.loading(false);
                if( typeof response.status !== 'undefined'){
                    if(response.status == 'success'){
                        SchoolSportFormReset[0].reset();
                        if(response.url !== 'undefined'){
                            setTimeout(function() {
                                window.top.location = response.url;
                            },500);
                        }
                    }
                }
            });
        }
        
    }
    user.init(); 
})