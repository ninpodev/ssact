<?php

Class SchoolSport_RegisterLogin  extends SchoolSport_Util{
    
	function __construct(){
        add_action( 'wp', array($this, 'school_sport_request_trigger'));

		add_action( 'wp_ajax_school_sport_login_ajax', array($this,'school_sport_login_ajax') );
        add_action( 'wp_ajax_nopriv_school_sport_login_ajax', array( $this,'school_sport_login_ajax') );
        
        add_action( 'wp_ajax_school_sport_register_ajax', array($this,'school_sport_register_ajax') );
		add_action( 'wp_ajax_nopriv_school_sport_register_ajax', array( $this,'school_sport_register_ajax') );

		add_action( 'wp_ajax_school_sport_recovery_ajax',  array($this,'school_sport_recovery_ajax') );
		add_action( 'wp_ajax_nopriv_school_sport_recovery_ajax', array( $this,'school_sport_recovery_ajax') );
		
		add_action( 'wp_ajax_school_sport_reset_ajax',  array($this,'school_sport_reset_ajax') );
		add_action( 'wp_ajax_nopriv_school_sport_reset_ajax', array( $this,'school_sport_reset_ajax') );
		

		add_filter('authenticate',array($this, 'school_restrict_login') , 30,3);

		add_filter( 'login_redirect', array($this, 'school_sport_login_redirect'), 10, 3 );
    }
    

    public function school_sport_request_trigger(){
		if( isset($_REQUEST['action']) ){
			if($_REQUEST['action'] == 'school_sport_active'){
				if(isset($_REQUEST['token'])  && isset($_REQUEST['user']) ){
					$token = $this->sanitize_input($_REQUEST['token']);
					$user = $this->sanitize_input($_REQUEST['user']);
					$this->school_sport_active_user($user, $token);
				} 
			} else if($_REQUEST['action'] == 'school_sport_reset') {
				if(isset($_REQUEST['token'])  && isset($_REQUEST['user']) ){
					$token = $this->sanitize_input($_REQUEST['token']);
					$user = $this->sanitize_input($_REQUEST['user']);
					$this->school_sport_is_recovery($user, $token);
				} 
			} else if($_REQUEST['action'] == 'school_sport_logout') {
				 $this->logut();
			}
		}

	}
	
	public function school_sport_login_ajax(){
		$creds = [];
		$request = $this->sanitize_input($_REQUEST);
		if(isset($request['pwd']) && isset($request['log']) ){
			if(!empty($request['pwd']) && !empty($request['log'])) {
				$creds['user_login'] =  $request['log'];
				$creds['user_password'] = $request['pwd'];
				$creds['remember'] = true;
				$user = wp_signon( $creds, false );
				$pending = get_user_meta($user->ID, 'pending');
				if ($pending == true or $pending == 'true') {
					die( json_encode ( 
					array('class'=>'alert alert-warning ',
						'message'=> 'Your user is not active yet.',
						'status' => 'error' 
					) ) );
				} else if ( is_wp_error($user) ){
					die( json_encode ( 
					array('class'=>'alert alert-warning ',
						'message'=> $user->get_error_message(),
						'status' => 'error' 
					) ) );
				} else {
					if ( isset( $user->roles ) && is_array( $user->roles ) ) {
						wp_set_current_user($user->ID);
						if ( in_array( 'official', $user->roles ) || 
							in_array( 'school', $user->roles ) || 
							in_array( 'administrator', $user->roles ) ) {
							die( json_encode ( 
							array( 
								'url'=> get_bloginfo('url').'/dashboard',
								'status' => 'success',
								'class'=>'alert alert-success',
							) ) );
						}
						die( json_encode ( 
							array( 
								'url'=> get_bloginfo('url').'/dashboard',
								'status' => 'success',
								'class'=>'alert alert-success',
							) ) );
					
					} 
				}
			}
		} 
		die( json_encode ( 
			array(
				'class'=>'alert alert-warning',
				'message'=> 'Check for required fields before continuing.',
				'status' => 'error' 
		) ) );
	
	}
	
	public function school_sport_login_redirect($redirect_to, $request, $user ){
		if ( isset( $user->roles ) && is_array( $user->roles ) ) {
			if ( in_array( 'school', $user->roles ) || 
				 in_array( 'official', $user->roles ) || 
				 in_array( 'administrator', $user->roles ) ) {
					$redirect_to = get_bloginfo('url').'/dashboard';
			} 
		}
		return $redirect_to;	
    }
    

    function school_sport_register_ajax(){
		$request = $this->sanitize_input($_REQUEST);
		if( isset($request['g-recaptcha-response']) && 
			! school_sport_recaptcha($request['g-recaptcha-response'])){
			die( json_encode ( 
				array('class'=>'alert alert-warning text-center',
					'message'=>  'Please click the reCAPTCHA checkbox below. ',
					'status' => 'error' 
			) ) );
		} else if(empty($request['email']) || empty($request['password']) 
			|| empty($request['first_name']) || empty($request['last_name'])   ){
            die( json_encode ( 
                array('class'=>'alert alert-warning text-center',
                'message'=> 'Check for required fields.',
                'status' => 'error' 
            ) ) );
		} else {
			if($request['role'] === 'official') {
				$request['role'] = 'official';
			} else {
				$request['role'] = 'parent';
			}
			
			$user = [
				'user_email' => esc_attr($request['email']),
				'user_login' => esc_attr($request['email']),
				'user_pass' => esc_attr($request['password']),
				'first_name' => esc_attr($request['first_name']),
				'last_name' => esc_attr($request['last_name']),
				'role' => esc_attr($request['role'])
			];
			
			$register_user = wp_insert_user($user);
			if (!is_wp_error($register_user)) {
				
				$token = uniqid();
				update_user_meta($register_user, 'token', $token );
				update_user_meta($register_user, 'pending', true);
				
				update_user_meta($register_user , 'phone', esc_attr($request['phone']) );
				update_user_meta($register_user , 'mobile', esc_attr($request['mobile']) );
				update_user_meta($register_user , 'address', esc_attr($request['address']) );
				update_user_meta($register_user , 'suburb', esc_attr($request['suburb']) );
				update_user_meta($register_user , 'post_code', esc_attr($request['post_code']) );
				update_user_meta($register_user , 'state', esc_attr($request['state']) );


				$this->wp_new_user_notification_school_sport($user, $token );
				die( json_encode ( 
				array('class'=>'alert alert-success text-center',
					'message'=> 'Welcome.',
					'url' => get_bloginfo('url').'/email-sent',
					'status' => 'success'
				) ) );
			}	else {
				die( json_encode ( 
				array('class'=>'alert alert-warning text-center',
					'message'=>  'Sorry, that email is already in use. ',
					'status' => 'error' 
				) ) );
			}
		}
		
		die( json_encode ( 
			array('class'=>'alert alert-warning text-center',
				'message'=>  'Sorry, registration not found. ',
				'status' => 'error' 
		) ) );
    }
    
	public function school_sport_recovery_ajax() {
		$request = $this->sanitize_input($_REQUEST);
		
		$user = get_user_by( 'email', $request['email']);
		
		if(isset($user->data)){
			$user = (array) $user->data;
			$token = uniqid();
			
			update_user_meta($user['ID'], 'reset', $token);

			$this->wp_new_user_recovery_school_sport($user, $token);
			die( json_encode ( 
				array('class'=>'alert alert-warning text-center',
					'message'=>  'An email has been sent with your recovery link.',
					'status' => 'success' 
			) ) );
		}
		die( json_encode ( 
			array('class'=>'alert alert-warning text-center',
				'message'=>  'Sorry, Username does not exist ',
				'status' => 'error' 
		) ) );
	}
	
    public function wp_new_user_notification_school_sport($user,  $token ){
		$headers = array('Content-Type: text/html; charset=UTF-8');
		$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
		$first_name = $user['first_name'];
		$last_name = $user['last_name'];

		if($user['role'] === 'official' || in_array('official', $user['role'])) {
			$message = school_sport_get_template('email/welcome-team');
		} else {
			$message = school_sport_get_template('email/welcome-parent');
		}
		
		$url_complete  = school_sport_get_url_confirmation();
		$url_complete = "{$url_complete}?token={$token}&action=school_sport_active&user={$user['user_login']}";
		$message = str_replace('#first_name#', $first_name, $message);
		$message = str_replace('#url_complete#', $url_complete, $message);
		
		wp_mail($user['user_email'], sprintf(__('[%s] Welcome'), $blogname), $message,$headers);
		
		//Send message to SSACT Admin on request to register new Official
		if($user['role'] === 'official' || in_array('official', $user['role'])) {
			$message = school_sport_get_template('email/approve-new-official');
			$message = str_replace('#first_name#', $first_name, $message);
			$message = str_replace('#last_name#', $last_name, $message);
			wp_mail("info@schoolsportact.org.au", sprintf(__('[%s] A new official has created an account'), $blogname), $message,$headers);
		}

	}

	public function wp_new_user_recovery_school_sport($user, $token){
		$headers = array('Content-Type: text/html; charset=UTF-8');
		$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
		$first_name = get_user_meta( $user['ID'], 'first_name', true );
		$url_reset = school_sport_get_url_reset_password();
		$url_reset = "{$url_reset}?token={$token}&action=school_sport_reset&user={$user['user_login']}";
		
		$message = school_sport_get_template('email/recovery');
		$message = str_replace('#first_name#', $first_name , $message);
		$message = str_replace('#url_reset#', $url_reset, $message);
		
		wp_mail($user['user_email'], sprintf(__('[%s] Password Reset'), $blogname), $message,$headers);
	}

	public function school_sport_active_user($user_login = '', $request_token = ''){
		$user = get_user_by( 'login', $user_login);
		if(isset($user->ID)){   
			$token = get_user_meta($user->ID, 'token', true);
            if($token != $request_token) { 
				wp_redirect(home_url());
				exit;
			}
			delete_user_meta($user->ID, 'pending');
			delete_user_meta($user->ID, 'token');
		}
		
	}
	
	public function school_sport_is_recovery($user_login = '', $request_token = ''){
		$user = get_user_by( 'login', $user_login);
		if(isset($user->ID)){   
			$token = get_user_meta($user->ID, 'reset', true);
            if($token !== $request_token) {
				wp_redirect(home_url());
				exit;
			}
			
		}
		
	}
	
	function school_restrict_login($user, $username, $password ) {
		if(isset($user->data) && isset($user->data->ID)) {
			$pending = get_user_meta($user->ID, 'pending', true);
			if ($pending == true or $pending == 'true') {
				remove_filter('authenticate','wp_authenticate_username_password',30,3);
				return null;
			}
		}
		return $user;
	}
	
	function school_sport_reset_ajax() {
		$password = esc_html($_REQUEST['password']);
		$user_login = esc_html($_REQUEST['user']);
		$request_token = esc_html($_REQUEST['token']);
		$user = get_user_by( 'login', $user_login);
		if(isset($user->ID)){   
			$token = get_user_meta($user->ID, 'reset', true);
			if($token == $request_token) {
				delete_user_meta($user->ID, 'reset');
				wp_set_password( $password, $user->ID );
				$creds['user_login'] =  $user->user_login;
				$creds['user_password'] = $password;
				$creds['remember'] = true;
				$user = wp_signon( $creds, false );
				die( json_encode ( 
					array('class'=>'alert alert-success text-center',
						'message'=>  'Your password has changed, redirecting to your Dashboard. ',
						'status' => 'success',
						'url' => home_url('dashboard')
				) ) );
			}
		}
		die( json_encode ( 
			array('class'=>'alert alert-warning text-center',
				'message'=>  'Problem changing your password.',
				'status' => 'error' 
		) ) );
	}
}

$schoolsport_registerlogin = new SchoolSport_RegisterLogin;