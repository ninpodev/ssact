<?php

class SchoolSport_Dashboard extends SchoolSport_Util {
	
	public $administrator;

	public $parent;
    
    public $official;
    
    public $school;

	public function __construct() {
		
		add_action('init', array($this, 'school_sport_auto_load'));
		add_action('init', array($this, 'school_sport_session'));
		
		add_action('wp', array($this, 'school_sport_request_trigger'));
		add_action('wp', array($this, 'wp_restrict_dashboard')); 
		
		add_action('wp_ajax_school_sport_update_user_ajax', array($this,'school_sport_update_user_ajax'));
	    
    	add_action('wp_enqueue_scripts', array($this,'schoolsport_enqueue_dashboard_script') );
		add_action('wp_enqueue_scripts', array($this, 'wp_remove_datepicker'));
		
		$shortcodes = array(
			'school_sport_user_details' =>	array($this, 'school_sport_user_details'),
    	);
		
		foreach ( $shortcodes as $shortcode => $function ) {
			add_shortcode( apply_filters( "{$shortcode}_shortcode_tag", $shortcode ), $function );
		}
	}
	
	public function school_sport_auto_load() {
		$this->administrator = new SchoolSport_SSACTAdmin();
		$this->official = new SchoolSport_Official();
		$this->parent = new SchoolSport_Parent();
        $this->school = new SchoolSport_School();
	}

	public function school_sport_session() {
		if( ! session_id() ) {
			session_start();
		}
	}
	
	public function school_sport_request_trigger() {
		if( isset($_REQUEST['logout']) ){
			$this->school_sport_logout_dashboard();
		}
	}

	public function school_sport_logout_dashboard() {
		wp_logout();
	}

	public function wp_remove_datepicker() {
		wp_deregister_script( 'jquery-ui-datepicker' );
		wp_deregister_script( 'jquery-ui-datepicker-local' );

	}
	
	public function wp_restrict_dashboard() {
		if(school_sport_is_dahsboard()) {
			if(!is_user_logged_in()) {
				wp_redirect(get_home_url());
				exit;
			}
		}
	}
	
	public function school_sport_user_details() {
		return school_sport_shortcode_wrapper('dashboard/component/user-edit');
	}	
	
	public function school_sport_update_user_ajax() {
		$request = $this->sanitize_input($_REQUEST);
		$current_user = get_current_user_id();
		$data = [
			'ID' => $current_user, 
			'first_name' => esc_attr($request['first_name']),
			'last_name' => esc_attr($request['last_name']) 
		];
		$user = wp_update_user($data);
		
		if(!is_wp_error($user)) {
			update_user_meta($current_user , 'phone', esc_attr($request['phone']) );
			update_user_meta($current_user , 'mobile', esc_attr($request['mobile']) );
			update_user_meta($current_user , 'address', esc_attr($request['address']) );
			update_user_meta($current_user , 'suburb', esc_attr($request['suburb']) );
			update_user_meta($current_user , 'post_code', esc_attr($request['post_code']));
			update_user_meta($current_user , 'city', esc_attr($request['city']) );
		
			die( json_encode ( array(
				'url'=> school_sport_get_url_dahsboard(),
				'message'=>  'User updated, redirecting to dashboard. ',
				'status' => 'success',
				'class'=>'alert alert-success',
			) ) );
		}
		die( json_encode ( 
			array('class'=>'alert alert-warning text-center',
				'message'=>  'Sorry, register not found. ',
				'status' => 'error' 
		) ) );
	}
	

	public function schoolsport_enqueue_dashboard_script() {
		wp_enqueue_style( 'schoolsport-dashboard-datepicker-style', SCHOOLSPORT_URL . '/public/assets/css/datepicker.min.css', array(), '1.1', 'all');
  		wp_enqueue_style( 'schoolsport-dashboard-timepicker-style', SCHOOLSPORT_URL . '/public/assets/css/timepicker.min.css', array(), '1.1', 'all');      
		wp_enqueue_script( 'schoolsport-dashboard-datepicker-script', SCHOOLSPORT_URL. '/public/assets/js/plugins/datepicker.min.js', array( 'jquery' ),'1.0', true);
		wp_enqueue_script( 'schoolsport-dashboard-datatable-script', SCHOOLSPORT_URL. '/public/assets/js/plugins/jquery.dataTables.min.js', array( 'jquery' ),'1.0', true);
		wp_enqueue_script( 'schoolsport-dashboard-script', SCHOOLSPORT_URL. '/public/assets/js/dashboard/index.js', array( 'jquery' ),'1.0', true);
        wp_enqueue_script( 'schoolsport-dashboard-timepicker-script', SCHOOLSPORT_URL. '/public/assets/js/plugins/jquery.timepicker.min.js', array( 'jquery' ),'1.0', true);
	}
}

$schoolSport_Dashboard = new SchoolSport_Dashboard();