<?php

class SchoolSport_SSACTAdmin extends SchoolSport_Util {
    
    public $role = 'administrator';

    private $user_id = null;

    public function __construct() {
        $this->user_id = get_current_user_id();
        $this->wp_load();
        if($this->is_role($this->role)) {
            $this->load_export();
            $this->load_scripts();
            $this->load_ajax_action();
        }
    }

    public function wp_load() {
        add_action('wp', array($this, 'load_short_code'));
        add_action('wp', array($this, 'wp_restriction_query_var'));
        
        add_filter('the_title', array($this, 'load_filter_title_team'));

    }

    public function load_short_code() {
        //$shortcodes = array(
		//	'school_sport_create_trial' =>	array($this, 'shortcode_create_trial'),
        //    'school_sport_trial_select_team' =>	array($this, 'shortcode_trial_select_team'),
        //);
        
        //foreach ( $shortcodes as $shortcode => $function ) {
        //    $this->wp_shortcode_restricted_redirect($this->role, $shortcode);
        //    $this->add_shortcode($shortcode, $function);
        //}
    }
    
    function load_filter_title_team($title) {
        if($trial_slug  = get_query_var('trial_slug')) {
            $page = get_page_by_path($trial_slug, OBJECT, 'trials');
            if(isset($page->post_title)) {
                $title = str_replace('[team_name]', $page->post_title, $title);
            }
        }
        return $title;
    }

    public function add_shortcode($shortcode, $function) {
        add_shortcode( apply_filters( "{$shortcode}_shortcode_tag", $shortcode ), $function );
    }

    public function load_export() {
        add_action('wp', array($this, 'export_student_trial'));    
        add_action('wp', array($this, 'export_officials'));
        add_action('wp', array($this, 'export_students'));
    }
    
    public function load_scripts() {
        add_action('wp_enqueue_scripts', array($this,'scripts_js') );
    }


    public function scripts_js() {
        wp_enqueue_script( 'schoolsport-dashboard-official-script', SCHOOLSPORT_URL. '/public/assets/js/dashboard/official.js', array( 'jquery' ),'1.0', true);
    }
	
	public function shortcode_create_trial() {
		return school_sport_shortcode_wrapper('dashboard/official/create-trial');
    }
    
    public function shortcode_trial_select_team() {
        return school_sport_shortcode_wrapper('dashboard/official/select-team-trial');
    }
    
    public function create_trial_ajax() {
        if($trial = $this->create_trial()) {
            die( json_encode ( 
                array('class'=>'alert alert-success text-center',
                    'message'=>  'Student information updated successfully.',
                    'status' => 'success',
                    'data' => [
                        'trial' => $trial 
                    ]
            ) ) );
        }
        die( json_encode ( 
            array('class'=>'alert alert-warning text-center',
                'message'=>  'Sorry do you don\'t have permision for this action. ',
                'status' => 'error' 
        ) ) );
    }

    public function create_trial() {
        $request = $this->sanitize_input($_REQUEST);
        $trial = $request['trial'];
        $author = get_post_field( 'post_author', $trial);
        if($trial && $this->user_id == $author) {
            return $this->update_trial($trial);
        }
        return $this->insert_trial();
    }

    public function insert_trial() {
        $request = $this->sanitize_input($_REQUEST);
        $data = array(
            'post_title'  => wp_strip_all_tags( $request['name'] ),
            'post_status' => 'draft',
            'post_type' => 'trials',
            'post_author' => $this->user_id,
        );
        $trial = wp_insert_post($data);
        if (!is_wp_error($trial)) {
			return $this->update_trial_field($trial);
		}
        return false;
    }

    public function update_trial($trial) {
        $request = $this->sanitize_input($_REQUEST);
        $id = $trial;
        $data = array(
            'ID' => $id,
            'post_title'  => wp_strip_all_tags( $request['name'] ),
            'post_status' => 'draft',
            'post_type' => 'trials',
            'post_author' => $this->user_id,
        );
        $trial = wp_update_post($data);
        if (!is_wp_error($trial)) {
			return $this->update_trial_field($trial);
		}
        return false;
    }


    
    

    public function update_trial_field($trial) {
        $request = $this->sanitize_input($_REQUEST);
        update_field( 'year', date('Y'), $trial );
        update_field( 'sport', $request['sport'], $trial );
        update_field( 'age', $request['age'], $trial );
        update_field( 'gender', $request['gender'], $trial );
        update_field( 'date_birth_restriction_from', $request['date_birth_from'], $trial );
        update_field( 'date_birth_restriction_to', $request['date_birth_to'], $trial );
        update_field( 'date_of_trial', $request['date_trial'], $trial );
        update_field( 'eligibility', $request['eligibility'], $trial );
        update_field( 'championship_date', $request['champ_date'], $trial );
        update_field( 'championship_venue', $request['champ_venue'], $trial);
        update_field( 'time_trial_from', $request['time_trial_from'], $trial);
        update_field( 'time_trial_to', $request['time_trial_to'], $trial);
        update_field( 'venue', $request['venue'], $trial );
        return $trial;
    }

    public function create_product_set_ajax() {
        $request = $this->sanitize_input($_REQUEST);
        $trial = $request['trial'];
        $product_set = $request['product_set'];
        $author = get_post_field( 'post_author', $trial );
    
        if($author == $this->user_id) {
            update_field( 'product_set', $product_set, $trial );
            die( json_encode ( 
                array('class'=>'alert alert-success text-center',
                    'message'=>  'The trial information was update successfully.',
                    'status' => 'success',
                    'data' => [
                        'trial' => $trial 
                    ]
            ) ) );
        }
        die( json_encode ( 
			array('class'=>'alert alert-warning text-center',
				'message'=>  'Sorry do you don\'t have permision for this action. ',
				'status' => 'error' 
		) ) );
    }

  /*   public function submit_trial_ajax() {
        $request = $this->sanitize_input($_REQUEST);
        $trial = $request['trial'];
        $author = get_post_field( 'post_author', $trial );
        if($author == $this->user_id) {
            if(wp_update_post(["ID" => $trial, 'post_status' => 'pending'])) {
                $this->send_mail_pending_trial($trial, $author);
                die( json_encode ( 
                    array( 
                    'url'=> school_sport_get_url_dahsboard(),
                    'message'=>  'Your trial has been submitted for review.',
                    'status' => 'success',
                    'class'=>'alert alert-success',
                ) ) );
            }
        }
		die( json_encode ( 
			array('class'=>'alert alert-warning text-center',
				'message'=>  'Sorry do you don\'t have permision for this action. ',
				'status' => 'error' 
		) ) );
    }
    
    public function send_mail_pending_trial($trial, $author = 0) {
        if($user = get_userdata($author)) {
            $headers = array('Content-Type: text/html; charset=UTF-8');
            $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
            $trial_name = get_the_title($trial);
            $first_name = $user->first_name;       
            $message = school_sport_get_template('email/trial-pending');
            $message = str_replace('#first_name#', $first_name, $message);
            $message = str_replace('#trial_name#', $trial_name , $message);
            wp_mail($user->user_email, sprintf(__('[%s] Trial Pending'), $blogname), $message,$headers);
        }
    }

    public function send_mail_trial_confirmed($trial, $author = 0) {

        if($user = get_user_by('id', $author)) {
            $headers = array('Content-Type: text/html; charset=UTF-8');
            $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
            $trial_name = get_the_title($trial);
            $first_name = $user->data->first_name;       
            $message = school_sport_get_template('email/trial-confirmed');
            $message = str_replace('#first_name#', '', $message);
            $message = str_replace('#trial_name#', $trial_name , $message);
            $email = $user->data->user_email;       


            wp_mail($email, 'Trial Approved!', $message,$headers);
        
        } 
    }
 */
    
    public function approve_trial($trial_id, $trial_after, $trial_before) { 
        if($trial_before->post_type == 'trials' ) {
            if($trial_before->post_status == 'pending' && $trial_after->post_status == 'publish') {
                $this->send_mail_trial_confirmed($trial_id, $trial_before->post_author);
            }
        }
    } 
        
    public function export_student_trial() {
        $request = $this->sanitize_input($_REQUEST);
        
        if(isset($request['action']) && $request['action'] === 'export_student') {
            $trial = (int) $request['trial'];
            //$author = get_post_field( 'post_author', $trial );
            //if($this->user_id == $author) {
                require(SCHOOLSPORT_PATH.'/plugins/excel/SchoolSport_Excel.php');
                $exporter = new ExportDataExcel('browser', 'student-trial.xls');
                $exporter->initialize(); 
                $exporter->addRow([ "StudentId", "Name", "BirthDay", "Gender", "School", "Parent", "Medical Condition",
                                   "Allergies", "Date Tetanus", "Medicare Number", "Name Emergency", "Mobile Emergency", 
                                  "Student Email", "Student Mobile"]);
                while(have_rows('students', $trial)) {
                    the_row();
                    if($student = get_sub_field('student')) {
                        $id = $student->ID;
                        $name = $student->post_title;  
                        $student = get_fields($id);
                        $gender = $student['gender'];
                        $date_birth = $student['date_of_birth'];
                        $school = @$student['school']->post_title;
                        $medical_condition = $student['medical_condition'];
                        $allergies = $student['allergies'];
                        $date_last_tetanus = $student['date_last_tetanus'];
                        $medical_number = $student['medical_number'];
                        $name_of_preferred_emergency = $student['name_of_preferred_emergency'];
                        $parent =  @$student['parent']->data->display_name;
                        $relationship = $student['relationship'];
                        $mobile_emergency = $student['mobile_emergency'];
                        $student_email = $student['student_email'];
                        $student_mobile = $student['student_mobile'];
                     
                        $exporter->addRow([ $id, $name, $date_birth, $gender, $school, $parent, $medical_condition,
                                          $allergies, $date_last_tetanus, $medical_number, $name_of_preferred_emergency, 
                                          $mobile_emergency, $student_email, $student_mobile]);
                    }
                }
              
                $exporter->finalize(); 
                exit(); // all done
            }
        //}
    }

    public function export_students() {
        $request = $this->sanitize_input($_REQUEST);
        
        if(isset($request['action']) && $request['action'] === 'export_students') {
            //$trial = (int) $request['trial'];
            //$author = get_post_field( 'post_author', $trial );
            //if(true){//$this->user_id == $author) {
                require(SCHOOLSPORT_PATH.'/plugins/excel/SchoolSport_Excel.php');
                $exporter = new ExportDataExcel('browser', 'students.xls');
                $exporter->initialize(); 
                $exporter->addRow([ "StudentId", "Name", "BirthDay", "Gender", "School", "Parent First Name", "Parent Last Name", "Parent email", "Parent Phone", "Parent Mobile", "Medical Condition",
                "Allergies", "Date Tetanus", "Medicare Number", "Name Emergency", "Mobile Emergency", 
               "Student Email", "Student Mobile"]);
                
                $allstudents = get_posts( array(
                    'post_type' => 'students',
                    'numberposts'=> 50
                ) );
                foreach($allstudents as $student) {
                    
                    $parent = get_user_by('id', $student->parent);
                    
                    $exporter->addRow([ 
                     $student->ID,
                     $student->post_title,
                     $student->date_of_birth, 
                     $student->gender, 
                     $student->school, 
                     $parent->first_name,
                     $parent->last_name,
                     $parent->user_email, 
                     $parent->phone,
                     $parent->mobile,
                     $student->medical_condition, 
                     $student->allergies, 
                     $student->date_last_tetanus, 
                     $student->medical_number, 
                     $student->name_of_preferred_emergency, 
                     $student->mobile_emergency, 
                     $student->student_email,
                     $student->student_mobile
                    ]);
                }
              
                $exporter->finalize(); 
                exit(); // all done
            }
        //}
    }


    public function export_officials() {
        $request = $this->sanitize_input($_REQUEST);
        
        if(isset($request['action']) && $request['action'] === 'export_officials') {
            //$trial = (int) $request['trial'];
            //$author = get_post_field( 'post_author', $trial );
            //if(true){//$this->user_id == $author) {
                require(SCHOOLSPORT_PATH.'/plugins/excel/SchoolSport_Excel.php');
                $exporter = new ExportDataExcel('browser', 'officials.xls');
                $exporter->initialize(); 
                $exporter->addRow([ "First name", "Last name", "Email", "Phone Number", "Mobile", "Address", "Suburb", "Post Code", "State", "City", ""]);
                
                $allusers = get_users("role=official");
                foreach($allusers as $user) {
                    $exporter->addRow([ $user->first_name, $user->last_name, $user->user_email, $user->phone, $user->mobile, $user->suburb, $user->postcode, $user->state, $user->city  ]);
                }
                // while(have_rows('students', $trial)) {
                //     the_row();
                //     if($student = get_sub_field('student')) {
                //         $id = $student->ID;
                //         $name = $student->post_title;  
                //         $student = get_fields($id);
                //         $gender = $student['gender'];
                //         $date_birth = $student['date_of_birth'];
                //         $school = @$student['school']->post_title;
                //         $medical_condition = $student['medical_condition'];
                //         $allergies = $student['allergies'];
                //         $date_last_tetanus = $student['date_last_tetanus'];
                //         $medical_number = $student['medical_number'];
                //         $name_of_preferred_emergency = $student['name_of_preferred_emergency'];
                //         $parent =  @$student['parent']->data->display_name;
                //         $relationship = $student['relationship'];
                //         $mobile_emergency = $student['mobile_emergency'];
                //         $student_email = $student['student_email'];
                //         $student_mobile = $student['student_mobile'];
                     
                //         $exporter->addRow([ $id, $name, $date_birth, $gender, $school, $parent, $medical_condition,
                //                           $allergies, $date_last_tetanus, $medical_number, $name_of_preferred_emergency, 
                //                           $mobile_emergency, $student_email, $student_mobile]);
                //     }
                // }
              
                $exporter->finalize(); 
                exit(); // all done
            }
        //}
    }
    
    public function team_selection_trial_action() {
        $request = $this->sanitize_input($_REQUEST);
        $selection = $this->wp_get_session_arg('selection');
        $selection['id'] = uniqid();
        if($selection['selection_status'] = $request['selection_status']) {
            $selection['trial'] = @$request['trial'];
            $students_data = $this->get_students_on_status($selection['selection_status']);
            $selection['students'] = $students_data;
            $this->wp_set_session_arg($selection, 'selection');            
            die( json_encode ( 
                array('class'=>'alert alert-success text-center',
                    'message'=>  'The student selection team successfully.',
                    'status' => 'success',
                    'data' => [
                        'selection' => $selection
                     ]
            ) ) );
        }
       die( json_encode ( 
            array('class'=>'alert alert-warning text-center',
                'message'=>  'Sorry do you don\'t have permision for this action. ',
                'status' => 'error' 
        ) ) );
    }
    
    public function get_students_on_status($selection) {
        $students = array_keys($selection);
        $students  = new WP_Query(['post_type' => 'students', 'post__in' => $students ] );
        $students_data = [];
         while($students->have_posts()) {
            $students->the_post();
            $title = get_the_title();
            $id = get_the_ID();
            $status = $this->get_status_student_trial(@$selection[$id]);
            $students_data[] = [
                'name' => $title,
                'status' => $status
            ];
        }
        return $students_data;
    }
    
    public function get_status_student_trial($status = '') {
        if($status == 'selected') {
            return 'Selected';
        } else if($status == 'shadow') {
            return 'Shadow';
        } else {
           return 'Not Selected';    
        }
    }
    
    public function team_try_on_date_action() {
        $request = $this->sanitize_input($_REQUEST);
        $selection = $this->wp_get_session_arg('selection');
        $selection['venue_of_try_on'] = $request['venue_of_try_on'];
        $selection['date_of_try_on'] = $request['date_of_try_on'];
        if( $selection['id'] == $request['selection']) {
            $this->wp_set_session_arg($selection, 'selection');
            die( json_encode ( 
                array('class'=>'alert alert-success text-center',
                    'message'=>  'The information selection team successfully.',
                    'status' => 'success',
                    'data' => [
                        'selection' => $selection
                     ]
            ) ) );
       }
        die( json_encode ( 
            array('class'=>'alert alert-warning text-center',
                'message'=>  'Sorry do you don\'t have permision for this action. ',
                'status' => 'error' 
        ) ) );
    }
    
    public function team_selection_submit_action() {
        $request = $this->sanitize_input($_REQUEST);
        $selection = $this->wp_get_session_arg('selection');
        if( $selection['id'] == $request['selection']) {
            $trial =  (int) $selection['trial'];
            $author = get_post_field( 'post_author', $trial ); 
            if($author == $this->user_id) {
                $post = get_post($trial); 
                while(have_rows('students', $trial)) {
                    the_row();
                    $student = get_sub_field('student');
                    if(isset($student->ID) && isset($selection['selection_status'])) {
                        $student_id = $student->ID;
                        if(isset($selection['selection_status'][$student_id])) {
                            update_sub_field('selection_status', $selection['selection_status'][$student_id]);
                            if($parent = get_field('parent', $student_id)) {
                               $data[$selection['selection_status'][$student_id]][] = [
                                   'trial' => $post,
                                   'parent' => $parent,
                                   'student' => $student
                               ];
                            } 
                        }
                    }
                }
                
                $this->send_mail_parent_selection($data, $post);
                $this->wp_clear_session_arg('selection');
                $arg = [ 'ID' => $post->ID]; //, 'post_type' => 'teams' ;
                wp_update_post( $arg );
                die( json_encode ( 
                    array('class'=>'alert alert-success text-center',
                        'message'=>  'The information selection team successfully.',
                        'status' => 'success',
                        'url' => school_sport_get_url_dahsboard(),
                        'data' => [
                            'selection' => $selection
                         ]
                ) ) );
            }
        }
         die( json_encode ( 
            array('class'=>'alert alert-warning text-center',
                'message'=>  'Sorry do you don\'t have permision for this action. ',
                'status' => 'error' 
        ) ) );
    }
    
    public function send_mail_parent_selection($data = [], $trial = []) {
        $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
        $message = school_sport_get_template('email/team-selected-admin', $data);
        $gender = get_field('gender', @$trial->ID);
        $gender = get_field('year', @$trial->ID);
        $sport =  get_field('sport', @$trial->ID);
        $subject = sprintf(__('[%s] Trial results %s %s %s'), $blogname, $year, $gender, @$sport->post_title);
        $to = 'operations@schoolsportact.org.au';
        $headers = array('Content-Type: text/html; charset=UTF-8', 'From: School Sport ACT <trial@schoolsportact.org.au>');
        wp_mail($to, $subject, $message,$headers);
        return true;
    }
        
    
        
        
    public function send_mail_parent_selection_alte() {
        require(SCHOOLSPORT_PATH.'/plugins/sendgrid/lib/loader.php');
        $from = new \SendGrid\Mail\From("test@ninpodesign.com", "Example User");
        $tos = [
            new \SendGrid\Mail\To(
                "sns.gfuentes@gmail.com",
                "Example User1",
                [
                    '-name-' => 'Example User 1',
                    '-github-' => 'http://github.com/example_user1'
                ],
                "Subject 1 -name-"
            ),
            new \SendGrid\Mail\To(
                "fuentesguillermo@live.com",
                "Example User2",
                [
                    '-name-' => 'Example User 2',
                    '-github-' => 'http://github.com/example_user2'
                ],
                "Subject 2 -name-"
            ),
            new \SendGrid\Mail\To(
                "guillermo.fuentes@globant.com",
                "Example User3",
                [
                    '-name-' => 'Example User 3',
                    '-github-' => 'http://github.com/example_user3'
                ]
            )
        ];
        $subject = new \SendGrid\Mail\Subject("Hi -name-!"); // default subject 
        $globalSubstitutions = [
            '-time-' => "2018-05-03 23:10:29"
        ];
        $plainTextContent = new \SendGrid\Mail\PlainTextContent(
            "Hello -name-, your github is -github- sent at -time-"
        );
        $htmlContent = new \SendGrid\Mail\HtmlContent(
            "<strong>Hello -name-, your github is <a href=\"-github-\">here</a></strong> sent at -time-"
        );
        $email = new \SendGrid\Mail\Mail(
            $from,
            $tos,
            $subject, // or array of subjects, these take precendence
            $plainTextContent,
            $htmlContent,
            $globalSubstitutions
        );

        $sendgrid = new \SendGrid('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');
       /* try {
            $response = $sendgrid->send($email);
            print $response->statusCode() . "\n";
            print_r($response->headers());
            print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: '.  $e->getMessage(). "\n";
        }*/
    }
    
    public function load_ajax_action(){
        add_action('post_updated', array($this, 'approve_trial'),10,3 ); 
        add_action('wp_ajax_school_sport_create_product_set_ajax', array($this, 'create_product_set_ajax'));
        /* add_action('wp_ajax_school_sport_submit_trial_ajax', array($this, 'submit_trial_ajax'));  
        add_action('wp_ajax_school_sport_team_selection_trial_ajax', array($this, 'team_selection_trial_action')); */
        add_action('wp_ajax_school_sport_team_try_on_date_ajax', array($this, 'team_try_on_date_action'));
        add_action('wp_ajax_school_sport_team_selection_submit_ajax', array($this, 'team_selection_submit_action'));
    }
  
    
  
}