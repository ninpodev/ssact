<?php 

class SchoolSport_School  extends SchoolSport_Util {
   
    private $user_id = null;
    
    private $role = 'school';

    public function __construct() {
        $this->user_id = get_current_user_id();
        $this->wp_load();
        if($this->is_role($this->role)) {
            $this->load_scripts();
            $this->load_ajax_action();
        }
    }
    
    public function wp_load() {
        add_action('wp', array($this, 'load_short_code'));
        add_filter('posts_where', array($this, 'posts_where'));
    }
    
    public function load_short_code() {
         
    }

    function posts_where( $where ) {
        $where = str_replace("meta_key = 'students_#index#", "meta_key LIKE 'students_%", $where);
        return $where;
      }
    
    public function load_ajax_action() {
        add_action('wp_ajax_school_sport_get_students_trial_ajax', array($this, 'get_students_trial_action'));
        add_action('wp_ajax_school_sport_trial_status_student_ajax', array($this, 'trial_status_student_action'));
    }

    public function load_scripts() {
        add_action('wp_enqueue_scripts', array($this,'scripts_js') );
    }
    
    public function scripts_js() {
        wp_enqueue_script( 'schoolsport-dashboard-school-script', SCHOOLSPORT_URL. '/public/assets/js/dashboard/school.js', array( 'jquery' ),'1.0', true);
    }
    
   
    public function getStudentsTrialbySchool($school = 0, $limit = 10) {
        global $wpdb;
        $students = $wpdb->get_results("SELECT trials.post_title as trial_name, students.ID as student_id, 
             students.ID as student_id, trials.ID as trial_id, students.post_title as student_name FROM ( 
                SELECT students.ID, students.post_title FROM wp_posts as students
                 INNER JOIN wp_postmeta as post_meta ON ( students.ID = post_meta.post_id ) 
                WHERE (students.post_type = 'students' ) AND (students.post_status = 'publish' OR students.post_status = 'acf-disabled') AND 
                ( post_meta.meta_key = 'school' AND post_meta.meta_value = '{$school}')
                ) as students 
             INNER JOIN wp_postmeta as post_meta_trial on (post_meta_trial.meta_key LIKE 'students_%_student' AND post_meta_trial.meta_value = students.ID) 
             INNER JOIN wp_posts as trials ON (post_meta_trial.post_id = trials.ID and (trials.post_type = 'trials' ) 
             AND (trials.post_status = 'publish' OR trials.post_status = 'acf-disabled') )");
        return $students;
    }
    
     public function get_students_trial_action() {
        $current_user = get_current_user_id();
        $request = $this->sanitize_input($_REQUEST);
        $school = get_user_meta($current_user, 'school', true);
        $students = $this->getStudentsTrialbySchool($school);
        $result = ["data" => []];
        foreach($students as $student) {
            if(isset($student->trial_id)) {
                $date = get_field('date_of_trial', $student->trial_id);
                $post_students = get_field('students', $student->trial_id);
                foreach($post_students as  $index => $post_student) {
                    if($post_student['student']->ID == $student->student_id && isset($request['status']) &&
                       $post_student['approval_status'] == $request['status']) {
                        $result["data"][] = [
                            'trial' => $student->trial_name,
                            'student' => $student->student_name,
                            'trial_id' => $student->trial_id,
                            'student_id' => $student->student_id,
                            'date' => $date  
                        ];
                    }

                }
            }
         }
        die(json_encode($result));
    }
    
    public function trial_status_student_action() {
        $request = $this->sanitize_input($_REQUEST);
        $trial = (int) $request['trial'];
        $student = (int) $request['student'];
        $status = $request['status'];
        $query = new WP_Query(['post_type' => 'trials', 'p'=> $trial]);
        if($query->have_posts()) { $query->the_post();
            while(have_rows('students')) { the_row();
                 $local_student = get_sub_field('student');
                 $approval_status = get_sub_field('approval_status');
                 if(isset($local_student->ID)  && $local_student->ID == $student) {
                     if( update_sub_field('approval_status', $status, $trial) ) {
                        die( json_encode ( [
                                'class'=>'alert alert-success text-center',
                                'message'=>  'Student information updated successfully.',
                                'status' => 'success',
                                'data' => [
                                'trial' => $trial 
                            ]
                        ] ) );
                     }

                 }
            }
         }
         die( json_encode ( [
                'class'=>'alert alert-warning text-center',
                'message'=>  'Sorry do you don\'t have permision for this action. ',
                'status' => 'error' 
        ] ) );
    }
    
}