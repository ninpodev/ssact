<?php

class SchoolSport_Parent extends SchoolSport_Util {
    
    public $role = 'parent';

    private $user_id = null;

    public function __construct() {
        $this->user_id = get_current_user_id();
        $this->wp_load();
        if($this->is_role($this->role)) {
            $this->load_scripts();
            $this->load_ajax_action();
        }
    }

    public function wp_load() {
        add_action('wp', array($this, 'load_short_code'));
        add_action('wp', array($this, 'wp_restriction_query_var'));
        
        add_filter('posts_where', array($this, 'posts_where'));
       
    }
  
    public function load_short_code(){
      $shortcodes = array(
        'school_sport_create_student' => array($this, 'short_code_create_student'),
        'school_sport_edit_student' => array($this, 'short_code_edit_student'),
        'school_sport_register_student_trial' => array($this, 'short_code_register_student_trial'),
        'school_sport_payment_fee' => array($this, 'short_code_payment_fee'),          
      );

      foreach ( $shortcodes as $shortcode => $function ) {
        $this->wp_shortcode_restricted_redirect($this->role, $shortcode);
        $this->add_shortcode($shortcode, $function);
          if($shortcode == 'school_sport_payment_fee') {
               $this->is_payment_fee_student();
          }
      }
    }
    
 

    public function posts_where( $where ) {
      $where = str_replace("meta_key = 'students_#index#", "meta_key LIKE 'students_%", $where);
      return $where;
    }


    public function add_shortcode($shortcode, $function) {
        add_shortcode( apply_filters( "{$shortcode}_shortcode_tag", $shortcode ), $function );
    }
 

    public function load_scripts() {
        add_action('wp_enqueue_scripts', array($this,'scripts_js') );
    }

    public function scripts_js() {
        wp_enqueue_script( 'schoolsport-dashboard-encrypt', 'https://secure.ewaypayments.com/scripts/eCrypt.min.js', array('jquery'), '1.0', true);
        wp_enqueue_script( 'schoolsport-dashboard-parent-script', SCHOOLSPORT_URL. '/public/assets/js/dashboard/parent.js', array( 'jquery' ),'1.0', true);
    }

    public function load_ajax_action(){
        add_action('wp_ajax_school_sport_create_student_ajax', array($this, 'create_student_action'));
        add_action('wp_ajax_school_sport_edit_student_ajax', array($this, 'edit_student_action'));
        add_action('wp_ajax_school_sport_medical_student_ajax', array($this, 'medical_student_action'));
        add_action('wp_ajax_school_sport_emergency_student_ajax', array($this, 'emergency_student_action'));
        add_action('wp_ajax_school_sport_add_student_ajax',  array($this, 'add_student_action'));
        add_action('wp_ajax_school_sport_trial_student_ajax', array($this, 'trial_student_action'));
        add_action('wp_ajax_school_sport_trial_student_information_ajax', array($this, 'trial_student_information_action'));
        add_action('wp_ajax_school_sport_trial_student_payment_eway_ajax', array($this, 'trial_student_payment_eway_action'));
        add_action('wp_ajax_school_sport_student_payment_fee_eway_ajax', array($this, 'student_payment_fee_eway_action'));
        add_action('wp_ajax_school_sport_select_trial_student', array($this, 'select_trial_student_action'));
    }
  
  	public function short_code_create_student() {
		return school_sport_shortcode_wrapper('dashboard/parent/create-student');
    }
    

    public function short_code_edit_student() {
        return school_sport_shortcode_wrapper('dashboard/parent/edit-student');
    }
   
    public function short_code_payment_fee() {
        return school_sport_shortcode_wrapper('dashboard/parent/payment-fee');
    }
    
    public function create_student_action() {
        $this->wp_clear_session_arg('student');
        if($student = $this->create_student()) {
            die( json_encode ( 
                array('class'=>'alert alert-success text-center',
                    'message'=>  'Student information updated successfully.',
                    'status' => 'success',
                    'data' => [
                        'student' => $student
                    ]
            ) ) );
        }
        die( json_encode ( 
            array('class'=>'alert alert-warning text-center',
                'message'=>  'Sorry do you don\'t have permision for this action. ',
                'status' => 'error' 
        ) ) );
    }

    public function create_student() {
        $request = $this->sanitize_input($_REQUEST);
        $student = $this->wp_get_session_arg('student');
        if( $student['id'] = uniqid()) {
            $student['fname'] = wp_strip_all_tags( $request['fname'] );
			$student['lname'] = wp_strip_all_tags( $request['lname'] );
            $student['school'] = $request['school'];
            $student['school_name'] = get_the_title($request['school']);
            $student['date_of_birth'] = $request['date_of_birth'];
            $student['gender'] = $request['gender'];
            $student['action'] = 'create';
            $this->wp_set_session_arg($student, 'student');
            return $student;
        }
        return false;
    }

    public function edit_student_action() {
        $this->wp_clear_session_arg('student');
        if($student = $this->edit_student()) {
            die( json_encode ( 
                array('class'=>'alert alert-success text-center',
                    'message'=>  'The student was update successfully.',
                    'status' => 'success',
                    'data' => [
                        'student' => $student
                    ]
            ) ) );
        }
        die( json_encode ( 
            array('class'=>'alert alert-warning text-center',
                'message'=>  'Sorry do you don\'t have permision for this action. ',
                'status' => 'error' 
        ) ) );
    }
    
    public function edit_student() {
        $request = $this->sanitize_input($_REQUEST);
        $student = $this->wp_get_session_arg('student');
        if( $student['id'] = $request['student']) {
            $student['name'] = wp_strip_all_tags( $request['name'] );
            $student['school'] = $request['school'];
            $student['school_name'] = get_the_title($request['school']);
            $student['date_of_birth'] = $request['date_of_birth'];
            $student['gender'] = $request['gender'];
            $student['action'] = 'update';
            $this->wp_set_session_arg($student, 'student');
            return $student;
        }
        return false;
    }
    
   

    public function medical_student_action() {
        if($student = $this->medical_student()) {
            die( json_encode ( 
                array('class'=>'alert alert-success text-center',
                    'message'=>  'Student information updated successfully.',
                    'status' => 'success',
                    'data' => [
                        'student' => $student
                    ]
            ) ) );
        }
        die( json_encode ( 
            array('class'=>'alert alert-warning text-center',
                'message'=>  'Sorry do you don\'t have permision for this action. ',
                'status' => 'error' 
        ) ) );
    }

    public function medical_student() {
        $request = $this->sanitize_input($_REQUEST);
        $student = $this->wp_get_session_arg('student');
     
        if($student['id'] == $request['student']) {
          $student['medical_condition']  = $request['medical_condition'];
          $student['allergies'] = $request['allergies'];
          $student['date_last_tetanus'] = $request['date_last_tetanus'];
          $student['medical_number'] = $request['medical_number'];
          $this->wp_set_session_arg($student, 'student');
          return $student;
        }
        return false;
    }

    public function emergency_student_action() {
        if($student = $this->emergency_student()) {
            die( json_encode ( 
                array('class'=>'alert alert-success text-center',
                    'message'=>  'Student information updated successfully.',
                    'status' => 'success',
                    'data' => [
                        'student' => $student
                    ]
            ) ) );
        }
        die( json_encode ( 
            array('class'=>'alert alert-warning text-center',
                'message'=>  'Sorry do you don\'t have permision for this action. ',
                'status' => 'error' 
        ) ) );
    }

    public function emergency_student() {
        $request = $this->sanitize_input($_REQUEST);
        $student = $this->wp_get_session_arg('student');
        if($student['id'] === $request['student']) {
            $student['name_of_preferred_emergency']  = $request['name_of_preferred_emergency'];
            $student['relationship'] = $request['relationship'];
            $student['mobile_emergency'] = $request['mobile_emergency'];
            $student['student_email'] = $request['student_email'];
            $student['student_mobile'] = $request['student_mobile'];
            $this->wp_set_session_arg($student, 'student');
            return $student;
        }
        return false;
    }
  
    public function add_student_action() {
       if($student = $this->insert_student()) {
            die( json_encode ( 
                array('class'=>'alert alert-success text-center',
                    'message'=>  'Student information updated successfully.',
                    'status' => 'success',
                    'url' => home_url('dashboard'),
                    'data' => [
                        'student' => $student
                    ]
            ) ) );
        }
        die( json_encode ( 
            array('class'=>'alert alert-warning text-center',
                'message'=>  'Sorry do you don\'t have permision for this action. ',
                'status' => 'error' 
        ) ) );
    }
  
    public function insert_student() {
        $request = $this->sanitize_input($_REQUEST);
        $student = $this->wp_get_session_arg('student');
        if($request['student'] == $student['id']) {
            $author = get_post_field('parent', $student['id']);
            $data = array(
                'post_title'  => wp_strip_all_tags( $student['fname'].' '.$student['lname'] ),
                'post_status' => 'publish',
                'post_type' => 'students',
                'post_author' => $this->user_id,
            );
            if( $student['action'] != 'update'){
                $student['id'] = wp_insert_post($data);
                if (!is_wp_error($student['id'])) {
                    return $this->update_student_field($student);
                }
            } else {
				$data['post_title'] = wp_strip_all_tags( $student['name'] );
                $data['ID'] = $student['id'];
                if($author == $this->user_id) {
                    $student['id'] = wp_update_post($data); 
                    if (!is_wp_error($student['id'])) {
                        return $this->update_student_field($student);
                    }
                }
            }
        }
        return false;
    }
  
   public function update_student_field($student) {
        update_field('parent', $this->user_id, $student['id']);
        update_field('school', $student['school'], $student['id'] );
        update_field('date_of_birth', $student['date_of_birth'], $student['id'] );
        update_field('gender', $student['gender'], $student['id'] );
       
        update_field('medical_condition', $student['medical_condition'], $student['id']);
        update_field('allergies', $student['allergies'], $student['id'] );
        update_field('date_last_tetanus', $student['date_last_tetanus'], $student['id'] );
        update_field('medical_number', $student['medical_number'], $student['id'] );
        update_field('mobile_emergency', $student['mobile_emergency'], $student['id']);
     
        update_field('name_of_preferred_emergency', $student['name_of_preferred_emergency'], $student['id']);
        update_field('relationship', $student['relationship'], $student['id'] );
        update_field('student_email', $student['student_email'], $student['id'] );
        update_field('student_mobile', $student['student_mobile'], $student['id'] );
        return $student;
    }

    public function short_code_register_student_trial() {
        return school_sport_shortcode_wrapper('dashboard/parent/register-student-trial');
    }

    public function trial_student_action() {
        $request = $this->sanitize_input($_REQUEST);
        $student = (int) $request['student'];
        $trial = (int) $request['trial'];
        if($this->student_on_trial($trial, $student) == null) {
            if($registration = $this->trial_student()) {
                die( json_encode ( 
                    array('class'=>'alert alert-success text-center',
                        'message'=>  'The student was add on trial successfully.',
                        'status' => 'success',
                        'data' => [
                            'registration' => $registration
                        ]
                ) ) );
            }
            die( json_encode ( 
                array('class'=>'alert alert-warning text-center',
                    'message'=>  'Sorry do you don\'t have permision for this action. ',
                    'status' => 'error' 
            ) ) );
        }
        die( json_encode ( 
            array('class'=>'alert alert-warning text-center',
                'message'=>  'Sorry student registered on trial. ',
                'status' => 'error' 
        ) ) );
    }

    public function trial_student() {
        $current_user = get_current_user_id();
        $request = $this->sanitize_input($_REQUEST);
        $student = (int) $request['student'];
        $trial = (int) $request['trial'];
        if($registration['student'] = $student) {
            $_SESSION['registration'] = $registration;  
            $_SESSION['registration']['trial'] = $trial;
            return $registration;
        }
        return false;
    }

    public function trial_student_information_action() {
        if($registration = $this->trial_student_information()) {
            die( json_encode ( 
                array('class'=>'alert alert-success text-center',
                    'message'=>  'The student was add on trial successfully.',
                    'status' => 'success',
                    'data' => [
                        'registration' => $registration
                    ]
            ) ) );
        }
        die( json_encode ( 
            array('class'=>'alert alert-warning text-center',
                'message'=>  'Sorry do you don\'t have permision for this action. ',
                'status' => 'error' 
        ) ) );
    }

    public function trial_student_information() {
        if($registration = $_SESSION['registration'] ){
            $request = $this->sanitize_input($_REQUEST);
            $registration['playing_history'] = $request['playing_history'];
            $registration['representative_experience'] = $request['representative_experience'];
            $registration['prefered_position'] = $request['prefered_position'];
            $registration['approval_status'] = 'pending';
            $registration['selection_status'] = 'pending';
            
            $registration['print_consent'] = $this->wp_get_checkbox_checked($request['print_consent'], false);
            $registration['image_consent'] = $this->wp_get_checkbox_checked($request['image_consent'], false);
            $registration['recorded_consent'] = $this->wp_get_checkbox_checked($request['recorded_consent'], false);
            
            
            $_SESSION['registration'] = $registration;

            return $registration;
        }
        return false;
    }
    
    public function trial_student_payment_eway_action() {
        if($transaction = $this->trial_student_payment_eway()) {
            die( json_encode ( 
                array('class'=>'alert alert-success text-center',
                    'message'=>  'The student was add on trial successfully.',
                    'status' => 'success',
                    'data' => [
                        'registration' => $transaction
                    ]
            ) ) );
        }
        die( json_encode ( 
            array('class'=>'alert alert-warning text-center',
                'message'=>  'Something went wrong with your payment.',
                'status' => 'error' 
        ) ) );
    }
    
    public function trial_student_payment_eway() {
        $request = $this->sanitize_input($_REQUEST);
        $expiry = explode('/', $request['card_date']);
        $expiry_month = @$expiry[0];
        $expiry_year  = @$expiry[1];
        $card_mumber = $request['card_number'];
        $cvv = $request['card_cvv'];
        $card_name = $request['card_name'];
        $apiKey = SCHOOLSPORT_EWAY_API_KEY;
        $apiPassword = SCHOOLSPORT_EWAY_PASSWORD;
        $apiEndpoint = \Eway\Rapid\Client::MODE_PRODUCTION ;
        $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);
        $transaction = [
            'Customer' => [
                'CardDetails' => [
                    'Name' => $card_name,
                    'Number' => $card_mumber,
                    'ExpiryMonth' => $expiry_month,
                    'ExpiryYear' => $expiry_year,
                    'CVN' => $cvv,
                ]
            ],
            'Payment' => [
                'TotalAmount' =>  1000, //10, //this is correct amount 1000,
            ],
            'TransactionType' => \Eway\Rapid\Enum\TransactionType::PURCHASE,
        ];

        $response = $client->createTransaction(\Eway\Rapid\Enum\ApiMethod::DIRECT, $transaction);
      
        if ($response->TransactionStatus) {
            $this->insert_stundent_trial($response->TransactionID);
            $this->send_payment_mail();
            return $response->TransactionID;
        }
        return false;
   }
  
    public function insert_stundent_trial($transaction){
        $registration = $_SESSION['registration'];
        $trial = $registration['trial'];
        $student = $registration['student'];
        $registration['approval_status'] = 'pending';
        $registration['selection_status'] = 'pending';     
        $trials_student = get_post_meta( $student, 'trials', true);
        $author = @get_field( 'parent', $student);
        $trials_student = (!is_array($trials_student))? [] : $trials_student;
        $trial_row = array_search($trial, array_column($trials_student, 'trial'));
        if( isset($author->ID) && $author->ID == $this->user_id) {
            $unikey = $this->student_on_trial($trial, $student);
            if($trial_row !== false && isset($trials_student[$trial_row])) {
                unset($trials_student[$trial_row]);
                $trials_student = array_merge($trials_student);
            }
            if( $unikey !== null ) {
                $trials_student[] = [
                    'trial' => $trial,
                    'row' => $unikey,
                    'transaction' => $transaction
                ];
            } else {
                $unikey = add_row('students', $registration, $trial);
                $trials_student[] = [
                    'trial' => $trial,
                    'row' => $unikey,
                    'transaction' => $transaction
                ];
            }
            update_post_meta($student, 'trials', $trials_student);
            return true;
        } 
        return false;
    }
  
    public function student_on_trial($trial_id, $student_id) {
        $students  = get_field('students', $trial_id);
        $id = null;
        foreach($students as  $index => $student) {
            if(isset($student['student']) && isset($student['student']->ID)) {
                if($student['student']->ID == $student_id) {
                    $id =  $index;
                    break;
                }
            }
        }
        return $id;
    }

    public function send_payment_mail() {
        if($user = get_userdata($this->user_id)) {
            $registration = $this->sanitize_input($_SESSION['registration']);
            $trial = $registration['trial'];
            $headers = array('Content-Type: text/html; charset=UTF-8');
            $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
            //$studentID = $registration['student'];
            //$school = @get_field( 'school', $student);



            $message = school_sport_get_template('email/payment-trial');
            $message = str_replace('#first_name#', $user->first_name, $message);
            $message = str_replace('#title_trial#', get_the_title($trial), $message);
            $message = str_replace('#trial_date#', get_field('date_of_trial', $trial), $message);
            $message = str_replace('#trial_venue#', get_field('venue', $trial), $message);
                                   
            wp_mail("andrew.pearson2@gmail.com", sprintf(__('[%s] Registered Trial'), $blogname), $message,$headers);

            //Send school-approval email to the registered students school
            //The school email address is in a user which has that school selected as their school.
            //$schoolID = get_field('school', get_post($student));
            // $schoolApproverID = get_users_by('school',$schoolID)[0];
            // $schoolApprover = get_user_data($schoolApproverID);
            // $schoolEmail = $schoolApprover->user_email;

            // $sample = "!HERE!";
             //$message = school_sport_get_template('email/school-approval');
            // $message = str_replace('#first_name#', $sample, $message);
            // $message = str_replace('#last_name#', $sample, $message);
            // $message = str_replace('#team_name#', $sample, $message);
            // $message = str_replace('#championship_date#', $sample, $message);
             //wp_mail("andrew.pearson2@gmail.com", sprintf(__('[%s] School Approval for Registered Student Required'), $blogname), $message,$headers);
        }
    }
    
     public function student_payment_fee_eway_action() {
        if($transaction = $this->payment_fee_eway_action()) {
            die( json_encode ( 
                array('class'=>'alert alert-success text-center',
                    'message'=>  'The student was payment fee, You will redirect to dashboard.',
                    'status' => 'success',
                    'url' => home_url('dashboard'),
                    'data' => [
                        'registration' => $transaction
                    ]
            ) ) );
        }
        die( json_encode ( 
            array('class'=>'alert alert-warning text-center',
                'message'=>  'Something went wrong with your payment.',
                'status' => 'error' 
        ) ) );
    }
    
    public function payment_fee_eway_action() {
        $request = $this->sanitize_input($_REQUEST);
        $expiry = explode('/', $request['card_date']);
        $expiry_month = @$expiry[0];
        $expiry_year  = @$expiry[1];
        $card_mumber = $request['card_number'];
        $cvv = $request['card_cvv'];
        $card_name = $request['card_name'];
        $trial_id = (int) $request['trial'];
        $student_id = (int) $request['student'];
        if($this->student_on_trial($trial_id, $student_id) !== null) {
            $apiKey = SCHOOLSPORT_EWAY_API_KEY;
            $apiPassword = SCHOOLSPORT_EWAY_PASSWORD;
            $apiEndpoint = \Eway\Rapid\Client::MODE_PRODUCTION;
            $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);
            $transaction = [
                'Customer' => [
                    'CardDetails' => [
                        'Name' => $card_name,
                        'Number' => $card_mumber,
                        'ExpiryMonth' => $expiry_month,
                        'ExpiryYear' => $expiry_year,
                        'CVN' => $cvv,
                    ]
                ],
                'Payment' => [
                    'TotalAmount' => 20000,
                ],
                'TransactionType' => \Eway\Rapid\Enum\TransactionType::PURCHASE,
            ];

            $response = $client->createTransaction(\Eway\Rapid\Enum\ApiMethod::DIRECT, $transaction);
            if ($response->TransactionStatus) {
                $this->insert_stundent_trial($response->TransactionID);
//                $this->send_payment_mail();
                $this->update_payment_initial($trial_id, $student_id, $response->TransactionID);
                update_field('payment_initial', $trial_id);
                return $response->TransactionID;
            }
        }
        return false;
    }
    
    public function update_payment_initial($trial_id, $student_id, $transaction = 0)  {
          while(have_rows('students', $trial_id)) {
            the_row();
            $student = get_sub_field('student');
            if(isset($student->ID)) {
                if($student_id == $student->ID) {
                    update_sub_field('payment_initial',true);
                    update_sub_field('payment_transaction', $transaction);
                }
            }
         }
    }
    
    public function is_payment_fee_student() {
        $student_id  = get_query_var('student');
        $trial_slug  = get_query_var('trial_slug');
        $page = get_posts( array( 'name' => $trial_slug, 'post_type' => 'any', 'posts_per_page'=> 1) );
        $page = current($page);
        if(isset($page->ID)) {
          $trial_id = $page->ID;
          while(have_rows('students', $trial_id)) {
            the_row();
            $student = get_sub_field('student');
            if(isset($student->ID)) {
                if($student_id == $student->ID) {
                   $paymen_initial = get_sub_field('payment_initial');
                    if($paymen_initial) {
                        wp_redirect(home_url('dashboard'));
                        exit;
                    }
                }
            }
         }
       }
    }
    
    public function select_trial_student_action() {
        $request = $this->sanitize_input($_REQUEST);
        $trial = (int) $request['trial'];
        $current_user = get_current_user_id();
        $students = new WP_Query(['post_type' => 'students', 'meta_key' => 'parent', 'meta_value' => $current_user]);
        $age = get_field('age', $trial);
        $gender = get_field('gender', $trial);
        $birthday_restriction_from = get_field('date_birth_restriction_from', $trial);
        $birthday_restriction_from_date =  DateTime::createFromFormat('m/d/Y', $birthday_restriction_from); 
        $birthday_restriction_to = get_field('date_birth_restriction_to', $trial);
        $birthday_restriction_to_date =  DateTime::createFromFormat('m/d/Y', $birthday_restriction_to);
        $now = new DateTime();
        $response = [];
        while($students->have_posts()) {
            $students->the_post();
            $student_id = get_the_ID();
            $birthday = get_field('date_of_birth');
            $birthday_date =  DateTime::createFromFormat('m/d/Y', $birthday);
            $student_age = $birthday_date->diff($now)->y;
            $student_gender = get_field('gender');
            if(($gender == "" || $gender == $student_gender ) &&
               ( ($birthday_restriction_from_date == '' ||  $birthday_restriction_from_date <= $birthday_date ) && 
               ($birthday_restriction_to_date == '' || $birthday_restriction_to_date >= $birthday_date) ) ) {
                $response[$student_id] = get_the_title(); 
            }
        }
        die(json_encode($response));
    }
  
}