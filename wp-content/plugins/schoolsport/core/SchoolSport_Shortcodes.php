<?php

Class SchoolSport_Shortcodes {

	public function __construct(){
		$shortcodes = array(
			'school_sport_register_official' =>	array($this, 'school_sport_register_official'),
			'school_sport_register_parent' =>	array($this, 'school_sport_register_parent'),
			'school_sport_login' => array($this, 'school_sport_login'),
			'school_sport_recovery' => array($this, 'school_sport_recovery'),
			'school_sport_dashboard' => array($this, 'school_sport_dashboard'),
			'school_sport_reset_password' => array($this, 'school_sport_reset_password'),
			'school_sport_register_confirmation' => array($this, 'school_sport_register_confirmation'),
			//confirmation-complete
        );
		foreach ( $shortcodes as $shortcode => $function ) {
			add_shortcode( apply_filters( "{$shortcode}_shortcode_tag", $shortcode ), $function );
		}
	}

	public  function school_sport_register_official( $atts ){
     
		return school_sport_shortcode_wrapper('user/register-official');
    }
    
    public  function school_sport_login( $atts ){
     
		return school_sport_shortcode_wrapper('user/login');
	}

	public function school_sport_register_parent($atts) {
		return school_sport_shortcode_wrapper('user/register-parent');
	}

	public function school_sport_recovery($atts) {
		return school_sport_shortcode_wrapper('user/recovery');
	}

	public function school_sport_reset_password($atts) {
		return school_sport_shortcode_wrapper('user/reset-password');
	}

	public function school_sport_register_confirmation() {
		return school_sport_shortcode_wrapper('user/confirmation-complete');
	}

	public function school_sport_dashboard($atts) {
		if(is_user_logged_in()) {
			$user = wp_get_current_user();
			$roles = ( array ) $user->roles;
			if(in_array('administrator', $roles)) {
				return school_sport_shortcode_wrapper('dashboard/admin/dashboard');
			}

			if(in_array('official', $roles)) {
				return school_sport_shortcode_wrapper('dashboard/official/dashboard');
			}
			if(in_array('school', $roles)) {
				return school_sport_shortcode_wrapper('dashboard/school/dashboard');
			} 
			
			if(in_array('parent', $roles)) {
				return school_sport_shortcode_wrapper('dashboard/parent/dashboard');
			} 
		
		}
		return null;
	}
}

$SchoolSport_Shortcodes = new SchoolSport_Shortcodes;