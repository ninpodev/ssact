<?php 

if (!function_exists('school_sport_shortcode_wrapper')) {
    function school_sport_shortcode_wrapper( $file = '') {
        ob_start();
        if(file_exists(get_template_directory()."/school-sport/${file}.php")) {
            include(get_template_directory()."/school-sport/${file}.php");
        } else {
           if(file_exists(SCHOOLSPORT_PATH."/template/${file}.php")) {
                include(SCHOOLSPORT_PATH."/template/${file}.php");
            }
        }
        return ob_get_clean();
    }
}


if (!function_exists('school_sport_get_template')) {
    function school_sport_get_template( $file = '', $arg = []) {
		ob_start();
		extract($arg, EXTR_SKIP);
        if(file_exists(get_template_directory()."/school-sport/${file}.php")) {
            include(get_template_directory()."/school-sport/${file}.php");
        } else {
           if(file_exists(SCHOOLSPORT_PATH."/template/${file}.php")) {
                include(SCHOOLSPORT_PATH."/template/${file}.php");
            }
        }
        $content = ob_get_contents();
        ob_get_clean();
        return $content;
    }
}

if (!function_exists('school_sport_recaptcha')) {
    function school_sport_recaptcha($response){
        $key = null;
        $ip = $_SERVER['REMOTE_ADDR'];
        $url = "https://www.google.com/recaptcha/api/siteverify";
        $key = get_field('google_secret_key','option');
        $captcha = json_decode(file_get_contents("{$url}?secret={$key}&response={$response}&remoteip={$ip}"));
        if(isset($captcha->success)){
            if($captcha->success == true){
                return true;
            }
        }
        return false;
    }
}

if(!function_exists('school_sport_get_url_login')) {
    function school_sport_get_url_login() {
        if($id = get_option(SCHOOLSPORT_PAGE_LOGIN)) {
            return get_permalink($id);
        }
        return false;
    }
}

if(!function_exists('school_sport_get_url_dahsboard')) {
    function school_sport_get_url_dahsboard() {
        if($id = get_option(SCHOOLSPORT_PAGE_DASHBOARD)) {
            return get_permalink($id);
        }
        return false;
    }
}


if(!function_exists('school_sport_is_dahsboard')) {
    function school_sport_is_page_dahsboard() {
		global $post;
		if($post && isset($post->ID) ) {
			$dashboard = get_option(SCHOOLSPORT_PAGE_DASHBOARD);
			if($dashboard == $post->ID ) {
				return true;
			}
		}
        return false;
    }
}

if(!function_exists('school_sport_is_dahsboard')) {
    function school_sport_is_dahsboard() {
		global $post;
		if($post && isset($post->ID) ) {
			$dashboard = get_option(SCHOOLSPORT_PAGE_DASHBOARD);
			if($dashboard == $post->ID || $post->post_parent == $dashboard) {
				return true;
			}
		}
        return false;
    }
}

if(!function_exists('school_sport_get_url_register')) {
    function school_sport_get_url_register() {
        if($id = get_option(SCHOOLSPORT_PAGE_REGISTER)) {
            return get_permalink($id);
        }
        return false;
    }
}

if(!function_exists('school_sport_get_url_recovery')) {
    function school_sport_get_url_recovery() {
        if($id = get_option(SCHOOLSPORT_PAGE_RECOVERY)) {
            return get_permalink($id);
        }
        return false;
    }
}

if(!function_exists('school_sport_get_url_confirmation')) {
    function school_sport_get_url_confirmation() {
        if($id = get_option(SCHOOLSPORT_PAGE_REGISTER_CONFIRMATION)) {
            return get_permalink($id);
        }
        return false;
    }
}

if(!function_exists('school_sport_get_url_reset_password')) {
    function school_sport_get_url_reset_password() {
        if($id = get_option(SCHOOLSPORT_PAGE_RESET_PASSWORD)) {
            return get_permalink($id);
        }
        return false;
    }
}


if(!function_exists('school_sport_get_url_registration')) {
    function school_sport_get_url_registration() {
        if($id = get_option(SCHOOLSPORT_PAGE_REGISTER)) {
            return get_permalink($id);
        }
        return false;
    }
}

if(!function_exists('school_sport_is_official')) {
	function school_sport_is_official() {
		if(is_user_logged_in()) {
			$user = wp_get_current_user();
			$roles = ( array ) $user->roles;
			if(in_array('official', $roles) || in_array('administrator', $roles)){
				return true;
			}
		}
		return false;
	}
}

if(!function_exists('school_sport_can_create_trial')) {
	function school_sport_can_create_trial() {
		if(is_user_logged_in()) {
			$user = wp_get_current_user();
			$id = get_current_user_id();
			$roles = ( array ) $user->roles;
			$trial = get_user_meta($id, 'trial', true);
			if(( in_array('official', $roles) && $trial ) || in_array('administrator', $roles)){
				return true;
			}
		}
		return false;
	}
}

if(!function_exists('school_sport_can_create_student')) {
	function school_sport_can_create_student() {
		if(is_user_logged_in()) {
			$user = wp_get_current_user();
			$id = get_current_user_id();
			$roles = ( array ) $user->roles;
			if(in_array('parent', $roles) || in_array('administrator', $roles)){
				return true;
			}
		}
		return false;
	}
}

if(!function_exists('school_sport_is_parent')) {
	function school_sport_is_parent() {
		if(is_user_logged_in()) {
			$user = wp_get_current_user();
			$roles = ( array ) $user->roles;
			if(in_array('parent', $roles) || in_array('administrator', $roles)){
				return true;
			}
		}
		return false;
	}
}

if(!function_exists('school_sport_is_school')) {
	function school_sport_is_school() {
		if(is_user_logged_in()) {
			$user = wp_get_current_user();
			$roles = ( array ) $user->roles;
			if(in_array('school', $roles) || in_array('administrator', $roles)){
				return true;
			}
		}
		return false;
	}
}



if(!function_exists('school_sport_trial_draft')) {
	function school_sport_trial_draft() {
		$post_trial = [
			'trial' => '', 'name' => '',
			'sport' => '', 'year' => date('Y'),
			'age' => '', 'gender' => '', 'date_birth_from' => '',
			'date_birth_to' => '', 'date_trial' => '',
			'eligibility' => '','champ_date' => '',
			'venue' => ''
		];
		if (school_sport_is_official()) {
			$current_user = get_current_user_id();
			$draft = new WP_Query([
				'post_type' => 'trials',
				'posts_per_page' => 1,
				'post_status' => 'draft',
				'author' => $current_user
			]);
			if ($draft->have_posts()) {  
				$draft->the_post();
				$sport = @get_field('sport')->ID;
				$name = get_the_title();
				$id = get_the_ID();
				$year = get_field('year');
				$age = get_field('age');
				$gender = get_field('gender');
				$date_birth_from = get_field('date_birth_restriction_from');
				$date_birth_to = get_field('date_birth_restriction_to');
				$date_trial = get_field('date_of_trial');
				$eligibility = get_field('eligibility');
				$champ_date = get_field('championship_date');
				$venue = get_field('venue');
				$post_trial = [
					'trial' => $id, 'name' => $name, 'sport' => $sport,
					'year' => $year, 'age' => $age, 'gender' => $gender,
					'date_birth_from' => $date_birth_from, 'date_birth_to' => $date_birth_to,
					'date_trial' => $date_trial, 'eligibility' => $eligibility,
					'champ_date' => $champ_date, 'venue' => $venue
				];
			}
		}
		return $post_trial;
	}
}

if(!function_exists('edit_student_rewrite')) {
    function edit_student_rewrite() {
        add_rewrite_rule(
            '^dashboard/edit-student/([0-9]+)/?',
            'index.php?pagename=dashboard/edit-student&student=$matches[1]',
            'top'
        );
	}
	add_action('init','edit_student_rewrite');
}

if(!function_exists('order_products_rewrite')) {
    function order_products_rewrite() {
        add_rewrite_rule(
            '^dashboard/order-products/([0-9]+)/?',
            'index.php?pagename=dashboard/order-products&trial=$matches[1]',
            'top'
        );
	}
	add_action('init','order_products_rewrite');
}

if(!function_exists('add_query_student')) {
    function add_query_student($vars) {
		array_push($vars, 'student');
		return $vars;
	}
	add_filter('query_vars',  'add_query_student');
}


if(!function_exists('edit_trial_team_rewrite')) {
    function edit_trial_team_rewrite() {
        add_rewrite_rule(
            '^dashboard/select-students-for-the-team/([a-zA-Z0-9-_]+)/?',
            'index.php?pagename=dashboard/select-students-for-the-team&trial_slug=$matches[1]',
            'top'
        );
	}
	add_action('init','edit_trial_team_rewrite');
}


if(!function_exists('payment_selected_team_rewrite')) {
    function payment_selected_team_rewrite() {
        add_rewrite_rule(
            '^dashboard/payment-fee/([a-zA-Z0-9-_]+)/([a-zA-Z0-9-_]+)/?',
            'index.php?pagename=dashboard/payment-fee&trial_slug=$matches[1]&student=$matches[2]',
            'top'
        );
	}
	add_action('init','payment_selected_team_rewrite');
}

if(!function_exists('add_query_trial_slug')) {
    function add_query_trial_slug($vars) {
		array_push($vars, 'trial_slug');
		return $vars;
	}
	add_filter('query_vars',  'add_query_trial_slug');
}


if(!function_exists('school_sport_seach_student_on_repeater')) {
    function school_sport_seach_student_on_repeater($value, $array = []) {
        $student = false;
        foreach($array as $key => $data) {
            if(isset($data['student'])) {
                if(isset($data['student']->ID) && $data['student']->ID === $value) {
                    $student = $data;
                    break;
                }
            }
        }
        return $student;
    }
}

if(!function_exists("school_sport_get_url_payment_fee_student")) {
    function school_sport_get_url_payment_fee_student($trial_id, $student) {
        $trial_slug  =get_post_field('post_name', $trial_id);
        if($trial_slug) {
              return home_url('dashboard/payment-fee/'.$trial_slug.'/'.$student);
        }
        return home_url('dashboard');
    }
}

