<?php 

class SchoolSport_Util {
    public function sanitize_input_html($post){
        if( is_array($post) ){
            foreach($post as $key => $value){
                if(!is_array($value)){
                    $post[$key] = $this->sanitize_html($value);
                } else {
                    $post[$key] = $this->sanitize_input_html($value);
                }
            }
        } else {
            $post = sanitize_html($post);
        }
        return $post;
    }

    public function sanitize_html($data){
        // Fix &entity\n;
        $data = str_replace(array('&amp;','&lt;','&gt;'), array('&amp;amp;','&amp;lt;','&amp;gt;'), $data);
        $data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
        $data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
        $data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

        // Remove any attribute starting with "on" or xmlns
        $data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

        // Remove javascript: and vbscript: protocols
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);

        // Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

        // Remove namespaced elements (we do not need them)
        $data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

        do
        {
            // Remove really unwanted tags
            $old_data = $data;
            $data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
        }
        while ($old_data !== $data);

        // we are done...
        return $data;
    }

    public function sanitize_input($post){
        if( is_array($post) ){
            foreach($post as $key => $value){
                if(!is_array($value)){
                    $post[$key] = sanitize_text_field($value);
                } else {
                    $post[$key] = $this->sanitize_input($value);
                }
            }
        } else {
            $post = sanitize_text_field($post);
        }
        return $post;
    }

    public function is_role($role = '') {
        if(is_user_logged_in()) {
            $user = wp_get_current_user();
            $roles = ( array ) $user->roles;
            if ( in_array( $role, $roles) || in_array('administrator', $roles) ) {
                return true;
            }
        }
        return false;
    }

    public function wp_shortcode_restricted_redirect($role, $schortcode) {
        global $post;
        if ( is_a( $post, 'WP_Post' ) && strpos($post->post_content, "[${schortcode}]") 
            && !$this->is_role($role) ) {
            wp_redirect(school_sport_get_url_dahsboard());
            exit;
        } 
    }
    
    public function wp_restriction_query_var() {
     
        if($trial_slug  = get_query_var('trial_slug')) {
            $page = get_page_by_path($trial_slug, OBJECT, 'trials');
            if(!isset($page->post_title)) {
                wp_redirect(school_sport_get_url_dahsboard());
                exit;
            }   
        }
        if($student = get_query_var('student')) {
            $parent = get_field('parent', $student);
            $current_user = get_current_user_id();
            if((isset($parent->ID) && $parent->ID != $current_user) || !isset($parent->ID)){
                wp_redirect(school_sport_get_url_dahsboard());
                exit;
            }
        }
    }

    public function wp_set_session_arg($arg, $space = false) {
        foreach($arg as $key => $value) {
            if($space) {
                $_SESSION[$space][$key] = $value;
            } else {
                $_SESSION[$key] = $value;
            }
        }
    }

    public function wp_get_session_arg($space = false) {
        if($space) {
          return $_SESSION[$space];
        }
        return $_SESSION;
    }

    public function wp_clear_session_arg($space= false){
        if($space) {
          $_SESSION[$space] = null;
        }
        $_SESSION = [];
    }


    public function wp_get_checkbox_checked($checkbox, $empty = 'no' ) {
        if(!isset($checkbox)) {
            return $empty;
        }
        return $checkbox;
    }
}

$schoolsport_util = new SchoolSport_Util;