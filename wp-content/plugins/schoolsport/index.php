<?php
    /**
     * @package SchoolSport
     * @version 0.0.1
     */

    /*
    Plugin Name: School Sport ACT
    Description: This plugin allows you to manage all the operation of teams, events and students of School Sport IACT
    Author: Guillermo Fuentes
    Version: 0.0.1
    Author URI: 
    */

    ini_set('session.gc_maxlifetime', '28800');
    
    define('SCHOOLSPORT_VERSION', '0.0.1');
    
    define('SCHOOLSPORT_URL',  plugin_dir_url( __FILE__ ));
    define('SCHOOLSPORT_PATH', dirname(__FILE__));
    
    define('SCHOOLSPORT_SENDGRID_API_KEY', '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');
    define('SCHOOLSPORT_EWAY_API_KEY',  'C3AB9AUe0iIIqcjLIrIFyNfwZjK8IjcKXvn30h+U6LGvG3Y67yKK9E2aEec6ThvEXjXCGW');
    define('SCHOOLSPORT_EWAY_PASSWORD', 'btGhCLoi');

    define('SCHOOLSPORT_PREFIX', 'sciact_');

    define('SCHOOLSPORT_PAGE_REGISTER', 'school_sport_register');
    define('SCHOOLSPORT_PAGE_REGISTER_PARENT', 'school_sport_register_parent');
    define('SCHOOLSPORT_PAGE_REGISTER_OFFICIAL', 'school_sport_register_official');
    define('SCHOOLSPORT_PAGE_RECOVERY', 'school_sport_recovery');
    define('SCHOOLSPORT_PAGE_LOGIN', 'school_sport_login');
    define('SCHOOLSPORT_PAGE_RESET_PASSWORD', 'school_sport_reset_password');
    define('SCHOOLSPORT_PAGE_REGISTER_CONFIRMATION', 'school_sport_register_confirmation');
    define('SCHOOLSPORT_PAGE_DASHBOARD', 'school_sport_dashboard');
    define('SCHOOLSPORT_PAGE_EMAIL_SENT', 'school_sport_email_sent');

    require(SCHOOLSPORT_PATH.'/plugins/sendgrid/lib/loader.php');
    require(SCHOOLSPORT_PATH.'/plugins/eway-rapid/include_eway.php');

    require(SCHOOLSPORT_PATH.'/core/SchoolSport_Util.php');
    require(SCHOOLSPORT_PATH.'/core/SchoolSport_Global.php');
    require(SCHOOLSPORT_PATH.'/core/SchoolSport_Shortcodes.php');

    require(SCHOOLSPORT_PATH.'/admin/core/SchoolSport_Setup.php');
    require(SCHOOLSPORT_PATH.'/admin/core/SchoolSport_PostType.php');
    require(SCHOOLSPORT_PATH.'/admin/core/SchoolSport_Admin.php');
    require(SCHOOLSPORT_PATH.'/admin/core/SchoolSport_User.php');

    require(SCHOOLSPORT_PATH.'/admin/SchoolSport_Main.php');
	
    require(SCHOOLSPORT_PATH.'/public/SchoolSport_Scripts.php');
    require(SCHOOLSPORT_PATH.'/public/SchoolSport_RegisterLogin.php');

    require(SCHOOLSPORT_PATH.'/public/dashboard/SchoolSport_SSACTAdmin.php');	
    require(SCHOOLSPORT_PATH.'/public/dashboard/SchoolSport_Official.php');	
    require(SCHOOLSPORT_PATH.'/public/dashboard/SchoolSport_Parent.php');
    require(SCHOOLSPORT_PATH.'/public/dashboard/SchoolSport_School.php');
    require(SCHOOLSPORT_PATH.'/public/SchoolSport_Dashboard.php');	


    register_activation_hook( __FILE__, array( 'SchoolSport', 'single_activate' ) );

    add_action( 'plugins_loaded',  array('SchoolSport', 'init' ));

 


?>