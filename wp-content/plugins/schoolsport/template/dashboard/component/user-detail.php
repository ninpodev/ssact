<?php
if(is_user_logged_in()) { 
	$user = wp_get_current_user();

	$phone = get_user_meta($user->ID, 'phone',  true);
	$mobile = get_user_meta($user->ID, 'mobile', true);
	$address = get_user_meta($user->ID, 'address', true);
	$suburb = get_user_meta($user->ID, 'suburb', true);
	$post_code = get_user_meta($user->ID, 'post_code', true);
	$state = get_user_meta($user->ID, 'state', true );
	$state_term = get_term_by('id', $state, 'states');

?>
<div class="row ">
    <div class="col-5 col-md-3 col-lg-2 column_labels">
        <ul>
            <li>First Name</li>
            <li>Last Name</li>
            <li>Email</li>
            <li>Phone number</li>
        </ul>
    </div>
    <div class="col-7 col-md-9 col-lg-4 column_data text-break">
        <ul>
            <li><?php echo $user->first_name;?></li>
            <li><?php echo $user->last_name;?></li>
            <li><?php echo $user->user_email;?></li>
            <li><?php echo $phone;?></li>
        </ul>
    </div>
    <div class="col-5 col-md-3 col-lg-2 column_labels">
        <ul>
            <li>Address</li>
            <li>Suburb</li>
            <!--li>City</li-->
            <li>Post Code</li>
        </ul>
    </div>
    <div class="col-7 col-md-9 col-lg-4 column_data text-break">
        <ul>
            <li><?php echo $address;?></li>
            <li><?php echo $suburb;?></li>
            <!--li>Canberra</li-->
            <li><?php echo $post_code;?></li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 text-right link_box">
        <p><a href="<?php echo home_url('dashboard/edit-my-details');?>" class="link">Edit my details</a></p>
    </div>
</div>
<?php } ?>