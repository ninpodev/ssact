<?php
if(is_user_logged_in()) { 
	$user = wp_get_current_user();

	$phone = get_user_meta($user->ID, 'phone',  true);
	$mobile = get_user_meta($user->ID, 'mobile', true);
	$address = get_user_meta($user->ID, 'address', true);
	$suburb = get_user_meta($user->ID, 'suburb', true);
	$post_code = get_user_meta($user->ID, 'post_code', true);
	$state = get_user_meta($user->ID, 'state', true );
	$city = get_user_meta($user->ID, 'city', true);
	$state_term = get_term_by('id', $state, 'states');
?>
<div class="row">
	<div class="col-md-10 offset-md-1 box_content reset_height">
		<form id="editDetailsForm" novalidate="" rel="user-update">
			<div class="row border_bottom_forms_fields">
				<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
					<div class="form-group floating-label-form-group controls">
						<label for="txt_first_name">First Name <span class="obligatory_field">*</span></label>
						<input type="text" class="form-control" placeholder="" id="txt_first_name" name="first_name" value="<?php echo $user->first_name;?>" required data-validation-required-message="Please enter your <b> First Name</b>.">
						<p class="help-block text-danger"></p>
					</div>
				</div>
				<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
					<div class="form-group floating-label-form-group controls">
						<label for="txt_last_name">Last Name <span class="obligatory_field">*</span></label>
						<input type="text" class="form-control" placeholder="Walker" id="txt_last_name" name="last_name" value="<?php echo $user->last_name;?>" required data-validation-required-message="Please enter your <b> Last Name</b>.">
						<p class="help-block text-danger"></p>
					</div>
				</div>
			</div>
			<div class="row border_bottom_forms_fields">
				<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
					<div class="form-group floating-label-form-group controls">
						<label for="txt_email_address">Email Address <span class="obligatory_field">*</span></label>
						<input type="email" class="form-control" placeholder="bob.walker@gmail.com" id="txt_email_address" name="user_email" value="<?php echo $user->user_email;?>" required data-validation-required-message="Please enter your <b> Email Address</b>.">
						<p class="help-block text-danger"></p>
					</div>
				</div>
				<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
					<div class="form-group floating-label-form-group controls">
						<label for="txt_phone">Phone Number </label>
						<input type="text" class="form-control" placeholder="9631 700 22" id="txt_phone" name="phone" value="<?php echo $phone;?>">
						<p class="help-block text-danger"></p>
					</div>
				</div>
			</div>
			<div class="row border_bottom_forms_fields">
			
				<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
					<div class="form-group floating-label-form-group controls">
						<label for="txt_mobile">Mobile Number <span class="obligatory_field">*</span></label>
						<input type="text" class="form-control" placeholder="9631 700 22" id="txt_mobile" name="mobile" value="<?php echo $mobile;?>" required>
						<p class="help-block text-danger"></p>
					</div>
				</div>
				<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
					<div class="form-group floating-label-form-group controls">
						<label for="txt_address">Address <span class="obligatory_field">*</span></label>
						<input type="text" class="form-control" placeholder="12 Cornet st." id="txt_address" name="address" value="<?php echo $address;?>" required data-validation-required-message="Please enter your <b> Address</b>.">
						<p class="help-block text-danger"></p>
					</div>
				</div>
				
			</div>
			<div class="row border_bottom_forms_fields">
				<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
					<div class="form-group floating-label-form-group controls">
						<label for="txt_suburb">Suburb <span class="obligatory_field">*</span></label>
						<input type="text" class="form-control" placeholder="Holt" id="txt_suburb" name="suburb"  value="<?php echo $suburb;?>"  required data-validation-required-message="Please enter your <b> Suburb</b>.">
						<p class="help-block text-danger"></p>
					</div>
				</div>
				<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
					<div class="form-group floating-label-form-group controls">
						<label for="txt_city">City <span class="obligatory_field">*</span></label>
						<input type="text" class="form-control" placeholder="Canberra" id="txt_city" name="city" value="<?php echo $city;?>" required data-validation-required-message="Please enter your <b> City</b>.">
						<p class="help-block text-danger"></p>
					</div>
				</div>
			</div>
			<div class="row border_bottom_forms_fields wb">
				<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
					<div class="form-group floating-label-form-group controls">
						<label for="txt_post_code">Post Code</label>
						<input type="text" class="form-control" placeholder="1234" id="txt_post_code" name="post_code" value="<?php echo $post_code;?>"  required data-validation-required-message="Please enter your <b> Post Code</b>.">
						<p class="help-block text-danger"></p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 text-center forms_box">
					<div id="success"></div>
					<input type="hidden" name="id_form" id="id_form" value="editDetailsForm">
					<button type="submit" class="btn btn-purple" id="btnSubmit">Save</button>
					<p class="txt_in_box_button">And return to my Dashboard</p>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<?php echo school_sport_get_template('component/notice');?>
				</div>
			</div>
		</form>
	</div>
</div>

<?php } ?>