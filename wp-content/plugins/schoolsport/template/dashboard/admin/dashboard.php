<style>
.btn-blue {
    color: #fff;
    background-color: #3399ff;
    border-color: #3399ff;
}

.btn-blue:hover {
    background-color: white;
    border-color: #3399ff;
    color: #3399ff;
}

.theme-administrator .content_page .box_content {
    border-top: 4px solid #3399ff;
}

.content_page .box_content_bt_blue {
    border-color: #3399ff;
}

.theme-administrator .btn-theme {
    color: #fff;
    background-color: #3399ff;
    border-color: #3399ff;
}

.theme-administrator .content_page .list_striped .page-item.active .page-link {
    background-color: #3399ff;
    border-color: #3399ff;
}
</style>
<script>
function sortTable(n) {
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.getElementById("trials");
  tablebody = table.getElementsByTagName("tbody")[0];
  switching = true;
  // Set the sorting direction to ascending:
  dir = "asc";
  /* Make a loop that will continue until
  no switching has been done: */
  while (switching) {
    // Start by saying: no switching is done:
    switching = false;
    rows = tablebody.rows;
    /* Loop through all table rows (except the
    first, which contains table headers): */
    for (i = 1; i < (rows.length - 1); i++) {
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Get the two elements you want to compare,
      one from current row and one from the next: */
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /* Check if the two rows should switch place,
      based on the direction, asc or desc: */
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /* If a switch has been marked, make the switch
      and mark that a switch has been done: */
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      // Each time a switch is done, increase this count by 1:
      switchcount ++;
    } else {
      /* If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again. */
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
  return false;
}


function searchFilter(tableid, colNumber) {
  // Declare variables
  var input, filter, table, tr, td, i, txtValue, tablebody;
  input = document.getElementById("myInput-" + colNumber);
  filter = input.value.toUpperCase();
  table = document.getElementById(tableid);
  tablebody = table.getElementsByTagName("tbody")[0];
  tr = tablebody.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[colNumber];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
  return false;
}

function tableFilter(tableid, colNumber, filterValue) {
  // Declare variables
  var filter, table, tr, td, i, txtValue, tablebody;
  filter = filterValue.toUpperCase();
  table = document.getElementById(tableid);
  tablebody = table.getElementsByTagName("tbody")[0];
  tr = tablebody.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[colNumber];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (filter == "ALL") {
        tr[i].style.display = "";
      } else if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
  return false;
}
</script>
<section class="dashboards">
    <article>
      <div class="container-fluid">
        <div class="row">
            <div class="col-xl-10 offset-xl-1 item pd_mobile_cero">
                <div class="row">
                    <div class="col-lg-12 title_dash_item">
                        <h4><span class="icon-user"></span>Exports</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 item">
                        <div class="box_content box_content_bt_blue reset_height">
                        <a href="<?php echo home_url('dashboard');?>?action=export_students" class="btn btn-blue btn-sm ml-4" id="btnSubmit">Export all students</a>
                        <a href="<?php echo home_url('dashboard');?>?action=export_officials" class="btn btn-blue btn-sm ml-4" id="btnSubmit">Export all officials</a>
                        <a href="#" class="btn btn-blue btn-sm ml-4" id="btnSubmit">Export ...</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-10 offset-xl-1 item pd_mobile_cero">
                <div class="row">
                    <div class="col-lg-12 title_dash_item">
                        <h4><span class="icon-user"></span>Actions</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 item">
                        <div class="box_content box_content_bt_blue reset_height">
                        <a href="/dashboard/create-new-trial" class="btn btn-blue btn-sm ml-4" id="btnSubmit">Create new trial</a>
                        <a href="#" class="btn btn-blue btn-sm ml-4" id="btnSubmit">Create ...</a>
                        <a href="#" class="btn btn-blue btn-sm ml-4" id="btnSubmit">Create ...</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
          <div class="col-xl-10 offset-xl-1 item pd_mobile_cero">
              <?php 
                $today = date('m/d/Y');
                $args = array (
                    'post_type' => 'trials',
                    'order' => 'ASC',
                     'meta_query' => array(
                      //  array(
                      //      'key'		=> 'date_of_trial',
                      //      'compare'	=> '>',
                      //      'value'		=> $today,
                      //      'type' => 'DATE',
                      //  ),
                       array(
                         'key'		=> 'year',
                         'compare'	=> '>',
                         'value'		=> '2018',
                       )
                     ),
                );
                $trials = new WP_Query($args);
                if($trials->have_posts()): 
             ?>
                <div class="row">
                  <div class="col-lg-12 title_dash_item">
                    <h4><span class="icon-user"></span>Trials</h4>
                  </div>
                </div>
                
                <div class="row">
                        <div class="col-lg-12 list_striped">
                          <table id="trials" class="table table-striped dataTable" style="width:100%">
                            <thead>
                              <tr>
                                <th onclick="sortTable(0)">Team</th>
                                <th onclick="sortTable(1)">Year</th>
                                <th onclick="sortTable(2)">Sport</th>
                                <th onclick="sortTable(3)">Trial date(s)</th>
                                <th onclick="sortTable(4)">Trial venue</th>
                                <th onclick="sortTable(5)">Championship date</th>
                                <th onclick="sortTable(6)">Championship venue</th>
                                <th onclick="sortTable(7)">Official</th>
                                <th onclick="sortTable(8)">&nbsp;</th>
                              </tr>
                              <tr>
                                <td><input type="text" id="myInput-0" style="width:100% !important;" onkeyup="searchFilter('trials', 0)" placeholder=""></td>
                                <td><input type="text" id="myInput-1" style="width:100% !important;" onkeyup="searchFilter('trials', 1)" placeholder=""></td>
                                <td><input type="text" id="myInput-2" style="width:100% !important;" onkeyup="searchFilter('trials', 2)" placeholder=""></td>
                                <td><input type="text" id="myInput-3" style="width:100% !important;" onkeyup="searchFilter('trials', 3)" placeholder=""></td>
                                <td><input type="text" id="myInput-4" style="width:100% !important;" onkeyup="searchFilter('trials', 4)" placeholder=""></td>
                                <td><input type="text" id="myInput-5" style="width:100% !important;" onkeyup="searchFilter('trials', 5)" placeholder=""></td>
                                <td><input type="text" id="myInput-6" style="width:100% !important;" onkeyup="searchFilter('trials', 6)" placeholder=""></td>
                                <td><input type="text" id="myInput-7" style="width:100% !important;" onkeyup="searchFilter('trials', 7)" placeholder=""></td>
                                <td><input type="text" id="myInput-8" style="width:100% !important;" onkeyup="searchFilter('trials', 8)" placeholder=""></td>
                              </tr>
                            </thead>
                            <tbody>
                              <?php while($trials->have_posts()): $trials->the_post();?>
                                <tr>
                                  <td><a href="#" data-toggle="modal" data-target="#modal-trial" class="txt_dgb"><?php echo the_title();?></a></td>
                                  <td><a href="#" data-toggle="modal" data-target="#modal-trial" class="txt_dgb"><?php echo get_post_field('year');?></a></td>
                                  <td><a href="#" data-toggle="modal" data-target="#modal-trial" class="txt_dgb"><?php echo get_the_title(get_post_field('sport'));?></a></td>
                                  <td><a href="#" data-toggle="modal" data-target="#modal-trial" class="txt_dgb"><?php echo get_post_field('time_trial_from').' - '.get_post_field('time_trial_to');?><br/><?php echo get_post_field('date_of_trial').' - '.get_post_field('end_date_of_trial');?></a></td>
                                  <td><a href="#" data-toggle="modal" data-target="#modal-trial" class="txt_dgb"><?php echo get_post_field('venue');?></a></td>
                                  <td><a href="#" data-toggle="modal" data-target="#modal-trial" class="txt_dgb"><?php echo get_post_field('championship_date');?></a></td>
                                  <td><a href="#" data-toggle="modal" data-target="#modal-trial" class="txt_dgb"><?php echo get_post_field('championship_venue');?></a></td>
                                  <td><a href="#" data-toggle="modal" data-target="#modal-trial" class="txt_dgb"><?php echo get_author_name();?></a></td>
                                  <td><a href="<?php echo get_edit_post_link();?>" class="btn btn-blue btn-sm ml-4">Edit Trial</a></td>
                                </tr></a>
                              <?php endwhile;?>
                            </tbody>
                          </table>
                      </div>
                </div>
            <?php endif;?>
          </div>
        </div>
        <div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modal-trial">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content parent-modal">
                    <div class="modal-header">
                        <h3 class="text-center text-bold w-100 mb-0" data-modal-student="name"><?php echo $student->post_title;?></h3>
                    </div>
                    <div class="modal-body" data-modal-student="content">
                      <?php if(have_rows('students')): ?>
                        <div class="row">
                          <div class="col-lg-12 item">
                            <div class="box_content box_content_bt_blue reset_height">
                              <div class="row">
                                <a href="" onclick="tableFilter(<?php echo get_the_ID();?>,0,'all')" class="btn btn-blue btn-sm ml-4">Show all</a>
                                <a href="" onclick="tableFilter(<?php echo get_the_ID();?>,5,'approved')" class="btn btn-blue btn-sm ml-4">Approved</a>
                                <a href="" onclick="tableFilter(<?php echo get_the_ID();?>,6,'selected')" class="btn btn-blue btn-sm ml-4">Selected</a>
                                <a href="" onclick="tableFilter(<?php echo get_the_ID();?>,6,'not selected')" class="btn btn-blue btn-sm ml-4">Not selected</a>
                                <div class="col-lg-12 list_striped">
                                  <table id="<?php echo get_the_ID();?>" class="table table-striped dataTable" style="width:100%">
                                    <thead>
                                      <tr>
                                        <th onclick="sortTable(0)">Student Name</th>
                                        <th onclick="sortTable(1)">Age</th>
                                        <th onclick="sortTable(2)">Date of Birth</th>
                                        <th onclick="sortTable(3)">Gender</th>
                                        <th onclick="sortTable(4)">School</th>
                                        <th onclick="sortTable(5)">Approval Status</th>
                                        <th onclick="sortTable(6)">Selection Status</th>
                                        <th onclick="sortTable(7)">Amount outstanding</th>
                                      </tr>
                                      <tr>
                                        <td onclick="sortTable(0)"><input type="text" id="myInput-0" style="width:100% !important;" onkeyup="searchFilter(<?php echo get_the_ID();?>, 0)" placeholder=""></td>
                                        <td onclick="sortTable(1)"><input type="text" id="myInput-1" style="width:100% !important;" onkeyup="searchFilter(<?php echo get_the_ID();?>, 1)" placeholder=""></td>
                                        <td onclick="sortTable(2)"><input type="text" id="myInput-2" style="width:100% !important;" onkeyup="searchFilter(<?php echo get_the_ID();?>, 2)" placeholder=""></td>
                                        <td onclick="sortTable(3)"><input type="text" id="myInput-3" style="width:100% !important;" onkeyup="searchFilter(<?php echo get_the_ID();?>, 3)" placeholder=""></td>
                                        <td onclick="sortTable(4)"><input type="text" id="myInput-4" style="width:100% !important;" onkeyup="searchFilter(<?php echo get_the_ID();?>, 4)" placeholder=""></td>
                                        <td onclick="sortTable(5)"><input type="text" id="myInput-5" style="width:100% !important;" onkeyup="searchFilter(<?php echo get_the_ID();?>, 5)" placeholder=""></td>
                                        <td onclick="sortTable(6)"><input type="text" id="myInput-6" style="width:100% !important;" onkeyup="searchFilter(<?php echo get_the_ID();?>, 6)" placeholder=""></td>
                                        <td onclick="sortTable(7)"><input type="text" id="myInput-7" style="width:100% !important;" onkeyup="searchFilter(<?php echo get_the_ID();?>, 7)" placeholder=""></td>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    <?php while(have_rows('students')): the_row();?>
                                        <?php 
                                            $student = get_sub_field('student');
                                            $field_approval = get_sub_field_object('approval_status');
                                            $approval = get_sub_field('approval_status');
                                            $field_selectionstatus = get_sub_field_object('selection_status');
                                            $selectionstatus = get_sub_field('selection_status');
                                            $age = date_diff(date_create($student->date_of_birth), date_create('today'))->y;
                                            $school = get_post($student->school);
                                      ?>
                                        <?php if(isset($student->post_title)) :?>
                                      <tr>
                                        <td><a href="#" data-toggle="modal" data-target="#modal-student" class="txt_dgb"><?php echo $student->post_title;?></a></td>
                                        <td><a href="#" data-toggle="modal" data-target="#modal-student" class="txt_dgb"><?php echo $age;?></a></td>
                                        <td><a href="#" data-toggle="modal" data-target="#modal-student" class="txt_dgb"><?php echo $student->date_of_birth;?></a></td>
                                        <td><a href="#" data-toggle="modal" data-target="#modal-student" class="txt_dgb"><?php echo $student->gender;?></a></td>
                                        <td><a href="#" data-toggle="modal" data-target="#modal-student" class="txt_dgb"><?php echo $school->post_title;?></a></td>
                                        <td><a href="#" data-toggle="modal" data-target="#modal-student" class="txt_dgb"><?php echo @$field_approval['choices'][ $approval ];?></a></td>
                                        <td><a href="#" data-toggle="modal" data-target="#modal-student" class="txt_dgb"><?php echo @$field_selectionstatus['choices'][ $selectionstatus ];?></a></td>
                                        <td><a href="#" data-toggle="modal" data-target="#modal-student" class="txt_dgb">$0.00</a></td>
                                      </tr>
                                        <?php endif;?>
                                    <?php endwhile;?>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <?php else : ?>
                        <div class="row">
                          <div class="col-lg-12 item">
                            <div class="box_content box_content_bt_blue reset_height">
                              <div class="row">
                                <div class="col-lg-12 list_striped">
                                  <table id="example" class="table dataTable" style="width:100%">
                                      <tbody>
                                          <tr>
                                            <td align="center">There are no students enrolled in this trial</td>
                                          </tr>
                                    </tbody>  
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                          </div>
                        <?php endif;?>
                    </div>
                    <div class="modal-footer border-0 flex-wrap">
                        
                    </div>
                </div>
            </div>
        </div>    

        <div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modal-student">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content parent-modal">
                    <div class="modal-header">
                        <h3 class="text-center text-bold w-100 mb-0" data-modal-student="name"><?php echo $student->post_title;?></h3>
                    </div>
                    <div class="modal-body" data-modal-student="content">
                        <table id="example" class="table table-bordered inline-table border-bottom mb-4 mt-4" style="width:100%">
                            <tbody>
                                <tr>
                                    <th class="col-lg-3 col-4 text-muted">Student Name</th>
                                    <td class="col-lg-9 col-8" data-modal-student="name"><?php echo $student->post_title;?></td>
                                </tr>
                                 <tr>
                                    <th class="col-lg-3 col-4 text-muted">Date of Birth</th>
                                    <td class="col-lg-9 col-8" data-modal-student="birthday"><?php echo $student->date_of_birth;?> </td>
                                </tr>
                                <tr>
                                    <th class="col-lg-3 col-4 text-muted">Gender</th>
                                    <td class="col-lg-9 col-8" data-modal-student="gender"><?php echo $student->gender;?> </td>
                                </tr>
                                <tr>
                                    <th class="col-lg-3 col-4  text-muted">School</th>
                                    <td class="col-lg-9 col-8" data-modal-student="school"><?php echo $school->post_title;?></td>
                                </tr>
                                <tr>
                                    <th class="col-lg-3 col-4 text-muted">Parent</th>
                                    <?php $parent = get_user_by('id', $student->parent);?>
                                    <td class="col-lg-9 col-8" data-modal-student="parent"><?php echo $parent->first_name." ".$parent->last_name;?></td>
                                </tr>
                                <tr>
                                    <th class="col-lg-3 col-4  text-muted">Parent Email</th>
                                    <td class="col-lg-9 col-8" data-modal-student="school"><?php echo $parent->user_email;?></td>
                                </tr>
                                <tr>
                                    <th class="col-lg-3 col-4  text-muted">Parent Mobile</th>
                                    <td class="col-lg-9 col-8" data-modal-student="school"><?php echo $parent->mobile;?></td>
                                </tr>
                                <tr>
                                    <th class="col-lg-3 col-4  text-muted">Parent Phone</th>
                                    <td class="col-lg-9 col-8" data-modal-student="school"><?php echo $parent->phone."&nbsp;";?></td>
                                </tr>
                            </tbody>
                          </table>
                    </div>
                    <div class="modal-footer border-0 flex-wrap">
                        
                    </div>
                </div>
            </div>
        </div>    
      </div>
  </article>
</section>
