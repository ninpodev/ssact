 <div class="row">
    <div class="col-lg-12 list_striped">
        <table id="students-approved" class="table table-striped dataTable dt-responsive " style="width:100%">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Trial Name</th>
                    <th>Student Name</th>
                    <th></th>
                   
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center" colspan="5">Loading ...</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <th>Date</th>
                    <th>Trial Name</th>
                    <th>Student Name</th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>