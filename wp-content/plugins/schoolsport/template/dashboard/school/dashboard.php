<div class="row">
    <div class="col-xl-10 offset-xl-1 item pd_mobile_cero">
        <div class="row">
            <div class="col-lg-12 title_dash_item">
                <h4 class="mb-15"><span class="icon-user"></span>Pending Approvals</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 item">
                <div class="box_content box_content_bt_blue reset_height">
                     <?php echo school_sport_get_template('dashboard/school/list-pending-student');?>
                </div>
            </div>
        </div>
    </div>
</div>
<!--div class="row">
    <div class="col-xl-10 offset-xl-1 item pd_mobile_cero sb_bttm">
        <div class="row">
            <div class="col-lg-12 title_dash_item">
                <h4 class="mb-15"><span class="icon-user"></span>Upcoming absences</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 item">
                <div class="box_content box_content_bt_blue reset_height">
                    <?php echo school_sport_get_template('dashboard/school/list-approved-student');?>
                </div>
            </div>
        </div>
    </div>
</div--!>