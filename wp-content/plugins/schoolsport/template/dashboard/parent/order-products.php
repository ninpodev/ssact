
<?php
	global $wp;
	$url = explode('/', $wp->request);
	$trial = $url[count($url) - 1];
	$product_set = get_post_meta($trial, 'product_set')[0][0];
	$products = get_post_meta($product_set, 'products')[0];
	//Validar padre pertenece a alumno y trial tb.

?>
<section>
	<article class="steps_content bg_dash">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 offset-sm-4 col-md-8 offset-md-4 col-lg-8 offset-lg-4 col-xl-8 offset-xl-4 text-center stepper stepper_green four_items">
					<div class="row">
						<div class="col col-sm-3 item">
							<div id="step1" class="circle active"></div>
							<h6>Place Orders</h6>
						</div>
						<div  class="col col-sm-3 item">
							<div id="step2" class="circle"></div>
							<h6>Pay</h6>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
</section>
<section id="step-container-1">
	<article class="content_page bg_dash">
		<div class="container">
			<div class="row">
				<div class="col-md-10 offset-md-1 box_content box_content_bt_green reset_height">
					<!--<h3 class="title_products">Mandatory</h3>-->
					<form id="product-order-form" novalidate>
						<div class="row title_products_forms">
							<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-3">
								<p>Item</p>  
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-3 box">
								<p>Price</p>  
							</div>
							
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-3 box">
								<p>Size</p>  
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-3 box">
								<p>Quantity</p>  
							</div>
						</div>

						<?php foreach($products as $product_id) {
							$product = get_fields($product_id);
							$sizes = explode(',', $product['available_sizes']);

						?>
							<div class="row box_products products" data-id="<?php echo $product_id; ?>" data-price="<?= $product['price']; ?>">
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-3">
									<h4><?= $product['description']; ?></h4>
								</div>
								<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-3 box">
									<div class="monto_price">
										<p>$ <?= $product['price']; ?></p>
									</div>
								</div>
								<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-3 box">
									<select required class="form-control custom-select select_product">
										<?php foreach ($sizes as $size){ ?>
											<option value="<?php echo $size; ?>"><?php echo $size; ?></option>
										<?php }?>				
									</select>
								</div>
								<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-3 box">
									<input type="number" required class="form-control custom-select select_product product-qty" min="0" value="1" name="qty-<?php echo $product_id; ?>" id="qty-<?php echo $product_id; ?>">
								</div>
							</div>
							<?php } ?>
						
						<!--div class="alert alert-warning" role="alert">
							<i class="fas fa-exclamation-triangle"></i> This is a warning alert with <a href="#" class="alert-link">an example link</a>. Give it a click if you like.
						</div-->
						

						<div class="row justify-content-end mt-5 mb-5">
							<div class="col-6">
								<table class="table">
									<tbody>
										<tr>
											<td class="p-2 align-middle"><h4>Subtotal</h4></td>
											<td class="p-2 align-middle">&nbsp;</td>
											<td class="text-right p-2 align-middle"><h3 id="price-amount"></h3></td>
										</tr>
										<tr>
											<td class="p-2 align-middle"><h4>Cost</h4></td>
											<td class="p-2 align-middle">&nbsp;</td>
											<td class="text-right p-2 align-middle"><h3>$<input type="number" id="amount" min="0" max="25" value="25" style="width:80px; border-radius:5px; border:1px solid #dee2e6; padding-left: 5px; text-align:right"></h3></td>
										</tr>
										
									
									</tbody>
								</table>

							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 d-flex justify-content-between buttons_two_adds p_bb">
								<a href="<?php echo home_url('/dashboard');?>" type="submit" class="btn btn-success"><span class="icon-chevron-left"></span>Back to Dashboard</a>
								<button class="btn btn-success" id="btnSubmitOrder">Next Step</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</article>
</section>

<section id="step-container-2" style="display: none">
	<article class="content_page bg_dash">
		<div class="container">
			<div class="row">
				<div class="col-md-10 offset-md-1 box_content box_content_bt_green reset_height py-lg-0">
					<h3 class="text-center py-5 text-bold">Payment</h3>
					<div class="px-lg-5">
						<form id="payment-form"  rel="payment-products" autocomplete="off" onsubmit="" data-eway-encrypt-key="5U8+2Mu2JyOL2ywZzQ9ZqXUzS6W/7mFA8s/EHiOWykD32HSZgM4U+cq9ADwg2MpBdZsK4gMKSB2X+J2M2zUD1I10WDS+VOiDJTx1tB7MIhvOmY3OhGHppiHM5TNm8OCroW6IT5sAc4VS8MGgLLTP1I+PqyVx4bXku2ggCm1VBNExuEu3OJSILwjEmzVafvxiFQp4js/sKfqHa0drAMv+ln6GWqz0Z58e+9g3uC4d7m0JGBQGb+iiHJC7ACULrLHXiBndK98AKBtIlPqoCk3mmO/ahAVuvFVSdO6X414DBU3sydvSXn26gJf9cv3CT/z47uPpvrt8yd+06f21XD9f3w==">
							<div class="row">
								<div class="col-lg-12">
									<?php echo school_sport_get_template('component/notice', ['class' => 'mb-5']);?>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="mb-5">
										<p>
											You will pay $ <span id="amount-to-pay"></span>
										</p>
									</div>
									<div class="w-100 mb-5">
										<img class="d-inline-block mx-auto eway w-75" src="<?php echo get_bloginfo('template_url');?>/images/eway.png" alt="eway">
									</div>
									<!--a href="#" class="d-inline-block grey-link" data-toggle="modal" data-target="#payment-modal"><u>I'll pay offline</u></a-->
								</div>
								<div class="col-md-6">
								
										<div class="row">
											<div class="control-group col-12 pb-lg-4 py-0 forms_box">
												<div class="form-group floating-label-form-group controls">
													<label for="card_number" class="text-bold">Credit Card Holder Name<span class="obligatory_field">*</span></label>
													<input type="text" class="form-control" placeholder="Name" id="card_name" name="card_name" required="" data-validation-required-message="Please enter the <b>Credit Card Holder Name</b>." aria-invalid="false">                                        
													<p class="help-block text-danger"></p>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="control-group col-12 pb-lg-4 py-0 forms_box">
												<div class="form-group floating-label-form-group controls">
													<label for="card_number" class="text-bold">Credit Card Number<span class="obligatory_field">*</span></label>
													<input type="text" class="form-control" placeholder="**** **** **** ****" id="card_number" name="card_number" required="" data-validation-required-message="Please enter the <b>Credit Card Number</b>." aria-invalid="false">                                        
													<p class="help-block text-danger"></p>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="control-group col-6 pb-lg-4 py-0 forms_box">
												<div class="form-group floating-label-form-group controls">
													<label for="card_date" class="text-bold">Expiration Date<span class="obligatory_field">*</span></label>
													<input type="text" class="form-control" placeholder="01/24" id="card_date" name="card_date" required="" data-validation-required-message="Please enter the <b>Expiration Date</b>." aria-invalid="false">                                        
													<p class="help-block text-danger"></p>
												</div>
											</div>   
											<div class="control-group col-6 pb-lg-4 py-0 forms_box">
												<div class="form-group floating-label-form-group controls">
													<label for="card_cvv" class="text-bold">CVV<span class="obligatory_field">*</span></label>
													<input type="text" class="form-control" placeholder="***" id="card_cvv" name="card_cvv" required="" data-validation-required-message="Please enter the <b>CVV</b>." aria-invalid="false">                                       
													<p class="help-block text-danger"></p>
												</div>
											</div>                                        
										</div>
										<input type="hidden" name="trial" value="<?php echo $trial;?>" >
										<input type="hidden" name="student" value="<?php echo $student;?>">
										<button type="submit" class="btn btn-success lg-green px-5 py-2 text-bold" id="go-eway">Pay <span id="amount-to-pay"></span> using eway</button>
								</div>
							</div>
							<div class="my-4"><div class="py-4"></div></div>
							<div class="text-center py-4">
								<a href="<?php echo home_url('/dashboard');?>" class="cancel-process d-inline-block mx-auto">Cancel process and back to Dashboard</a>
							</div>
						</form>
					</div>
				</div> 
			</div>
		</div>
	</article>
</section>



