<?php   
    $student  = get_query_var('student');
    $trial_slug  = get_query_var('trial_slug');
    $page = get_posts( array( 'name' => $trial_slug, 'post_type' => 'any', 'posts_per_page'=> 1) );
    $page = current($page);
    if(isset($page->ID)) {
        $trial = $page->ID;
?>
    <section class="step">
        <article class="content_page bg_dash">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 offset-md-1 box_content box_content_bt_green reset_height py-lg-0">
                        <h3 class="text-center py-5 text-bold">Payment</h3>
                        <div class="px-lg-5">
                            <form id="payment-form"  rel="payment-fee" autocomplete="off" onsubmit="" data-eway-encrypt-key="5U8+2Mu2JyOL2ywZzQ9ZqXUzS6W/7mFA8s/EHiOWykD32HSZgM4U+cq9ADwg2MpBdZsK4gMKSB2X+J2M2zUD1I10WDS+VOiDJTx1tB7MIhvOmY3OhGHppiHM5TNm8OCroW6IT5sAc4VS8MGgLLTP1I+PqyVx4bXku2ggCm1VBNExuEu3OJSILwjEmzVafvxiFQp4js/sKfqHa0drAMv+ln6GWqz0Z58e+9g3uC4d7m0JGBQGb+iiHJC7ACULrLHXiBndK98AKBtIlPqoCk3mmO/ahAVuvFVSdO6X414DBU3sydvSXn26gJf9cv3CT/z47uPpvrt8yd+06f21XD9f3w==">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <?php echo school_sport_get_template('component/notice', ['class' => 'mb-5']);?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-5">
                                            <p>
                                               Lorem Ipsum $200 dolor sit amet
                                            </p>
                                        </div>
                                        <div class="w-100 mb-5">
                                            <img class="d-inline-block mx-auto eway w-75" src="<?php echo get_bloginfo('template_url');?>/images/eway.png" alt="eway">
                                        </div>
                                        <!--a href="#" class="d-inline-block grey-link" data-toggle="modal" data-target="#payment-modal"><u>I'll pay offline</u></a-->
                                    </div>
                                    <div class="col-md-6">
                                    
                                            <div class="row">
                                                <div class="control-group col-12 pb-lg-4 py-0 forms_box">
                                                    <div class="form-group floating-label-form-group controls">
                                                        <label for="card_number" class="text-bold">Credit Card Holder Name<span class="obligatory_field">*</span></label>
                                                        <input type="text" class="form-control" placeholder="Name" id="card_name" name="card_name" required="" data-validation-required-message="Please enter the <b>Credit Card Holder Name</b>." aria-invalid="false">                                        
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="control-group col-12 pb-lg-4 py-0 forms_box">
                                                    <div class="form-group floating-label-form-group controls">
                                                        <label for="card_number" class="text-bold">Credit Card Number<span class="obligatory_field">*</span></label>
                                                        <input type="text" class="form-control" placeholder="**** **** **** ****" id="card_number" name="card_number" required="" data-validation-required-message="Please enter the <b>Credit Card Number</b>." aria-invalid="false">                                        
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="control-group col-6 pb-lg-4 py-0 forms_box">
                                                    <div class="form-group floating-label-form-group controls">
                                                        <label for="card_date" class="text-bold">Expiration Date<span class="obligatory_field">*</span></label>
                                                        <input type="text" class="form-control" placeholder="01/24" id="card_date" name="card_date" required="" data-validation-required-message="Please enter the <b>Expiration Date</b>." aria-invalid="false">                                        
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>   
                                                <div class="control-group col-6 pb-lg-4 py-0 forms_box">
                                                    <div class="form-group floating-label-form-group controls">
                                                        <label for="card_cvv" class="text-bold">CVV<span class="obligatory_field">*</span></label>
                                                        <input type="text" class="form-control" placeholder="***" id="card_cvv" name="card_cvv" required="" data-validation-required-message="Please enter the <b>CVV</b>." aria-invalid="false">                                       
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>                                        
                                            </div>
                                            <input type="hidden" name="trial" value="<?php echo $trial;?>" >
                                            <input type="hidden" name="student" value="<?php echo $student;?>">
                                            <button type="submit" class="btn btn-success lg-green px-5 py-2 text-bold" id="go-eway">Pay $200 using eway</button>
                                        
                                        <!-- <a href="http://www.ninpodesign.com/ssact/?p=520" class="blue-link mt-4 d-inline-block"><small>[Go to error page]</small></a> -->
                                    </div>
                                </div>
                                <div class="my-4"><div class="py-4"></div></div>
                                <div class="text-center py-4">
                                    <a href="<?php echo home_url('/');?>" class="cancel-process d-inline-block mx-auto">Cancel process and back to Dashboard</a>
                                </div>
                            </form>
                        </div>
                    </div> 
                </div>
            </div>
        </article>
    </section>
<?php } ?>