<div class="steps">
    <section>
        <article class="steps_content bg_dash">
            <div class="container">
                <div class="row">
                    <div class="col-md-11 offset-md-1 col-xl-10 offset-xl-1 text-center stepper stepper_green four_items">
                        <div class="row justify-content-center">
                            <div class="col col-sm-3 item">
                                <div class="circle active" data-step="progress"></div>
                                <h6 class="text-center">Basic Information</h6>
                            </div>
                            <div class="col col-sm-3 item">
                                <div class="circle" data-step="progress"></div>
                                <h6 class="text-center">Additional Information</h6>
                            </div>
                            <div class="col col-sm-3 item">
                                <div class="circle" data-step="progress"></div>
                                <h6 class="text-center">Payment</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </section>

    <section class="step" >
        <article class="content_page bg_dash">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 offset-md-1 box_content box_content_bt_green reset_height py-lg-0">
                        <form id="add-student-trial-1" novalidate="" rel="register-student" autocomplete="off">
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php echo school_sport_get_template('component/notice', ['class' => 'mt-3']);?>
                                </div>
                            </div>
                            <div class="row border_bottom_forms_fields">
                                <div class="control-group col-sm-12 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="trial_name">Trial Selected<span class="obligatory_field">*</span></label>
                                        <select required="" class="form-control custom-select" id="trial_name" name="trial" data-validation-required-message="Please select the <b>Trial.</b>" aria-invalid="false">
                                            <option value="" selected="">Choose...</option>
                                            <?php $trials = new WP_Query(['post_type' => 'trials', 'posts_per_page' => -1, 'meta_query' => [['key' => 'date_of_trial', 'compare' => '>=', 'value' =>  date('Ymd'), 'type' => 'DATE']]]);?>
                                            <?php while($trials->have_posts()): $trials->the_post();?>
                                            <option value="<?php the_ID();?>"><?php the_title();?></option>
                                            <?php endwhile;?>
                                        </select>
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row border_bottom_forms_fields">
                                <div class="control-group col-sm-12 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="student_name">Select Student<span class="obligatory_field">*</span></label>
                                        <?php 
                                            $current_user = get_current_user_id();
                                            $students = new WP_Query(['post_type' => 'students', 'meta_key' => 'parent', 'meta_value' => $current_user]); 
                                        ?>
                                        <select required="" class="form-control custom-select" id="student_name" name="student" data-validation-required-message="Please select the <b>Student.</b>" aria-invalid="false">
                                            <option value="" selected="">Student Name</option>
                                           <!--
                                            <?php while($students->have_posts()) : $students->the_post();?>
                                            <option value="<?php the_ID();?>"><?php the_title();?></option>
                                            <?php endwhile;?>-->
                                        </select>
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 text-center forms_box">
                                    <div class="row">
                                        <div class="col-lg-12 d-flex justify-content-between buttons_two_adds">
                                            <a href="<?php echo home_url('/dashboard');?>" class="btn btn-success"><span class="icon-chevron-left"></span>Back to Dashboard</a>
                                            <button type="submit" class="btn btn-success">Next Step</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </article>
    </section>

    <section  class="step"  style="display:none;">
        <article class="content_page bg_dash">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 offset-md-1 box_content box_content_bt_green reset_height py-lg-0">
                        <form id="add-student-trial-2" novalidate="" rel="register-information-student" autocomplete="off">
                            <div class="row">
                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_history">Student Playing History<span class="obligatory_field">*</span></label>
                                        <textarea class="form-control" name="playing_history" id="txt_history" rows="5" required="" data-validation-required-message="Please ensure this information is up to date." spellcheck="false" aria-invalid="false"></textarea>                                        
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_experience">Representative Experience<span class="obligatory_field">*</span></label>
                                        <textarea class="form-control" name="representative_experience" id="txt_experience" rows="5" required="" data-validation-required-message="Please ensure this information is up to date." spellcheck="false" aria-invalid="false"></textarea>                                        
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="control-group col-12 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_position">Prefered Position<span class="obligatory_field">*</span></label>
                                        <input type="text" class="form-control" placeholder="Position Name" id="txt_position" name="prefered_position" required="" data-validation-required-message="Please enter the <b> Prefered Position</b>." aria-invalid="false">                                        
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>                                
                            </div> 
                            <div class="row border_bottom_forms_fields">
                                <div class="control-group col-sm-12 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <div class="form-check d-flex justify-content-between w-100 pl-3 pr-5 pr-md-3 py-2 bg-light">
                                            <label class="form-check-label" for="info-correct">I confirm that the above information is correct. <span class="obligatory_field">*</span></label>
                                            <input class="form-check-input check-right" required="" data-validation-required-message="Missing required checkbox." type="checkbox" id="info-correct" value="check-1">
                                        </div>
                                        <div class="form-check d-flex justify-content-between w-100 pl-3 pr-5 pr-md-3 py-2">
                                            <label class="form-check-label" for="agree-conduct">I agree to abide by the School Sport ACT&nbsp;<a class="green-link" href="/wp-content/uploads/2020/02/Codes-of-Conduct-for-SSACT-Team-Members-and-Parents.pdf">Codes of conduct.</a> <span class="obligatory_field">*</span></label>
                                            <input class="form-check-input check-right" required="" data-validation-required-message="Missing required checkbox." type="checkbox" id="agree-conduct" value="check-2">
                                        </div>
                                        <div class="form-check d-flex justify-content-between w-100 pl-3 pr-5 pr-md-3 py-2 bg-light">
                                            <label class="form-check-label" for="agree-acceptance">I have read and agree to the School Sport ACT&nbsp;<a class="green-link" href="/wp-content/uploads/2020/02/SSACT-State-Representative-Team-Acceptance-Policy.pdf">State Representative Team Acceptance Policy.</a> <span class="obligatory_field">*</span></label>
                                            <input class="form-check-input check-right" required="" data-validation-required-message="Missing required checkbox." type="checkbox" id="agree-acceptance" value="check-3">
                                        </div>
                                        <div class="form-check d-flex justify-content-between w-100 pl-3 pr-5 pr-md-3 py-2 flex-wrap bg-light">
                                            <label class="form-check-label text-bold d-inline pr-4" for="print-consent">
                                            	I give consent for my student�s name to appear in <b>print.</b> 
                                            </label>
                                            <input class="form-check-input check-right" name="print_consent" type="checkbox" id="print-consent" value="no">
                                            <small class="pr-4">(This means your child�s name may appear in the team announcement on Facebook, Championship Program, individual sport draws, on merchandise, print media coverage nor be visible in any results announcements)</small>
                                        </div>
                                        <div class="form-check d-flex justify-content-between w-100 pl-3 pr-5 pr-md-3 py-2 flex-wrap">
                                            <label class="form-check-label text-bold d-inline pr-4" for="image-consent">
                                            	I give consent for my student�s <b>image</b> to be taken, recorded and used for SSACT or School Sport Australia purposes.    
                                            </label>
                                            <input class="form-check-input check-right" type="checkbox" name="image_consent" id="image-consent" value="no">
                                            <small class="pr-4">(This means that your child�s image may appear on the SSACT website, SSACT Facebook page when sharing results and in any external media coverage)</small>
                                        </div>
                                        <div class="form-check d-flex justify-content-between w-100 pl-3 pr-5 pr-md-3 py-2 flex-wrap bg-light">
                                            <label class="form-check-label text-bold d-inline pr-4" for="recorded-consent">
                                            	I give consent for my student to be <b>recorded</b> during the Championship.  
                                            </label>
                                            <input class="form-check-input check-right" name="recorded_consent" type="checkbox" id="recorded-consent" value="no">
                                            <small class="pr-4">(This means your child may be present in any video footage for memento or judicial purposes or any live streaming of an event or game played within the Championship. Note all approved live streaming is restricted to the School Sport Australia YouTube Channel or external channel shared within Championship Bulletins and approved by the host state)</small>
                                        </div>
                                        <p class="help-block text-danger mt-2"></p>                              
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 text-center forms_box">
                                    <div class="row">
                                        <div class="col-lg-12 d-flex justify-content-between buttons_two_adds">
                                            <a href="#" data-step-go="1" type="submit" class="btn btn-success"><span class="icon-chevron-left"></span>Back to Step 1</a>
                                            <button type="submit" class="btn btn-success">Next Step</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </article>
    </section>


    <section class="step" style="display:none;">
        <article class="content_page bg_dash">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 offset-md-1 box_content box_content_bt_green reset_height py-lg-0">
                        <h3 class="text-center py-5 text-bold">Payment</h3>
                        <div class="px-lg-5">
                            <form id="payment-form"  rel="payment-student" onsubmit="" data-eway-encrypt-key="5U8+2Mu2JyOL2ywZzQ9ZqXUzS6W/7mFA8s/EHiOWykD32HSZgM4U+cq9ADwg2MpBdZsK4gMKSB2X+J2M2zUD1I10WDS+VOiDJTx1tB7MIhvOmY3OhGHppiHM5TNm8OCroW6IT5sAc4VS8MGgLLTP1I+PqyVx4bXku2ggCm1VBNExuEu3OJSILwjEmzVafvxiFQp4js/sKfqHa0drAMv+ln6GWqz0Z58e+9g3uC4d7m0JGBQGb+iiHJC7ACULrLHXiBndK98AKBtIlPqoCk3mmO/ahAVuvFVSdO6X414DBU3sydvSXn26gJf9cv3CT/z47uPpvrt8yd+06f21XD9f3w==">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <?php echo school_sport_get_template('component/notice', ['class' => 'mb-5']);?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="mb-5">
                                            To register a student to trial, a payment of <strong>$10</strong> is required via credit card to cover the costs of administration of data and the cost of venue where appropriate.
                                        </p>
                                        <div class="w-100 mb-5">
                                            <img class="d-inline-block mx-auto eway w-75" src="<?php echo get_bloginfo('template_url');?>/images/eway.png" alt="eway">
                                        </div>
                                        <!--a href="#" class="d-inline-block grey-link" data-toggle="modal" data-target="#payment-modal"><u>I'll pay offline</u></a-->
                                    </div>
                                    <div class="col-md-6">
                                    
                                            <div class="row">
                                                <div class="control-group col-12 pb-lg-4 py-0 forms_box">
                                                    <div class="form-group floating-label-form-group controls">
                                                        <label for="card_number" class="text-bold">Credit Card Holder Name<span class="obligatory_field">*</span></label>
                                                        <input type="text" class="form-control" placeholder="Name" id="card_name" name="card_name" required="" data-validation-required-message="Please enter the <b>Credit Card Holder Name</b>." aria-invalid="false">                                        
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="control-group col-12 pb-lg-4 py-0 forms_box">
                                                    <div class="form-group floating-label-form-group controls">
                                                        <label for="card_number" class="text-bold">Credit Card Number<span class="obligatory_field">*</span></label>
                                                        <input type="text" class="form-control" placeholder="**** **** **** ****" id="card_number" name="card_number" required="" data-validation-required-message="Please enter the <b>Credit Card Number</b>." aria-invalid="false">                                        
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="control-group col-6 pb-lg-4 py-0 forms_box">
                                                    <div class="form-group floating-label-form-group controls">
                                                        <label for="card_date" class="text-bold">Expiration Date<span class="obligatory_field">*</span></label>
                                                        <input type="text" class="form-control" placeholder="01/24" id="card_date" name="card_date" required="" data-validation-required-message="Please enter the <b>Expiration Date</b>." aria-invalid="false">                                        
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>   
                                                <div class="control-group col-6 pb-lg-4 py-0 forms_box">
                                                    <div class="form-group floating-label-form-group controls">
                                                        <label for="card_cvv" class="text-bold">CVV<span class="obligatory_field">*</span></label>
                                                        <input type="text" class="form-control" placeholder="***" id="card_cvv" name="card_cvv" required="" data-validation-required-message="Please enter the <b>CVV</b>." aria-invalid="false">                                       
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>                                        
                                            </div>
                                            <button type="submit" class="btn btn-success lg-green px-5 py-2 text-bold" id="go-eway">Pay $10 using eway</button>
                                        
                                        <!-- <a href="http://www.ninpodesign.com/ssact/?p=520" class="blue-link mt-4 d-inline-block"><small>[Go to error page]</small></a> -->
                                    </div>
                                </div>
                                <div class="my-4"><div class="py-4"></div></div>
                                <div class="text-center py-4">
                                    <a href="<?php echo home_url('/dashboard');?>" class="cancel-process d-inline-block mx-auto">Cancel process and back to Dashboard</a>
                                </div>
                            </form>
                        </div>
                    </div> 
                </div>
            </div>
        </article>
    </section>
    

    <section  class="step" style="display:none;">
        <article class="content_page bg_dash">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 offset-md-1 box_content box_content_bt_green reset_height py-lg-0 px-lg-5">
                        <h3 class="text-center py-5 text-bold">Payment</h3>
                        <p class="text-center mb-4">
                            This message is an automated confirmation of your successful trial registration, with payment, into the School Sport ACT website. 
                        </p>
                        <p class="text-center mb-4">
                            You�ll receive in your registered email all the payment details and welcome messages.<br>Your information has been received, you can monitor the status of your selected event�s progress at your dashboard.
                        </p>
                        <!-- <a href="http://www.ninpodesign.com/ssact/html/payment_success_mail.html" class="blue-link mt-4 d-block text-center"><small>[Go to Email Page]</small></a> -->
                        <div class="my-4"><div class="py-4"></div></div>
                        <div class="text-center py-4">
                            <a href="<?php echo home_url('/dashboard');?>" class="d-inline-block btn btn-success lg-green px-5 py-2 text-bold">Back to my dashboard</a>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </section>
  
</div>