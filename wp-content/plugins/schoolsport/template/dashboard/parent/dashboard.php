
<section class="dashboards">
    <article>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-10 item">
                        <div class="row">
                            <div class="col-lg-12 title_dash_item">
                                <h4><span class="icon-user"></span>Your Details</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 item">
                                <div class="box_content box_content_bt_green reset_height py-4 px-3">
                                    <?php echo school_sport_get_template('dashboard/component/user-detail');?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                    $current_user = get_current_user_id();
                    $students = new WP_Query(['post_type'=> 'students', 'posts_per_page'=>-1, 'meta_key'=> 'parent', 'meta_value' => $current_user]);
                    if($students->have_posts()):
                ?>
                <div class="row justify-content-center">
                    <div class="col-xl-10">
                        <div class="row">
                            <div class="col-lg-12 title_dash_item">
                                <h4><span class="icon-user"></span>Your registered students (<?php echo $students->found_posts;?>)</h4>
                            </div>
                        </div>
                    </div>
                </div>
                
              
              <div class="row justify-content-center">
                  <div class="col-xl-10 item">
                      <div class="row">
                          <?php while($students->have_posts()): $students->the_post();?>
                           <?php $student_id = get_the_ID();?>
                           <?php  $trials = new WP_Query(['post_type'=> 'trials', 'meta_key' => 'students_#index#_student', 'meta_value'	=>  $student_id ]);?>
                          <div class="col-12 item">
                              <div class="box_content box_content_bt_green reset_height py-4 px-3">
                                  <div class="row border-bottom">
                                      <div class="col-12 d-flex justify-content-between align-items-center pb-4">
                                          <h3 class="text-normal mb-0"><?php the_title();?></h3>
                                          <a href="<?php echo home_url('dashboard/edit-student/'.get_the_ID());?>" class="blue-link text-bold"><u>Edit profile</u></a>
                                      </div>
                                  </div>
                                <?php if($trials->have_posts()):?>
                                <?php $first_element = true;?>
                                  <?php while($trials->have_posts()): $trials->the_post();?>
                                  <div class="row">
                                      <div class="col-12 pt-4 d-flex justify-content-between align-items-center flex-wrap">
                                        <div class="col-md-6 px-0">
                                            <h4 class="text-bold"><?php the_title();?></h4>
                                            <?php 
                                                $trial_id = get_the_ID();
                                                $date_of_trial = get_field('date_of_trial');
                                                $date_trial =  DateTime::createFromFormat('d/m/Y', $date_of_trial);
                                                $now    = new DateTime();
                                                $students_trial = get_field('students');
                                                $student_data =  school_sport_seach_student_on_repeater($student_id, $students_trial);
                                            ?>
                                            <?php if($date_trial && $now < $date_trial) {?>
                                            <p class="mb-0"><b>Date of Trial</b>
                                                <span class="text_data ml-4"><?php echo $date_trial->format('dS M Y');?> </span> 
                                                 <?php if($now < $date_trial) { ?>
                                                <small class="d-inline-block ml-4">Trial Finished</small>
                                                <?php } ?>
                                            </p>   
                                            <?php } ?>
						                    <!--p class="mb-0"><b>Date of Championship</b>
                                                <span class="text_data ml-4"><?php //echo $champ_date?> </span>
                                            </p-->                                        		
                                        </div>
                                       
                                        <div class="col-md-6 px-0 text-center text-lg-right pt-4 pt-lg-0">
                                            <?php if($first_element) {?>
                                            <?php $first_element = false;?>
                                            <!--a href="<?php echo home_url('dashboard/add-student-to-trial');?>" class="btn btn-success btn-auto px-4 py-2 text-bold d-inline-block h-auto">Register for a new trial</a-->
                                            <?php } ?>
                                            <?php  if($now > $date_trial && $student_data['approval_status'] == 'approved' && $student_data['selection_status'] == 'selected' && !$student_data['payment_initial']) {?>
                                             <a href="<?php echo school_sport_get_url_payment_fee_student($trial_id, $student_id);?>" class="btn btn-success btn-auto px-4 py-2 text-bold d-inline-block h-auto">Pay Fee</a>                                           
                                            <?php } ?>
                                        </div>
                                      
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-12 pt-4 pb-4 text-center">
                                          <div class="reg-step <?php if($student_data['approval_status'] == 'approved') { ?> complete <?php } ?> d-inline-block py-2 px-2">1. Registered</div>
                                          <div class="reg-step d-inline-block py-2 px-2 <?php if($now > $date_trial && $student_data['approval_status'] == 'approved') { ?> complete <?php } ?>">2. Trial</div>
                                          <div class="reg-step d-inline-block py-2 px-2 <?php if($now > $date_trial && $student_data['approval_status'] == 'approved' && $student_data['selection_status'] == 'selected') { ?> complete <?php } ?>">3. Team Selection</div>
                                          <div class="reg-step d-inline-block py-2 px-2 <?php if($now > $date_trial && $student_data['approval_status'] == 'approved' && $student_data['selection_status'] == 'selected' && $student_data['payment_initial'] ) { ?> complete <?php } ?>" >4. Uniform Order</div>
                                          <div class="reg-step d-inline-block py-2 px-2">5. Event Fees</div>
                                      </div>
                                  </div>
                                  <div class="row border-top">
                                      <div class="col-12 pt-4 text-center">
                                          <a href="<?php echo home_url('dashboard/add-student-to-trial');?>" class="btn btn-success btn-auto px-4 py-2 text-bold d-inline-block h-auto">Register for a Trial</a>
                                      </div>
                                  </div>  
                                <?php endwhile;?>
                              <?php else : ?>
                                <div class="row">
                                      <div class="col-12 pt-4 text-center">
                                          <a href="<?php echo home_url('dashboard/add-student-to-trial');?>" class="btn btn-success btn-auto px-4 py-2 text-bold d-inline-block h-auto">Register for a Trial</a>
                                      </div>
                                  </div>
                              <?php endif;?>
                              </div>
                          </div>
                        <?php endwhile;?> 
                      </div>
                    </div>
                </div>
                <?php endif;?>
            </div>
        
    </article>
</section>