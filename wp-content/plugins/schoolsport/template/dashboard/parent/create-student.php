<div class="steps">
    <section>
        <article class="steps_content bg_dash">
            <div class="container">
                <div class="row">
                    <div class="col-md-11 offset-md-1 col-xl-10 offset-xl-1 text-center stepper stepper_green four_items">
                        <div class="row">
                            <div class="col col-sm-3 item">
                                <div class="circle active" data-step="progress"></div>
                                <h6>Basic Information</h6>
                            </div>
                            <div class="col col-sm-3 item">
                                <div class="circle"  data-step="progress"></div>
                                <h6>Medical Information</h6>
                            </div>
                            <div class="col col-sm-3 item">
                                <div class="circle"  data-step="progress"></div>
                                <h6>Emergency Contact</h6>
                            </div>
                            <div class="col col-sm-3 item">
                                <div class="circle"  data-step="progress"></div>
                                <h6>Summary.</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </section>
    <section  class="step" >
        <article class="content_page bg_dash">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 offset-md-1 box_content box_content_bt_green reset_height">
                        <form id="editDetailsForm" novalidate="" rel="create-student" autocomplete="off" onsubmit="return false;">
                            <div class="row border_bottom_forms_fields">
                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_fname">First Name <span class="obligatory_field">*</span></label>
                                        <input type="text" class="form-control" placeholder="First Name" id="txt_fname" name="fname" required="" data-validation-required-message="Please enter the <b> First Name of Student</b>.">
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_lname">Last Name <span class="obligatory_field">*</span></label>
                                        <input type="text" class="form-control" placeholder="Last Name" id="txt_lname" name="lname" required="" data-validation-required-message="Please enter the <b> Last Name of Student</b>.">
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row border_bottom_forms_fields">
                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_date_birth">Date of Birth <span class="obligatory_field">*</span></label>
                                        <input type="text" class="form-control datepicker-here" placeholder="Date" id="txt_date_birth" name="date_of_birth" data-language="en" required="" data-validation-required-message="Please enter the <b> Date of Birth </b>." aria-invalid="false">
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_gender">Gender <span class="obligatory_field">*</span></label>
                                        <select required="" class="form-control custom-select" id="txt_gender" name="gender" data-validation-required-message="Please select the <b> Gender</b>." aria-invalid="false">
                                            <option value="" selected="">Choose...</option>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row border_bottom_forms_fields">
                                <div class="control-group col-sm-12 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_school">School <span class="obligatory_field">*</span></label>
                                        <select required="" class="form-control custom-select" id="txt_school" name="school" data-validation-required-message="Please select the <b> School</b>." aria-invalid="false">
                                            <option value="" selected="">Choose...</option>
                                            <?php $school = new WP_Query(['post_type' => 'schools', 'posts_per_page' => -1]);?>
                                            <?php while($school->have_posts()): $school->the_post();?>
                                            <option value="<?php the_ID();?>"><?php the_title();?></option>
                                            <?php endwhile;?>
                                        </select>
                                        <span class="txt_under_fields">To ensure that this student can be approved for a team, please make sure their school is up to date. For more info, please see our Acceptance Process.</span>
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 text-center forms_box">
                                    <div id="success"></div>
                                    <input type="hidden" name="id_form" id="id_form" value="trial1Form">
                                    <div class="row">
                                        <div class="col-lg-12 d-flex justify-content-between buttons_two_adds">
                                            <a href="<?php echo home_url('/dashboard');?>" type="submit" class="btn btn-success"><span class="icon-chevron-left"></span>Back to Dashboard</a>
                                            <button type="submit" class="btn btn-success" id="btnSubmit">Next Step</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </article>
    </section>

    <section class="step" style="display:none;">
        <article class="content_page bg_dash">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10 box_content box_content_bt_green reset_height py-lg-0">
                        <form id="add-student-step-2" novalidate  autocomplete="off" onsubmit="return false;" rel='medical-student'>
                            <div class="row border_bottom_forms_fields">
                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_conditions" class="text-bold">Medical Conditions<span class="obligatory_field">*</span></label>
                                        <textarea class="form-control" name="medical_condition" id="txt_conditions" rows="5" required data-validation-required-message="Please complete the field." spellcheck="false" aria-invalid="false"></textarea> 
                                        <span class="txt_under_fields mt-2">Please ensure this information is up to date.</span>                                       
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_allergies" class="text-bold">Allergies<span class="obligatory_field">*</span></label>
                                        <textarea class="form-control" name="allergies" id="txt_allergies" rows="5" required data-validation-required-message="Please ensure the field." spellcheck="false" aria-invalid="false"></textarea>  
                                        <span class="txt_under_fields mt-2">Please ensure this information is up to date.</span>                                      
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row border_bottom_forms_fields">
                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_date_tetanus" class="text-bold">Date of last Tetanus Shot <span class="obligatory_field">*</span></label>
                                        <input type="text" class="form-control datepicker-here" placeholder="Date" id="txt_date_tetanus" name="date_last_tetanus" data-language='en' required data-validation-required-message="Please enter the date of the last <b>Tetanus Shot</b>.">
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
                                    <div class="form-group floating-label-form-group controls"> 
                                        <label for="txt_medicar" class="text-bold">Medicare Number <span class="obligatory_field">*</span></label>
                                        <input type="number" class="form-control" placeholder="Enter your 10 digit number" id="txt_medicar" name="medical_number" required data-validation-required-message="Please enter phone number for <b>Medicar</b>." aria-invalid="false">                                                                            
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row border_bottom_forms_fields">
                                <div class="col-lg-12 d-flex justify-content-between buttons_two_adds py-4">
                                    <a href="#" data-step-go="1"  class="btn btn-success text-bold"><span class="icon-chevron-left"></span>Back to Step 01</a>
                                    <!--<button type="submit" class="btn btn-success" id="btnSubmit">Next Step</button>-->
                                    <button type="submit" class="btn btn-success text-bold" >Next Step</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 py-4">
                                    <a href="<?php echo esc_url( home_url( '/dashboard' ) ); ?>?p=115" class="cancel-process d-inline-block mx-auto">Cancel process and back to Dashboard</a>
                                </div>
                            </div>                            
                        </form>
                    </div>
                </div>
            </div>
        </article>
    </section>
    <section class="step" style="display:none;">
        <article class="content_page bg_dash">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10 box_content box_content_bt_green reset_height py-lg-0">
                        <form id="add-student-step-3"  autocomplete="off" onsubmit="return false;" novalidate rel='emergency-student'>
                            <div class="row border_bottom_forms_fields">
                                <div class="control-group col-12 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_preferred">Name of Preferred Emergency Contact<span class="obligatory_field">*</span></label>
                                        <input type="text" name="name_of_preferred_emergency" class="form-control" placeholder="Contact Name" id="txt_preferred"  required data-validation-required-message="Please enter the <b> Prefered Emergency Contact</b>." aria-invalid="false">                                        
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>                             
                            </div>
                            <div class="row border_bottom_forms_fields">
                                <div class="control-group col-sm-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_relationship" class="text-bold">Relationship <span class="obligatory_field">*</span></label>
                                        <input type="text" class="form-control" placeholder="Father" id="txt_relationship" name="relationship" required data-validation-required-message="Please enter the <b>Relationship</b>." aria-invalid="false">                                        
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="control-group col-sm-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_mobile" class="text-bold">Mobile Phone Number <span class="obligatory_field">*</span></label>
                                        <input type="number" class="form-control" placeholder="(+61) 012345678" id="txt_mobile" name="mobile_emergency" required data-validation-required-message="Please enter the <b>Mobile Phone Number</b>." aria-invalid="false">                                        
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row border_bottom_forms_fields">
                                <div class="control-group col-sm-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_email" class="text-bold">Student Email</label>
                                        <input type="email" class="form-control" placeholder="student@school.com.au" id="txt_email" name="student_email" aria-invalid="false">                               
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="control-group col-sm-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_student_mobile" class="text-bold">Student Mobile Phone Number </label>
                                        <input type="number" class="form-control" placeholder="(+61) 012345678" id="txt_student_mobile" name="student_mobile"   aria-invalid="false">                                        
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row border_bottom_forms_fields">
                                <div class="col-lg-12 d-flex justify-content-between buttons_two_adds py-4">
                                    <a href="#" data-step-go="2" class="btn btn-success text-bold"><span class="icon-chevron-left"></span>Back to Step 02</a>
                                    <!--<button type="submit" class="btn btn-success" id="btnSubmit">Next Step</button>-->
                                    <button type="submit" class="btn btn-success text-bold" >Next Step</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 py-4">
                                    <a href="<?php echo esc_url( home_url( '/dashboard' ) ); ?>" class="cancel-process d-inline-block mx-auto">Cancel process and back to Dashboard</a>
                                </div>
                            </div>                            
                        </form>
                    </div>
                </div>
            </div>
        </article>
    </section>
    <section class="step" style="display:none;">
        <article class="content_page bg_dash">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10 box_content box_content_bt_green reset_height py-lg-0">
                        <form id="add-student-step-4" novalidate rel="add-student"  autocomplete="off" onsubmit="return false;">
                            <div class="row border_bottom_forms_fields">
                                <div class="control-group col-sm-4 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_fname">First Name <span class="obligatory_field">*</span></label>
                                        <input type="text" class="form-control" placeholder="First Name" name="fname" required readonly>
                                    </div>
                                </div>
                                <div class="control-group col-sm-4 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_lname">Last Name <span class="obligatory_field">*</span></label>
                                        <input type="text" class="form-control" placeholder="Last Name" name="lname" required readonly>
                                    </div>
                                </div>
                                <div class="control-group col-sm-4 forms_box">
                                    <div class="form-group floating-label-form-group controls"> 
                                        <label for="txt_school" class="text-bold">School <span class="obligatory_field">*</span></label>
                                        <input type="text" class="form-control" placeholder="School Name"  name="school" required readonly>  
                                    </div>
                                </div>
                            </div>
                            <div class="row border_bottom_forms_fields">
                                <div class="control-group col-sm-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_dob" class="text-bold">Date of Birth <span class="obligatory_field">*</span></label>
                                        <input type="text" class="form-control" placeholder="01/01/2002" id="txt_dob" name="date_of_birth" required readonly>
                                    </div>
                                </div>
                                <div class="control-group col-sm-6 forms_box">
                                    <div class="form-group floating-label-form-group controls"> 
                                        <label for="txt_gender" class="text-bold">Gender <span class="obligatory_field">*</span></label>
                                        <input type="text" class="form-control" placeholder="Male"  name="gender" required readonly>  
                                    </div>
                                </div>
                            </div>           
                            <div class="row border_bottom_forms_fields">
                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_conditions" class="text-bold">Medical Conditions<span class="obligatory_field">*</span></label>
                                        <textarea class="form-control" name="medical_condition"  rows="5" required spellcheck="false" aria-invalid="false" readonly placeholder="List of Medical Conditions"></textarea> 
                                        <span class="txt_under_fields mt-2">Please ensure this information is up to date.</span>                                      
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_allergies" class="text-bold">Allergies<span class="obligatory_field">*</span></label>
                                        <textarea class="form-control" name="allergies" rows="5" required spellcheck="false" aria-invalid="false" readonly placeholder="List of Allergies"></textarea>   
                                        <span class="txt_under_fields mt-2">Please ensure this information is up to date.</span>                                      
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row border_bottom_forms_fields">
                                <div class="control-group col-sm-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_date_tetanus" class="text-bold">Date of last Tetanus Shot <span class="obligatory_field">*</span></label>
                                        <input type="text" class="form-control" placeholder="Date" name="date_last_tetanus" data-language='en' required readonly value="">                                      
                                    </div>
                                </div>
                                <div class="control-group col-sm-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_medicar" class="text-bold">Medicar Number <span class="obligatory_field">*</span></label>
                                        <input type="text" class="form-control" placeholder="(+61) 012345678" name="medical_number" required readonly value="">                                        
                                    </div>
                                </div>
                            </div>                            
                            <div class="row border_bottom_forms_fields">
                                <div class="control-group col-sm-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_preferred">Preferred Emergency Contact<span class="obligatory_field">*</span></label>
                                        <input type="text" class="form-control" placeholder="Contact Name"  name="name_of_preferred_emergency" required readonly value="">                                        
                                    </div>
                                </div>
                                <div class="control-group col-sm-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_mobile" class="text-bold">Preferred Emergency Contact Number <span class="obligatory_field">*</span></label>
                                        <input type="text" class="form-control" placeholder="(+61) 012345678" name="mobile_emergency" required readonly value="">                                        
                                    </div>
                                </div>
                            </div>  
                            <div class="row border_bottom_forms_fields">
                                <div class="control-group col-sm-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_email" class="text-bold">Student Email</label>
                                        <input type="text" class="form-control" placeholder="" id="txt_email" name="student_email" readonly value="">                                        
                                    </div>
                                </div>
                                <div class="control-group col-sm-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_student_mobile" class="text-bold">Student Mobile Phone Number</label>
                                        <input type="text" class="form-control" placeholder=""  name="student_mobile" readonly value="">                                         
                                    </div>
                                </div>
                            </div>   
                            <div class="row border_bottom_forms_fields">
                                <div class="control-group col-sm-12 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <div class="form-check d-flex justify-content-between w-100 pl-3 pr-5 pr-md-3 py-2 bg-light">
                                            <label class="form-check-label text-bold" for="info-correct">I confirm that the above information is correct. <span class="obligatory_field">*</span></label>
                                            <input class="form-check-input check-right" type="checkbox" required="" data-validation-required-message="Missing required checkbox." id="info-correct" value="check-1">
                                        </div>
                                        <div class="form-check d-flex justify-content-between w-100 pl-3 pr-5 pr-md-3 py-2">
                                            <label class="form-check-label text-bold" for="agree-conduct">I agree to abide by the School Sport ACT&nbsp;<a class="green-link" href="/wp-content/uploads/2020/02/Codes-of-Conduct-for-SSACT-Team-Members-and-Parents.pdf">Codes of conduct.</a> <span class="obligatory_field">*</span></label>
                                            <input class="form-check-input check-right" type="checkbox"  required="" data-validation-required-message="Missing required checkbox." value="check-2">
                                        </div>
                                        <div class="form-check d-flex justify-content-between w-100 pl-3 pr-5 pr-md-3 py-2 bg-light">
                                            <label class="form-check-label text-bold" for="agree-acceptance">I have read and agree to the School Sport ACT&nbsp;<a class="green-link" href="/wp-content/uploads/2020/02/SSACT-State-Representative-Team-Acceptance-Policy.pdf">State Representative Team Acceptance Policy.</a> <span class="obligatory_field">*</span></label>
                                            <input class="form-check-input check-right" type="checkbox" required="" data-validation-required-message="Missing required checkbox." value="check-3">
                                        </div>                                                                                                                   
                                    </div>
                                </div>
                            </div>                                                                                                   
                            <div class="row border_bottom_forms_fields">
                                <div class="col-lg-12 d-flex justify-content-between buttons_two_adds py-4">
                                    <button type="submit" data-step-go="3" class="btn btn-success text-bold"><span class="icon-chevron-left"></span>Back to Step 03</button>
                                    <!--<button type="submit" class="btn btn-success" id="btnSubmit">Next Step</button>-->
                                    <button type="submit" class="btn btn-success text-bold" >Confirm and Submit</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 py-4">
                                    <a href="<?php echo esc_url( home_url( '/dashboard' ) ); ?>" class="cancel-process d-inline-block mx-auto">Cancel process and back to Dashboard</a>
                                </div>
                            </div> 
                          <div class="row">
                            <div class="col-lg-12">
                              <?php echo school_sport_get_template('component/notice');?>
                            </div>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </article>
    </section>
</div>