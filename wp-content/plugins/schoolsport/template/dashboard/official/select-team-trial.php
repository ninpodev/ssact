<?php  
    $trial_slug  = get_query_var('trial_slug');
    $page = get_page_by_path($trial_slug, OBJECT, 'trials');
    if($trial = $page->ID)  {
        $trial_posts = new WP_Query(['p' => $trial, 'post_type' => 'trials']);
?>
<div class="steps">
    <section>
        <article class="steps_content bg_dash">
            <div class="container">
                <div class="row">
                    <div class="col-md-11 offset-md-1 col-xl-10 offset-xl-1 text-center stepper stepper_violet four_items">
                        <div class="row justify-content-center">
                            <div class="col col-sm-4 item">
                                <div class="circle active" data-step="progress"></div>
                                <h6 class="text-center text-bold">Team<br>Selection</h6>
                            </div>
                            <div class="col col-sm-4 item">
                                <div class="circle" data-step="progress"></div>
                                <h6 class="text-center text-bold">Select<br>Try-on Date</h6>
                            </div>
                            <div class="col col-sm-4 item">
                                <div class="circle" data-step="progress"></div>
                                <h6 class="text-center text-bold">Summary</h6>
                            </div>                  
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </section>
    <?php if($trial_posts->have_posts()): $trial_posts->the_post(); ?>
    <section  class="step">
        <article class="content_page bg_dash">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10 box_content box_content_bt_purple reset_height py-lg-0">
                        <form rel="team-selection-trial" class="list_striped" onsubmit="return false;" autocomplete="off">
                       
                            <div class="py-4 border-bottom mb-4">
                                <h6 class="text-bold mb-3">Team Selection</h6>
                                <p class="mb-3">Use the list below to select students for the Team.<br>This will notify their parents and give them the opportunity to accept the position.</p>
                                <p class="mb-0"><small>For more information, please see the School Sport ACT <a href="#" class="green-link">Acceptance Process.</a></small></p>
                            </div>
                            <table class="table table-striped inline-table" id="select-team-trial">
                                <thead>
                                    <tr class="border-0">
                                        <th scope="col" class="col-3 col-md-5 border-0 text-muted">Student Name</th>
                                        <th scope="col" class="col-4 border-0 text-normal text-right d-none d-md-inline-block">Select one option for every student:</th>
                                        <th scope="col" class="col-3 col-md-1 border-0 text-muted text-center" >Selected</th>
                                        <th scope="col" class="col-3 col-md-1 border-0 text-muted text-center">Shadow</th>
                                        <th scope="col" class="col-3 col-md-1 border-0 text-muted text-center">Not Selected</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php while(have_rows('students')) : the_row();?>
                                        <?php $status = get_sub_field('approval_status');?>
                                        <?php if($status === 'approved'):?>
                                    <tr class="border-0 d-flex">
                                        <?php $student = get_sub_field('student');?>
                                        <td class="col-3 col-md-5 border-0 text-muted text-normal"><?php echo $student->post_title;?> </td>
                                        <td class="col-4 col-md-4 border-0 text-right d-none d-md-inline-block"><a href="#" class="grey-link"><small><u>View Student Info</u></small></a></td>
                                        <td class="col-3 col-md-1 border-0 text-center"><input class="form-check-input check-green" name="selection_status[<?php echo $student->ID;?>]" value="selected" type="checkbox"></td>
                                        <td class="col-3 col-md-1 border-0 text-center"><input class="form-check-input check-black"  name="selection_status[<?php echo $student->ID;?>]" value="shadow" type="checkbox"></td>
                                        <td class="col-3 col-md-1 border-0 text-center"><input class="form-check-input check-fault"   name="selection_status[<?php echo $student->ID;?>]" value="not_selected" type="checkbox"></td>
                                    </tr>
                                        <?php endif;?>
                                    <?php endwhile;?>
                                </tbody>
                            </table>  
                            <div class="d-flex justify-content-between align-items-center buttons_two_adds py-4 border-bottom">
                                <a href="<?php echo home_url('dashboard');?>?action=export_student&trial=<?php echo $trial;?>" class="btn btn-success text-bold"><span class="icon-table-of-contents mr-2"></span>Export list of students</a>
                                <!--<button type="submit" class="btn btn-success" id="btnSubmit">Next Step</button>-->
                                <input type="hidden" name="trial" value="<?php the_ID();?>">
                                <button type="submit" class="btn btn-purple btn-auto text-bold px-3 py-2">Continue</button>
                            </div>
                            <div class="row">
                                    <div class="col-12 py-4">
                                        <a href="<?php echo home_url('dashboard');?>" class="cancel-process d-inline-block mx-auto">Cancel process and back to Dashboard</a>
                                    </div>
                            </div>  
                        </form>
                    </div>
                </div>
            </div>
        </article>
    </section>
    <section class="step" style="display:none">
        <article class="content_page bg_dash">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10 box_content box_content_bt_purple reset_height py-lg-0">
                        <div class="py-4 border-bottom mb-4">
                            <h6 class="text-bold mb-3">Set the Try-on Date</h6>
                            <p class="mb-3">Please set a date for students to try on their equipment.<br>This will be required for parents to be able to order uniforms in a timely manner.</p>
                            <p class="mb-0"><small>For more information, please see the School Sport ACT <a href="#" class="green-link">Acceptance Process.</a></small></p>
                        </div>                        
                        <form id="select-student-2" novalidate="" rel="team-try-on-date" autocomplete="off">
                            <div class="row border_bottom_forms_fields">
                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_age" class="text-bold">Venue of Try-on <span class="obligatory_field">*</span></label>
                                        <input type="text" class="form-control" placeholder="Venue" id="txt_date_tetanus" name="venue_of_try_on" autocomplete="off" data-language="en" required="" data-validation-required-message="Please enter the <b>Age</b>.">
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
                                    <div class="form-group floating-label-form-group controls"> 
                                        <label for="txt_date" class="text-bold">Date of Try-on <span class="obligatory_field">*</span></label>
                                        <input type="text" class="form-control datepicker-here" placeholder="Date" id="txt_date_birth" autocomplete="off" name="date_of_try_on" data-language="en" required="" data-validation-required-message="Please enter the <b>Date</b>.">
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row border_bottom_forms_fields">
                                <div class="col-lg-12 d-flex justify-content-between buttons_two_adds py-4">
                                    <a href="#" data-step-go="1"  class="btn btn-purple text-bold"><span class="icon-chevron-left"></span>Back to Step 01</a>
                                    <!--<button type="submit" class="btn btn-success" id="btnSubmit">Next Step</button>-->
                                    <button  type="submit" class="btn btn-purple text-bold">Continue</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 py-4">
                                    <a href="<?php echo home_url('/dashboard');?>" class="cancel-process d-inline-block mx-auto">Cancel process and back to Dashboard</a>
                                </div>
                            </div>                            
                        </form>
                    </div>
                </div>
            </div>
        </article>
    </section>
    <section class="step"  style="display:none">
        <article class="content_page bg_dash">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10 box_content box_content_bt_purple reset_height py-lg-0">
                        <div class="pt-4 pb-4 pb-md-0">
                            <h6 class="text-bold mb-0">Summary</h6>
                        </div>                        
                        <form id="select-student-2" class="list_striped" novalidate="" rel="team-selection-submit">
                            <div class="row">
                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
                                    <div class="form-group floating-label-form-group controls">
                                        <label for="txt_age" class="text-bold">Venue of Try-on <span class="obligatory_field">*</span></label>
                                        <input type="text" class="form-control" readonly placeholder="Venue" id="txt_date_tetanus" name="venue_of_try_on" data-language="en" data-validation-required-message="Please enter the <b>Age</b>.">
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
                                    <div class="form-group floating-label-form-group controls"> 
                                        <label for="txt_date" class="text-bold">Date of Try-on <span class="obligatory_field">*</span></label>
                                        <input type="text" class="form-control datepicker-here" readonly placeholder="Date" id="txt_date" name="date_of_try_on" data-language="en"  data-validation-required-message="Please enter the <b>Date</b>.">
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                            </div>
                            <table class="table table-striped inline-table border-bottom mb-4" id="select-team-submit-table">
                                <thead>
                                    <tr class="border-0">
                                        <th scope="col" class="col-6 border-0 text-muted">Student Name</th>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                </tbody>
                            </table>
                            <p class="mb-5">Please set a date for students to try on their equipment.<br>This will be required for parents to be able to order uniforms in a timely manner.</p>
                            <div class="row">
                                <div class="col-lg-12">
                                  <?php echo school_sport_get_template('component/notice');?>
                                </div>
                            </div>
                            <div class="d-flex justify-content-between buttons_two_adds py-4">
                                <a href="#"  data-step-go="2" class="btn btn-purple text-bold"><span class="icon-chevron-left"></span>Back to Step 02</a>
                                <!--<button type="submit" class="btn btn-success" id="btnSubmit">Next Step</button>-->
                                <button type="submit" class="btn btn-purple text-bold">Submit</button>
                            </div>
                            <div class="py-4 border-top">
                                <a href="<?php echo home_url('/dashboard');?>" class="cancel-process d-inline-block mx-auto">Cancel process and back to Dashboard</a>
                            </div>     
                            
                        </form>
                    </div>
                </div>
            </div>
        </article>
    </section>
    <?php endif;?>
</div>

<?php } ?>