<section class="dashboards">
    <article>
      <div class="container">
        <div class="row">
            <div class="col-xl-10 offset-xl-1 item pd_mobile_cero">
                <div class="row">
                    <div class="col-lg-12 title_dash_item">
                        <h4><span class="icon-user"></span>Your Details</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 item">
                        <div class="box_content box_content_bt_purple reset_height">
                            <?php echo school_sport_get_template('dashboard/component/user-detail');?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
          <div class="col-xl-10 offset-xl-1 item pd_mobile_cero">
              <?php 
                $current_user = get_current_user_id();
                $trials = new WP_Query(['post_type'=> 'trials', 'author' => $current_user ]);
                if($trials->have_posts()): 
             ?>
             <div class="row">
              <div class="col-lg-12 title_dash_item">
                <h4><span class="icon-user"></span>Your Trials</h4>
              </div>
             
            </div>
            <?php while($trials->have_posts()): $trials->the_post();?>
            <div class="row">
                <div class="col-lg-12 title_dash_item info_dash_item">
                    <h4><?php the_title();?></h4>
                    <?php $date_of_trial = get_field('date_of_trial');?>
                    <?php $date =  DateTime::createFromFormat('m/d/Y', $date_of_trial);?>
                    <?php $now    = new DateTime();?>
                    <?php if($date) {?>
                    <p>
                        <?php $slug = basename(get_permalink());?>
                        <span class="txt_bold">Date of Trial</span>
                        <?php echo $date->format('dS M Y');?>
                         <?php if($now > $date_trial) { ?>
                        <a href="<?php echo home_url('/dashboard/select-students-for-the-team/'.$slug);?>" class="btn btn-purple btn-sm ml-4" id="btnSubmit">Select Students for Team</a>
                        <?php } ?>
                    </p>
                    <?php } ?>
                </div>    
            </div>
            <?php if(have_rows('students')): ?>
            <div class="row">
              <div class="col-lg-12 item">
                <div class="box_content box_content_bt_purple reset_height">
                  <div class="row">
                    <div class="col-lg-12 list_striped">
                      <table id="example" class="table table-striped dataTable" style="width:100%">
                        <thead>
                          <tr>
                            <th>Student Name</th>
                            <th>Status</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php while(have_rows('students')): the_row();?>
                            <?php 
                                $student = get_sub_field('student');
                                $field_approval = get_sub_field_object('approval_status');
                                $approval = get_sub_field('approval_status');
                           ?>
                            <?php if(isset($student->post_title)) :?>
                          <tr>
                            <td><?php echo $student->post_title;?></td>
                            <td><?php echo @$field_approval['choices'][ $approval ];?></td>
                            <td><a href="#" data-toggle="modal" data-target="#modal-student" class="txt_dgb">View Info</a></td>
                          </tr>
                            <?php endif;?>
                        <?php endwhile;?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php else : ?>
             <div class="row">
              <div class="col-lg-12 item">
                <div class="box_content box_content_bt_purple reset_height">
                  <div class="row">
                    <div class="col-lg-12 list_striped">
                      <table id="example" class="table dataTable" style="width:100%">
                          <tbody>
                              <tr>
                                <td align="center">There are no students enrolled in this trial</td>
                              </tr>
                         </tbody>  
                      </table>
                    </div>
                  </div>
                </div>
               </div>
              </div>
            <?php endif;?>
          <?php endwhile;?>
        <?php endif;?>
          </div>
        </div>
        <div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modal-student">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content parent-modal">
                    <div class="modal-header">
                        <h3 class="text-center text-bold w-100 mb-0" data-modal-student="name">[Name]</h3>
                    </div>
                    <div class="modal-body" data-modal-student="content">
                        <table id="example" class="table table-bordered inline-table border-bottom mb-4 mt-4" style="width:100%">
                            <tbody>
                                <tr>
                                    <th class="col-lg-3 col-4 text-muted">Student Name</th>
                                    <td class="col-lg-9 col-8" data-modal-student="name"> x</td>
                                </tr>
                                 <tr>
                                    <th class="col-lg-3 col-4 text-muted">Birthday</th>
                                    <td class="col-lg-9 col-8" data-modal-student="birthday">x </td>
                                </tr>
                                <tr>
                                    <th class="col-lg-3 col-4 text-muted">Gender</th>
                                    <td class="col-lg-9 col-8" data-modal-student="gender">x </td>
                                </tr>
                                <tr>
                                    <th class="col-lg-3 col-4  text-muted">School</th>
                                    <td class="col-lg-9 col-8" data-modal-student="school"> x</td>
                                </tr>
                                <tr>
                                    <th class="col-lg-3 col-4 text-muted">Parent</th>
                                    <td class="col-lg-9 col-8" data-modal-student="parent"> x</td>
                                </tr>
                            </tbody>
                          </table>
                    </div>
                    <div class="modal-footer border-0 flex-wrap">
                        
                    </div>
                </div>
            </div>
        </div>    
      </div>
  </article>
</section>
