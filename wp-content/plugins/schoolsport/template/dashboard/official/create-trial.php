<?php $draft = school_sport_trial_draft();?>
<div class="steps">
	<section  class="step_progress">
		<article class="steps_content bg_dash">
			<div class="container">
				<div class="row">
					<div class="col-md-10 offset-md-1 col-xl-8 offset-xl-2 text-center stepper stepper_violet">
						<div class="row">
							<div class="col col-sm-6 item" >
								<div class="circle active" data-step="progress"></div>
								<h6>Basic Information</h6>
							</div>
							<?php /*<div class="col col-sm-4 item">
								<div class="circle" data-step="progress"></div>
								<h6>Product Set</h6>
							</div>*/ ?>
							<div class="col col-sm-6 item">
								<div class="circle" data-step="progress"></div>
								<h6>Submit for Review</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
		</article>
	</section>

	<section class="step">
		<article class="content_page bg_dash">
			<div class="container">
				<div class="row">
					<div class="col-md-10 offset-md-1 box_content reset_height">
						<form id="editDetailsForm" novalidate="" rel="create-trial"  onsubmit="return false;">
							<div class="row border_bottom_forms_fields">
								<div class="control-group col-sm-12 col-md-12 col-lg-12 forms_box">
									<div class="form-group floating-label-form-group controls">
										<label for="txt_name">Name of the Trial </label>
										<input type="text" class="form-control" placeholder="Name" id="txt_name" name="name" value="<?php echo $draft['name'];?>" readonly>
										<p class="help-block text-danger"></p>
									</div>
								</div>
							</div>
							<div class="row border_bottom_forms_fields">
								<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
									<div class="form-group floating-label-form-group controls">
										<label for="txt_year">Year <span class="obligatory_field">*</span></label>
										<input type="text" class="form-control" placeholder="Bob" value="<?php echo $draft['year'];?>" id="txt_year" name="year" required="" readonly="" data-validation-required-message="Please enter the <b> Year</b>." value="2019">
										<p class="help-block text-danger"></p>
									</div>
								</div>
								<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
									<div class="form-group floating-label-form-group controls">
										<label for="txt_sport">Sport <span class="obligatory_field">*</span></label>
										<?php $sports = new WP_Query(['post_type'=>'sports', 'posts_per_page'=>-1]);?>
										
										<select class="form-control custom-select" id="txt_sport" name="sport" required="" data-validation-required-message="Please select the <b> Sport</b>.">
											<option value="" selected="">Choose...</option>
											<?php while($sports->have_posts()): $sports->the_post();?>
											<option value="<?php the_ID();?>" <?php if($draft['sport'] == get_the_ID()) {?>selected<?php } ?>><?php the_title();?></option>
											<?php endwhile;?>
										</select>
										
										<p class="help-block text-danger"></p>
									</div>
								</div>
							</div>
							<div class="row border_bottom_forms_fields">
								<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
									<div class="form-group floating-label-form-group controls">
										<label for="txt_age">Age <span class="obligatory_field">*</span></label>
										<?php $age_choices = get_field_object('field_5dffdb2157168')['choices']; ?>
										
										<select class="form-control custom-select" id="txt_age" name="age" required="" data-validation-required-message="Please select the <b> Age</b>.">
											<option value="" selected="">Choose...</option>
											<?php if(!empty($age_choices)) : foreach($age_choices as $age_choice) : ?>
											<option value="<?php echo $age_choice; ?>"><?php echo $age_choice; ?></option>
											<?php endforeach; endif; ?>
										</select>
										<p class="help-block text-danger"></p>
									</div>
								</div>
								<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
									<div class="form-group floating-label-form-group controls">
										<label for="txt_gender">Gender <span class="obligatory_field">*</span></label>
										<select required="" class="form-control custom-select" id="txt_gender" name="gender" data-validation-required-message="Please select the <b> Gender</b>.">
											<option value="" selected="">Choose...</option>
											<option value="male" <?php if($draft['gender'] =='male') {?> selected <?php } ?>>Male</option>
											<option value="female" <?php if($draft['gender'] =='female') {?> selected <?php } ?>>Female</option>
										</select>
										<p class="help-block text-danger"></p>
									</div>
								</div>
							</div>
							<div class="row border_bottom_forms_fields">
								<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
									<div class="form-group floating-label-form-group controls">
										<label for="txt_date_birth">Date of Birth Restrictions (from date) <span class="obligatory_field">*</span></label>
										<input type="text" class="form-control datepicker-here"  value="<?php echo $draft['date_birth_from'];?>" placeholder="Date" id="txt_date_birth" name="date_birth_from" data-language="en" required="" autocomplete="off" data-validation-required-message="Please enter the <b> Date of Birth Restrictions</b>.">
										<p class="help-block text-danger"></p>
									</div>
								</div>
								<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
									<div class="form-group floating-label-form-group controls">
										<label for="txt_date_trial">Date of Birth Restrictions (to date) <span class="obligatory_field">*</span></label>
										<input type="text" class="form-control datepicker-here" placeholder="Date" value="<?php echo $draft['date_birth_to'];?>" id="txt_date_birth_to" name="date_birth_to" data-language="en" required="" autocomplete="off" data-validation-required-message="Please enter the <b> Date of Trial</b>.">
										<p class="help-block text-danger"></p>
									</div>
								</div>
							</div>
							<div class="row border_bottom_forms_fields">
								<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
									<div class="form-group floating-label-form-group controls">
										<div class="row">
											<div class="col-sm-6"> 
												<label for="txt_date_trial">Start Date of Trial <span class="obligatory_field">*</span></label>
												<input type="text" class="form-control datepicker-here" placeholder="Start Date" value="<?php echo $draft['date_trial'];?>" id="txt_date_trial" name="date_trial" data-language="en" required="" autocomplete="off" data-validation-required-message="Please enter the <b> Start Date of Trial</b>.">
												<p class="help-block text-danger"></p>
											</div>
											<div class="col-sm-6"> 
												<label for="txt_end_date_trial">End Date of Trial <span class="obligatory_field">*</span></label>
												<input type="text" class="form-control datepicker-here" placeholder="End Date" value="<?php echo $draft['end_date_trial'];?>" id="txt_end_date_trial" name="end_date_trial" data-language="en" required="" autocomplete="off" data-validation-required-message="Please enter the <b> End Date of Trial</b>.">
												<p class="help-block text-danger"></p>
											</div>
										</div>
									</div>
								</div>
								<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
									<div class="form-group floating-label-form-group controls">
										<label for="txt_champ_date">Time of the Trial </label>
                                        <div class="row">
                                            <div class="col-sm-6"> 
                                                <input type="text" class="form-control timepicker" value="<?php echo $draft['time_trial_from'];?>" placeholder="From" id="" name="time_trial_from" autocomplete="off" >
                                            </div>
                                             <div class="col-sm-6">
                                                <input type="text" class="form-control timepicker" value="<?php echo $draft['time_trial_to'];?>" placeholder="To" id="" name="time_trial_to" autocomplete="off" >
                                            </div>
                                        </div>
										<p class="help-block text-danger"></p>
									</div>
								</div>
							</div>
                            <div class="row border_bottom_forms_fields">
                              <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
									<div class="form-group floating-label-form-group controls">
										<label for="txt_champ_date">Venue of the Trial  <span class="obligatory_field">*</span></label>
										<input type="text" class="form-control" value="<?php echo $draft['venue'];?>" placeholder="Venue of the Trial" id="txt_venue" name="venue" autocomplete="off" data-validation-required-message="Please enter the <b> Venue</b>."   required="">
										<p class="help-block text-danger"></p>
									</div>
								</div>
                            </div>
							<div class="row border_bottom_forms_fields">
                              
								<div class="control-group col-sm-12 col-md-12 col-lg-12 forms_box">
									<div class="form-group floating-label-form-group controls">
										<label for="txt_eligibility">Eligibility <span class="obligatory_field">*</span></label>
										<textarea class="form-control" name="eligibility" id="txt_eligibility" rows="5" required="" data-validation-required-message="Please enter the <b> Eligibility</b>."><?php echo $draft['eligibility'];?></textarea>
										<p class="help-block text-danger"></p>
									</div>
								</div>
							</div>
							
							<!--Required Equipment-->
							<div class="row border_bottom_forms_fields">
								<div class="control-group col-sm-12 col-md-12 col-lg-12 forms_box">
									<div class="form-group floating-label-form-group controls">
										<label for="txt_requiredequipment">Required Equipment <span class="obligatory_field">*</span></label>
										<textarea class="form-control" name="requiredequipment" id="txt_requiredequipment" rows="5" required="" data-validation-required-message="Please enter the <b> Required Equipment</b>."><?php echo $draft['required_equipment'];?></textarea>
										<p class="help-block text-danger"></p>
									</div>
								</div>
							</div>
							
							<div class="row border_bottom_forms_fields">
								<div class="control-group col-sm-12 col-md-12 col-lg-12 forms_box">
									<div class="form-group floating-label-form-group controls">
										<label for="txt_champ_date">Championship date</label>
										<input type="text" class="form-control datepicker-here" value="<?php echo $draft['champ_date'];?>" placeholder="Date" id="txt_champ_date" name="champ_date" autocomplete="off"  data-language="en">
										<div class="help-block"></div>
									</div>
								</div>
							</div>
                            <div class="row border_bottom_forms_fields">
								<div class="control-group col-sm-12 col-md-12 col-lg-12 forms_box">
									<div class="form-group floating-label-form-group controls">
										<label for="txt_champ_date">Championship Venue</label>
										<input type="text" class="form-control" value="<?php echo $draft['champ_venue'];?>" placeholder="Venue" id="txt_champ_date" name="champ_venue" autocomplete="off" >
										<div class="help-block"></div>
									</div>
								</div>
							</div>
							
							<?php
								$user = wp_get_current_user();
								$phone = get_user_meta($user->ID, 'phone',  true);
								
							?>
							<div class="row">
								<div class="control-group col-sm-12 col-md-12 col-lg-10 col-xl-10 forms_box mt-mobile-20">
									<h3 class="mb-2">Confirm Official Details</h3>
									<p class="mb-3">Please confirm the details of the official are correct below, if they are incorrect you can update your profile <a href="https://schoolsportact.org.au/dashboard/edit-my-details">here</a>.</p>
									
									
									<table class="table table-bordered table_trial">
										<thead>
											<tr>
												<th><div class="icon-avatar"></div> Confirm name of the Official</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><?php echo $user->first_name;?> <?php echo $user->last_name;?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							
							
							<div class="row">
								<div class="control-group col-sm-12 col-md-12 col-lg-12 forms_box">
									<div class="table-responsive-sm">
										<table class="table table-bordered table_trial">
											<thead>
												<tr>
													<th colspan="4">@ Contact Information</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>First Name</td>
													<td><b><?php echo $user->first_name;?></b></td>
													<td>Email</td>
													<td class="text-wrap"><b><?php echo $user->user_email;?></b></td>
												</tr>
												<tr>
													<td>Last Name</td>
													<td><b><?php echo $user->last_name;?></b></td>
													<td>Phone Number</td>
													<td class="text-wrap"><b><?php echo $phone;?></b></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="control-group col-sm-12 col-md-12 col-lg-8 col-xl-7 forms_box">
									<div class="table-responsive-sm">
										<table class="table table-bordered table_trial">
											<thead>
												<tr>
													<th><div class="icon-ticket"></div> Cost of registration</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Registrations to this Trial will have a fee of $10.</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 text-center forms_box">
									<div id="success"></div>
									<input type="hidden" name="trial" id="trial" value="<?php echo $draft['trial'];?>">
									<input type="hidden" name="id_form" id="id_form" value="trial1Form">
									<button type="submit" onclick="" class="btn btn-purple" id="btnSubmit">Next Step</button>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<?php echo school_sport_get_template('component/notice');?>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</article>
	</section>

	<?php /*<section class="step" style="display:none">
		<article class="content_page bg_dash">
			<div class="container">
				<div class="row">
					<div class="col-md-10 offset-md-1 box_content reset_height" >
					
						<form id="trialForm2" novalidate rel="product-set-trial"  onsubmit="return false;">
							<div class="row border_bottom_forms_fields">
								<div class="control-group col-sm-12 forms_box">
									<h6>Create a product set</h6>
									<p>
										Use the list below to select all products that students will need to take part in the championship.
										<br>
										The set will only become visible to parents once a student has been selected for the team.
									</p>
								</div>
							</div>
							<div class="row border_bottom_forms_fields">
								<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
									<div class="form-group floating-label-form-group controls" >
										<label for="pre_product_set">Master product list</label>
										<select id="pre_product_set" name="pre_product_set" size="8" multiple class="form-control multiple_list_select">
											<?php 
												$product_set = new WP_Query(['post_type' => 'product_sets']);
												while($product_set->have_posts()): $product_set->the_post();
											?>
											<option value="<?php the_ID();?>"><?php the_title();?></option>
											<?php endwhile;?>
										</select>
										<p class="help-block text-danger"></p>
										<a href="javascript:void(0);" class="btn btn-purple btn-list-products" rel="product-add-set" id="btnAddProduct"><span class="icon-chevron-right"></span>Add Product/s</a href="javascript:void(0);">
									</div>
								</div>
								<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
									<div class="form-group floating-label-form-group controls">
										<label for="product_set">Selected products</label>
										<select id="product_set" name="product_set[]" size="8" multiple class="form-control multiple_list_select"  required
										data-validation-required-message="Required"></select>
										<p class="help-block text-danger"></p>
										<a href="javascript:void(0);" class="btn btn-purple btn-list-products" rel="product-remove-set"  id="btnRemoveProduct"><span class="icon-close"></span>Remove selected products</a>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 text-center forms_box mtb-15">
									<div id="success"></div>
									<input type="hidden" name="id_form" id="id_form_trial" value="trial2Form">
									<button type="submit" class="btn btn-purple">Next Step</button>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<?php echo school_sport_get_template('component/notice');?>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</article>
	</section>*/ ?>

	<section class="step" style="display:none;">
		<article class="content_page bg_dash">
			<div class="container">
				<div class="row">
					<div class="col-md-10 offset-md-1 box_content reset_height">
						<form id="trialForm3" novalidate="" rel="create-submit-trial"  onsubmit="return false;">
							<div class="row">
								<div class="control-group col-sm-12 text-center forms_box">
									<h3 class="trial-msg-title">Your Trial will now be reviewed for publication</h3>
									<p>
										Thank you for submitting a Trial!
									</p>
									<p>
										Our team will now review it to ensure it meets the School Sport ACT <a href="/resources/">policies</a>. 
									</p>
									<p>
										You can follow the Trial�s status on your Dashboard.
									</p>
									<p>
										Once the review is complete, <span class="font-weight-bold">we will send you a notification via email.</span>
									</p>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 text-center forms_box mtb-15">
									<div id="success"></div>
									<input type="hidden" name="id_form" id="id_form" value="trial2Form">
									<button type="submit" class="btn btn-purple btn-submit-trial">Submit</button>
									<p>and return to my Dashboard</p>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<?php echo school_sport_get_template('component/notice');?>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</article>
	</section>
</div>