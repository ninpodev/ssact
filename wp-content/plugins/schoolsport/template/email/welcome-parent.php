<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>..:: SSACT ::..</title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet">
	<style>
		body { font-family: 'Open Sans', sans-serif; font-size: 16px; }
		#top_list { list-style: none; } 
		#top_list li { display: inline-block; position: relative; padding:0px 12px;} 
		#top_list li:after { content: "●"; position: absolute; right: -6px; top: 0; bottom: 0; margin: auto 0; }
		#top_list li:last-child:after { content: "";}
		#link_button { width: 187px; border-radius: 4px; background-color: #62cb32; color: #ffffff;font-size: 16px; font-weight: 700; padding: 6px 26px 8px; text-decoration: none; -webkit-transition: .3s all; -moz-transition: .3s all; -o-transition: .3s all; -ms-transition: .3s all; transition: .3s all; }
		#link_button:hover {color: #62cb32; background-color: #ffffff; border: 1px solid #62cb32}
	</style>
</head>
<body style="margin: 0; padding: 0;">
	<table align="center" cellpadding="0" cellspacing="0" width="600" style="border:1px solid #e7e7e7;">
	 	<tr>
	  		<td align="center" bgcolor="#ffffff" style="padding: 19px 0px 14px;">
	 			<img src="<?php bloginfo('template_url');?>/images/logo_black.png" alt="SSACT" style="display: block; margin: 0px auto; max-height: 45px;" />
			</td>
		</tr>
		<tr>
			<td align="center" bgcolor="#e7e7e7" style="display:block; margin: 0px 22px; height: 2px;"></td>
		</tr>
		<tr>
			<td style="padding: 17px 0px 18px;display:block; margin: 0 auto;text-align: center;">
				<ul id="top_list" style="font-size: 16px; color: #666666;padding: 0; margin: 0;">
					<li>Participation</li>
					<li>Potential</li>
					<li>Pathways</li>
				</ul>
			</td>
		</tr>
		<tr>
			<td align="center" bgcolor="#ffffff" style="display:block; margin: 0px 22px; background: url('<?php bloginfo('template_url');?>/images/bg_home.jpeg') no-repeat; background-position: center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; min-height: 141px; position: relative;"></td>
		</tr>
		<tr>
			<td style="color: #666666; padding: 25px 0px 108px; margin: 0px 22px; text-align: center;">
				<h1 style="font-size: 40px;font-weight: 700; line-height: normal; margin: 0px 0px 32px;">Welcome</h1>
				<p style="margin-bottom: 28px;">Hello <b>#first_name#</b></p>
				<p style="margin-bottom: 28px;">This is an automatic message from the website at School Sport ACT.</p>
				<p style="margin-bottom: 28px;">Please confirm your account by clicking this button</p>
				<p style="margin-bottom: 28px;"><a href="#url_complete#" id="link_button">Confirm Account</a></p>
				<p style="margin-bottom: 28px;">Kind regards,</p>
				<p><b>School Sport ACT Team</b></p>
			</td>
		</tr>
		<tr>
			<td align="center" bgcolor="#e7e7e7" style="display:block; margin: 0px 22px; height: 2px;"></td>
		</tr>
		<tr>
			<td style="padding: 0px 22px;">
				<table style="margin: 16px 0px 15px;" width="100%">
					<tr>
						<td style="color: #666666; font-size: 12px; text-align: left;" width="50%">
							100 Maitland St, Hackett 2602
						</td>
						<td style="color: #666666; font-size: 12px; text-align: right;" width="50%">
							P 6103 0777 | F 02 6257 8744
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" bgcolor="#e7e7e7" style="display:block; margin: 0px 22px; height: 2px;"></td>
		</tr>
		<tr>
			<td style="padding: 20px 0px 23px;display:block; margin: 0 auto;text-align: center;">
				<a href="#" style="color:#62cb32;font-size: 12px;">www.schoolsportact.asn.au</a>
			</td>
		</tr>
	</table>
</body>
</html>