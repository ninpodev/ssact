<p>Dear SSACT administrator,</p>
<p>Please review and update the below template.</p>
<table style="font-size:10pt;font-family:Arial;width:0px;border-collapse:collapse;border:none;margin-top:10px;" cellspacing="0" cellpadding="0">
     <?php if($selected){ ?>
    <tbody>
        <tr>
           <td colspan="11" style="border:1px solid black;padding:2px 3px;">Selected</td>
        </tr>
        <?php $user = wp_get_current_user();?>  
        <?php foreach($selected as $table) {?>
         <tr>
            <?php $sport =  get_field('sport', @$table["trial"]->ID);?>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo @$table["student"]->post_title;?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo @$table["parent"]->data->display_name;?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo get_user_meta( @$table["parent"]->ID,'address', true);?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo the_field('year', @$table["trial"]->ID);?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo the_field('gender', @$table["trial"]->ID);?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo @$sport->post_title; ?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo the_field('championship_venue', @$table["trial"]->ID);?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo the_field('championship_date', @$table["trial"]->ID);?></td>
            <td style="border:1px solid black;padding:3px 3px;"><a href="<?php home_url('dashboard');?>">Dashboard</a></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo @$user->display_name ;?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo @$user->user_email ;?></td>
        </tr>
        <?php } ?>
    </tbody>     
    <?php } ?>
</table>

<table style="font-size:10pt;font-family:Arial;width:0px;border-collapse:collapse;border:none;margin-top:10px;" cellspacing="0" cellpadding="0">
     <?php if($shadow){ ?>
    <tbody>
       
        <tr>
            <td colspan="11" style="border:1px solid black;padding:2px 3px;">Shadow</td>
        </tr>
        <?php $user = wp_get_current_user();?>  
        <?php foreach($shadow as $table) {?>
         <tr>
            <?php $sport =  get_field('sport', @$table["trial"]->ID);?>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo @$table["student"]->post_title;?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo @$table["parent"]->data->display_name;?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo get_user_meta( @$table["parent"]->ID,'address', true);?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo the_field('year', @$table["trial"]->ID);?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo the_field('gender', @$table["trial"]->ID);?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo @$sport->post_title; ?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo the_field('championship_venue', @$table["trial"]->ID);?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo the_field('championship_date', @$table["trial"]->ID);?></td>
            <td style="border:1px solid black;padding:3px 3px;"><a href="<?php home_url('dashboard');?>">Dashboard</a></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo @$user->display_name ;?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo @$user->user_email ;?></td>
        </tr>
        <?php } ?>
    </tbody>     
    <?php } ?>
</table>

<table style="font-size:10pt;font-family:Arial;width:0px;border-collapse:collapse;border:none; margin-top:10px;" cellspacing="0" cellpadding="0">
     <?php if($not_selected){ ?>
    <tbody>
        <tr>
           <td colspan="11" style="border:1px solid black;padding:2px 3px;">Not Selected</td>
        </tr>
        <?php $user = wp_get_current_user();?>  
        <?php foreach($not_selected as $table) {?>
         <tr>
            <?php $sport =  get_field('sport', @$table["trial"]->ID);?>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo @$table["student"]->post_title;?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo @$table["parent"]->data->display_name;?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo get_user_meta( @$table["parent"]->ID,'address', true);?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo the_field('year', @$table["trial"]->ID);?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo the_field('gender', @$table["trial"]->ID);?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo @$sport->post_title; ?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo the_field('championship_venue', @$table["trial"]->ID);?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo the_field('championship_date', @$table["trial"]->ID);?></td>
            <td style="border:1px solid black;padding:3px 3px;"><a href="<?php home_url('dashboard');?>">Dashboard</a></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo @$user->display_name ;?></td>
            <td style="border:1px solid black;padding:3px 3px;"><?php echo @$user->user_email ;?></td>
        </tr>
        <?php } ?>
    </tbody>     
    <?php } ?>
</table>