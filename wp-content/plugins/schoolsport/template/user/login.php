<section>
	<article class="content_page login">
		<div class="container">
			<div class="row">
				<div class="col-md-8 offset-md-2 col-lg-6 offset-lg-3 box_content box_content_bt_orange reset_height">
					<form id="contactForm" novalidate=""  rel="user-login">
						<div class="row border_bottom_forms_fields">
							<div class="control-group col-lg-12 forms_box">
								<div id="success"></div>
								<div class="form-group floating-label-form-group controls">
									<label for="txt_username">Email or Username</label>
									<input type="text" class="form-control" placeholder="username / example@example.com" id="txt_username" name="log" required="" data-validation-required-message="Please enter your <b> username</b>.">
									<p class="help-block text-danger"></p>
								</div>
							</div>
							<div class="control-group col-lg-12 forms_box">
								<div class="form-group floating-label-form-group controls">
									<label for="txt_password">Password</label>
									<input type="password" class="form-control" placeholder="*******" id="txt_password" name="pwd" required="" data-validation-required-message="Please enter your <b> password</b>.">
									<p class="help-block text-danger"></p>
								</div>
							</div>
							<div class="control-group col-sm-6 forms_box">
								<div class="form-group floating-label-form-group controls">
									<div class="custom-control custom-checkbox my-1 mr-sm-2">
										<input type="checkbox" class="custom-control-input" id="txt_stay_signed">
										<label class="custom-control-label" for="txt_stay_signed">Stay Signed In</label>
									</div>
								<div class="help-block"></div></div>
							</div>
							<div class="control-group col-sm-6 text-right forms_box btn_mobile">
								<div class="form-group floating-label-form-group controls">
									<input type="hidden" name="id_form" id="id_form" value="loginForm">
									<button type="submit" class="btn btn-success btn-login" id="btnSubmit">Login</button>
								<div class="help-block"></div></div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<?php echo school_sport_get_template('component/notice');?>
								</div>
							</div>
						</div>
						<div class="row border_bottom_forms_fields">
							<div class="col-lg-12 forms_box">
								<h5>Don’t have an account yet?</h5>
								<p>Click <a href="#">Sign Up</a> to register.</p>
							</div>
						</div>
						<div class="row border_bottom_forms_fields">
							<div class="col-lg-12 forms_box">
								<h5>Forgot your Password?</h5>
								<p>No worries, <a href="<?php echo school_sport_get_url_recovery();?>">click here</a> to reset your password.</p>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</article>
</section>