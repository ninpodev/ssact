<section>
    <article class="content_page">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 other_sections">
                    <p>
                        Thank you for confirming your email. Please 
                        <a href="<?php echo home_url('login');?>">Click here to Log in</a>
                    </p>
                </div>
            </div>
        </div>
    </article>
</section>