<section>
    <article class="content_page login">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 col-lg-6 offset-lg-3 box_content box_content_bt_orange reset_height">
                    <form id="contactForm" novalidate="" rel="user-recovery">
                        <div class="row">
                            <div class="control-group col-lg-12 forms_box">
                                <div id="success"></div>
                                <div class="form-group floating-label-form-group controls">
                                    <label for="txt_email">Email</label>
                                    <input type="email" class="form-control" placeholder="example@example.com" id="txt_email" name="email" required="" data-validation-required-message="Please enter your <b> email</b>.">
                                    <span class="txt_under_fields">Use your registered email.</span>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="control-group col-sm-12 text-right forms_box btn_mobile">
                                <div class="form-group floating-label-form-group controls">
                                    <input type="hidden" name="id_form" id="id_form" value="recoverPasswordForm">
                                    <button type="submit" class="btn btn-success btn-login" id="btnSubmit">Send recovery email</button>
                                <div class="help-block"></div></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <?php echo school_sport_get_template('component/notice');?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </article>
</section>