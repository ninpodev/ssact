<section>
	<article class="content_page registration">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text_presentation">
					<p class="text-center">
						Already Signed Up? <br>
						Click <a href="<?php echo home_url( 'login' ) ;?>">Log In</a> to access your account.
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-10 offset-lg-1 box_content box_content_bt_purple reset_height">
					<form id="contactForm" novalidate rel="user-register" autocomplete="off">
						<div class="row border_bottom_forms_fields">
							<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
								<div class="form-group floating-label-form-group controls">
									<label for="txt_first_name">First Name <span class="obligatory_field">*</span></label>
									<input type="text" class="form-control" placeholder="First Name" id="txt_first_name" name="first_name" required data-validation-required-message="Please enter your <b> First Name</b>.">
									<p class="help-block text-danger"></p>
								</div>
							</div>
							<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
								<div class="form-group floating-label-form-group controls">
									<label for="txt_last_name">Last Name <span class="obligatory_field">*</span></label>
									<input type="text" class="form-control" placeholder="Last Name" id="txt_last_name" name="last_name" required data-validation-required-message="Please enter your <b> Last Name</b>.">
									<p class="help-block text-danger"></p>
								</div>
							</div>
						</div>
						<div class="row border_bottom_forms_fields">
							<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
								<div class="form-group floating-label-form-group controls">
									<label for="txt_email_address">Email Address <span class="obligatory_field">*</span></label>
									<input type="email" class="form-control" placeholder="Email Address" id="txt_email_address" name="email" required data-validation-required-message="Please enter your <b> Email Address</b>.">
									<p class="help-block text-danger"></p>
								</div>
							</div>
							<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
								<div class="form-group floating-label-form-group controls">
									<label for="txt_email_confirmation">Confirm Email Address <span class="obligatory_field">*</span></label>
									<input type="email" class="form-control" placeholder="Confirm Email Address" id="txt_email_confirmation" name="email_confirmation" required 
										data-validation-matches-match="email" 
										data-validation-matches-message= "Must match the email entered previously." 
										data-validation-required-message="Please enter your <b> Confirm Email Address</b>.">
									<p class="help-block text-danger"></p>
								</div>
							</div>
						</div>
						<div class="row border_bottom_forms_fields">
							<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
								<div class="form-group floating-label-form-group controls">
									<label for="txt_mobile_phone">Mobile Phone <span class="obligatory_field">*</span></label>
									<input type="text" class="form-control" placeholder="Mobile Phone" id="txt_mobile_phone" name="mobile" required data-validation-required-message="Please enter your <b> Mobile Phone</b>.">
									<p class="help-block text-danger"></p>
								</div>
							</div>
							<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
								<div class="form-group floating-label-form-group controls">
									<label for="txt_phone">Phone</label>
									<input type="text" class="form-control" placeholder="Phone" id="txt_phone" name="phone">
								</div>
							</div>
						</div>
						<div class="row border_bottom_forms_fields">
							<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
								<div class="form-group floating-label-form-group controls">
									<label for="txt_address">Address <span class="obligatory_field">*</span></label>
									<input type="text" class="form-control" placeholder="Address" id="txt_address" name="address" required data-validation-required-message="Please enter your <b> Address</b>.">
									<p class="help-block text-danger"></p>
								</div>
							</div>
							<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
								<div class="form-group floating-label-form-group controls">
									<label for="txt_suburb">Suburb</label>
									<input type="text" class="form-control" placeholder="Suburb" id="txt_suburb" name="suburb">
								</div>
							</div>
						</div>
						<div class="row border_bottom_forms_fields">
							<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
								<div class="form-group floating-label-form-group controls">
									<label for="txt_state">State</label>
									<select class="form-control custom-select" id="txt_state" name="state">
										<option value="">Select State</option>
										<?php
											$terms = get_terms( array(
												'taxonomy' => 'states',
												'hide_empty' => false,
											) );
											foreach($terms as $term){
										?>
										<option value="<?php echo $term->term_id;?>"><?php echo $term->name;?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
								<div class="form-group floating-label-form-group controls">
									<label for="txt_post_code">Post Code</label>
									<input type="text" class="form-control" placeholder="Post Code" id="txt_post_code" name="post_code">
								</div>
							</div>
						</div>
						<div class="row border_bottom_forms_fields">
							<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
								<div class="form-group floating-label-form-group controls">
									<label for="txt_password">Password <span class="obligatory_field">*</span></label>
									<input type="password" class="form-control" placeholder="Password" id="txt_password" name="password" required data-validation-required-message="Please enter your <b> Password</b>.">
									<span class="txt_under_fields">Must be at least 8 characters and include one or more non-alphanumeric characters (such as !, #, $, %)</span>
									<p class="help-block text-danger"></p>
								</div>
							</div>
							<div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">
								<div class="form-group floating-label-form-group controls">
									<label for="txt_conf_password">Confirm Password <span class="obligatory_field">*</span></label>
									<input type="password" class="form-control" placeholder="Confirm Password" id="txt_conf_password" name="password_repeat" required
										data-validation-matches-match="password" 
										data-validation-matches-message= "Must match the password entered previously." 
										data-validation-required-message="Please enter your <b> Confirm Password</b>.">
									<p class="help-block text-danger"></p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 text-center forms_box">
								<div id="success"></div>
								<input type="hidden" name="role" value="official">
								<input type="hidden" name="id_form" id="id_form" value="registrationForm">
								<button type="submit" class="btn btn-success" id="btnSubmit">Submit</button>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<?php echo school_sport_get_template('component/notice');?>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</article>
</section>