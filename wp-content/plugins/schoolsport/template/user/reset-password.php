<section>
    <article class="content_page login">
        <div class="container">       
            <div class="row">
                <div class="col-md-8 offset-md-2 col-lg-6 offset-lg-3 box_content box_content_bt_orange reset_height">
                    <form id="contactForm" novalidate="" rel="user-reset">
                        <div class="row">
                            <div class="control-group col-lg-12 forms_box">
                                <div class="form-group floating-label-form-group controls">
                                    <label for="txt_password">Enter your password <span class="obligatory_field">*</span></label>
                                    <input type="password" class="form-control" placeholder="Enter your password" id="txt_password" name="password" required="" data-validation-required-message="Please enter your <b> password</b>.">
                                    <span class="txt_under_fields">Must be at least 8 characters and include one or more non-alphanumeric characters (such as !, #, $, %)</span>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="control-group col-lg-12 forms_box">
                                <div class="form-group floating-label-form-group controls">
                                    <label for="txt_rep_password">Repeat your new password <span class="obligatory_field">*</span></label>
                                    <input type="password" class="form-control" placeholder="Repeat your new password" id="txt_rep_password" name="passsword_repeat" required="" data-validation-matches-match="password" data-validation-matches-message="Must match the password entered previously." data-validation-required-message="Please enter your <b> password</b>.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div id="success"></div>
                            <div class="control-group col-sm-12 text-right forms_box btn_mobile">
                                <div class="form-group floating-label-form-group controls">
                                    <?php 
                                        $token = esc_html(@$_GET['token']);
                                        $user = esc_html(@$_GET['user']);
                                    ?>
                                    <input type="hidden" name="token" value="<?php echo $token;?>">
                                    <input type="hidden" name="user" value="<?php echo $user;?>">
                                    <input type="hidden" name="id_form" id="id_form" value="resetPasswordForm">
                                    <button type="submit" class="btn btn-success btn-login" id="btnSubmit">Reset my password</button>
                                <div class="help-block"></div></div>
                            </div>
                        </div>
                        <div class="row">
							<div class="col-lg-12">
								<?php echo school_sport_get_template('component/notice');?>
							</div>
						</div>
                    </form>
                </div>
            </div>
        </div>
    </article>
</section>