
<div class="message_form_box <?php echo @$class;?>" style="display:none;" data-notice>
    <div class="alert alert-success" role="alert">
        <span class="icon_info" data-icon="icon-checkmark"></span>
        <span class="txt_message" data-message="An email has been sent with your recovery link."></span>
        <!--button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span class="icon-close"></span>
        </button-->
    </div>
</div>
