<?php $user_id = (int) $_REQUEST['user_id'];?>
<?php

$phone = get_user_meta($user_id , 'phone', true );
$mobile = get_user_meta($user_id , 'mobile',true);
$address = get_user_meta($user_id , 'address', true);
$suburb = get_user_meta($user_id , 'suburb', true);
$post_code = get_user_meta($user_id , 'post_code', true);
$state = get_user_meta($user_id , 'state', true);
$school = get_user_meta($user_id , 'school', true);
?>
<h3><?php _e("Aditional information", "blank"); ?></h3>
<table class="form-table" role="presentation">
	<tbody>
        <tr class="user-rich-editing-wrap">
            <th scope="row">Address</th>
            <td>
                <label>
                    <input name="address" type="text"  value="<?php echo $address;?>">
                </label>
            </td>
        </tr>
        <tr class="user-rich-editing-wrap">
            <th scope="row">Phone</th>
            <td>
                <label>
                    <input name="phone" type="text"  value="<?php echo $phone;?>">
                </label>
            </td>
        </tr>
        <tr class="user-rich-editing-wrap">
            <th scope="row">Mobile</th>
            <td>
                <label>
                    <input name="mobile" type="text"  value="<?php echo $mobile;?>">
                </label>
            </td>
        </tr>
        <tr class="user-rich-editing-wrap">
            <th scope="row">Suburb</th>
            <td>
                <label>
                    <input name="suburb" type="text"  value="<?php echo $suburb;?>">
                </label>
            </td>
        </tr>
        <tr class="user-rich-editing-wrap">
            <th scope="row">Postcode</th>
            <td>
                <label>
                    <input name="post_code" type="text"  value="<?php echo $post_code;?>">
                </label>
            </td>
        </tr>
        <tr class="user-rich-editing-wrap">
            <th scope="row">State</th>
            <td>
                <label>
                <select class="form-control custom-select" id="txt_state" name="state">
                    <option value="">Select State</option>
                    <?php
                        
                        $terms = get_terms( array(
                            'taxonomy' => 'states',
                            'hide_empty' => false,
                        ) );
                        foreach($terms as $term){
                    ?>
                    <option value="<?php echo $term->term_id;?>" <?php if($state == $term->term_id){?>selected<?php }?>><?php echo $term->name;?></option>
                    <?php } ?>
                </select>
                </label>
            </td>
        </tr>
         <tr class="user-rich-editing-wrap">
            <th scope="row">School</th>
            <td>
                <label>
                <select class="form-control custom-select" id="txt_school" name="school">
                    <option value="">Select School</option>
                    <?php
                        $schools = new WP_Query(['post_type' => 'schools', 'posts_per_page' => -1 ]);
                        if($schools->have_posts()) while($schools->have_posts()): $schools->the_post();
                    ?>
                    <option value="<?php the_ID();?>" <?php if(get_the_ID() == $school){?>selected<?php }?>><?php the_title();?></option>
                    <?php endwhile; ?>
                </select>
                </label>
            </td>
        </tr>
    </tbody>
</table>

<h3><?php _e("Profile information", "blank"); ?></h3>
<table class="form-table" role="presentation">
	<tbody>
        <tr class="user-rich-editing-wrap">
            <th scope="row">User Activated</th>
            <td>
                <label>
                    <?php $pending = get_user_meta($user_id, 'pending',  true);?>
                    <input name="pending" type="checkbox"  <?php if(!$pending) { ?>checked<?php } ?> value="true">
                    Account Confirmed		
                </label>
            </td>
        </tr>
        <?php if(school_sport_is_official()) { ?>
        <tr class="user-rich-editing-wrap">
            <th scope="row">Official User</th>
            <td>
                <label>
                    <?php $trial = get_user_meta($user_id, 'trial',  true);?>
                    <input name="trial" type="checkbox"  <?php if($trial) { ?>checked<?php } ?> value="true">
                    Can Create Trials (Official Role)		
                </label>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>