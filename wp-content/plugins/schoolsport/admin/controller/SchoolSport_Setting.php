<?php 

class SchoolSport_Setting {

    public function __construct() {
        add_action( 'admin_menu' , array($this, 'create_setting_menu'));
    }

    public function create_setting_menu() {
        add_submenu_page( 
            'options-general.php',
            __( 'School Sport', 'schoolsport' ),
            __( 'School Sport', 'schoolsport' ),
            'manage_options',
            'schoolsport-setting',
            array( $this, 'schoolsport_setting' )
        );
    }

    public function schoolsport_setting() {
      include (SCHOOLSPORT_PATH.'/admin/views/setting.php');
    }
}

$SchoolSport_Setting = new SchoolSport_Setting();