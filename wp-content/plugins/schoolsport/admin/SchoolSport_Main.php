<?php

class SchoolSport {

    static $plugin_slug = 'schoolsport-iact';    

    public static function init() {
        $plugin_schooladmin = new SchoolSport_Admin();
        $plugin_post_type = new SchoolSport_PostType();
    }

    public static function single_activate(){
        $plugin_schoolsport = new SchoolSport_Setup();
        $plugin_schoolsport->install();
    }
}