<?php

    class SchoolSport_Setup {
        
        public $table_db_version = SCHOOLSPORT_VERSION;
        public $prefix_table = '';
        
        private $pages = [
            [
                'slug' => 'registration',
                'post_title' => 'Registration',
                'post_content' => '',
                'option_meta' => SCHOOLSPORT_PAGE_REGISTER
            ],
            [
                'slug' => 'parents-registration',
                'post_title' => 'Parents Registration',
                'post_content' => '['.SCHOOLSPORT_PAGE_REGISTER_PARENT.']',
                'option_meta' => SCHOOLSPORT_PAGE_REGISTER_PARENT
            ],
            [
                'slug' => 'team-official-registration',
                'post_title' => 'Team Official Registration',
                'post_content' => '['.SCHOOLSPORT_PAGE_REGISTER_OFFICIAL.']',
                'option_meta' => SCHOOLSPORT_PAGE_REGISTER_OFFICIAL
            ],
            [
                'slug' => 'recover-your-password',
                'post_title' => 'Recover your password',
                'post_content' => '['.SCHOOLSPORT_PAGE_RECOVERY.']',
                'option_meta' => SCHOOLSPORT_PAGE_RECOVERY
            ],
            [
                'slug' => 'login',
                'post_title' => 'Login',
                'post_content' => '['.SCHOOLSPORT_PAGE_LOGIN.']',
                'option_meta' => SCHOOLSPORT_PAGE_LOGIN
                
            ],
            [
                'slug' => 'reset-my-password',
                'post_title' => 'Reset my password',
                'post_content' => '['.SCHOOLSPORT_PAGE_RESET_PASSWORD.']',
                'option_meta' => SCHOOLSPORT_PAGE_RESET_PASSWORD
            ],
            [
                'slug' => 'email-sent',
                'post_title' => 'Email Sent',
                'post_content' => '',
                'option_meta' => SCHOOLSPORT_PAGE_EMAIL_SENT
            ],
            [
                'slug' => 'confirmation-complete',
                'post_title' => 'Confirmation Complete',
                'post_content' => '['.SCHOOLSPORT_PAGE_REGISTER_CONFIRMATION.']',
                'option_meta' => SCHOOLSPORT_PAGE_REGISTER_CONFIRMATION
            ],[
                'slug' => 'dashboard',
                'post_title' => 'Dashboard',
                'post_content' => '['.SCHOOLSPORT_PAGE_DASHBOARD.']',
                'option_meta' => SCHOOLSPORT_PAGE_DASHBOARD
            ]
        ];
        
        public function __construct() {
            global $wpdb;
            $this->prefix_table =  $wpdb->prefix . SCHOOLSPORT_PREFIX;
        }

        public function install() {
            if($this->pages && is_array($this->pages)) {
                $options_pages = [];
                foreach($this->pages as $page) {
                    $page_current = get_page_by_path( $page['slug'] , OBJECT );
                    if(!$page_current) {
                        $my_post = array(
                            'post_title'    => wp_strip_all_tags( $page['post_title'] ),
                            'post_content'  => $page['post_content'],
                            'post_status'   => 'publish',
                            'post_type'     => 'page',
                            'post_author'   => 1,
                        );
                        $post_id = wp_insert_post( $my_post );
                        $options_pages[ $page['option_meta'] ] = $post_id;
                        update_option($page['option_meta'], $post_id);
                    } else {
                        if(isset($page_current->ID)) {
                            $options_pages[$page['option_meta']] = $page_current->ID;
                        }
                    }
                }
               
                update_option('school_sport_pages', $options_pages);
            }
        }

       
    }