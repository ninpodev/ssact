<?php 

class SchoolSport_User {

    public function __construct() {
        add_action( 'show_user_profile', array($this, 'user_profile_fields'));
        add_action( 'edit_user_profile', array($this, 'user_profile_fields'));
        add_action( 'personal_options_update', array($this, 'save_user_profile_fields'));
        add_action( 'edit_user_profile_update',  array($this, 'save_user_profile_fields'));
    }

    public function user_profile_fields() {
        include(SCHOOLSPORT_PATH."/admin/views/user.php");
    }
  
    public function save_user_profile_fields( $user_id ) {
        $request = $_REQUEST;
        if ( !current_user_can( 'edit_user', $user_id ) ) { 
            return false; 
        }
        if(!isset($_POST['pending'])) {
            $trial = update_user_meta($user_id, 'pending', true);
        } else {
            $trial = delete_user_meta($user_id, 'pending');
        }

        if(isset($_POST['trial'])) {
            $approve = get_user_meta($user_id, 'trial', true);
            $trial = update_user_meta($user_id, 'trial', true);
            $this->wp_new_user_approve_official($user_id);
        } else {
            $trial = delete_user_meta($user_id, 'trial');
        }
      
        update_user_meta($user_id , 'phone', esc_attr($request['phone']) );
        update_user_meta($user_id , 'mobile', esc_attr($request['mobile']) );
        update_user_meta($user_id , 'address', esc_attr($request['address']) );
        update_user_meta($user_id , 'suburb', esc_attr($request['suburb']) );
        update_user_meta($user_id , 'post_code', esc_attr($request['post_code']) );
        update_user_meta($user_id , 'state', esc_attr($request['state']) );
        update_user_meta($user_id , 'school', esc_attr($request['school']) );
    }

    
	public function wp_new_user_approve_official($user_id, $approve = false){
        if($user = get_userdata($user_id) && !$approve) {
            update_user_meta($user_id, 'approve', true);
            $headers = array('Content-Type: text/html; charset=UTF-8');
            $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
            $first_name = get_user_meta( $user->ID, 'first_name', true );
            $url_login = school_sport_get_url_login();
            
            $message = school_sport_get_template('email/approve-official');
            $message = str_replace('#first_name#', $first_name , $message);
            $message = str_replace('#url_login#', $url_login, $message);

            wp_mail($user->user_email, sprintf(__('[%s] Team official account approved'), $blogname), $message,$headers);
        }
	}
}

$SchoolSport_User = new SchoolSport_User();