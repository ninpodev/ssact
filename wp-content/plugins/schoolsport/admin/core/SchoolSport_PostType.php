<?php

    class SchoolSport_PostType {

        function __construct() {
            add_action( 'init', array( $this, 'create_post_type_students'));
            add_action( 'init', array( $this, 'create_post_type_schools'));
            add_action( 'init', array( $this, 'create_post_type_sports'));
            add_action( 'init', array( $this, 'create_post_type_teams'));
            add_action( 'init', array( $this, 'create_post_type_trials'));
            add_action( 'init', array( $this, 'create_post_type_news'));
            add_action( 'init', array( $this, 'create_post_type_bulletins'));
            add_action( 'init', array( $this, 'create_post_type_products'));
            add_action( 'init', array( $this, 'create_post_type_product_sets'));

            add_action( 'init', array( $this, 'create_post_type_states'));
            add_action( 'init', array( $this, 'create_post_type_regions'));
            add_action( 'init', array( $this, 'create_post_type_fees'));
            
        }
       

        public function create_post_type_students() {
            register_post_type( 'students',
                array(
                    'labels' => array(
                        'name' => __( 'Students' ),
                        'singular_name' => __( 'Students' )
                    ),
                    'public' => true,
                    'has_archive' => true,
                    'rewrite' => array('slug' => 'students'),
                    'menu_icon' => 'dashicons-welcome-learn-more',
                    'menu_position' => 20
                )
            );
        }

        public function create_post_type_schools() {
            register_post_type( 'schools',
                array(
                    'labels' => array(
                        'name' => __( 'Schools' ),
                        'singular_name' => __( 'Schools' )
                    ),
                    'public' => true,
                    'has_archive' => true,
                    'rewrite' => array('slug' => 'schools'),
                    'menu_icon' => 'dashicons-admin-home',
                    'menu_position' => 20
                )
            );
           
            
        }


        public function create_post_type_sports() {
            register_post_type( 'sports',
                array(
                    'labels' => array(
                        'name' => __( 'Sports' ),
                        'singular_name' => __( 'Sports' )
                    ),
                    'public' => true,
                    'has_archive' => true,
                    'rewrite' => array('slug' => 'sports'),
                    'menu_icon' => 'dashicons-sos',
                    'menu_position' => 20
                )
            );
        }


        public function create_post_type_teams() {
            register_post_type( 'teams',
                array(
                    'labels' => array(
                        'name' => __( 'Teams' ),
                        'singular_name' => __( 'Teams' )
                    ),
                    'public' => true,
                    'has_archive' => true,
                    'rewrite' => array('slug' => 'teams'),
                    'menu_icon' => 'dashicons-groups',
                    'menu_position' => 20
                )
            );
        }

        public function create_post_type_trials() {
            register_post_type( 'trials',
                array(
                    'labels' => array(
                        'name' => __( 'Trials' ),
                        'singular_name' => __( 'Trials' )
                    ),
                    'public' => true,
                    'has_archive' => true,
                    'rewrite' => array('slug' => 'trials'),
                    'menu_icon' => 'dashicons-groups',
                    'menu_position' => 20,
                    'supports'   => array( 'title', 'author' )
                )
            );
        }

        public function create_post_type_news() {
            register_post_type( 'news',
                array(
                    'labels' => array(
                        'name' => __( 'News' ),
                        'singular_name' => __( 'News' )
                    ),
                    'public' => true,
                    'has_archive' => true,
                    'rewrite' => array('slug' => 'news'),
                    'menu_icon' => 'dashicons-media-spreadsheet',
                    'menu_position' => 20,
                    'supports'   => array( 'title', 'editor','thumbnail' )
                )
            );
        }

        public function create_post_type_bulletins() {
            register_post_type( 'bulletins',
                array(
                    'labels' => array(
                        'name' => __( 'Bulletins' ),
                        'singular_name' => __( 'Bulletins' )
                    ),
                    'public' => true,
                    'has_archive' => true,
                    'rewrite' => array('slug' => 'bulletins'),
                    'menu_icon' => 'dashicons-text-page',
                    'menu_position' => 20
                )
            );
        }

        public function create_post_type_products() {
            register_post_type( 'products',
                array(
                    'labels' => array(
                        'name' => __( 'Products' ),
                        'singular_name' => __( 'Products' )
                    ),
                    'public' => true,
                    'has_archive' => false,
                    'rewrite' => array('slug' => 'products'),
                    'menu_icon' => 'dashicons-products',
                    'menu_position' => 20
                )
            );
        }
        

        public function create_post_type_product_sets() {
            register_post_type( 'product_sets',
                array(
                    'labels' => array(
                        'name' => __( 'Product Sets' ),
                        'singular_name' => __( 'Products Sets' )
                    ),
                    'public' => false,  
                    'publicly_queryable' => true, 
                    'show_ui' => true,  
                    'exclude_from_search' => true, 
                    'show_in_nav_menus' => false, 
                    'has_archive' => false, 
                    'rewrite' => false, 
                    'with_front' => false,
                    'menu_icon' => 'dashicons-products',
                    'menu_position' => 20
                )
            );
        }

        public function create_post_type_states() {
            register_taxonomy('states', 'other', array(
                'hierarchical' => false,
                'show_ui' => true,
                'show_admin_column' => true,
                'show_in_menu' => true,
                'query_var' => true,
                'rewrite' => array( 'slug' => 'states' ),
                'labels' => array(
                    'name'		=> 'States',
                ),
                'update_count_callback' => function() {
                    return; //important
                }
            ));
        }

        

        public function create_post_type_regions() {
            register_taxonomy( 'regions', 'other',array(
                'hierarchical' => false,
                'show_ui' => true,
                'show_admin_column' => true,
                'show_in_menu' => true,
                'query_var' => true,
                'rewrite' => array( 'slug' => 'regions' ),
                'labels' => array(
                    'name'		=> 'Regions',
                ),
                'update_count_callback' => function() {
                    return; //important
                }
            ));
        }


        
        public function create_post_type_fees() {
            register_taxonomy( 'fees', 'other',array(
                'hierarchical' => false,
                'show_ui' => true,
                'show_admin_column' => true,
                'show_in_menu' => true,
                'query_var' => true,
                'rewrite' => array( 'slug' => 'fees' ),
                'labels' => array(
                    'name'		=> 'Fees',
                ),
                'update_count_callback' => function() {
                    return; //important
                }
            ));
        }
        
    }