<?php
class SchoolSport_Admin {

    public function __construct() {

        add_action('init', array($this, 'create_roles' ));
        add_action('init', array($this, 'options_page'));
        add_action('init', array($this, 'redirect_wp_admin_dashboard'));

        add_action('admin_menu', array($this,  'custom_admin_menu'));
        
        add_filter('submenu_file', array($this, 'taxonomy_other_sub_menu'));
        add_filter('page_row_actions', array($this, 'disabled_delete_page'), 10, 2);
        add_action('wp_trash_post', array($this, 'restrict_page_deletion'));
        add_action('after_setup_theme', array($this, 'remove_admin_bar'));
 
        
    }


    public function options_page() {
       
        if( function_exists('acf_add_options_page') ) {
            acf_add_options_page(array(
                'page_title' => 'Home',
                'menu_title' => 'Home',
                'menu_slug'  => 'home-settings',
                'capability' => 'edit_posts',
                'redirect'	 => false,
                'icon_url'  => 'dashicons-admin-home',
                'position'   => 20
            ));

            acf_add_options_page(array(
                'page_title' => 'School Sport',
                'menu_title' => 'School Sport',
                'menu_slug'  => 'school-settings',
                'capability' => 'edit_posts',
                'parent_slug' => 'options-general.php',
            ));
        }
    }
    
    public function disabled_delete_page($actions, $post){
        $pages = get_option('school_sport_pages');
        foreach($pages as $key => $id) {
            if($post->ID == $id) {
              unset($actions['trash']);
            }
        }
        return $actions;
    }

    function restrict_page_deletion($post_id) {
        $pages = get_option('school_sport_pages');
        foreach($pages as $key => $id) {
            if($post_id == $id) {
                wp_die('The post you were trying to delete is protected.');
            }
        }
    }


  
    public function create_roles() {
        add_role( 'official', 'Official', array( 'read' => true) );
        add_role( 'parent', 'Parent', array( 'read' => true) );
        add_role( 'school', 'School', array('read' => true));
    }



    function remove_admin_bar() {
        if (!current_user_can('administrator') && !is_admin()) {
          show_admin_bar(false);
        }
    }

    function redirect_wp_admin_dashboard() {
        if ( is_user_logged_in() && is_admin() && !current_user_can( 'administrator' ) && !defined('DOING_AJAX')  ) {
          wp_redirect( school_sport_get_url_dahsboard() );
          exit;
        }
      }
 
    function custom_admin_menu() {
        add_menu_page('Other', 'Other', 'manage_options', 'other',  '__return_null','', 30);
        
        add_submenu_page('other', 'States', 'States', 'manage_options', admin_url('edit-tags.php').'?taxonomy=states');
        add_submenu_page('other', 'Regions', 'Regions', 'manage_options', admin_url('edit-tags.php').'?taxonomy=regions');
        add_submenu_page('other', 'Fees Type', 'Fees Type', 'manage_options', admin_url('edit-tags.php').'?taxonomy=fees');

        remove_submenu_page('other','other');
        remove_menu_page( 'edit-comments.php' );
    }  


    function taxonomy_other_sub_menu($submenu_file) {
        global $parent_file;
        if( 'edit-tags.php?taxonomy=states' == $submenu_file ) {
            $parent_file = 'other';
        }
        if( 'edit-tags.php?taxonomy=regions' == $submenu_file ) {
            $parent_file = 'other';
        }
        if( 'edit-tags.php?taxonomy=fees' == $submenu_file ) {
            $parent_file = 'other';
        }
        return $submenu_file;
    }
    
}