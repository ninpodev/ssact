<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<title><?php bloginfo('name'); ?> <?php is_front_page() ? bloginfo('description') : wp_title(' · '); ?></title>
	
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>">
    <?php endif; ?>
	
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
	
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/fontawesome.min.css" type="text/css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css" type="text/css" />

    <!-- CTHIERS CSS-->

    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/custom.css" type="text/css" />

	<?php wp_head(); ?>
</head>
<body class="<?php echo get_class_roles();?>">

    <header>
        <article class="top-menu">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                            <a class="navbar-brand" href="<?php echo home_url('/');?>">
                                <img src="<?php bloginfo('template_url');?>/images/logo_black.png" alt="School Sport" class="img-fluid">
                            </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                                <?php
                                    wp_nav_menu([
                                        'menu'            => 'top',
                                        'theme_location'  => 'top',
                                        'container'       => 'div',
                                        'container_id'    => 'bs4navbar',
                                        'container_class' => 'collapse navbar-collapse',
                                        'menu_id'         => false,
                                        'menu_class'      => 'navbar-nav mr-auto',
                                        'depth'           => 2,
                                        'fallback_cb'     => 'bs4navwalker::fallback',
                                        'walker'          => new bs4navwalker()
                                    ]);
                                ?>
                                
                                <ul class="navbar-nav ml-auto">
									<?php if(!is_user_logged_in()) { ?>
									<li class="nav-item">
                                        <a class="nav-link access" href="<?php echo home_url('registration');?>">Register</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link access" href="<?php echo home_url('login');?>">Login</a>
									</li>
                                    <?php } else { ?>
									<li class="nav-item">
                                        <a class="nav-link access" href="<?php echo home_url('/dashboard');?>">My Dashboard</a>        
                                    </li>
									<li class="nav-item">
                                        <a class="nav-link access" href="<?php echo home_url('/?logout');?>">Logout</a>        
                                    </li>
									<?php } ?>
									
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </article>
        <?php if(!is_home() && !is_page('dashboard')) {?>
        <article class="breadcumbs">
            <div class="container">
                <div class="row">
                    <div class="col-12 d-flex align-items-center bc-gen">
						
						<?php if(function_exists('bcn_display')) { bcn_display(); } ?>
						
						<?php /*
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo home_url('/');?>">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Registration</li>
                            </ol>
                        </nav>
						*/?>
						
                    </div>
                </div>
            </div>
        </article>
        <?php }  ?>
        <?php if(is_home() || is_front_page() ) { ?>
        
        <article class="home-slider">
            <!-- Slider main container -->
            <div class="swiper-container">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    <?php if(have_rows('banners', 'option')): ?>
                   
                        <?php while( have_rows('banners', 'option') ): the_row(); ?>
                        <?php $image = get_sub_field('image');?>
                    <div class="swiper-slide" style="background: url('<?php echo @$image['url'];?>');">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 content">
                                    <div class="info-content">
                                    <?php if(have_rows('content', 'option')): ?>
                                        <?php while( have_rows('content', 'option') ): the_row(); ?>
                                        <h2><?php the_sub_field('title');?></h2>
                                        <?php endwhile;?>
                                    <?php endif;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                     <?php endwhile;?>
                    <?php endif ?>
                    
                </div>
                <!-- If we need pagination -->
                <!--
                <div class="swiper-pagination"></div>
                -->
            
                <!-- If we need navigation buttons -->
                <!--<div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>-->
            </div>
            <!--div class="container ctn_title_home">
                <div class="row">
                    <div class="col-10 offset-1 col-md-10 offset-md-1 col-lg-8 offset-lg-2 title_home">
                        <h1><?php the_field('title', 'option');?></h1>
                    </div>
                </div>
            </div-->
        </article>
        <?php } ?>
    </header>