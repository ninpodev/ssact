<?php get_header();?>

<?php /*
<?php if(have_posts()): the_post();?>
<?php endif;?>
*/?>




<section>
	<article class="content_page pt-4 pb-0">
		<div class="container">
			<div class="row">
				<div class="col-12 mb-4">
					<h1><?php the_title(); ?></h1>
				</div>
				<div class="col-lg-6 mb-50 other_sections">



					<h4 class="mb-20"><span class="icon-event"></span>Upcoming <?php the_title(); ?> Trials</h4>


					<!--list of trials-->
                    <?php 
                    $sport = the_title();

                    query_posts(array( 
                        'post_type' => 'trials',
                        'meta_key'		=> 'sport',
                        'meta_query' => array(
                            $post->ID,
                            array(
                                'key'		=> 'date_of_trial',
                                'compare'	=> '>',
                                'value'		=> $today,
                            ),
                            array(
                                'key'		=> 'sport',
                                'compare'	=> '=',
                                'value'		=> $sport,
                            ),
                            array(
                            'key'		=> 'year',
                            'compare'	=> '>',
                            'value'		=> '2019',
                            )
                        ),
                        //'showposts' => 10
                    ) );  
					?>
					<?php while (have_posts()) : the_post(); ?>

					<div class="row">
						<div class="col-lg-12 trials">
							<div class="box_content box_content_bt_orange">
								<div class="row align-items-center">
									<div class="col-4 item"><?php the_title(); ?></div>
									<div class="col-3 item"><span class="font-weight-bold">Date of Trial</span><br><?php the_field('date_of_trial'); ?></div>
									<div class="col-4 item"><button class="btn btn-success" data-toggle="modal" data-target="#sportsSingleOverlay<?php the_ID(); ?>">Trial Information</button></div>
								</div>
							</div>
						</div>
					</div>

					<!-- Modal -->
					<div class="modal fade" id="sportsSingleOverlay<?php the_ID(); ?>" tabindex="-1" role="dialog" aria-labelledby="sportsSingleOverlayLabel" aria-hidden="true">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title text-center w-100" id="sportsSingleOverlayLabel"><?php the_title(); ?></h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<div class="row">
										<div class="col-lg-12 tabla_modal">
											<table class="table table-bordered tableSportsSingleOverlay">
												<tbody>
													<tr>
														<td class="text-break">
															<h6>Age Group</h6>
														</td>
														<td class="text-break">
															<span class="txt_info"><?php the_field('age'); ?> / <?php the_field('gender'); ?></span>
														</td>
													</tr>
													<tr>
														<td class="text-break">
															<h6>Sport</h6>
														</td>
														<td class="text-break">
															<?php

	$post_object = get_field('sport');

	if( $post_object ): 

	// override $post
	$post = $post_object;
	setup_postdata( $post ); 

															?>

															<span class="txt_info"><?php the_title(); ?></span>
															<?php wp_reset_postdata(); ?>
															<?php endif; ?>

														</td>
													</tr>
													<tr>
														<td class="text-break">
															<h6>Date of Trial</h6>
															<!--<small>(Must attend at least one)</small>-->
														</td>
														<td class="text-break">
															<span class="txt_info"><?php the_field('date_of_trial'); ?></span>
														</td>
													</tr>
                                                    
													<tr>
														<td class="text-break">
															<h6>Venue</h6>
														</td>
														<td class="text-break">
															<span class="txt_info"><?php the_field('venue'); ?></span>
														</td>
													</tr>
                                                    <?php 
                                                        $time_from = get_field('time_trial_from');
                                                        $time_to = get_field('time_trial_to');
                                                        if(!empty($time_from) || !empty($time_to)) {
                                                     ?>
                                                    <tr>
                                                        <td class="text-break">
                                                        <h6>Time</h6>
                                                        </td>
                                                        <td class="text-break">
                                                        <span class="txt_info"><?php echo $time_from;?>-<?php echo $time_to;?></span>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>  
													<tr>
														<td class="text-break">
															<h6>Requirements</h6>
															<small>(Specific gear)</small>
														</td>
														<td class="text-break">
															<span class="txt_info">
																<?php the_field('required_equipment'); ?>
															</span>
														</td>
													</tr>

													<!--
<tr>
<td class="text-break">
<h6>ACT Event Details</h6>
<small>(if trial is for a regional team)</small>
</td>
<td class="text-break">
<span class="txt_info">
12&U Hockey Championship, 18/08/2019 – 23/08/2019, Bendigo, Victoria
</span>
</td>
</tr>
-->

													<tr>
														<td class="text-break">
															<h6>Cost of the Event Trial Registration</h6>
														</td>
														<td class="text-break">
															<span class="txt_info">
																Online registration fee - $10.00
															</span>
														</td>
													</tr>
													<tr>
														<td class="text-break">
															<h6>Eligibility</h6>
														</td>
														<td class="text-break">
															<span class="txt_info">
																<?php the_field('eligibility'); ?>
															</span>
														</td>
													</tr>
													<tr>
														<td class="text-break">
															<h6>Further Information</h6>
														</td>
														<td class="text-break">
															<span class="txt_info">
																<?php echo get_the_author_meta('user_email'); ?>
															</span>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
									<?php if ( is_user_logged_in() ) { ?>
									<div class="row">
										<div class="col-lg-12 text-center reg_content">
											<a href="<?php echo home_url('/');?>/dashboard" class="btn btn-success"><span class="icon-follow"></span>Register a Student for this Trial</a>
										</div>
									</div>
									<?php } else { ?>
									<div class="row">
										<div class="col-lg-12 text-center reg_content">
											<p><a href="<?php echo home_url('/');?>/registration">Create an Account</a> or <a href="<?php echo home_url('/');?>/login">Log In to Register</a> a Student for this Trial</p>
										</div>
									</div>
									<?php } ?>


								</div>
							</div>
						</div>
					</div>


					<?php endwhile;?>
					<?php wp_reset_query(); ?>



				</div>

				<div class="col-lg-6 mb-50 other_sections">
					<h4 class="mb-20"><span class="icon-time"></span><?php the_title(); ?> History</h4>
					<div class="box_content box_content_bt_orange">
						<?php the_field('history'); ?>
					</div>

				</div>
			</div>
		</div>
	</article>
</section>

<?php /*
<?php
echo '<pre>';
print_r( $post->ID );
echo '</pre>';
?>
*/?>


<?php /*

<section>
	<article class="content_page content_with_border">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 other_sections">
					<h4><span class="icon-star"></span><?php the_title(); ?> Results</h4>
					<div class="box_content box_content_bt_orange">
						<p>
							No Results
						</p>
					</div>
				</div>
			</div>
		</div>
	</article>
</section>
*/?>

<section>
	<article class="content_page content_with_border ">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 other_sections">
					<h4><span class="icon-document"></span>Attachments</h4>
					<div id="attachments" class="box_content box_content_bt_orange">

						<ul>
							<?php

							// check if the repeater field has rows of data
							if( have_rows('documents') ):

							// loop through the rows of data
							while ( have_rows('documents') ) : the_row(); ?>

							<li class="attachment"><p><?php the_sub_field('name_of_the_document'); ?></p> <a href="<?php the_sub_field('file'); ?>" download>Download File</a></li>

							<?php endwhile; ?>

							<?php else : ?>

							<p>Content coming soon</p>

							<?php endif; ?>


						</ul>

					</div>
				</div>
			</div>
		</div>
	</article>
</section>

<section class="mb-5">
	<article class="content_page content_with_border">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 other_sections">
					<h4><span class="icon-event"></span>Details</h4>
					<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?> 
					<div class="clearfix"></div>
					<?php if( get_field('file') ): ?>
					<a href="<?php the_field("file"); ?>" class="btn btn-success mt-2 mb-2" download>Download PDF</a>
					<?php endif; ?>

					<div class="clearfix"></div>
					<?php the_content(); ?>
					<?php endwhile; ?>
					<?php else : ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</article>
</section>

<?php if( get_field('header_image') ): ?>
<section>
	<article class="sports_single_hero">

		<div class="container">
			<div class="row">
				<div class="col-12">
					<img src="<?php the_field('header_image'); ?>" alt="<?php the_title(); ?> ">
				</div>
			</div>
		</div>
	</article>
</section>
<?php endif; ?>



<section>
	<article class="ptb-50 content_page">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 other_sections">
					<h4>Registration Process</h4>
					<p class="mb-40">
						School Sport ACT now collects all registration information online – this is to prevent the movement to and fro of a paper note and have all information readily available to officials prior to the trial. As this process incurs a fee for data storage, system maintenance and admin, a small fee payment via credit card will be required.
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 offset-md-1 col-lg-10 offset-lg-1 plr-32 ptb-40 box_content box_content_clean">
					<h4 class="mb-30">How to register a Student?</h4>
					<ul class="list_inside_box_content">
						<li>
							<span class="font-weight-bold">1-</span> Go to <a href="registration.html">Register</a> and create an account. After completion, check your registered email to <span class="font-weight-bold">verify your account</span>. (Always check your spam folder in case you didn't receive immediately the verification email). 
						</li>
						<li>
							<span class="font-weight-bold">2-</span> After verifying your account, <span class="font-weight-bold">Log In</span> using your registered email and password. You’ll be presented with your <span class="font-weight-bold">Parent Dashboard</span>. From there you can start the process of registering a Student into a Trial.
							<div class="additional_info">
								<span class="icon-info-filled info-icon"></span> Look for the <a href="#" class="btn btn-success"><span class="icon-follow"></span> Add Student</a> button.
							</div>
						</li>
						<li>
							<span class="font-weight-bold">3- Complete all of the information</span> required for your Student (e.g: Medical Details, Playing History, Preferred Position, School, etc.).
						</li>
						<li>
							<span class="font-weight-bold">4- Make the small credit card payment.</span> After payment your Principal/School Approver will receive an email for their approval to trial.
							<div class="additional_info next_to_each_other">
								<span class="icon-info-filled info-icon"></span>
								<span class="txt">You will be able to see the process has been complete back in My Profile – once your Principal/School Approver has completed the approval process your child’s information will be collated onto a list for the Team Manager and Coach prior to trial.</span>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</article>
</section>


<!-- Modal -->
<div class="modal fade" id="sportsSingleOverlay" tabindex="-1" role="dialog" aria-labelledby="sportsSingleOverlayLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-center w-100" id="sportsSingleOverlayLabel">12&U Hockey Boys 2019</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12 tabla_modal">
						<table class="table table-bordered tableSportsSingleOverlay">
							<tbody>
								<tr>
									<td class="text-break">
										<h6>Age Group</h6>
										<small>(12&U or 13&O)</small>
									</td>
									<td class="text-break">
										<span class="txt_info">12&U Boys</span>
									</td>
								</tr>
								<tr>
									<td class="text-break">
										<h6>Sport</h6>
									</td>
									<td class="text-break">
										<span class="txt_info">Hockey</span>
									</td>
								</tr>
								<tr>
									<td class="text-break">
										<h6>Date(s) of Trial</h6>
										<small>(Must attend at least one)</small>
									</td>
									<td class="text-break">
										<span class="txt_info">13th and 20th May</span>
									</td>
								</tr>
								<tr>
									<td class="text-break">
										<h6>Venue</h6>
									</td>
									<td class="text-break">
										<span class="txt_info">National Hockey Centre, Lyneham. Powell Field.</span>
									</td>
								</tr>
								<tr>
									<td class="text-break">
										<h6>Time</h6>
									</td>
									<td class="text-break">
										<span class="txt_info">4:30-5:30pm</span>
									</td>
								</tr>
								<tr>
									<td class="text-break">
										<h6>Requirements</h6>
										<small>(Specific gear)</small>
									</td>
									<td class="text-break">
										<span class="txt_info">
											All necessary hockey equipment and protective gear including a drink bottle. <br>
											Please arrive kitted and ready to commence at 4:30pm.
										</span>
									</td>
								</tr>
								<tr>
									<td class="text-break">
										<h6>ACT Event Details</h6>
										<small>(if trial is for a regional team)</small>
									</td>
									<td class="text-break">
										<span class="txt_info">
											12&U Hockey Championship, 18/08/2019 – 23/08/2019, Bendigo, Victoria
										</span>
									</td>
								</tr>
								<tr>
									<td class="text-break">
										<h6>Cost of the Event Trial Registration</h6>
									</td>
									<td class="text-break">
										<span class="txt_info">
											Online registration fee - $10.00
										</span>
									</td>
								</tr>
								<tr>
									<td class="text-break">
										<h6>Eligibility</h6>
									</td>
									<td class="text-break">
										<span class="txt_info">
											Due to age dispensation granted by School Sport Australia; <br>
											students born between 1st July – 31st December 2006, are eligible to trial.
										</span>
									</td>
								</tr>
								<tr>
									<td class="text-break">
										<h6>Further Information</h6>
									</td>
									<td class="text-break">
										<span class="txt_info">
											Contact: Ben.macintyre@cg.catholic.edu.au
										</span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<?php if ( is_user_logged_in() ) { ?>
				<div class="row">
					<div class="col-lg-12 text-center reg_content">
						<a href="<?php echo home_url('/');?>/dashboard" class="btn btn-success"><span class="icon-follow"></span>Register a Student for this Trial</a>
					</div>
				</div>
				<?php } else { ?>
				<div class="row">
					<div class="col-lg-12 text-center reg_content">
						<p><a href="<?php echo home_url('/');?>/registration">Create an Account</a> or <a href="<?php echo home_url('/');?>/login">Log In to Register</a> a Student for this Trial</p>
					</div>
				</div>
				<?php } ?>


			</div>
		</div>
	</div>
</div>

<?php get_footer();?>





