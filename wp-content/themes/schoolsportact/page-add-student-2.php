<?php

/*
 * Template Name: Add Student Step 2
 *
 */

?>

<?php get_header();?>

<?php if(have_posts()): the_post();?>

    <section>

        <article class="box_title_internal_pages bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-12 d-flex jutify-content-start align-items-center py-4">

                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=453" class="btn btn-green btn-back position-relative d-inline-block mr-4"><span class="icon-chevron-left"></span></a>

                        <h1 class="forms_trial text-bold d-inline-block mb-0"><span class="icon-request-quote"></span>Add new Student</h1>

                    </div>

                </div>

            </div>

        </article>     

    </section>

    <section>

        <article class="steps_content bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-md-11 offset-md-1 col-xl-10 offset-xl-1 text-center stepper stepper_green four_items">

                        <div class="row justify-content-center">

                            <div class="col col-sm-3 item">

                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Basic<br>Information</h6>

                            </div>

                            <div class="col col-sm-3 item">

                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Medical<br>Information</h6>

                            </div>

                            <div class="col col-sm-3 item">

                                <div class="circle"></div>
                                <h6 class="text-center text-bold">Emergency<br>Contact</h6>

                            </div>

                            <div class="col col-sm-3 item">

                                <div class="circle"></div>
                                <h6 class="text-center text-bold">Summary</h6>

                            </div>                  

                        </div>

                    </div>

                </div>

            </div>

        </article>

    </section>

    <section>

        <article class="content_page bg_dash">

            <div class="container">

                <div class="row justify-content-center">

                    <div class="col-md-10 box_content box_content_bt_green reset_height py-lg-0">

                        <form id="add-student-step-2" novalidate>

                            <div class="row border_bottom_forms_fields">

                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <label for="txt_conditions" class="text-bold">Medical Conditions<span class="obligatory_field">*</span></label>

                                        <textarea class="form-control" name="conditions" id="txt_conditions" rows="5" required data-validation-required-message="Please complete the field." spellcheck="false" aria-invalid="false"></textarea> 

                                        <span class="txt_under_fields mt-2">Please ensure this information is up to date.</span>                                       

                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>

                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <label for="txt_allergies" class="text-bold">Allergies<span class="obligatory_field">*</span></label>

                                        <textarea class="form-control" name="allergies" id="txt_allergies" rows="5" required data-validation-required-message="Please ensure the field." spellcheck="false" aria-invalid="false"></textarea>  

                                        <span class="txt_under_fields mt-2">Please ensure this information is up to date.</span>                                      

                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>

                            </div>

                            <div class="row border_bottom_forms_fields">

                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <label for="txt_date_tetanus" class="text-bold">Date of last Tetanus Shot <span class="obligatory_field">*</span></label>
                                        
                                        <input type="text" class="form-control datepicker-here" placeholder="Date" id="txt_date_tetanus" name="txt_date_tetanus" data-language='en' required data-validation-required-message="Please enter the date of the last <b>Tetanus Shot</b>.">
                                        
                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>

                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">

                                    <div class="form-group floating-label-form-group controls"> 
                                        
                                        <label for="txt_medicar" class="text-bold">Medicar Number <span class="obligatory_field">*</span></label>
                                        
                                        <input type="number" class="form-control" placeholder="(+61) 012345678" id="txt_medicar" name="medicar" required data-validation-required-message="Please enter phone number for <b>Medicar</b>." aria-invalid="false">                                        
                                        
                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>

                            </div>

                            <div class="row border_bottom_forms_fields">

                                <div class="col-lg-12 d-flex justify-content-between buttons_two_adds py-4">

                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=442" class="btn btn-success text-bold"><span class="icon-chevron-left"></span>Back to Step 01</a>

                                    <!--<button type="submit" class="btn btn-success" id="btnSubmit">Next Step</button>-->

                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=446" class="btn btn-success text-bold" >Next Step</a>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-12 py-4">

                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=453" class="cancel-process d-inline-block mx-auto">Cancel process and back to Dashboard</a>

                                </div>

                            </div>                            

                        </form>

                    </div>

                </div>

            </div>

        </article>

    </section>

<?php endif;?>

<?php get_footer();?>