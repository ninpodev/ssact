<?php

/*
 * Template Name: Add New Student 3
 *
 */

?>

<?php get_header();?>

<?php if(have_posts()): the_post();?>

    <section>

        <article class="box_title_internal_pages bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-12 d-flex jutify-content-start align-items-center py-4">

                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=453" class="btn btn-green btn-back position-relative d-inline-block mr-4"><span class="icon-chevron-left"></span></a>

                        <h1 class="forms_trial text-bold d-inline-block mb-0"><span class="icon-request-quote"></span>Add new Student to Trial Name</h1>

                    </div>

                </div>

            </div>

        </article>    

    </section>

    <section>

        <article class="steps_content bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-md-11 offset-md-1 col-xl-10 offset-xl-1 text-center stepper stepper_green four_items">

                        <div class="row justify-content-center">

                            <div class="col col-sm-3 item">
                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Basic<br>Information</h6>
                            </div>

                            <div class="col col-sm-3 item">
                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Additional<br>Information</h6>
                            </div>

                            <div class="col col-sm-3 item">
                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Payment</h6>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </article>

    </section>

    <section>

        <article class="content_page bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-md-10 offset-md-1 box_content box_content_bt_green reset_height py-lg-0">

                        <h3 class="text-center py-5 text-bold">Payment</h3>

                        <div class="px-lg-5">

                            <div class="row">
                                
                                <div class="col-md-6">
                                    
                                    <p class="mb-5">
                                        
                                      To register a student to trial, a payment of $10 is required via credit card to cover the costs of administration of data and the cost of venue where appropriate.
  
                                    </p>

                                    <div class="w-100 mb-5">

                                        <img class="d-inline-block mx-auto eway w-75" src="<?php echo get_template_directory_uri(); ?>/images/eway.png" alt="eway">

                                    </div>

                                    <a href="#" class="d-inline-block grey-link" data-toggle="modal" data-target="#payment-modal"><u>I'll pay offline</u></a>

                                </div>

                                <div class="col-md-6">

                                    <form id="payment-form">

                                        <div class="row">

                                            <div class="control-group col-12 pb-lg-4 py-0 forms_box">

                                                <div class="form-group floating-label-form-group controls">

                                                    <label for="card_number" class="text-bold">Credit Card Number<span class="obligatory_field">*</span></label>

                                                    <input type="text" class="form-control" placeholder="**** **** **** ****" id="card_number" name="position" required data-validation-required-message="Please enter the <b>Credit Card Number</b>." aria-invalid="false">                                        

                                                    <p class="help-block text-danger"></p>

                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">
                                            
                                            <div class="control-group col-6 pb-lg-4 py-0 forms_box">

                                                <div class="form-group floating-label-form-group controls">

                                                    <label for="card_date" class="text-bold">Expiration Date<span class="obligatory_field">*</span></label>

                                                    <input type="text" class="form-control" placeholder="01/24" id="card_date" name="position" required data-validation-required-message="Please enter the <b>Expiration Date</b>." aria-invalid="false">                                        

                                                    <p class="help-block text-danger"></p>

                                                </div>

                                            </div>   

                                            <div class="control-group col-6 pb-lg-4 py-0 forms_box">

                                                <div class="form-group floating-label-form-group controls">

                                                    <label for="card_cvv" class="text-bold">CVV<span class="obligatory_field">*</span></label>

                                                    <input type="text" class="form-control" placeholder="***" id="card_cvv" name="position" required data-validation-required-message="Please enter the <b>CVV</b>." aria-invalid="false">                                       
                                                    <p class="help-block text-danger"></p>

                                                </div>

                                            </div>                                        

                                        </div>
                                    
                                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=518" class="btn btn-success lg-green px-5 py-2 text-bold" id="go-eway">Pay $10 using eway</a>

                                    </form>

                                    <!--<a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=520" class="blue-link mt-4 d-inline-block"><small>[Go to error page]</small></a>-->

                                </div>

                            </div>

                            <div class="my-4"><div class="py-4"></div></div>

                            <div class="text-center py-4">

                                <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=453" class="cancel-process d-inline-block mx-auto">Cancel process and back to Dashboard</a>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </article>

    </section>

<div class="modal fade" id="payment-modal" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">

        <div class="modal-content parent-modal">

            <div class="modal-header">

                <h4 class="text-center text-bold w-100 mb-0">Payment Information</h4>

            </div>

            <div class="modal-body">

                <p class="text-center">...</p>

            </div>

            <div class="modal-footer border-0 flex-wrap">

                <div class="w-100 text-center py-5">

                <button type="submit" class="btn btn-success btn-auto d-inline-block text-bold p-2 w-auto h-auto" id="save-payment"><span class="icon-save mr-2"></span>Save</button>

                </div>

                <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=453" class="cancel-process d-inline-block mx-auto">Cancel process and back to Dashboard</a>

            </div>

        </div>

    </div>

</div>    

<?php endif;?>

<?php get_footer();?>
