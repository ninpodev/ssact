<?php

/*
 * Template Name: Add Student Step 3
 *
 */

?>

<?php get_header();?>

<?php if(have_posts()): the_post();?>

    <section>

        <article class="box_title_internal_pages bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-12 d-flex jutify-content-start align-items-center py-4">

                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=453" class="btn btn-green btn-back position-relative d-inline-block mr-4"><span class="icon-chevron-left"></span></a>

                        <h1 class="forms_trial text-bold d-inline-block mb-0"><span class="icon-request-quote"></span>Add new Student</h1>

                    </div>

                </div>

            </div>

        </article>     

    </section>

    <section>

        <article class="steps_content bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-md-11 offset-md-1 col-xl-10 offset-xl-1 text-center stepper stepper_green four_items">

                        <div class="row justify-content-center">

                            <div class="col col-sm-3 item">

                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Basic<br>Information</h6>

                            </div>

                            <div class="col col-sm-3 item">

                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Medical<br>Information</h6>

                            </div>

                            <div class="col col-sm-3 item">

                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Emergency<br>Contact</h6>

                            </div>

                            <div class="col col-sm-3 item">

                                <div class="circle"></div>
                                <h6 class="text-center text-bold">Summary</h6>

                            </div>                  

                        </div>

                    </div>

                </div>

            </div>

        </article>

    </section>

    <section>

        <article class="content_page bg_dash">

            <div class="container">

                <div class="row justify-content-center">

                    <div class="col-md-10 box_content box_content_bt_green reset_height py-lg-0">

                        <form id="add-student-step-3" novalidate>

                            <div class="row border_bottom_forms_fields">

                                <div class="control-group col-12 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <label for="txt_preferred">Name of Preferred Emergency Contact<span class="obligatory_field">*</span></label>

                                        <input type="text" class="form-control" placeholder="Contact Name" id="txt_preferred" name="preferred" required data-validation-required-message="Please enter the <b> Prefered Emergency Contact</b>." aria-invalid="false">                                        

                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>                             

                            </div>

                            <div class="row border_bottom_forms_fields">

                                <div class="control-group col-sm-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">
 
                                        <label for="txt_relationship" class="text-bold">Relationship <span class="obligatory_field">*</span></label>

                                        <input type="text" class="form-control" placeholder="Father" id="txt_relationship" name="relatiopnship" required data-validation-required-message="Please enter the <b>Relationship</b>." aria-invalid="false">                                        
                                        
                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>


                                <div class="control-group col-sm-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">
 
                                        <label for="txt_mobile" class="text-bold">Mobile Phone Number <span class="obligatory_field">*</span></label>

                                        <input type="number" class="form-control" placeholder="(+61) 012345678" id="txt_mobile" name="mobile" required data-validation-required-message="Please enter the <b>Mobile Phone Number</b>." aria-invalid="false">                                        
                                        
                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>

                            </div>

                            <div class="row border_bottom_forms_fields">

                                <div class="control-group col-sm-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">
 
                                        <label for="txt_email" class="text-bold">Student Email <span class="obligatory_field">*</span></label>

                                        <input type="email" class="form-control" placeholder="student@school.com.au" id="txt_email" name="studentEmail" required data-validation-required-message="Please enter the <b>Student Email</b>." aria-invalid="false">                                        
                                        
                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>

                                <div class="control-group col-sm-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">
 
                                        <label for="txt_student_mobile" class="text-bold">Student Mobile Phone Number <span class="obligatory_field">*</span></label>

                                        <input type="number" class="form-control" placeholder="(+61) 012345678" id="txt_student_mobile" name="studentMobile" required data-validation-required-message="Please enter the <b>Student Mobile Number</b>." aria-invalid="false">                                        
                                        
                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>

                            </div>

                            <div class="row border_bottom_forms_fields">

                                <div class="col-lg-12 d-flex justify-content-between buttons_two_adds py-4">

                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=444" class="btn btn-success text-bold"><span class="icon-chevron-left"></span>Back to Step 02</a>

                                    <!--<button type="submit" class="btn btn-success" id="btnSubmit">Next Step</button>-->

                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=448" class="btn btn-success text-bold" >Next Step</a>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-12 py-4">

                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=453" class="cancel-process d-inline-block mx-auto">Cancel process and back to Dashboard</a>

                                </div>

                            </div>                            

                        </form>

                    </div>

                </div>

            </div>

        </article>

    </section>

<?php endif;?>

<?php get_footer();?>