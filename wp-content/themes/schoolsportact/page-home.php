<?php /* template name: Home*/ ?>

<?php get_header();?>
<section>
	<article class="content_page">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-9 content">
					<?php the_field('content', 'option');?>
				</div>
				<div class="col-sm-12 col-md-3 dw_box">
					<div class="download_box">
						<div class="left_side">
							<span class="icon-calendar"></span>
						</div>
						<div class="right_side">
							<span class="title">SSACT Calendar</span>
							<?php $calendar = get_field('calendar', 'option'); ?>
							<?php if(isset($calendar['url'])) {?>
							<a href="<?php echo $calendar['url'];?>" class="link">Download here</a>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
</section>
<section>
	<article class="content_page">
		<div class="container">
			<div class="row mb-6">
				<div class="col-lg-12 title">
					<h2><span class="icon-request-quote"></span>Register your student</h2>
				</div>
				<div class="col-md-6 col-lg-6 box_content without_radius">
					<?php the_field('registration', 'option');?>
					<div class="buttons">
						<a href="<?php echo home_url('parents-registration')?>" class="btn btn-success">Register your student</a>
					</div>
				</div>
				<?php $image = get_field('registration_image', 'option');?>
				<div class="col-md-6 col-lg-6 box_image reg_children"  style="background: url('<?php echo @$image['url'];?>');"></div>
			</div>
			<div class="row">
				<div class="col-lg-12 title">
					<h2><span class="icon-partnership"></span>Register as an official</h2>
				</div>
				<?php $image = get_field('help_organise_image', 'option');?>
				<div class="col-md-6 col-lg-6 box_image help_corgev" style="background: url('<?php echo @$image['url'];?>');"></div>
				<div class="col-md-6 col-lg-6 box_content without_radius">
					<?php the_field('help_organise', 'option');?>
					<div class="buttons">
						<a href="<?php echo home_url('contact')?>" class="link">Contact Us or</a>
						<a href="<?php echo home_url('team-official-registration')?>" class="btn btn-success">Register as an Official</a>
					</div>
				</div>
			</div>
		</div>
	</article>
</section>

<?php /*
<section>
	<article class="content_page">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-lg-6 other_sections upcoming-trials">
					<h4><span class="icon-event"></span>Upcoming Trials</h4>
					<div class="box_content">
						<?php $news = new WP_Query(['post_type'=> 'trials', 'posts_per_page'=>5]);?>
						<?php while($news->have_posts()): $news->the_post();?>
						<li>
							<span class="title"><?php the_title();?></span>
							<span>&nbsp;/&nbsp;<small>Date of trial: <?php the_field('date_of_trial');  ?></small></span>
						</li>
						<?php endwhile;?>
					</div>
				</div>
				<div class="col-md-6 col-lg-6 other_sections">
					<h4><span class="icon-star"></span>Results</h4>
					<div class="box_content">
						<!--
<div class="results_box">
<div class="date_left">
<span class="item">30 DEC 2017</span>
</div>
<div class="info">
<span class="title">2019 Cross Country</span>
<span class="text">Team Results Now Available <a href="#">View Results</a></span>
</div>
</div>
<div class="results_box">
<div class="date_left">
<span class="item">30 DEC 2017</span>
</div>
<div class="info">
<span class="title">2019 Cross Country</span>
<span class="text">Team Results Now Available <a href="#">View Results</a></span>
</div>
</div>
-->
					</div>
				</div>
			</div>
		</div>
	</article>
</section>
*/?>


<section>
	<article class="content_page">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-lg-6 other_sections">
					<h4>Our Funding Partners</h4>
					<div class="box_content without_shadow without_radius logos_content">
						<?php

	// check if the repeater field has rows of data
	if( have_rows('partners_logos', 'option') ):

						   // loop through the rows of data
						   while ( have_rows('partners_logos', 'option') ) : the_row(); ?>


						<figure><a href="<?php the_sub_field('url');?>" target="_blank"><img src="<?php the_sub_field('logo');?>" alt="" class="img-fluid"></a></figure>

						<?php endwhile;

						else :

						// no rows found

						endif;

						?>

					</div>
				</div>
				<div class="col-md-6 col-lg-6 other_sections">
					<h4>Our Sponsors</h4>
					<div class="box_content without_shadow without_radius logos_content">

						<?php

						// check if the repeater field has rows of data
						if( have_rows('sponsors_logos', 'option') ):

						// loop through the rows of data
						while ( have_rows('sponsors_logos', 'option') ) : the_row(); ?>


						<figure><a href="<?php the_sub_field('url');?>" target="_blank"><img src="<?php the_sub_field('logo');?>" alt="" class="img-fluid"></a></figure>

						<?php endwhile;

						else :

						// no rows found

						endif;

						?>


					</div>
				</div>
			</div>
		</div>
	</article>
</section>
<section>
	<article class="content_page">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 fb_box">
					<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FSchoolSportACT%2F&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=158660700815141" width="340" height="500"
							style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
				</div>
				<div class="col-md-8 col-lg-5 other_sections">
					<h4><span class="icon-blog"></span>Latest News</h4>
					<div class="box_content without_shadow without_radius ln_cont">
						<ul class="last_news">
							<?php $news = new WP_Query(['post_type'=> 'news', 'posts_per_page'=>3]);?>
							<?php while($news->have_posts()): $news->the_post();?>
							<li>
								<a href="<?php echo get_permalink();?>">
									<span class="title"><?php the_title();?></span>
									<span class="text">
										<?php the_excerpt();?>
									</span>
								</a>
							</li>
							<?php endwhile;?>

						</ul>
					</div>
				</div>
				<div class="col-md-4 col-lg-3 dw_box">
					<div class="logo_promo">
						<h6>School Sport Australia</h6>
						<a href="http://www.schoolsportaustralia.edu.au/" target="_blank"><figure><img src="<?php bloginfo('template_url');?>/images/logo_amver.jpeg" alt="Logo" class="img-fluid"></figure>
							Read More
						</a>
					</div>

					<div class="logo_promo">
						<h6>Regional School Sport Advisory Group</h6>
						<figure><img src="<?php bloginfo('template_url');?>/images/logo_black.png" alt="Logo" class="img-fluid"></figure>
					</div>

					<div class="logo_promo">
						<h6>Australian Sports Foundation Scholarship Fund</h6>
						<a href="https://asf.org.au/projects/school-sport-act/" target="_blank"> <figure><img src="<?php bloginfo('template_url');?>/images/rw7CGYag.png" alt="Logo" class="img-fluid"></figure>
						<br>
							Read More
						</a>
					</div>
				</div>
			</div>
		</div>
	</article>
</section>
<?php get_footer();?>