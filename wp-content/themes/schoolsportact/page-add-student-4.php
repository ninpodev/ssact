<?php

/*
 * Template Name: Add Student Step 4
 *
 */

?>

<?php get_header();?>

<?php if(have_posts()): the_post();?>

    <section>

        <article class="box_title_internal_pages bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-12 d-flex jutify-content-start align-items-center py-4">

                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=453" class="btn btn-green btn-back position-relative d-inline-block mr-4"><span class="icon-chevron-left"></span></a>

                        <h1 class="forms_trial text-bold d-inline-block mb-0"><span class="icon-request-quote"></span>Add new Student</h1>

                    </div>

                </div>

            </div>

        </article>     

    </section>

    <section>

        <article class="steps_content bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-md-11 offset-md-1 col-xl-10 offset-xl-1 text-center stepper stepper_green four_items">

                        <div class="row justify-content-center">

                            <div class="col col-sm-3 item">

                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Basic<br>Information</h6>

                            </div>

                            <div class="col col-sm-3 item">

                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Medical<br>Information</h6>

                            </div>

                            <div class="col col-sm-3 item">

                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Emergency<br>Contact</h6>

                            </div>

                            <div class="col col-sm-3 item">

                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Summary</h6>

                            </div>                  

                        </div>

                    </div>

                </div>

            </div>

        </article>

    </section>

    <section>

        <article class="content_page bg_dash">

            <div class="container">

                <div class="row justify-content-center">

                    <div class="col-md-10 box_content box_content_bt_green reset_height py-lg-0">

                        <form id="add-student-step-4" novalidate>

                            <div class="row border_bottom_forms_fields">

                                <div class="control-group col-sm-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <label for="txt_name" class="text-bold">Name of Student <span class="obligatory_field">*</span></label>

                                        <input type="text" class="form-control" placeholder="Student's Name" id="txt_name" name="name" required readonly value="Michael Walker">   
                                                                           
                                    </div>

                                </div>

                                <div class="control-group col-sm-6 forms_box">

                                    <div class="form-group floating-label-form-group controls"> 

                                        <label for="txt_school" class="text-bold">School <span class="obligatory_field">*</span></label>

                                        <input type="text" class="form-control" placeholder="School Name" id="txt_school" name="school" required readonly value="Ainslie School">  

                                    </div>

                                </div>

                            </div>

                            <div class="row border_bottom_forms_fields">

                                <div class="control-group col-sm-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <label for="txt_dob" class="text-bold">Date of Birth <span class="obligatory_field">*</span></label>

                                        <input type="text" class="form-control" placeholder="01/01/2002" id="txt_dob" name="dob" required readonly value="03/04/2006">   
                                                                           
                                    </div>

                                </div>

                                <div class="control-group col-sm-6 forms_box">

                                    <div class="form-group floating-label-form-group controls"> 

                                        <label for="txt_gender" class="text-bold">Gender <span class="obligatory_field">*</span></label>

                                        <input type="text" class="form-control" placeholder="Male" id="txt_gender" name="gender" required readonly value="Male">  

                                    </div>

                                </div>

                            </div>           
                            
                            <div class="row border_bottom_forms_fields">

                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <label for="txt_conditions" class="text-bold">Medical Conditions<span class="obligatory_field">*</span></label>

                                        <textarea class="form-control" name="conditions" id="txt_conditions" rows="5" required spellcheck="false" aria-invalid="false" readonly placeholder="List of Medical Conditions"></textarea> 

                                        <span class="txt_under_fields mt-2">Please ensure this information is up to date.</span>                                       

                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>

                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <label for="txt_allergies" class="text-bold">Allergies<span class="obligatory_field">*</span></label>

                                        <textarea class="form-control" name="allergies" id="txt_allergies" rows="5" required spellcheck="false" aria-invalid="false" readonly placeholder="List of Allergies"></textarea>   

                                        <span class="txt_under_fields mt-2">Please ensure this information is up to date.</span>                                      

                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>

                            </div>

                            <div class="row border_bottom_forms_fields">

                                <div class="control-group col-sm-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <label for="txt_date_tetanus" class="text-bold">Date of last Tetanus Shot <span class="obligatory_field">*</span></label>
                                        
                                        <input type="text" class="form-control" placeholder="Date" id="txt_date_tetanus" name="txt_date_tetanus" data-language='en' required readonly value="05/08/2018">                                      
 
                                    </div>

                                </div>

                                <div class="control-group col-sm-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">
 
                                        <label for="txt_medicar" class="text-bold">Medicar Number <span class="obligatory_field">*</span></label>
                                        
                                        <input type="text" class="form-control" placeholder="(+61) 012345678" id="txt_medicar" name="medicar" required readonly value="+61 258 369 147">                                        

                                    </div>

                                </div>

                            </div>                            

                            <div class="row border_bottom_forms_fields">

                                <div class="control-group col-sm-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <label for="txt_preferred">Preferred Emergency Contact<span class="obligatory_field">*</span></label>

                                        <input type="text" class="form-control" placeholder="Contact Name" id="txt_preferred" name="preferred" required readonly value="Bob Walker">                                        

                                    </div>

                                </div>

                                <div class="control-group col-sm-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <label for="txt_mobile" class="text-bold">Preferred Emergency Contact Number <span class="obligatory_field">*</span></label>

                                        <input type="text" class="form-control" placeholder="(+61) 012345678" id="txt_mobile" name="mobile" required readonly value="+61 678 741 852">                                        
                                       
                                    </div>

                                </div>

                            </div>  

                            <div class="row border_bottom_forms_fields">

                                <div class="control-group col-sm-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <label for="txt_email" class="text-bold">Student Email <span class="obligatory_field">*</span></label>

                                        <input type="text" class="form-control" placeholder="student@school.com.au" id="txt_email" name="studentEmail" required readonly value="michael.walker@school.com.au">                                        
   
                                    </div>

                                </div>

                                <div class="control-group col-sm-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">
 
                                        <label for="txt_student_mobile" class="text-bold">Student Mobile Phone Number <span class="obligatory_field">*</span></label>

                                        <input type="text" class="form-control" placeholder="(+61) 012345678" id="txt_student_mobile" name="studentMobile" required readonly value="+61 741 852 963">                                         

                                    </div>

                                </div>

                            </div>   

                            <div class="row border_bottom_forms_fields">

                                <div class="control-group col-sm-12 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <div class="form-check d-flex justify-content-between w-100 pl-3 pr-5 pr-md-3 py-2 bg-light">

                                            <label class="form-check-label text-bold" for="info-correct">I confirm that the above information is correct. <span class="obligatory_field">*</span></label>

                                            <input class="form-check-input check-right" type="checkbox" id="info-correct" value="check-1" required>
                                            
                                        </div>

                                        <div class="form-check d-flex justify-content-between w-100 pl-3 pr-5 pr-md-3 py-2">

                                            <label class="form-check-label text-bold" for="agree-conduct">I agree to abide by the School Sport ACT <a class="green-link text-normal" href="#">Code of conduct.</a> <span class="obligatory_field">*</span></label>

                                            <input class="form-check-input check-right" type="checkbox" id="agree-conduct" value="check-2" required>
                                            
                                        </div>

                                        <div class="form-check d-flex justify-content-between w-100 pl-3 pr-5 pr-md-3 py-2 bg-light">

                                            <label class="form-check-label text-bold" for="agree-acceptance">I have read and agree to the School Sport ACT <a class="green-link text-normal" href="#">Acceptance process.</a> <span class="obligatory_field">*</span></label>

                                            <input class="form-check-input check-right" type="checkbox" id="agree-acceptance" value="check-3" required>
                                            
                                        </div>                                                                                                                   

                                    </div>

                                </div>

                            </div>                                                                                                   

                            <div class="row border_bottom_forms_fields">

                                <div class="col-lg-12 d-flex justify-content-between buttons_two_adds py-4">

                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=446" class="btn btn-success text-bold"><span class="icon-chevron-left"></span>Back to Step 03</a>

                                    <!--<button type="submit" class="btn btn-success" id="btnSubmit">Next Step</button>-->

                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=453" class="btn btn-success text-bold" >Confirm and Submit</a>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-12 py-4">

                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=453" class="cancel-process d-inline-block mx-auto">Cancel process and back to Dashboard</a>

                                </div>

                            </div>                            

                        </form>

                    </div>

                </div>

            </div>

        </article>

    </section>

<?php endif;?>

<?php get_footer();?>
