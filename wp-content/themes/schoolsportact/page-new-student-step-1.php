<?php

/*
 * Template Name: Add New Student 1
 *
 */

?>

<?php get_header();?>

<?php if(have_posts()): the_post();?>

    <section>

        <article class="box_title_internal_pages bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-12 d-flex jutify-content-start align-items-center py-4">

                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=453" class="btn btn-green btn-back position-relative d-inline-block mr-4"><span class="icon-chevron-left"></span></a>

                        <h1 class="forms_trial text-bold d-inline-block mb-0"><span class="icon-request-quote"></span>Add new Student to Trial Name</h1>

                    </div>

                </div>

            </div>

        </article>    

    </section>

    <section>

        <article class="steps_content bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-md-11 offset-md-1 col-xl-10 offset-xl-1 text-center stepper stepper_green four_items">

                        <div class="row justify-content-center">

                            <div class="col col-sm-3 item">
                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Basic<br>Information</h6>
                            </div>

                            <div class="col col-sm-3 item">
                                <div class="circle"></div>
                                <h6 class="text-center text-bold">Additional<br>Information</h6>
                            </div>

                            <div class="col col-sm-3 item">
                                <div class="circle"></div>
                                <h6 class="text-center text-bold">Payment</h6>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </article>

    </section>

    <section>

        <article class="content_page bg_dash">

            <div class="container">

                <div class="row justify-content-center">

                    <div class="col-md-10 box_content box_content_bt_green reset_height py-lg-0">

                        <form id="add-student-trial-1" novalidate="" rel="add-student">

                            <div class="row border_bottom_forms_fields">

                                <div class="control-group col-sm-12 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <label for="trial_name" class="text-bold">Trial Selected<span class="obligatory_field">*</span></label>

                                        <select required="" class="form-control custom-select" id="trial_name" name="trialName" data-validation-required-message="Please select the <b>Trial.</b>" aria-invalid="false">

                                            <option value="" selected="">Choose...</option>
                                            <option value="trial-name-1">Trial Name 1</option>
                                            <option value="trial-name-2">Trial Name 2</option>
                                            <option value="trial-name-3">Trial Name 3</option>

                                        </select>

                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>

                            </div>

                            <div class="row border_bottom_forms_fields">

                                <div class="control-group col-12 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <label for="student_name" class="text-bold">Select Student<span class="obligatory_field">*</span></label>

                                        <select required="" class="form-control custom-select" id="student_name" name="studentName" data-validation-required-message="Please select the <b>Student.</b>" aria-invalid="false">

                                            <option value="" selected="">Student Name</option>
                                            <option value="trial-name-1">Student Name 1</option>
                                            <option value="trial-name-2">Student Name 2</option>
                                            <option value="trial-name-3">Student Name 3</option>

                                        </select>

                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-lg-12 text-center forms_box">

                                    <div class="row">

                                        <div class="col-lg-12 d-flex justify-content-between buttons_two_adds">

                                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=453" type="submit" class="btn btn-success text-bold"><span class="icon-chevron-left"></span>Back to Dashboard</a>

                                            <!--<button type="submit" class="btn btn-success" id="btnSubmit">Next Step</button>-->

                                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=417" class="btn btn-success text-bold" >Next Step</a>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </form>

                    </div>

                </div>

            </div>

        </article>

    </section>

<?php endif;?>

<?php get_footer();?>