<?php

/*
 * Template Name: Add Student Step 1
 *
 */

?>

<?php get_header();?>

<?php if(have_posts()): the_post();?>

    <section>

        <article class="box_title_internal_pages bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-12 d-flex jutify-content-start align-items-center py-4">

                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=453" class="btn btn-green btn-back position-relative d-inline-block mr-4"><span class="icon-chevron-left"></span></a>

                        <h1 class="forms_trial text-bold d-inline-block mb-0"><span class="icon-request-quote"></span>Add new Student</h1>

                    </div>

                </div>

            </div>

        </article>     

    </section>

    <section>

        <article class="steps_content bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-md-11 offset-md-1 col-xl-10 offset-xl-1 text-center stepper stepper_green four_items">

                        <div class="row justify-content-center">

                            <div class="col col-sm-3 item">

                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Basic<br>Information</h6>

                            </div>

                            <div class="col col-sm-3 item">

                                <div class="circle"></div>
                                <h6 class="text-center text-bold">Medical<br>Information</h6>

                            </div>

                            <div class="col col-sm-3 item">

                                <div class="circle"></div>
                                <h6 class="text-center text-bold">Emergency<br>Contact</h6>

                            </div>

                            <div class="col col-sm-3 item">

                                <div class="circle"></div>
                                <h6 class="text-center text-bold">Summary</h6>

                            </div>                  

                        </div>

                    </div>

                </div>

            </div>

        </article>

    </section>

    <section>

        <article class="content_page bg_dash">

            <div class="container">

                <div class="row justify-content-center">

                    <div class="col-md-10 box_content box_content_bt_green reset_height py-lg-0">

                        <form id="add-student-step-1" novalidate>

                            <div class="row border_bottom_forms_fields">

                                <div class="control-group col-sm-12 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <label for="txt_name" class="text-bold">Name of Student <span class="obligatory_field">*</span></label>

                                        <input type="text" class="form-control" placeholder="Student's Name" id="txt_name" name="txt_name" required data-validation-required-message="Please enter the <b> Name of Student</b>.">
                                        
                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>

                            </div>

                            <div class="row border_bottom_forms_fields">

                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <label for="txt_date_birth" class="text-bold">Date of Birth <span class="obligatory_field">*</span></label>
                                        
                                        <input type="text" class="form-control datepicker-here" placeholder="Date" id="txt_date_birth" name="txt_date_birth" data-language='en' required data-validation-required-message="Please enter the <b> Date of Birth </b>.">             
                                        
                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>

                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">
 
                                        <label for="txt_gender" class="text-bold">Gender <span class="obligatory_field">*</span></label>
                                        
                                        <select required class="form-control custom-select" id="txt_gender" name="txt_gender" data-validation-required-message="Please select the <b> Gender</b>.">
                                            
                                            <option value="" selected>Choose...</option>
                                            <option value="1">Male</option>
                                            <option value="2">Female</option>
                                            <option value="3">Other</option>
                                            
                                        </select>
                                        
                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>

                            </div>

                            <div class="row border_bottom_forms_fields">

                                <div class="control-group col-sm-12 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <label for="txt_school" class="text-bold">School <span class="obligatory_field">*</span></label>
                                        
                                        <select required class="form-control custom-select" id="txt_school" name="txt_school" data-validation-required-message="Please select the <b> School</b>.">
                                            
                                            <option value="" selected>Choose...</option>
                                            <option value="1">School Name 1</option>
                                            <option value="2">School Name 2</option>
                                            <option value="3">School Name 3</option>

                                        </select>
                                        
                                        <span class="txt_under_fields mt-2">To ensure that this student can be approved for a team, please make sure their school is up to date. For more info, please see our Acceptance Process.</span>
                                        
                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-lg-12 d-flex justify-content-between buttons_two_adds py-4">

                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=453" class="btn btn-success text-bold"><span class="icon-chevron-left"></span>Back to Dashboard</a>

                                    <!--<button type="submit" class="btn btn-success" id="btnSubmit">Next Step</button>-->

                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=444" class="btn btn-success text-bold" >Next Step</a>

                                </div>

                            </div>

                        </form>

                    </div>

                </div>

            </div>

        </article>

    </section>

<?php endif;?>

<?php get_footer();?>
