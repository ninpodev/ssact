<?php

/*
 * Template Name: Add New Student 2
 *
 */

?>

<?php get_header();?>

<?php if(have_posts()): the_post();?>

    <section>

        <article class="box_title_internal_pages bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-12 d-flex jutify-content-start align-items-center py-4">

                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=453" class="btn btn-green btn-back position-relative d-inline-block mr-4"><span class="icon-chevron-left"></span></a>

                        <h1 class="forms_trial text-bold d-inline-block mb-0"><span class="icon-request-quote"></span>Add new Student to Trial Name</h1>

                    </div>

                </div>

            </div>

        </article>    

    </section>

    <section>

        <article class="steps_content bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-md-11 offset-md-1 col-xl-10 offset-xl-1 text-center stepper stepper_green four_items">

                        <div class="row justify-content-center">

                            <div class="col col-sm-3 item">
                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Basic<br>Information</h6>
                            </div>

                            <div class="col col-sm-3 item">
                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Additional<br>Information</h6>
                            </div>

                            <div class="col col-sm-3 item">
                                <div class="circle"></div>
                                <h6 class="text-center text-bold">Payment</h6>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </article>

    </section>

    <section>

        <article class="content_page bg_dash">

            <div class="container">

                <div class="row justify-content-center">

                    <div class="col-md-10 box_content box_content_bt_green reset_height py-lg-0">

                        <form id="add-student-trial-2" novalidate rel="add-student">

                            <div class="row">

                                <div class="control-group col-sm-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <label for="txt_history" class="text-bold">Student Playing History<span class="obligatory_field">*</span></label>

                                        <textarea class="form-control" name="playingHistory" id="txt_history" rows="5" required="" data-validation-required-message="Please ensure this information is up to date." spellcheck="false" aria-invalid="false"></textarea>                                        

                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>

                                <div class="control-group col-sm-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <label for="txt_experience" class="text-bold">Representative Experience<span class="obligatory_field">*</span></label>

                                        <textarea class="form-control" name="repExperience" id="txt_experience" rows="5" required="" data-validation-required-message="Please ensure this information is up to date." spellcheck="false" aria-invalid="false"></textarea>                                        

                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>

                                <div class="control-group col-12 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <label for="txt_position" class="text-bold">Preferred Position<span class="obligatory_field">*</span></label>

                                        <input type="text" class="form-control" placeholder="Position Name" id="txt_position" name="position" required="" data-validation-required-message="Please enter the <b> Prefered Position</b>." aria-invalid="false">                                        

                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>                                

                            </div> 

                            <div class="row border_bottom_forms_fields">

                                <div class="control-group col-sm-12 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <div class="form-check d-flex justify-content-between w-100 pl-3 pr-5 pr-md-3 py-2 bg-light">

                                            <label class="form-check-label text-bold" for="info-correct">

                                            	I confirm that the above information is correct.

                                            	<span class="obligatory_field">*</span>

                                            </label>

                                            <input class="form-check-input check-right" type="checkbox" id="info-correct" value="check-1">
                                            
                                        </div>

                                        <div class="form-check d-flex justify-content-between w-100 pl-3 pr-5 pr-md-3 py-2">

                                            <label class="form-check-label text-bold d-inline pr-4" for="agree-conduct">

                                            	I, and my student, have read, understood and agree to abide by the 

                                            	<a class="green-link" href="#"> Codes of Conduct for SSACT Team Members and Parents</a> and the

                                            	<a class="green-link" href="#">Privacy Policy.</a>

                                            	<span class="obligatory_field">*</span>

                                            </label>

                                            <input class="form-check-input check-right" type="checkbox" id="agree-conduct" value="check-2">
                                            
                                        </div>

                                        <div class="form-check d-flex justify-content-between w-100 pl-3 pr-5 pr-md-3 py-2 bg-light">

                                            <label class="form-check-label text-bold d-inline pr-4" for="agree-acceptance">

                                            	I, and my student, have read, understood and agree to abide by the <a class="green-link" href="#">SSACT State Representative Team Acceptance Policy.</a>

                                            	<span class="obligatory_field">*</span>

                                            </label>

                                            <input class="form-check-input check-right" type="checkbox" id="agree-acceptance" value="check-3">
                                            
                                        </div>

                                        <div class="form-check d-flex justify-content-between w-100 pl-3 pr-5 pr-md-3 py-2">

                                            <label class="form-check-label text-bold d-inline pr-4" for="media-consent">

                                            	I, and my student, have read, understood and agree to abide by the <a class="green-link" href="http://www.schoolsportaustralia.edu.au/wp-content/uploads/2017/10/SSA-Privacy-Policy.pdf" target="_blank">School Sport Australia Privacy Policy.</a>
                                            	
                                            	<span class="obligatory_field">*</span>

                                            </label>

                                            <input class="form-check-input check-right" type="checkbox" id="media-consent" value="check-4">
                                            
                                        </div>   

                                        <div class="form-check d-flex justify-content-between w-100 pl-3 pr-5 pr-md-3 py-2 flex-wrap bg-light">

                                            <label class="form-check-label text-bold d-inline pr-4" for="print-consent">

                                            	I give consent for my student’s name to appear in <b>print.</b>                                            	

                                            </label>

                                            <input class="form-check-input check-right" type="checkbox" id="print-consent" value="check-5">

                                            <small class="pr-4">(This means your child’s name will not appear in the team announcement on Facebook, Championship Program, individual sport draws, on merchandise, print media coverage nor be visible in any results announcements)</small>

                                        </div>   

                                        <div class="form-check d-flex justify-content-between w-100 pl-3 pr-5 pr-md-3 py-2 flex-wrap">

                                            <label class="form-check-label text-bold d-inline pr-4" for="image-consent">

                                            	I give consent for my student’s <b>image</b> to be taken, recorded and used for SSACT or School Sport Australia purposes.                                            	

                                            </label>

                                            <input class="form-check-input check-right" type="checkbox" id="image-consent" value="check-6">

                                            <small class="pr-4">(This means that your child’s image will not appear on the SSACT website, SSACT Facebook page when sharing results and in any external media coverage)</small>

                                        </div>  

                                        <div class="form-check d-flex justify-content-between w-100 pl-3 pr-5 pr-md-3 py-2 flex-wrap bg-light">

                                            <label class="form-check-label text-bold d-inline pr-4" for="recorded-consent">

                                            	I give consent for my student to be <b>recorded</b> during the Championship.                                           	

                                            </label>

                                            <input class="form-check-input check-right" type="checkbox" id="recorded-consent" value="check-7">

                                            <small class="pr-4">(This means your child will not be present in any video footage for memento or judicial purposes nor any live streaming of an event or game played within the Championship. Note all approved live streaming is restricted to the School Sport Australia YouTube Channel or external channel shared within Championship Bulletins and approved by the host state)</small>

                                        </div>                                                                                                                                                                                                                                      

                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-lg-12 text-center forms_box">

                                    <div class="row">

                                        <div class="col-lg-12 d-flex justify-content-between buttons_two_adds">

                                            <a href="#" type="submit" class="btn btn-success text-bold"><span class="icon-chevron-left"></span>Back to Step 01</a>

                                            <!--<button type="submit" class="btn btn-success" id="btnSubmit">Next Step</button>-->

                                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=419" class="btn btn-success text-bold">Next Step</a>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </form>

                    </div>

                </div>

            </div>

        </article>

    </section>

<?php endif;?>

<?php get_footer();?>
