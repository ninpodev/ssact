$(function() {
    
    $("input, textarea, select").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            // Prevent spam click and default submit behaviour
            $("#btnSubmit").attr("disabled", true);
            event.preventDefault();
            
            var form_req = $("input#id_form").val();
            // get values from FORM
            switch (form_req) {
                case "registrationForm":
                    var first_name = $("input#txt_first_name").val();
                    var last_name = $("input#txt_last_name").val();
                    var email_address = $("input#txt_email_address").val();
                    var email_confirmation = $("input#txt_email_confirmation").val();
                    var mobile_phone = $("input#txt_mobile_phone").val();
                    var phone = $("input#txt_phone").val();
                    var address = $("input#txt_address").val();
                    var post_code = $("input#txt_post_code").val();
                    var password = $("input#txt_password").val();
                    var forSend = {
                        form_req: form_req,
                        first_name: first_name,
                        last_name: last_name,
                        email_address: email_address,
                        email_confirmation: email_confirmation,
                        mobile_phone: mobile_phone,
                        phone: phone,
                        address: address,
                        post_code: post_code,
                        password: password
                    };
                break;
                case "parentRegistrationForm":
                    var first_name = $("input#txt_first_name").val();
                    var last_name = $("input#txt_last_name").val();
                    var email_address = $("input#txt_email_address").val();
                    var email_confirmation = $("input#txt_email_confirmation").val();
                    var mobile_phone = $("input#txt_mobile_phone").val();
                    var phone = $("input#txt_phone").val();
                    var address = $("input#txt_address").val();
                    var post_code = $("input#txt_post_code").val();
                    var password = $("input#txt_password").val();
                    var forSend = {
                        form_req: form_req,
                        first_name: first_name,
                        last_name: last_name,
                        email_address: email_address,
                        email_confirmation: email_confirmation,
                        mobile_phone: mobile_phone,
                        phone: phone,
                        address: address,
                        post_code: post_code,
                        password: password
                    };
                break;
                case "editDetailsForm":
                    var first_name = $("input#txt_first_name").val();
                    var last_name = $("input#txt_last_name").val();
                    var email_address = $("input#txt_email_address").val();
                    var phone = $("input#txt_phone").val();
                    var address = $("input#txt_address").val();
                    var suburb = $("input#txt_suburb").val();
                    var post_code = $("input#txt_post_code").val();
                    var city = $("input#txt_city").val();
                    var forSend = {
                        form_req: form_req,
                        first_name: first_name,
                        last_name: last_name,
                        email_address: email_address,
                        email_confirmation: email_confirmation,
                        suburb: suburb,
                        phone: phone,
                        address: address,
                        post_code: post_code,
                        city: city
                    };
                break;
                case "loginForm":
                    var username = $("input#txt_username").val();
                    var password = $("input#txt_password").val();
                    var forSend = {
                        form_req: form_req,
                        username: username,
                        password: password
                    };
                break;
                case "recoverPasswordForm":
                    var email = $("input#txt_email").val();
                    var forSend = {
                        form_req: form_req,
                        email: email
                    };
                break;
                case "resetPasswordForm":
                    var password = $("input#txt_password").val();
                    var forSend = {
                        form_req: form_req,
                        password: password
                    };
                break;
                case "trial1Form":
                    var year = $("input#txt_year").val();
                    var sport = $("input#txt_sport").val();
                    var age = $("input#txt_age").val();
                    var gender = $("input#txt_gender").val();
                    var date_birth = $("input#txt_date_birth").val();
                    var date_trial = $("input#txt_date_trial").val();
                    var eligibility = $("input#txt_eligibility").val();
                    var champ_date = $("input#txt_champ_date").val();
                    var forSend = {
                        year: year,
                        sport: sport,
                        age: age,
                        gender: gender,
                        date_birth: date_birth,
                        date_trial: date_trial,
                        eligibility: eligibility,
                        champ_date: champ_date
                    };
                break;
                case "trial2Form":
                    var products = $("input#txt_selprod").val();
                    var forSend = {
                        form_req: form_req,
                        products: products
                    };
                break;
            }            
            /*
            var firstName = first_name; // For Success/Failure Message
            // Check for white space in nombre for Success/Fail message
            if (firstName.indexOf(' ') >= 0) {
                firstName = first_name.split(' ').slice(0, -1).join(' ');
            }
            */
            /* IMPORTANT! THIS AJAX METHOD MUST BE CHANGED OR ADAPTED BY THE BACKEND DEVELOPER */
            $.ajax({
                url: "././mail/contact_me.php",
                type: "POST",
                data: forSend,
                cache: false,
                success: function() {
                    // Enable button & show success message
                    $("#btnSubmit").attr("disabled", false);
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-success')
                        .append("<strong><i class='fas fa-check'></i> Su mensaje ha sido enviado. </strong>");
                    $('#success > .alert-success')
                        .append('</div>');

                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Disculpe, está fallando el servidor de correos. Por favor, intente nuevamente!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});

// When clicking on Full hide fail/success boxes
$('#txt_nombre').focus(function() {
    $('#success').html('');
});
