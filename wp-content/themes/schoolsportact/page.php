<?php get_header();?>
    <?php if(have_posts()): the_post();?>
    <section>
        <article class="box_title_internal_pages">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 title_internal_pages">
                        <?php if(is_page('login') || is_page('recover-your-password')) {?>
                            <h1><span class="icon-launch"></span><?php the_title();?></h1>
                        <?php } else if(is_page('reset-my-password')) {?>
                            <h1><span class="icon-password"></span><?php the_title();?></h1>    
                        <?php } else if (is_page('email-sent')) { ?>
                            <h1><span class="icon-email"></span><?php the_title();?></h1>  
                        <?php } else { ?>
                            <h1><span class="icon-request-quote"></span><?php the_title();?></h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </article>
    </section>
    <section>
        <article class="content_page messages_post">
                <div class="container">
                    <?php the_content();?>
                </div>
        </article>
    </section>
    <?php endif;?>
<?php get_footer();?>