<?php

/*
 * Template Name: Parent Dashboard
 *
 */

?>

<?php get_header();?>

<?php if(have_posts()): the_post();?>

    <section>

        <article class="box_title_internal_pages bg_dash">

            <div class="container">

                <div class="row justify-content-center">

                    <div class="col-xl-10 title_internal_pages d-flex justify-content-between align-items-center py-4">

                    	<div>

                        	<h1 class="text-bold text-grey mb-0"><span class="icon-template text-normal"></span>Parent Dashboard</h1>

                        </div>

                        <div>
                        	
                        	<a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=442" class="btn btn-success btn-auto d-inline-block text-bold p-3 w-auto h-auto" id="add-student"><span class="icon-follow mr-2"></span>Add a new Student</a>

                        </div>

                    </div>

                </div>

            </div>

        </article>

    </section>

    <section class="dashboards"> 

        <article>

            <div class="container">

                <div class="row justify-content-center">

                    <div class="col-xl-10 item">

                        <div class="row">

                            <div class="col-12 title_dash_item">

                                <h4 class="text-bold"><span class="icon-user"></span>Your Details</h4>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-12 item">

                                <div class="box_content box_content_bt_green reset_height py-4 px-3">

                                    <div class="row">

                                        <div class="col-5 col-md-3 col-lg-2 column_labels">

                                            <ul>
                                                <li>First Name</li>
                                                <li>Last Name</li>
                                                <li>Email</li>
                                                <li>Phone number</li>
                                            </ul>

                                        </div>

                                        <div class="col-7 col-md-9 col-lg-4 column_data text-break text-bold">

                                            <ul>
                                                <li class="text-bold">Bob</li>
                                                <li class="text-bold">Walker</li>
                                                <li class="text-bold">bob.walker@gmail.com</li>
                                                <li class="text-bold">0411 234 567</li>
                                            </ul>

                                        </div>

                                        <div class="col-5 col-md-3 col-lg-2 column_labels">

                                            <ul>
                                                <li>Address</li>
                                                <li>Suburb</li>
                                                <li>City</li>
                                                <li>Post Code</li>
                                            </ul>

                                        </div>

                                        <div class="col-7 col-md-9 col-lg-4 column_data text-break text-bold">

                                            <ul>
                                                <li class="text-bold">12 Corner st.</li>
                                                <li class="text-bold">Holt</li>
                                                <li class="text-bold">Canberra</li>
                                                <li class="text-bold">2615</li>
                                            </ul>

                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-lg-12 text-right link_box">

                                            <p><a href="#" class="blue-link"><u>Edit my details</u></a></p>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="row justify-content-center">

                    <div class="col-xl-10 item">

                        <div class="row">

                            <div class="col-12 title_dash_item">

                                <h4 class="text-bold"><span class="icon-user"></span>Your registered students (2)</h4>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-12 item">

                                <div class="box_content box_content_bt_green reset_height py-4 px-3">

                                    <div class="row border-bottom">

                                        <div class="col-12 d-flex justify-content-between align-items-center pb-4">

                                            <h3 class="text-normal mb-0">Michael Walker</h3>

                                            <a href="#" class="blue-link text-bold"><u>Edit profile</u></a>

                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-12 pt-4 text-center">

                                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=415" class="btn btn-success btn-auto px-4 py-2 text-bold d-inline-block h-auto">Register for a Trial</a>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <div class="col-12 item">

                                <div class="box_content box_content_bt_green reset_height py-4 px-3">

                                    <div class="row border-bottom">

                                        <div class="col-12 d-flex justify-content-between align-items-center pb-4">

                                            <h3 class="text-normal mb-0">Michael Walker</h3>

                                            <a href="#" class="blue-link text-bold"><u>Edit profile</u></a>

                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-12 pt-4 text-center text-lg-right">

                                            <a href="#" class="btn btn-success btn-auto px-4 py-2 text-bold d-inline-block h-auto">Complete Registration</a>

                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-12 pt-4 text-center">

                                            <div class="reg-step d-inline-block py-2 px-3">1. Registered</div>

                                            <div class="reg-step d-inline-block py-2 px-3">2. Trial</div>

                                            <div class="reg-step d-inline-block py-2 px-3">3. Order Uniform</div>

                                            <div class="reg-step d-inline-block py-2 px-3">4. Pay Fees</div>

                                            <div class="reg-step d-inline-block py-2 px-3">5. Championship</div>

                                        </div>

                                    </div>                                        

                                </div>

                            </div>

                            <div class="col-12 item">

                                <div class="box_content box_content_bt_green reset_height py-4 px-3">

                                    <div class="row border-bottom">

                                        <div class="col-12 d-flex justify-content-between align-items-center pb-4">

                                            <h3 class="text-normal mb-0">Michael Walker</h3>

                                            <a href="#" class="blue-link text-bold"><u>Edit profile</u></a>

                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-12 pt-4 d-flex justify-content-between align-items-center flex-wrap">

                                        	<div class="col-md-6 px-0">
                                        		
												<h4 class="text-bold">12&U Hockey Boys 2019</h4>

												<p class="mb-0"><b>Date of Trial</b><span class="text_data ml-4">30 December 2019 - AIS Sports field</span></p>                                            		

                                        	</div>
                                            
                                            <div class="col-md-6 px-0 text-center text-lg-right pt-4 pt-lg-0">

                                            	<a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=415" class="btn btn-success btn-auto px-4 py-2 text-bold d-inline-block h-auto">Register for a new trial</a>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-12 pt-4 text-center">

                                            <div class="reg-step complete d-inline-block py-2 px-3">1. Registered</div>

                                            <div class="reg-step d-inline-block py-2 px-3">2. Trial</div>

                                            <div class="reg-step d-inline-block py-2 px-3">3. Order Uniform</div>

                                            <div class="reg-step d-inline-block py-2 px-3">4. Pay Fees</div>

                                            <div class="reg-step d-inline-block py-2 px-3">5. Championship</div>

                                        </div>

                                    </div>                                        

                                </div>

                            </div>                                                                 

                        </div>

                    </div>

                </div>

                <div class="row justify-content-center">

                    <div class="col-xl-10 item">

                        <div class="row">

                            <div class="col-12 item">

                                <div class="box_content box_content_bt_green reset_height py-4 px-3">

                                    <div class="row border-bottom">

                                        <div class="col-12 d-flex justify-content-between align-items-center pb-4">

                                            <h3 class="text-normal mb-0">Annie Walker</h3>

                                            <a href="#" class="blue-link text-bold"><u>Edit profile</u></a>

                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-12 pt-4 d-flex justify-content-between align-items-center flex-wrap">

                                        	<div class="col-md-6 px-0">
                                        		
												<h4 class="text-bold">12&U Hockey Girls 2019</h4>

												<p class="mb-0"><b>Date of Trial</b><span class="text_data ml-4">30 December 2019 - AIS Sports field</span></p>                                            		

                                        	</div>
                                            
                                            <div class="col-md-6 px-0 text-center text-lg-right pt-4 pt-lg-0">

                                            	<a href="#" class="btn btn-success btn-auto px-4 py-2 text-bold d-inline-block h-auto">Order Uniform</a>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="row border-bottom">

                                        <div class="col-12 py-4 text-center">

                                            <div class="reg-step complete d-inline-block py-2 px-3">1. Registered</div>

                                            <div class="reg-step complete d-inline-block py-2 px-3">2. Trial</div>

                                            <div class="reg-step d-inline-block py-2 px-3">3. Order Uniform</div>

                                            <div class="reg-step d-inline-block py-2 px-3">4. Pay Fees</div>

                                            <div class="reg-step d-inline-block py-2 px-3">5. Championship</div>

                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-12 pt-4 d-flex justify-content-between align-items-center flex-wrap">

                                        	<div class="col-md-6 px-0">
                                        		
												<h4 class="text-bold">Girls Soccer 2019</h4>

												<p class="mb-0"><b>Date of Trial</b><span class="text_data ml-4">30 December 2019 - AIS Sports field</span></p>                                            		

                                        	</div>
                                            
                                            <div class="col-md-6 px-0 text-center text-lg-right pt-4 pt-lg-0">

                                            	<a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=419" class="btn btn-success btn-auto px-4 py-2 text-bold d-inline-block h-auto">Pay Fees</a>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-12 pt-4 text-center">

                                            <div class="reg-step complete d-inline-block py-2 px-3">1. Registered</div>

                                            <div class="reg-step complete d-inline-block py-2 px-3">2. Trial</div>

                                            <div class="reg-step complete d-inline-block py-2 px-3">3. Order Uniform</div>

                                            <div class="reg-step d-inline-block py-2 px-3">4. Pay Fees</div>

                                            <div class="reg-step d-inline-block py-2 px-3">5. Championship</div>

                                        </div>

                                    </div>                                                                                 

                                </div>

                            </div>  

                        </div>

                    </div>

                </div>

            </div>

        </article>

    </section>	

<?php endif;?>

<?php get_footer();?>	
