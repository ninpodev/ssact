<?php
/*
 * Template Name: Registration Select
 *
 */

?>

<?php get_header();?>
<?php if(have_posts()): the_post();?>
<section>
    <article class="box_title_internal_pages">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 d-flex align-items-center justify-content-between title_internal_pages">
                    <h1><span class="icon-request-quote"></span><?php the_title();?></h1>
                    <p class="text-right mb-0">Already Signed Up? <br>Click <a href="<?php echo home_url('login');?>">Log In</a> to access your account.</p>
                </div>
            </div>
        </div>
    </article>
</section>
<section>
    <article class="content_page ptb-50 registration">
        <div class="container">
            <div class="row">
                <div class="col-md-10 offset-md-1 registration_ppal">
                <?php if( have_rows('registration') ): ?> 
                    <div class="row">
                    <?php while( have_rows('registration') ): the_row(); ?>
                        <a class="col-lg-6 link_reg_main" href="<?php the_sub_field('link');?>">
                            <div class="box_content box_content_bt_orange reset_height registration_main">
                                <h3 class="text-center"><?php the_sub_field('title');?></h3>
                                <?php $img = get_sub_field('image');?>
                                <figure>
                                    <?php if(isset($img['url'])) {?>
                                    <img src="<?php echo $img['url'];?>" alt="<?php the_sub_field('title');?>" class="img-fluid">
                                    <?php } ?>
                                </figure>
                            </div>
                        </a>
                    <?php endwhile;?>
                        
                    </div>
                <?php endif;?>
                </div>
            </div>
        </div>
    </article>
</section>
<?php endif;?>
<?php get_footer();?>