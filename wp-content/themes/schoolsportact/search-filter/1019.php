<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

if ( $query->have_posts() )
{
?>

<!--Found <?php echo $query->found_posts; ?> Results<br />
Page <?php echo $query->query['paged']; ?> of <?php echo $query->max_num_pages; ?><br />

<div class="pagination">

<div class="nav-previous"><?php next_posts_link( 'Older posts', $query->max_num_pages ); ?></div>
<div class="nav-next"><?php previous_posts_link( 'Newer posts' ); ?></div>
<?php
 /* example code for using the wp_pagenavi plugin */
 /*if (function_exists('wp_pagenavi'))
			{
				echo "<br />";
				wp_pagenavi( array( 'query' => $query ) );
			}*/
?>
</div>-->
<style>
	#resTable {
		width: 100%;
		margin-bottom: 50px;
	}
	#resTable tr:nth-child(even) {
		background: #eee;
	}
	#resTable td {
		padding: 5px;
	}
	#resTable th {
		padding-bottom:1rem;
	}
	#resTable p {
		margin: 6px 10px 0;
	}
	#resTable td small {
		margin: 0 10px 5px;
		display: block;
	}
</style>

<table id="resTable">
	<tr>
		<th>Resource name</th>
		<th class="text-right">Download file</th>
		<!--
<th style="width:50px;">Size</th>
<th>File type</th>
-->
	</tr>

	<?php
 while ($query->have_posts())
 {
	 $query->the_post();

	?>
	<tr>
		<td>					
			<p>
				<?php the_title(); ?></p>
			<?php if( get_field('short_description') ): ?>
			<small><?php the_field('short_description'); ?></small>
			<?php endif; ?>


			<?php 
	 /*if ( has_post_thumbnail() ) {
							echo '<p>';
							the_post_thumbnail("small");
							echo '</p>';
						}*/
			?>
		</td>
		<td>
			<p><a href="<?php the_field('file'); ?>" target="_blank" download>Download</a> </p>
		</td>
		<!--
<td><small>File size here</small></td>
<td><small>File type here</small></td>
-->


	</td>
</tr>
<?php
 }
?>
</table>

<!--Page <?php echo $query->query['paged']; ?> of <?php echo $query->max_num_pages; ?><br />

<div class="pagination">

<div class="nav-previous"><?php next_posts_link( 'Older posts', $query->max_num_pages ); ?></div>
<div class="nav-next"><?php previous_posts_link( 'Newer posts' ); ?></div>
<?php
 /* example code for using the wp_pagenavi plugin */
 /*if (function_exists('wp_pagenavi'))
			{
				echo "<br />";
				wp_pagenavi( array( 'query' => $query ) );
			}*/
?>
</div>-->
<?php
}
else
{
	echo "No Results Found";
}
?>