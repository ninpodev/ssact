<?php

/*
 * Template Name: Add New Student 4
 *
 */

?>

<?php get_header();?>

<?php if(have_posts()): the_post();?>

    <section>

        <article class="box_title_internal_pages bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-12 d-flex jutify-content-start align-items-center py-4">

                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=453" class="btn btn-green btn-back position-relative d-inline-block mr-4"><span class="icon-chevron-left"></span></a>

                        <h1 class="forms_trial text-bold d-inline-block mb-0"><span class="icon-request-quote"></span>Add new Student to Trial Name</h1>

                    </div>

                </div>

            </div>

        </article>    

    </section>

    <section>

        <article class="steps_content bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-md-11 offset-md-1 col-xl-10 offset-xl-1 text-center stepper stepper_green four_items">

                        <div class="row justify-content-center">

                            <div class="col col-sm-3 item">
                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Basic<br>Information</h6>
                            </div>

                            <div class="col col-sm-3 item">
                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Additional<br>Information</h6>
                            </div>

                            <div class="col col-sm-3 item">
                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Payment</h6>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </article>

    </section>

    <section>

        <article class="content_page bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-md-10 offset-md-1 box_content box_content_bt_green reset_height py-lg-0 px-lg-5">

                        <h3 class="text-center py-5 text-bold">Payment</h3>

                        <p class="text-center mb-4">
                            
                            This message is an automated confirmation of your successful trial registration, with payment, into the School Sport ACT website. 

                        </p>

                        <p class="text-center mb-4">
                            
                            You’ll receive in your registered email all the payment details and welcome messages.<br>Your information has been received, you can monitor the status of your selected event’s progress at your dashboard.

                        </p>

                        <!--<a href="http://www.ninpodesign.com/ssact/html/payment_success_mail.html" class="blue-link mt-4 d-block text-center"><small>[Go to Email Page]</small></a>-->

                        <div class="my-4"><div class="py-4"></div></div>

                        <div class="text-center py-4">

                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=453" class="d-inline-block btn btn-success lg-green px-5 py-2 text-bold">Back to my dashboard</a>

                        </div>

                    </div>

                </div>

            </div>

        </article>

    </section>

<?php endif;?>

<?php get_footer();?>
