<?php
/*
 * Template Name: Dashboard
 *
 */

?>
 
<?php get_header();?>
    <?php if(have_posts() && is_user_logged_in()): the_post();?>
    <?php
        $user = wp_get_current_user();
        $roles = ( array ) $user->roles;
    ?>
    <section>
        <article class="box_title_internal_pages bg_dash">
            <div class="container">
                <div class="row justify-content-center">
					<?php if(school_sport_is_page_dahsboard()){ ?>
                    <div class="col-xl-10 title_internal_pages d-flex justify-content-between align-items-center py-4">
                        <div>
                          <?php if(in_array('official', $roles)) {?>
                          <h1><span class="icon-template"></span>Official Dashboard</h1>
                           
                          <?php } else if(in_array('school', $roles)) {?>
                          <h1><span class="icon-template"></span>School Dashboard</h1>
                          <?php } else { ?>
                          <h1><span class="icon-template"></span>Dashboard</h1>
                          <?php } ?>
                        </div>
                        <?php if(in_array('parent', $roles)) {?>
                         <div>
                        	<a href="<?php echo esc_url( home_url( 'dashboard/add-new-student/' ) ); ?>" class="btn btn-success btn-auto d-inline-block text-bold p-3 w-auto h-auto" id="add-student"><span class="icon-follow mr-2"></span>Add a new Student</a>
                        </div>
                        <?php } ?>
                        <?php if(school_sport_can_create_trial()) {?>
                        <div>
                          <a class="btn btn-theme" href="<?php echo home_url('dashboard/create-new-trial');?>"><span class="icon-compass"></span>Create new Trial</a>
                        </div>   
                        <?php } ?>
                    </div>
					<?php } else {?>
					        <div class="col-md-11 offset-md-1 title_internal_pages">
                        <button class="btn btn-theme btn-arrow-title"><span class="icon-chevron-left"></span></button>
                        <h1 class="forms_trial"><span class="icon-request-quote"></span><?php the_title();?></h1>
                    </div>
					<?php } ?>
                </div>
            </div>
        </article>
    </section>
    <section>
        <article>
            <div class="content_page dashboards">
                <div class="container">
                    <?php the_content();?> 
                </div>
            </div>
        </article>
    </section>
    <?php endif;?>
<?php get_footer();?>