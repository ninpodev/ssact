<?php
update_option( 'siteurl', 'https://schoolsportact.org.au' );
update_option( 'home', 'https://schoolsportact.org.au' );

require_once('bs4navwalker.php');
add_theme_support( 'post-thumbnails' );

register_nav_menu('top', 'Top menu');


if(!function_exists('get_class_roles')) {
	function get_class_roles(){
		$user = wp_get_current_user();
		$roles = ( array ) $user->roles;
		if(is_array($roles)) {
			return "theme-${roles[0]}";
		}
		return "theme-default";
	}
} 

function create_posttype() {
 
    register_post_type( 'footer_links',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Footer Links' ),
                'singular_name' => __( 'Footer Link' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'footer_links'),
        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );


