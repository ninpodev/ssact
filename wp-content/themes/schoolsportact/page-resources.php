<?php /*
<?php get_header();?>
    <?php if(have_posts()): the_post();?>
    <section>
        <article class="box_title_internal_pages">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 title_internal_pages">
						<h1><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </article>
    </section>
    <section>
        <article class="content_page messages_post">
                <div class="container">
					<div class="row">
						<div class="col-12"><h2>Public</h2></div>
						<div class="col-3">
							<?php echo do_shortcode('[searchandfilter id="1019"]'); ?>
						</div>
						<div class="col-9">
							<?php echo do_shortcode('[searchandfilter id="1019" show="results"]'); ?>
						</div>
					</div>
                </div>
        </article>
    </section>
    <?php endif;?>
<?php get_footer();?>
*/?>



<?php get_header();?>
<section>
	<article class="box_title_internal_pages">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 title_internal_pages">
					<h1>Resources</h1>
				</div>
			</div>
		</div>
	</article>
</section>

<style>
	#resTable {
		width: 100%;
		margin-bottom: 50px;
	}
	#resTable tr:nth-child(even) {
		background: #eee;
	}
	#resTable td {
		padding: 5px;
	}
	#resTable th {
		padding-bottom:1rem;
	}
	#resTable p {
		margin: 6px 10px 0;
	}
	#resTable td small {
		margin: 0 10px 5px;
		display: block;
	}
</style>


<section>
	<article class="content_page messages_post">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<p>
						Only public documents are shown by default, if a resource you need does not appear try <a href="/login">logging into the site</a> and refreshing this page.
					</p>
					<table id="resTable">
						<tr>
							<th>Resource name</th>
							<th class="text-right">Download file</th>
							<!--
<th style="width:50px;">Size</th>
<th>File type</th>
-->
						</tr>
						

						<?php /*
						hola!
						<?php
						$user = wp_get_current_user();
							if ( in_array( 'official', (array) $user->roles ) ) {
								//The user has the "author" role
								echo 'tiene rol';
							} else {
								echo 'no rol';
							}
						?>
*/?>
						
						

						<?php
						get_header();
						$user = wp_get_current_user();					
						
						$args = array(
							'post_type'=>'resources_files',
							'post_status'=>'publish',
						);

						$query = new WP_Query($args);
						if($query->have_posts()) :
						while ($query->have_posts()) : $query->the_post();
						
						
						$aux_permission = false;
						// Cuando el contenido NO tiene restricciones de rol
						if( !members_has_post_roles( $post->ID ) ){
							$aux_permission = true;
						} else {
							// Cuando el contenido SI tiene restricciones de rol
							if( members_can_current_user_view_post( $post->ID ) ){
								$aux_permission = true;
							}
							
						}
						
						
						
						//
						if( $aux_permission ){
						
						?>

						<tr>
							<td>					
								<p>
									<?php the_title(); ?></p>
								<?php if( get_field('short_description') ): ?>
								<small><?php the_field('short_description'); ?></small>
								<?php endif; ?>

							</td>
							<td>
								<p><a href="<?php the_field('file'); ?>" target="_blank" download>Download</a> </p>
							</td>
						</tr>



						<?php } endwhile;
						endif;
						?>


					</table>



				</div>
			</div>
		</div>
	</article>
</section>
<?php get_footer();?>