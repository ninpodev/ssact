<footer>
	<article class="top-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-lg-3 item">
					<h4>Latest Bulletins</h4>
					<ul class="list_lb">
						<?php $bulletins = new WP_Query(['post_type'=> 'bulletins']);?>
						<?php while($bulletins->have_posts()): $bulletins->the_post();?>
						<li>
							<a href="<?php the_field('pdf_file')?>" target="_blank">
								<span class="lb_title"><?php the_title();?></span>
								<span class="dates"><?php the_date( 'd M Y');?></span>
							</a>
						</li>
						<?php endwhile;?>

					</ul>
				</div>
				<div class="col-md-6 col-lg-3 item">
					<h4>Useful Links</h4>
					<ul class="list_ul">

						<?php $footer_links = new WP_Query(['post_type'=> 'footer_links']);?>
						<?php while($footer_links->have_posts()): $footer_links->the_post();?>
						<li>
							<a href="<?php the_field('url')?>" target="_blank">
								<?php the_title();?>
							</a>
						</li>
						<?php endwhile;?>

					</ul>
				</div>
				<div class="col-md-6 col-lg-3 item">
					<h4>Contact Us</h4>
					<ul class="list_cu">
						<li>
							<a href="https://goo.gl/maps/pxAj9aTEmNTeSguM8" target="_blank">
								ACT Sports House 100 Maitland Street, Hackett, ACT 2602
							</a>
						</li>
						<li>
							<a href="tel:+61261030777" target="_blank">
								Phone: (61) 2 6103 0777
							</a>
						</li>
						<li>
							<a href="#" target="_blank">
								Fax: (61) 2 6205 7799
							</a>
						</li>
						<li>
							<a href="mailto:info@schoolsportact.asn.au" target="_blank">
								Email: info@schoolsportact.asn.au
							</a>
						</li>
						<li>
							<a href="#" target="_blank">
								ABN: 95 825 767 889
							</a>
						</li>
					</ul>
				</div>
				<div class="col-md-6 col-lg-3 item">
					<h4>&nbsp;</h4>
					<ul>
						<li class="mb-3">
							<a href="https://www.facebook.com/SchoolSportACT/" target="_blank"><span class="icon-facebook"></span> Visit our Facebook Page</a>
						</li>
						<li>
							<script type="text/javascript"> //<![CDATA[
  var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.trust-provider.com/" : "http://www.trustlogo.com/");
  document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
//]]></script>
<script language="JavaScript" type="text/javascript">
  TrustLogo("https://www.positivessl.com/images/seals/positivessl_trust_seal_sm_124x32.png", "POSDV", "none");
</script>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</article>
	<article class="bottom-footer">
		<h5>2020 SSACT ©All Rights Reserved.</h5>
	</article>
</footer>
<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/popper.min.js"></script>


<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>


<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/swiper.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/wow.min.js"></script>

<?php wp_footer();?>

<script type="text/javascript">
	$(document).ready(function(){
		$('.swiper-wrapper').slick({
			autoplay:true,
			autoplaySpeed:6000,
			arrows:true

		});
	});
</script>


</body>
</html>