<?php

/*
 * Template Name: Select Students 2
 *
 */

?>

<?php get_header();?>

<?php if(have_posts()): the_post();?>

    <section>

        <article class="box_title_internal_pages bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-12 d-flex jutify-content-start align-items-center py-4">

                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=453" class="btn btn-purple btn-back position-relative d-inline-block mr-4"><span class="icon-chevron-left"></span></a>

                        <h1 class="forms_trial text-bold d-inline-block mb-0"><span class="icon-request-quote mr-3"></span>Select Students for the [Team Name] Team.</h1>

                    </div>

                </div>

            </div>

        </article>     

    </section>

    <section>

        <article class="steps_content bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-md-11 offset-md-1 col-xl-10 offset-xl-1 text-center stepper stepper_violet four_items">

                        <div class="row justify-content-center">

                            <div class="col col-sm-4 item">

                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Team<br>Selection</h6>

                            </div>

                            <div class="col col-sm-4 item">

                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Select<br>Try-on Date</h6>

                            </div>

                            <div class="col col-sm-4 item">

                                <div class="circle"></div>
                                <h6 class="text-center text-bold">Summary</h6>

                            </div>                  

                        </div>

                    </div>

                </div>

            </div>

        </article>

    </section>

    <section>

        <article class="content_page bg_dash">

            <div class="container">

                <div class="row justify-content-center">

                    <div class="col-md-10 box_content box_content_bt_purple reset_height py-lg-0">

                        <div class="py-4 border-bottom mb-4">

                            <h6 class="text-bold mb-3">Set the Try-on Date</h6>

                            <p class="mb-3">Please set a date for students to try on their equipment.<br>This will be required for parents to be able to order uniforms in a timely manner.</p>

                            <p class="mb-0"><small>For more information, please see the School Sport ACT <a href="#" class="green-link">Acceptance Process.</a></small></p>

                        </div>                        

                        <form id="select-student-2" novalidate>

                            <div class="row border_bottom_forms_fields">

                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">

                                    <div class="form-group floating-label-form-group controls">

                                        <label for="txt_age" class="text-bold">Venue of Try-on <span class="obligatory_field">*</span></label>
                                        
                                        <input type="text" class="form-control" placeholder="Age" id="txt_date_tetanus" name="age" data-language='en' required data-validation-required-message="Please enter the <b>Age</b>.">
                                        
                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>

                                <div class="control-group col-sm-6 col-md-6 col-lg-6 forms_box">

                                    <div class="form-group floating-label-form-group controls"> 
                                        
                                        <label for="txt_date" class="text-bold">Date of Try-on <span class="obligatory_field">*</span></label>
                                        
                                        <input type="text" class="form-control datepicker-here" placeholder="Date" id="txt_date_birth" name="txt_date_birth" data-language="en" required data-validation-required-message="Please enter the <b>Date</b>.">

                                        <p class="help-block text-danger"></p>

                                    </div>

                                </div>

                            </div>

                            <div class="row border_bottom_forms_fields">

                                <div class="col-lg-12 d-flex justify-content-between buttons_two_adds py-4">

                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=835" class="btn btn-purple text-bold"><span class="icon-chevron-left"></span>Back to Step 01</a>

                                    <!--<button type="submit" class="btn btn-success" id="btnSubmit">Next Step</button>-->

                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=839" class="btn btn-purple text-bold" >Continue</a>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-12 py-4">

                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=453" class="cancel-process d-inline-block mx-auto">Cancel process and back to Dashboard</a>

                                </div>

                            </div>                            

                        </form>

                    </div>

                </div>

            </div>

        </article>

    </section>

<?php endif;?>

<?php get_footer();?>
