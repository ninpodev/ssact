<?php

/*
 * Template Name: Select Students 1
 *
 */

?>

<?php get_header();?>

<?php if(have_posts()): the_post();?>

    <section>

        <article class="box_title_internal_pages bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-12 d-flex jutify-content-start align-items-center py-4">

                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=453" class="btn btn-purple btn-back position-relative d-inline-block mr-4"><span class="icon-chevron-left"></span></a>

                        <h1 class="forms_trial text-bold d-inline-block mb-0"><span class="icon-request-quote mr-3"></span>Select Students for the [Team Name] Team.</h1>

                    </div>

                </div>

            </div>

        </article>     

    </section>

    <section>

        <article class="steps_content bg_dash">

            <div class="container">

                <div class="row">

                    <div class="col-md-11 offset-md-1 col-xl-10 offset-xl-1 text-center stepper stepper_violet four_items">

                        <div class="row justify-content-center">

                            <div class="col col-sm-4 item">

                                <div class="circle active"></div>
                                <h6 class="text-center text-bold">Team<br>Selection</h6>

                            </div>

                            <div class="col col-sm-4 item">

                                <div class="circle"></div>
                                <h6 class="text-center text-bold">Select<br>Try-on Date</h6>

                            </div>

                            <div class="col col-sm-4 item">

                                <div class="circle"></div>
                                <h6 class="text-center text-bold">Summary</h6>

                            </div>                  

                        </div>

                    </div>

                </div>

            </div>

        </article>

    </section>

    <section>

        <article class="content_page bg_dash">

            <div class="container">

                <div class="row justify-content-center">

                    <div class="col-md-10 box_content box_content_bt_purple reset_height py-lg-0">

                        <div class="py-4 border-bottom mb-4">

                            <h6 class="text-bold mb-3">Team Selection</h6>

                            <p class="mb-3">Use the list below to select students for the Team.<br>This will notify their parents and give them the opportunity to accept the position.</p>

                            <p class="mb-0"><small>For more information, please see the School Sport ACT <a href="#" class="green-link">Acceptance Process.</a></small></p>

                        </div>

                        <table class="table table-striped inline-table">

                            <thead>

                                <tr class="border-0">
                                    <th scope="col" class="col-3 col-md-5 border-0 text-muted">Student Name</th>
                                    <th scope="col" class="col-4 border-0 text-normal text-right d-none d-md-inline-block">Select one option for every student:</th>
                                    <th scope="col" class="col-3 col-md-1 border-0 text-muted text-center">Status</th>
                                    <th scope="col" class="col-3 col-md-1 border-0 text-muted text-center">Shadow</th>
                                    <th scope="col" class="col-3 col-md-1 border-0 text-muted text-center">Not Selected</th>
                                </tr>

                            </thead>

                            <tbody>

                                <tr class="border-0">
                                    <th scope="row" class="col-3 col-md-5 border-0 text-muted text-normal">Roman Kutepov</th>
                                    <td class="col-4 border-0 text-right d-none d-md-inline-block"><a href="#" class="grey-link"><small><u>View Student Info</u></small></a></td>
                                    <td class="col-3 col-md-1 border-0 text-center"><input class="form-check-input check-green" type="checkbox"></td>
                                    <td class="col-3 col-md-1 border-0 text-center"><input class="form-check-input check-black" type="checkbox"></td>
                                    <td class="col-3 col-md-1 border-0 text-center"><input class="form-check-input check-fault" type="checkbox"></td>
                                </tr>

                                <tr class="border-0">
                                    <th scope="row" class="col-3 col-md-5 border-0 text-muted text-normal">Aneta Škodová</th>
                                    <td class="col-4 border-0 text-right d-none d-md-inline-block"><a href="#" class="grey-link"><small><u>View Student Info</u></small></a></td>
                                    <td class="col-3 col-md-1 border-0 text-center"><input class="form-check-input check-green" type="checkbox"></td>
                                    <td class="col-3 col-md-1 border-0 text-center"><input class="form-check-input check-black" type="checkbox"></td>
                                    <td class="col-3 col-md-1 border-0 text-center"><input class="form-check-input check-fault" type="checkbox"></td>
                                </tr>

                                <tr class="border-0">
                                    <th scope="row" class="col-3 col-md-5 border-0 text-muted text-normal">Ren Xue</th>
                                    <td class="col-4 border-0 text-right d-none d-md-inline-block"><a href="#" class="grey-link"><small><u>View Student Info</u></small></a></td>
                                    <td class="col-3 col-md-1 border-0 text-center"><input class="form-check-input check-green" type="checkbox"></td>
                                    <td class="col-3 col-md-1 border-0 text-center"><input class="form-check-input check-black" type="checkbox"></td>
                                    <td class="col-3 col-md-1 border-0 text-center"><input class="form-check-input check-fault" type="checkbox"></td>
                                </tr>

                                <tr class="border-0">
                                    <th scope="row" class="col-3 col-md-5 border-0 text-muted text-normal">Phet Putrie</th>
                                    <td class="col-4 border-0 text-right d-none d-md-inline-block"><a href="#" class="grey-link"><small><u>View Student Info</u></small></a></td>
                                    <td class="col-3 col-md-1 border-0 text-center"><input class="form-check-input check-green" type="checkbox"></td>
                                    <td class="col-3 col-md-1 border-0 text-center"><input class="form-check-input check-black" type="checkbox"></td>
                                    <td class="col-3 col-md-1 border-0 text-center"><input class="form-check-input check-fault" type="checkbox"></td>
                                </tr>

                            </tbody>

                        </table>  

                        <div class="d-flex justify-content-between align-items-center buttons_two_adds py-4 border-bottom">

                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=453" class="btn btn-success text-bold"><span class="icon-table-of-contents mr-2"></span>Export list of students</a>

                            <!--<button type="submit" class="btn btn-success" id="btnSubmit">Next Step</button>-->

                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=837" class="btn btn-purple btn-auto text-bold px-3 py-2">Continue</a>

                        </div>

                        <div class="row">

                            <div class="col-12 py-4">

                                    <a href="http://www.ninpodesign.com/ssact/?p=453" class="cancel-process d-inline-block mx-auto">Cancel process and back to Dashboard</a>

                                </div>

                            </div>                        

                        </div>

                </div>

            </div>

        </article>

    </section>

<?php endif;?>

<?php get_footer();?>
